Ext.define('pho.view.asignatura.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.asignaturaGrid',

    store : Ext.create('Ext.ux.uji.data.Store',
    {
        model : 'pho.model.Asignatura',
        url : '/pho/rest/asignaturas'
    }),

    title : 'Asignatura',

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'codigoAsignatura',
        align : 'center',
        text : 'Codi',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    },
    {
        dataIndex : 'nombre',
        text : 'Nom',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        },
        flex : 1
    },
    {
        dataIndex : 'curso',
        text : 'Curs',
        align : 'center',
        editor :
        {
            field :
            {
                allowBlank : false,
                xtype : 'combobox',
                editable : false,
                matchFieldWidth : false,
                displayField : 'id',
                store : Ext.create('Ext.data.Store',
                {
                    fields : [ 'id' ],
                    data : [
                    {
                        id : '3',
                    },
                    {
                        id : '4',
                    },
                    {
                        id : '5',
                    },
                    {
                        id : '6',
                    } ]
                })
            }
        }
    },
    {
        dataIndex : 'semestre',
        text : 'Semestre',
        align : 'center',
        editor :
        {
            field :
            {
                allowBlank : false,
                editable : false,
                xtype : 'combobox',
                displayField : 'text',
                valueField : 'id',
                matchFieldWidth : false,

                store : Ext.create('Ext.data.Store',
                {
                    fields : [ 'id', 'text' ],
                    data : [
                    {
                        id : 'A',
                        text : 'Anual'
                    },
                    {
                        id : '1',
                        text : '1'
                    },
                    {
                        id : '2',
                        text : '2'
                    } ]
                })
            }
        }
    },
    {
        dataIndex : 'orden',
        text : 'Ordre',
        align : 'center',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    } ]
});
