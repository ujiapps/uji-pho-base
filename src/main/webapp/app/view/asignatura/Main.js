Ext.define('pho.view.asignatura.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.asignaturaMain',
    title : 'Gestió: Asignatura',
    requires : [ 'pho.view.asignatura.Grid' ],
    layout: 'fit',
    padding: 10,

    items : [
    {
        xtype : 'asignaturaGrid'
    } ]
});
