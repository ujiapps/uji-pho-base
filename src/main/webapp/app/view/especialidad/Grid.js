Ext.define('pho.view.especialidad.Grid',
    {
        extend: 'Ext.ux.uji.grid.Panel',

        alias: 'widget.especialidadGrid',
        name: 'especialidad',

        requires: ['pho.view.especialidad.GridController'],
        controller: 'especialidadGridController',

        reloadAfterInsert: true,

        store: Ext.create('Ext.ux.uji.data.Store',
            {
                model: 'pho.model.Especialidad',
                url: '/pho/rest/especialidades'
            }),

        tbar: [
            {
                xtype: 'button',
                iconCls: 'fa fa-plus',
                text: 'Afegir',
                handler: 'onAdd'
            },
            {
                xtype: 'button',
                iconCls: 'fa fa-edit',
                text: 'Editar',
                handler: 'onEdit'
            },
            {
                xtype: 'button',
                iconCls: 'fa fa-remove',
                text: 'Borrar',
                handler: 'onDelete'
            },
            {
                xtype: 'button',
                iconCls: 'fa fa-calendar',
                text: 'Detallar períodes',
                handler: 'onDetallePeriodos'
            }],

        columns: [
            {
                text: 'ID',
                dataIndex: 'id',
                hidden: true
            },
            {
                dataIndex: 'nombre',
                flex: 2,
                text: 'Nom',
                editor:
                    {
                        field:
                            {
                                allowBlank: false
                            }
                    }
            },
            {
                dataIndex: 'curso',
                header: 'Curs',
                align: 'center',
                editor:
                    {
                        field:
                            {
                                allowBlank: false
                            }
                    }
            },
            {
                dataIndex: 'tipo',
                text: 'Tipus',
                align: 'center',
                editor:
                    {
                        field:
                            {
                                allowBlank: false,
                                editable: false,
                                xtype: 'combobox',
                                displayField: 'text',
                                valueField: 'id',
                                matchFieldWidth: false,

                                store: Ext.create('Ext.data.Store',
                                    {
                                        fields: ['id', 'text'],
                                        data: [
                                            {
                                                id: 'OP',
                                                text: 'Optativa'
                                            },
                                            {
                                                id: 'OB',
                                                text: 'Obligatòria'
                                            },
                                            {
                                                id: 'OB/OP',
                                                text: 'Obligatória/Optativa'
                                            }]
                                    })
                            }
                    }
            },
            {
                xtype: 'foreigncolumn',
                dataIndex: 'bloqueId',
                displayField: 'cursoNombre',
                flex: 2,
                header: 'Bloc',
                editable: true,
                matchFieldWidth: false,
                allowEdit: false
            }, {
                xtype: 'actioncolumn',
                text: 'Comentari',
                align: 'center',
                dataIndex: 'comentarios',
                width: 120,
                menuDisabled: true,
                sortable: false,
                items: [
                    {
                        handler: 'onAddComentario',
                        getTip: function (value, metadata) {
                            return value ? Ext.util.Format.htmlEncode(value) : 'Afegir comentari';
                        },
                        getClass: function (comentarios) {
                            return comentarios ? 'x-fa fa-file-text-o' : 'x-fa fa-plus';
                        },
                        width: 180
                    }]
            },
            {
                dataIndex: 'semestre',
                text: 'Semestre',
                width: 80,
                align: 'center',
                editor:
                    {
                        field:
                            {
                                allowBlank: false,
                                editable: false,
                                xtype: 'combobox',
                                displayField: 'text',
                                valueField: 'id',
                                matchFieldWidth: false,

                                store: Ext.create('Ext.data.Store',
                                    {
                                        fields: ['id', 'text'],
                                        data: [
                                            {
                                                id: 'A',
                                                text: 'Anual'
                                            },
                                            {
                                                id: '1',
                                                text: '1'
                                            },
                                            {
                                                id: '2',
                                                text: '2'
                                            }]
                                    })
                            }
                    }
            },
            {
                dataIndex: 'periodosObligatorios',
                text: 'Tam. obligatoris',
                align: 'center',
                editor:
                    {
                        field:
                            {
                                allowBlank: false
                            }
                    }
            },
            {
                dataIndex: 'periodosOptativos',
                text: 'Tam. optatius',
                align: 'center',
                editor:
                    {
                        field:
                            {
                                allowBlank: false
                            }
                    }
            },
            {
                dataIndex: 'periodoInicialId',
                text: 'Període inicial',
                align: 'center',
                editor:
                    {
                        field:
                            {
                                allowBlank: true
                            }
                    }
            },
            {
                dataIndex: 'periodoFinalId',
                text: 'Període final',
                align: 'center',
                editor:
                    {
                        field:
                            {
                                allowBlank: true
                            }
                    }
            },
            {
                dataIndex: 'orden',
                text: 'Ordre',
                width: 70,
                align: 'center',
                editor:
                    {
                        field:
                            {
                                allowBlank: false
                            }
                    }
            },
            {
                dataIndex: 'numeroPeriodos',
                text: 'Períodes',
                align: 'center',
                renderer: function (value) {
                    if (value > 0) {
                        return '<span style="font-size: 10px;">' + value + '</span>&nbsp;&nbsp;<span class="fa fa-calendar"></span>'
                    }
                    return ''
                }
            }],
        listeners:
            {
                beforeedit: 'onBeforeEdit',
                render: 'onLoad',
                celldblclick: 'onEdit',
                onDetallePeriodos: 'onDetallePeriodos'
            }
    });