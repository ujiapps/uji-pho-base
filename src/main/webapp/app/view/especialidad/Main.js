Ext.define('pho.view.especialidad.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.especialidadMain',
    title : 'Gestió: Especialidad',
    requires : [
        'pho.view.especialidad.ComboCurso',
        'pho.view.especialidad.MainController',
        'pho.view.especialidad.Grid',
        'pho.view.especialidad.FormPeriodos',
        'pho.view.especialidad.FormComentario'
    ],
    controller: 'especialidadMainController',
    layout: 'fit',
    padding: 10,

    tbar: [ '->', {
        xtype: 'especialidadComboCurso'
    }],

    items : [
    {
        xtype : 'especialidadGrid'
    } ],

    listeners: {
        cursoSelected: 'onCursoSelected'
    }
});
