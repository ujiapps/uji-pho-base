Ext.define('pho.view.especialidad.MainController',
{
    extend : 'Ext.app.ViewController',
    alias : 'controller.especialidadMainController',

    onCursoSelected : function(cursoId)
    {
        var panel = this.getView();
        var grid = panel.down('especialidadGrid')

        if (cursoId)
        {
            grid.getStore().addFilter(new Ext.util.Filter(
            {
                id : 'cursoId',
                filterFn : function(rec)
                {
                    return rec.get('curso') === cursoId || rec.get('curso') === 0;
                }
            }));
        }
        else
        {
            grid.getStore().removeFilter('cursoId');
        }
    }

});
