Ext.define('pho.view.especialidad.ComboCurso',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.especialidadComboCurso',
    fieldLabel : 'Curs',
    labelWidth: 60,
    showClearIcon : true,
    allowBlank: true,

    store : Ext.create('Ext.data.Store',
    {
        model : 'pho.model.Lookup',
        autoLoad : true,
        sorters : [ 'nombre' ],
        proxy :
        {
            type : 'memory'
        }
    }),
    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('especialidadMain').fireEvent('cursoSelected', recordId);
        }
    }
});