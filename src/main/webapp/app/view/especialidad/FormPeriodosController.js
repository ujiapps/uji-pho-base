Ext.define('pho.view.especialidad.FormPeriodosController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.formPeriodosController',

    onLoad: function() {
        var grid = this.getView().down('grid[name=periodos]');
        var viewModel = this.getViewModel();
        var especialidadId = viewModel.get('id');
        var store = viewModel.get('store');

        store.load({
            callback: function(records) {
                Ext.Ajax.request({
                    url: '/pho/rest/especialidades/' + especialidadId + '/periodos',
                    success: function(response, opts) {
                        var periodos = Ext.decode(response.responseText);
                        this.marcaPeriodos(periodos.data);
                    },
                    scope: this
                });
            },
            scope: this
        });
    },

    marcaPeriodos: function(periodos) {
        var grid = this.getView().down('grid[name=periodos]');
        var store = grid.getStore();

        for (var i in periodos) {
            var periodo = periodos[i];
            var record = store.getById(periodo.periodoId);
            if (record) {
                record.set('seleccionado', true);
            }
        }
    },

    onClose: function () {
        var win = Ext.WindowManager.getActive();
        if (win) {
            win.destroy();
        }
    },

    reloadParentGrid: function() {
        var grid = Ext.ComponentQuery.query('grid[name=especialidad]')[0];
        grid.getStore().reload();
    },

    onSaveRecord: function() {
        var grid = this.getView().down('grid[name=periodos]');
        var viewModel = this.getViewModel();
        var especialidadId = viewModel.get('id');
        var store = grid.getStore();
        var estadoPeriodos = [];
        store.getData().each(function(periodo) {
            estadoPeriodos.push({ id: periodo.get('id'), seleccionado: periodo.get('seleccionado') });
        });

        Ext.Ajax.request({
            url: '/pho/rest/especialidades/' + especialidadId + '/periodos',
            method: 'PUT',
            jsonData: { periodos: estadoPeriodos },
            success: function(response, opts) {
                this.reloadParentGrid();
                this.onClose();
            },
            scope: this
        });

    }
});