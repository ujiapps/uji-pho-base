Ext.define('pho.view.especialidad.FormComentario',
{
    extend : 'Ext.window.Window',
    alias : 'widget.formComentario',

    title : 'Comentari',
    width : 640,
    manageHeight : true,
    modal : true,
    bodyPadding : 10,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    requires : [ 'pho.view.especialidad.FormComentarioController' ],
    controller : 'formComentarioController',

    items : [
    {
        xtype : 'form',
        layout : 'anchor',
        items : [
        {
            xtype : 'textarea',
            name : 'comentarios',
            allowBlank : true,
            anchor : '100%',
            padding : 10,
            flex : 1,
            bind :
            {
                disabled : '{!selectedConvocatoria.editable}',
                value : '{record.comentarios}'
            }
        } ]
    } ],
    bbar : [ '->', '->',
    {
        xtype : 'button',
        text : 'Desar',
        handler : 'onSave',
        bind :
        {
            disabled : '{!selectedConvocatoria.editable}'
        }
    },
    {
        xtype : 'panel',
        html : '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
        listeners :
        {
            render : function(component)
            {
                component.getEl().on('click', 'onClose');
            }
        }
    } ]
});