Ext.define('pho.view.especialidad.GridController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.especialidadGridController',

        getValoresUnicos: function (store, field) {
            var items = [];

            var data = store.getData();
            var snapshot = data.getSource();
            var unfilteredCollection = snapshot || data;
            var allRecords = unfilteredCollection.getRange();

            Ext.Array.map(allRecords, function (rec) {
                Ext.Array.include(items, rec.get(field));
            });

            return Ext.Array.map(items, function (item) {
                var data =
                    {
                        id: item,
                        nombre: item
                    };
                return data;
            });
        },

        onAddComentario: function (view, rowIndex, colIndex, item, event, record) {
            var view = this.getView().up('especialidadMain');
            var grid = this.getView();
            var vm = this.getViewModel();
            var modal =
                {
                    xtype: 'formComentario',
                    viewModel:
                        {
                            data:
                                {
                                    record: record,
                                    store: grid.getStore()
                                }
                        }
                };

            view.add(modal).show();
        },

        onAdd: function () {
            var grid = this.getView();
            var comboCurso = grid.up('especialidadMain').down('especialidadComboCurso');

            if (!grid.allowEdit) return;

            var rec = Ext.create(grid.getStore().model.entityName, {
                id: null,
                curso: comboCurso.getValue()
            });
            grid.getStore().insert(0, rec);
            var editor = grid.plugins[0];
            editor.cancelEdit();
            editor.startEdit(rec, 0);
        },

        onLoad: function () {
            var grid = this.getView();
            var store = grid.getStore();
            store.load(
                {
                    callback: function () {
                        var cursos = this.getValoresUnicos(store, 'curso');
                        var comboCurso = grid.up('especialidadMain').down('especialidadComboCurso');
                        comboCurso.clearValue();
                        comboCurso.getStore().loadData(cursos);
                    },
                    scope: this
                });
        },

        onBeforeEdit: function (plugin, cell) {
            return cell.field !== "numeroPeriodos";
        },

        onEdit: function (table, td, cellindex, record) {
            var grid = this.getView();
            var column = grid.getColumns()[cellindex];
            if (column.dataIndex === "numeroPeriodos") {
                return this.onDetallePeriodos();
            }

            var selection = grid.getView().getSelectionModel().getSelection()[0];
            if (selection) {
                var editor = grid.plugins[0];
                editor.cancelEdit();
                editor.startEdit(selection);
            }
        },

        onDetallePeriodos: function () {
            var grid = this.getView();
            var record = grid.getView().getSelectionModel().getSelection()[0];
            var panel = this.getView().up('panel');

            if (!record) {
                return;
            }

            this.modal = panel.add(
                {
                    xtype: 'formPeriodos',
                    viewModel:
                        {
                            data:
                                {
                                    title: 'Períodes de la especialiatat: ' + record.get('nombre'),
                                    id: record.get('id'),
                                    store: Ext.create('Ext.ux.uji.data.Store',
                                        {
                                            model: 'pho.model.Periodo',
                                            url: '/pho/rest/periodos'
                                        })
                                }
                        }
                });

            ref = this;

            if (record.get('periodoInicialId')) {
                Ext.Msg.confirm('Períodes no consecutius', 'Si tens definit períodes Inicial i Final, la definició de períodes no consecutius no tindrà cap efecte. Estàs segur de voler continuar?', function (result) {
                    if (result === 'yes') {
                        ref.modal.show();
                    }
                });
            } else {
                ref.modal.show();
            }

        }
    });
