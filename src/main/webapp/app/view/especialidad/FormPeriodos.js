Ext.define('pho.view.especialidad.FormPeriodos',
{
    extend : 'Ext.window.Window',
    xtype : 'formPeriodos',

    requires: [
        'pho.view.especialidad.FormPeriodosController'
    ],
    controller: 'formPeriodosController',
    bind :
    {
        title : '{title}'
    },
    width : 320,
    height : 600,
    modal : true,
    bodyPadding : 10,
    layout : 'fit',
    bbar : [ '->',
    {
        xtype : 'button',
        text : 'Guardar',
        handler : 'onSaveRecord'
    },
    {
        xtype : 'panel',
        html : '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
        listeners :
        {
            render : function(component)
            {
                component.getEl().on('click', 'onClose');
            }
        }
    } ],

    items : [
    {
        xtype : 'grid',
        name: 'periodos',
        viewConfig:{
            markDirty:false
        },
        bind :
        {
            store : '{store}'
        },
        columns : [
        {
            text : 'ID',
            dataIndex : 'id',
            hidden: true
        },
        {
            dataIndex : 'nombre',
            flex : 1,
            text : 'Nom',
            editor :
            {
                field :
                {
                    allowBlank : false
                }
            }
        },
        {
            dataIndex : 'seleccionado',
            text : 'Sel·lecció',
            xtype : 'checkcolumn'
        } ]

    } ],

    listeners: {
        render: 'onLoad'
    }
})
