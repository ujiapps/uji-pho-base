Ext.define('pho.view.fase.FormAsignaturas',
{
    extend : 'Ext.window.Window',
    alias : 'widget.formAsignaturas',

    title : 'Assignatures',
    width : 640,
    manageHeight : true,
    modal : true,
    bodyPadding : 10,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },
    viewConfig :
    {
        deferEmptyText : false,
        emptyText : 'Encara sense assignatures'
    },

    requires : [ 'pho.view.fase.FormAsignaturasController' ],
    controller : 'formAsignaturasController',

    items : [
    {
        xtype : 'form',
        layout : 'anchor',
        name : 'asignaturas',
        items : [
        {
            xtype : 'multiselector',
            emptyText : 'Encara sense assignatures',
            name : 'asignaturas',
            title : 'Afegir noves assignatures',
            anchor : '100%',
            padding : 10,
            flex : 1,
            bind :
            {
                store : '{asignaturasStore}'
            },
            columns :
            {
                items : [
                {
                    text : 'id',
                    dataIndex : 'id',
                    hidden : true
                },
                {
                    text : 'Codi',
                    dataIndex : 'codigoAsignatura',
                    align : 'center'
                },
                {
                    text : 'Assignatura',
                    dataIndex : 'nombre',
                    flex : 1
                },
                {
                    text : 'Curs',
                    dataIndex : 'curso',
                    align : 'center'
                },
                {
                    text : 'Semestre',
                    dataIndex : 'semestre',
                    align : 'center'
                },
                {
                    xtype : 'actioncolumn',
                    align : 'right',
                    width : 25,
                    items : [
                    {
                        iconCls : 'x-fa fa-remove',
                        tooltip : 'Esborrar',
                        handler : 'borraAsignatura'
                    } ]
                } ]
            },
            search :
            {
                width : 500,
                field : 'nombreCompleto',
                makeItems : function()
                {
                    return [
                    {
                        xtype : 'grid',
                        reference : 'searchGrid',
                        trailingBufferZone : 2,
                        leadingBufferZone : 2,
                        viewConfig :
                        {
                            deferEmptyText : false,
                            emptyText : 'Cap resultat.'
                        },
                        selModel :
                        {
                            type : 'checkboxmodel',
                            checkOnly : true,
                            pruneRemoved : false,
                            listeners :
                            {
                                selectionchange : 'onSelectionChange'
                            }
                        }
                    } ];
                },

                store :
                {
                    fields : [ 'id', 'codigoAsignatura', 'semestre', 'curso', 'nombre',
                    {
                        name : 'nombreCompleto',
                        type : 'string',
                        calculate : function(data)
                        {
                            return data.codigoAsignatura + ' - ' + data.curso + ' - ' + data.semestre + 'S - ' + data.nombre;
                        }
                    }, ],
                    proxy :
                    {
                        type : 'rest',
                        url : '/pho/rest/asignaturas',
                        reader :
                        {
                            type : 'json',
                            rootProperty : 'data'
                        }
                    }
                },
                search : function(text)
                {
                    var me = this, filter = me.searchFilter, filters = me.getSearchStore().getFilters();

                    if (text)
                    {
                        filters.beginUpdate();

                        if (filter)
                        {
                            filter.setValue(text);
                        }
                        else
                        {
                            me.searchFilter = filter = new Ext.util.Filter(
                            {
                                id : 'search',
                                property : me.field,
                                value : text,
                                anyMatch : true
                            });
                        }

                        filters.add(filter);

                        filters.endUpdate();
                    }
                    else if (filter)
                    {
                        filters.remove(filter);
                    }
                }
            }
        } ]
    } ],

    bbar : [ '->', '->',
    {
        xtype : 'button',
        text : 'Desar',
        handler : 'onSave',
        iconCls : 'x-fa fa-save',
        bind :
        {
            disabled : '{!selectedConvocatoria.editable}'
        }
    },
    {
        xtype : 'panel',
        border : 0,
        html : '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
        listeners :
        {
            render : function(component)
            {
                component.getEl().on('click', 'onClose');
            }
        }
    } ]
});