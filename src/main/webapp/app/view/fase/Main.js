Ext.define('pho.view.fase.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.faseMain',
    title : 'Gestió de Fases',
    requires : [ 'pho.view.fase.ViewModel', 'pho.view.fase.Grid', 'pho.view.fase.MainController' ],
    viewModel : 'faseViewModel',
    controller : 'faseMainController',
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'faseGrid',
        flex : 1,
        scrollable : true
    } ],

    listeners :
    {
        activate : 'onActivate'
    }
});
