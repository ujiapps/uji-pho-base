Ext.define('pho.view.fase.GridController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.faseGridController',

    gestionaAsignaturas : function(grid, index)
    {
        var grid = this.getView();
        var fase = grid.getStore().getAt(index);
        var faseId = fase.get('id');

        var vm = this.getViewModel();

        var fasesAsignaturasStore = vm.getStore('fasesAsignaturasStore');
        var asignaturasStore = vm.getStore('asignaturasStore');

        var view = this.getView().up('faseMain')
        fasesAsignaturasStore.setUrl(faseId);

        grid.setLoading(true);
        fasesAsignaturasStore.load(
        {
            callback : function(data)
            {
                grid.setLoading(false);

                var store = Ext.create('Ext.data.Store', {
                    model: 'pho.model.Asignatura',
                    autoLoad: true,
                    proxy: {
                        type: 'memory',
                        reader: {
                            type: 'json'
                        }
                    }
                });

                data.map(function(rec) {
                    store.add(asignaturasStore.findRecord('id', rec.get('asignaturaId')));
                });

                var modal =
                {
                    xtype : 'formAsignaturas',
                    viewModel :
                    {
                        data :
                        {
                            fase : fase,
                            asignaturasStore : store
                        }
                    }
                };
                view.add(modal).show();
            },
            scope : this
        })
    }

});
