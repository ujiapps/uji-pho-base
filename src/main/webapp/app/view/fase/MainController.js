Ext.define('pho.view.fase.MainController',
{
    extend : 'Ext.app.ViewController',
    alias : 'controller.faseMainController',

    onActivate: function() {
        var vm = this.getViewModel();
        var store = vm.getStore('fasesStore');
        store.reload();
    }
});
