Ext.define('pho.view.fase.FormAsignaturasController',
{
    extend : 'Ext.app.ViewController',
    alias : 'controller.formAsignaturasController',

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        if (win)
        {
            win.destroy();
        }
    },

    borraAsignatura : function(grid, index)
    {
        var rec = grid.getStore().getAt(index);
        grid.getStore().remove(rec);
    },

    onSave : function()
    {
        var vm = this.getViewModel();
        var view = this.getView();
        var multiselector = view.down('multiselector[name=asignaturas]');
        var asignaturasStore = vm.get('asignaturasStore');
        var fasesStore = vm.get('fasesStore');

        var fase = vm.get('fase');

        view.setLoading(true);
        var codigosAsig = [];
        var idsAsig = [];

        asignaturasStore.getData().each(function(asignatura)
        {
            codigosAsig.push(asignatura.get('codigoAsignatura'));
            idsAsig.push(asignatura.get('id'));
        });

        fase.set('asignaturas', codigosAsig.join(', '));
        fase.set('asignaturasIds', idsAsig.join(', '));

        fasesStore.sync(
        {
            params: {
                syncAsignaturas: true
            },
            success : function()
            {
                view.setLoading(false);
                this.onClose();
            },
            failure : function()
            {
                view.setLoading(false);
            },
            scope : this
        });
    }
});
