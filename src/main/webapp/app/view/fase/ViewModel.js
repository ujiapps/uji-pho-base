Ext.define('pho.view.fase.ViewModel',
{
    extend : 'Ext.app.ViewModel',
    alias : 'viewmodel.faseViewModel',
    requires : [ 'pho.model.Asignatura', 'pho.model.Fase', 'pho.model.FaseAsignatura' ],
    stores :
    {
        fasesStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Fase',
            url : '/pho/rest/fases',
            sorters: [ 'nombre' ]
        }),
        fasesAsignaturasStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.FaseAsignatura',
            url : '/pho/rest/fases/faseasignaturas',
            setUrl : function (faseId) {
                this.getProxy().url = '/pho/rest/fases/' + faseId + '/faseasignaturas';
            }
        }),
        asignaturasStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Asignatura',
            url : '/pho/rest/asignaturas',
            autoLoad: true
        })
    }
});
