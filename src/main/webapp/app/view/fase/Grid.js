Ext.define('pho.view.fase.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.faseGrid',

    padding : 10,
    bind :
    {
        store : '{fasesStore}'
    },

    requires : [ 'pho.view.fase.GridController', 'pho.view.fase.FormAsignaturas' ],
    controller : 'faseGridController',

    title : 'Fases',

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'cursoId',
        text : 'Curs',
        width : 60,
        align : 'center',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    },
    {
        dataIndex : 'nombre',
        text : 'Nom',
        flex : 1,
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    },
    {
        xtype : 'actioncolumn',
        align : 'center',
        width : 25,
        items : [
        {
            iconCls : 'x-fa fa-gear',
            tooltip : 'Gestió d\'assignatures',
            handler : 'gestionaAsignaturas'
        } ]
    },
    {
        text : 'Assignatures',
        dataIndex : 'asignaturas',
        flex : 1
    },
    {
        xtype : 'datecolumn',
        dataIndex : 'fechaDesde',
        format : 'd/m/Y',
        align : 'center',
        text : 'Data des de',
        width : 150,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            allowBlank : false
        }
    },
    {
        xtype : 'datecolumn',
        dataIndex : 'fechaHasta',
        format : 'd/m/Y',
        align : 'center',
        text : 'Data fins',
        width : 150,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            allowBlank : false
        }
    },
    {
        dataIndex : 'tipo',
        text : 'Tipus',
        align : 'center',
        editor :
        {
            field :
            {
                editable : false,
                allowBlank : false,
                xtype : 'combobox',
                displayField : 'text',
                valueField : 'id',
                store : Ext.create('Ext.data.Store',
                {
                    fields : [ 'id', 'text' ],
                    data : [
                    {
                        id : 'OP',
                        text : 'Optativa'
                    },
                    {
                        id : 'OB',
                        text : 'Obligatòria'
                    },
                    {
                        id : 'EX',
                        text : 'Extraordinària'
                    } ]
                })
            }
        }
    } ]
});