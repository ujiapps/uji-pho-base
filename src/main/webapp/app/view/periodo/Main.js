Ext.define('pho.view.periodo.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.periodoMain',
    title : 'Gestió: Periodo',
    requires : [ 'pho.view.periodo.Grid' ],

    items : [
    {
        xtype : 'periodoGrid'
    } ]
});
