Ext.define('pho.view.periodo.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.periodoGrid',

    store : Ext.create('Ext.ux.uji.data.Store',
    {
        model : 'pho.model.Periodo',
        url : '/pho/rest/periodos'
    }),

    title : 'Periodo',

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'nombre',
        text: 'Nom',
        editor :
        {
            field :
            {
              allowBlank : false
            }
        }
    },
    {
        dataIndex : 'semestre1',
        text: 'Semestre 1',
        editor :
        {
            field :
            {
              allowBlank : false
            }
        }
    },
    {
        dataIndex : 'semestre2',
        text: 'Semestre 2',
        editor :
        {
            field :
            {
              allowBlank : false
            }
        }
    },
  ]
});