Ext.define('pho.view.bloque.Main',
{
    extend : 'Ext.panel.Panel',

    requires: [ 'pho.view.bloque.ComboCurso', 'pho.view.bloque.MainController', 'pho.view.bloque.Grid' ],

    controller: 'bloqueMainController',
    alias : 'widget.bloqueMain',
    title : 'Gestió: Bloque',
    layout: 'fit',

    tbar: [ '->', {
        xtype: 'bloqueComboCurso'
    }],

    items : [
    {
        xtype : 'bloqueGrid'
    } ],

    listeners: {
        cursoSelected: 'onCursoSelected'
    }
});
