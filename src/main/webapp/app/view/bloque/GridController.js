Ext.define('pho.view.bloque.GridController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.bloqueGridController',

    getValoresUnicos : function(store, field)
    {
        var items = [];

        var data = store.getData();
        var snapshot = data.getSource();
        var unfilteredCollection = snapshot || data;
        var allRecords = unfilteredCollection.getRange();

        Ext.Array.map(allRecords, function(rec) {
            Ext.Array.include(items, rec.get(field));
        });

        return Ext.Array.map(items, function(item)
        {
            var data =
                {
                    id : item,
                    nombre : item
                };
            return data;
        });
    },

    onEditComplete : function(editor, context)
    {
        var grid = this.getView();
        var newrecord = grid.getStore().getNewRecords().length > 0;
        grid.getStore().sync({
            success : function() {
                if (newrecord && grid.reloadAfterInsert) {
                    grid.getStore().reload({});
                } else {
                    grid.getView().refresh();
                }
            }
        });
    },


    onAdd : function()
    {
        var grid = this.getView();
        var comboCurso = grid.up('bloqueMain').down('bloqueComboCurso');

        if (!grid.allowEdit) return;

        var rec = Ext.create(grid.getStore().model.entityName, {
            id : null,
            curso: comboCurso.getValue()
        });
        grid.getStore().insert(0, rec);
        var editor = grid.plugins[0];
        editor.cancelEdit();
        editor.startEdit(rec, 0);
    },


    onLoad : function()
    {
        var grid = this.getView();
        var store = grid.getStore();
        store.load({
            callback: function() {
                store.clearFilter();
                var cursos = this.getValoresUnicos(store, 'curso');
                var comboCurso = grid.up('bloqueMain').down('bloqueComboCurso');
                comboCurso.clearValue();
                comboCurso.getStore().loadData(cursos);
            },
            scope: this
        });
    }
});
