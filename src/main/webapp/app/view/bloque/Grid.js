Ext.define('pho.view.bloque.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.bloqueGrid',

    requires : [ 'pho.view.bloque.GridController' ],
    controller : 'bloqueGridController',
    padding : 10,
    store : Ext.create('Ext.ux.uji.data.Store',
    {
        model : 'pho.model.Bloque',
        url : '/pho/rest/bloques',
        sorters : [ 'orden' ]
    }),

    title : 'Blocs',
    reloadAfterInsert : true,

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'nombre',
        text : 'Nom',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        },
        flex : 1
    },
    {
        dataIndex : 'curso',
        text : 'Curs',
        align: 'center',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    },
    {
        dataIndex : 'numeroObligatorias',
        text : 'Obligatòries',
        align : 'center',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    },
    {
        dataIndex : 'numeroOptativas',
        text : 'Optatives',
        align : 'center',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    },
    {
        xtype : 'foreigncolumn',
        header : 'Assignatura',
        dataIndex : 'asignaturaId',
        align : 'center',
        editable : true,
        width: 150,
        allowBlank : false,
        displayField : 'codigoAsignatura'
    },
    {
        dataIndex : 'orden',
        text : 'Ordre',
        align : 'center',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    } ]
});
