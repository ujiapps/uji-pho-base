Ext.define('pho.view.bloque.ComboCurso',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.bloqueComboCurso',
    fieldLabel : 'Curs',
    labelWidth: 60,
    showClearIcon : true,

    allowBlank: true,

    store : Ext.create('Ext.data.Store',
    {
        model : 'pho.model.Lookup',
        autoLoad : true,
        sorters : [ 'nombre' ],
        proxy :
        {
            type : 'memory'
        }
    }),
    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('bloqueMain').fireEvent('cursoSelected', recordId);
        }
    }
});