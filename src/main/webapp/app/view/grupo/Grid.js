Ext.define('pho.view.grupo.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.grupoGrid',
    reference: 'grupoGrid',

    bind :
    {
        store : '{gruposStore}',
        disabled : '{!faseSelected}'
    },

    title : 'Grups',

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'faseId',
        hidden : true
    },
    {
        dataIndex : 'nombre',
        text : 'Nom',
        flex : 1,
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    } ],

    listeners :
    {
        selectionchange : function(cmp, selection)
        {
            if (!selection.length) return;
            this.up('grupoMain').fireEvent('grupoSelected', selection[0].get('id'));
        }
    }
});