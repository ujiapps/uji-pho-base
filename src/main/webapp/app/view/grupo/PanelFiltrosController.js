Ext.define('pho.view.grupo.PanelFiltrosController',
{
    extend : 'Ext.app.ViewController',
    alias : 'controller.grupoPanelFiltrosController',

    onFaseSelected : function(faseId)
    {
        if (faseId == null)
        {
            return;
        }

        var panel = this.getView();
        var store = this.getStore('gruposStore');

        panel.setLoading(true);
        store.setUrl(faseId);

        store.load(
        {
            callback : function(records)
            {
                panel.setLoading(false);
            }
        });
    }
});
