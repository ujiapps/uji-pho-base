Ext.define('pho.view.grupo.PanelFiltros',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.grupoPanelFiltros',
    border : 0,
    padding : 10,
    requires : [ 'pho.view.grupo.PanelFiltrosController', 'pho.view.grupo.ComboFase' ],
    controller : 'grupoPanelFiltrosController',

    layout : 'anchor',
    items : [
    {
        xtype : 'grupoComboFase'
    } ],

    listeners :
    {
        faseSelected : 'onFaseSelected'
    }
});
