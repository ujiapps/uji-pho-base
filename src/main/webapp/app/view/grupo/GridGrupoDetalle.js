Ext.define('pho.view.grupo.GridGrupoDetalle',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.grupoDetalleGrid',

    bind :
    {
        store : '{gruposDetalleStore}',
        disabled : '{!grupoSelected}'
    },

    reloadAfterInsert : true,

    title : 'Detalls del Grup',

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'grupoId',
        hidden : true
    },
    {
        xtype : 'foreigncolumn',
        header : 'Periode',
        dataIndex : 'periodoId',
        editable : true,
        model : 'Periodo',
        bindStore : '{periodosStore}',
        flex : 1
    },
    {
        xtype : 'foreigncolumn',
        header : 'Especialitat',
        dataIndex : 'especialidadId',
        displayField : 'cursoNombre',
        editable : true,
        model : 'Especialidad',
        bindStore : '{especialidadesStore}',
        flex : 4
    } ]
});