Ext.define('pho.view.grupo.ViewModel',
{
    extend : 'Ext.app.ViewModel',
    alias : 'viewmodel.grupoViewModel',
    requires : [ 'pho.model.Grupo', 'pho.model.GrupoDetalle', 'pho.model.Fase', 'pho.model.Periodo', 'pho.model.Especialidad' ],
    formulas :
    {
        faseSelected : function(get)
        {
            return get('grupoComboFase.selection');
        },
        grupoSelected : function(get)
        {
            return get('grupoGrid.selection');
        }
    },
    stores :
    {
        gruposStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Grupo',
            url : '/pho/rest/fases/-1/grupos',
            setUrl : function(faseId)
            {
                this.getProxy().url = '/pho/rest/fases/' + faseId + '/grupos';
            },
            sorters : [
            {
                property : 'nombre',
                direction : 'ASC'
            } ]
        }),
        gruposDetalleStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.GrupoDetalle',
            url : '/pho/rest/grupos/-1/gruposdetalle',
            setUrl : function(grupoId)
            {
                this.getProxy().url = '/pho/rest/grupos/' + grupoId + '/gruposdetalle';
            },
            sorters : [
            {
                property : 'periodoId',
                direction : 'ASC'
            } ]
        }),
        fasesStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Fase',
            url : '/pho/rest/fases',
            autoLoad : true,
            sorters : [
            {
                property : 'nombre',
                direction : 'ASC'
            } ]
        }),
        periodosStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Periodo',
            url : '/pho/rest/periodos',
            autoLoad : true,
            sorters : [
            {
                property : 'id',
                direction : 'ASC'
            } ]
        }),
        especialidadesStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Especialidad',
            url : '/pho/rest/especialidades',
            autoLoad : true,
            sorters : [
            {
                property : 'cursoNombre',
                direction : 'ASC'
            } ]
        })
    }
});
