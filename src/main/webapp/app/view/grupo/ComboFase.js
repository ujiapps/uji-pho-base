Ext.define('pho.view.grupo.ComboFase',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.grupoComboFase',
    reference: 'grupoComboFase',
    fieldLabel : 'Fase',
    showClearIcon : true,
    emptyText: 'Sel·lecciona una fase',
    labelWidth : 70,
    width : 450,
    padding : 5,

    bind :
    {
        store : '{fasesStore}'
    },

    listeners :
    {
        change : function(combo, faseId)
        {
            combo.up('grupoPanelFiltros').fireEvent('faseSelected', faseId);
        }
    }
});