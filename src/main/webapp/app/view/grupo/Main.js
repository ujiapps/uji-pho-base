Ext.define('pho.view.grupo.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.grupoMain',
    title : 'Gestió: Grups',
    requires : [ 'pho.view.grupo.ViewModel', 'pho.view.grupo.Grid', 'pho.view.grupo.GridGrupoDetalle', 'pho.view.grupo.PanelFiltros', 'pho.view.grupo.MainController' ],

    padding: 10,
    controller : 'grupoMainController',

    viewModel :
    {
        type : 'grupoViewModel'
    },

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'grupoPanelFiltros',
        height : 60
    },
    {
        xtype : 'grupoGrid',
        flex : 1,
        scrollable : true
    },
    {
        xtype : 'grupoDetalleGrid',
        flex : 1,
        scrollable : true
    } ],

    listeners :
    {
        grupoSelected : 'onGrupoSelected'
    }
});
