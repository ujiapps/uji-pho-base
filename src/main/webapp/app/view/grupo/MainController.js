Ext.define('pho.view.grupo.MainController',
{
    extend : 'Ext.app.ViewController',
    alias : 'controller.grupoMainController',

    onGrupoSelected : function(grupoId)
    {
        if (grupoId == null)
        {
            return;
        }

        var vm = this.getViewModel();
        var store = vm.getStore('gruposDetalleStore');
        var panel = this.getView();

        panel.setLoading(true);
        store.setUrl(grupoId);

        store.load(
        {
            callback : function(records)
            {
                panel.setLoading(false);
            }
        });
    }
});
