Ext.define('pho.view.limite.FormComentarioController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.formComentarioController',

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        if (win)
        {
            win.destroy();
        }
    },

    onSave : function()
    {
        var view = this.getView();
        var vm = this.getViewModel();
        var form = view.down('form');
        var record = vm.get('record');
        var store = vm.get('store');

        if (form.isValid())
        {
            var comentarios = form.getValues().comentarios;

            record.set('comentarios', comentarios);
            store.sync({
                success: function() {
                    this.onClose();
                },
                scope: this
            })

        }
    }
});
