Ext.define('pho.view.limite.MainController',
{
    extend : 'Ext.app.ViewController',
    alias : 'controller.limiteMainController',

    onEspecialidadSelected : function(recordId)
    {
        var grid = Ext.ComponentQuery.query('limiteGrid')[0];
        grid.fireEvent('especialidadSelected', recordId);
    },

    onCursoSelected : function(recordId)
    {
        var grid = Ext.ComponentQuery.query('limiteGrid')[0];
        grid.fireEvent('cursoSelected', recordId);
    },
});
