Ext.define('pho.view.limite.Main',
    {
        extend: 'Ext.panel.Panel',

        alias: 'widget.limiteMain',
        title: 'Gestió: Plaçes',
        padding: 10,

        requires: ['pho.view.limite.ViewModel', 'pho.view.limite.Grid', 'pho.view.limite.MainController', 'pho.view.limite.ComboEspecialidad', 'pho.view.limite.ComboCurso', 'pho.view.limite.FormComentario'],
        controller: 'limiteMainController',

        viewModel:
            {
                type: 'limiteViewModel'
            },

        layout: 'fit',
        border: 0,

        items: [
            {
                xtype: 'limiteGrid'
            }],
        listeners:
            {
                especialidadSelected: 'onEspecialidadSelected',
                cursoSelected: 'onCursoSelected'
            }
    });
