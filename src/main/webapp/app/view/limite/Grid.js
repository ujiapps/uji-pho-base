Ext.define('pho.view.limite.Grid',
    {
        extend: 'Ext.ux.uji.grid.Panel',
        alias: 'widget.limiteGrid',

        bind:
            {
                store: '{limitesStore}'
            },

        reloadAfterInsert: true,

        requires: ['pho.view.limite.GridController', 'pho.view.limite.ComboCurso'],
        controller: 'limiteGridController',

        plugins: [
            {
                ptype: 'rowediting',
                clicksToEdit: 2
            }],

        tbar: [
            {
                xtype: 'button',
                iconCls: 'fa fa-plus',
                text: 'Afegir',
                handler: 'onAdd'
            },
            {
                xtype: 'button',
                iconCls: 'fa fa-edit',
                text: 'Editar',
                handler: 'onEdit'
            },
            {
                xtype: 'button',
                iconCls: 'fa fa-remove',
                text: 'Borrar',
                handler: 'onDelete'
            }, '->',
            {
                xtype: 'limiteComboCurso',
                margin: '0 0 10 0'
            },
            {
                xtype: 'comboEspecialidad',
                margin: '0 10 10 10'
            }],

        columns: [
            {
                text: 'ID',
                dataIndex: 'id',
                hidden: true
            },
            {
                xtype: 'foreigncolumn',
                dataIndex: 'cursoId',
                align: 'center',
                header: 'Curs Acadèmic',
                editable: true,
                bindStore: '{cursosStore}',
                displayField: 'id'
            },
            {
                xtype: 'foreigncolumn',
                flex: 1,
                dataIndex: 'especialidadId',
                header: 'Especialitat',
                editable: true,
                bindStore: '{especialidadesStore}',
                displayField: 'cursoNombre'
            },
            {
                xtype: 'foreigncolumn',
                header: 'Hospital',
                flex: 1,
                dataIndex: 'hospitalId',
                text: 'Hospital',
                editable: true,
                bindStore: '{hospitalesStore}'
            },
            {
                dataIndex: 'plazas',
                text: 'Plaçes',
                align: 'center',
                editor:
                    {
                        field:
                            {
                                allowBlank: false
                            }
                    }
            }, {
                xtype: 'actioncolumn',
                text: 'Comentari',
                align: 'center',
                dataIndex: 'comentarios',
                width: 120,
                menuDisabled: true,
                sortable: false,
                items: [
                    {
                        handler: 'onAddComentario',
                        getTip: function (value, metadata) {
                            return value ? Ext.util.Format.htmlEncode(value) : 'Afegir comentari';
                        },
                        getClass: function (comentarios) {
                            return comentarios ? 'x-fa fa-file-text-o' : 'x-fa fa-plus';
                        },
                        width: 180
                    }]
            }],
        listeners:
            {
                especialidadSelected: 'onEspecialidadSelected',
                cursoSelected: 'onCursoSelected',
                render: 'onRender',
                edit: 'onEditComplete',
                onEdit: 'onEdit',
                canceledit: 'onCancelEdit'
            }
    });