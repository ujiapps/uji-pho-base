Ext.define('pho.view.limite.ComboCurso',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.limiteComboCurso',
    fieldLabel : 'Curs',
    labelWidth: 60,
    width: 180,
    showClearIcon : true,
    allowBlank: true,

    store : Ext.create('Ext.data.Store',
    {
        model : 'pho.model.Lookup',
        autoLoad : true,
        sorters : [ 'nombre' ],
        proxy :
        {
            type : 'memory'
        }
    }),
    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('limiteMain').fireEvent('cursoSelected', recordId);
        }
    }
});