Ext.define('pho.view.limite.ComboEspecialidad',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.comboEspecialidad',
    fieldLabel : 'Especialitat',
    showClearIcon : true,
    width : 380,
    allowBlank: true,
    matchFieldWidth: false,

    displayField: 'nombreCurso',

    store : Ext.create('Ext.ux.uji.data.Store',
    {
        model : 'pho.model.Especialidad',
        url : '/pho/rest/especialidades',
        autoLoad : true,
        sorters : [ 'nombreCurso' ]
    }),
    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('limiteMain').fireEvent('especialidadSelected', recordId);
        }
    }

});