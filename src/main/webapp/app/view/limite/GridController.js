Ext.define('pho.view.limite.GridController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.limiteGridController',

        onEspecialidadSelected: function (recordId) {
            var grid = this.getView();

            if (recordId) {
                grid.getStore().addFilter(new Ext.util.Filter(
                    {
                        id: 'especialidadId',
                        filterFn: function (rec) {
                            return !rec.get('especialidadId') || rec.get('especialidadId') === recordId;
                        }
                    }));
            } else {
                grid.getStore().clearFilter();
            }
        },

        onAddComentario: function (view, rowIndex, colIndex, item, event, record) {
            var view = this.getView().up('limiteMain')
            var vm = this.getViewModel();
            var modal =
                {
                    xtype: 'formComentario',
                    viewModel:
                        {
                            data:
                                {
                                    record: record,
                                    store: vm.getStore('limitesStore')
                                }
                        }
                };

            view.add(modal).show();
        },

        onCursoSelected: function (cursoId) {
            var grid = this.getView();

            if (cursoId) {
                grid.getStore().addFilter(new Ext.util.Filter(
                    {
                        id: 'cursoId',
                        filterFn: function (rec) {
                            return !rec.get('_especialidad') || rec.get('_especialidad').curso === cursoId;
                        }
                    }));

                var storeEspecialidad = grid.down('comboEspecialidad').getStore();
                storeEspecialidad.addFilter(new Ext.util.Filter(
                    {
                        id: 'cursoId',
                        filterFn: function (rec) {
                            return rec.get('curso') === parseInt(cursoId, 10);
                        }
                    }));
            } else {
                grid.getStore().removeFilter('cursoId');
                var storeEspecialidad = grid.down('comboEspecialidad').getStore();
                storeEspecialidad.removeFilter('cursoId');
            }
        },

        onAdd: function () {
            var grid = this.getView();
            var especialidadId = grid.down('comboEspecialidad').getValue();

            if (!grid.allowEdit) return;

            var rec = Ext.create(grid.getStore().model.entityName, {
                id: null,
                especialidadId: especialidadId
            });
            grid.getStore().insert(0, rec);
            var editor = grid.plugins[0];
            editor.cancelEdit();
            editor.startEdit(rec, 0);
        },

        getValoresUnicos: function (store, field) {
            var items = [];

            var data = store.getData();
            var snapshot = data.getSource();
            var unfilteredCollection = snapshot || data;
            var allRecords = unfilteredCollection.getRange();

            Ext.Array.map(allRecords, function (rec) {
                Ext.Array.include(items, rec.get(field));
            });

            return Ext.Array.map(items, function (item) {
                var data =
                    {
                        id: item,
                        nombre: item
                    };
                return data;
            });
        },

        onRender: function () {
            var vm = this.getViewModel();
            var grid = this.getView();
            var store = vm.getStore('limitesStore');
            store.load(
                {
                    callback: function (data) {
                        var cursos = this.getValoresUnicos(store, 'curso');
                        var comboCurso = grid.down('limiteComboCurso');
                        comboCurso.getStore().loadData(cursos);
                    },
                    scope: this
                });
        }

    });
