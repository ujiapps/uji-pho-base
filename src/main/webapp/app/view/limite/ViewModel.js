Ext.define('pho.view.limite.ViewModel',
{
    extend : 'Ext.app.ViewModel',
    alias : 'viewmodel.limiteViewModel',
    requires : [ 'pho.model.Hospital', 'pho.model.Especialidad' ],
    stores :
    {
        cursosStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Curso',
            url : '/pho/rest/cursos',
            autoLoad: true
        }),
        limitesStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Limite',
            url : '/pho/rest/limites',
            sorters : [ 'nombreEspecialidad', 'nombreHospital' ]
        }),
        hospitalesStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Hospital',
            url : '/pho/rest/hospitales',
            autoLoad : true,
            sorters : [
            {
                property : 'nombre',
                direction : 'ASC'
            } ]
        }),
        especialidadesStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Especialidad',
            url : '/pho/rest/especialidades',
            autoLoad : true,
            sorters : [
            {
                property : 'cursoNombre',
                direction : 'ASC'
            } ]
        })
    }
});
