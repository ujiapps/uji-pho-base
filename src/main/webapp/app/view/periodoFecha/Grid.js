Ext.define('pho.view.periodoFecha.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.periodoFechaGrid',
    reference : 'periodoFechaGrid',

    bind :
    {
        store : '{periodoFechasStore}',
        disabled : '{!faseSelected}'
    },

    title : 'Dates periodes',

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'faseId',
        hidden : true
    },
    {
        dataIndex : 'cursoId',
        text : 'Curs',
        align: 'center',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    },
    {
        xtype : 'foreigncolumn',
        dataIndex : 'periodoId',
        header : 'Periode',
        align: 'center',
        editable : true
    },
    {
        xtype : 'datecolumn',
        dataIndex : 'fechaInicio',
        format : 'd/m/Y',
        text : 'Data Inici',
        align: 'center',
        width : 150,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            allowBlank : false
        }
    },
    {
        xtype : 'datecolumn',
        dataIndex : 'fechaFin',
        text : 'Data Fi',
        align: 'center',
        width : 150,
        format : 'd/m/Y',
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            allowBlank : false
        }
    } ]
});