Ext.define('pho.view.periodoFecha.ViewModel',
{
    extend : 'Ext.app.ViewModel',
    alias : 'viewmodel.periodoFechaViewModel',
    requires : [ 'pho.model.PeriodoFecha', 'pho.model.Fase' ],
    formulas :
    {
        faseSelected : function(get)
        {
            return get('periodoFechaComboFase.selection');
        }
    },
    stores :
    {
        periodoFechasStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.PeriodoFecha',
            url : '/pho/rest/fases/1/periodosfechas',
            setUrl : function(faseId)
            {
                this.getProxy().url = '/pho/rest/fases/' + faseId + '/periodosfechas';
            },
            sorters : [ 'periodoId' ]
        }),
        fasesStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Fase',
            url : '/pho/rest/fases',
            autoLoad : true,
            sorters : [
            {
                property : 'nombre',
                direction : 'ASC'
            } ]
        })
    }
});
