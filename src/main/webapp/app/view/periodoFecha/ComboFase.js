Ext.define('pho.view.periodoFecha.ComboFase',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.periodoFechaComboFase',
    reference : 'periodoFechaComboFase',
    emptyText: 'Sel·lecciona una fase',
    fieldLabel : 'Fase',
    showClearIcon : true,
    allowBlank: true,
    labelWidth : 70,
    width : 450,
    bind :
    {
        store : '{fasesStore}'
    },

    listeners :
    {
        change : function(combo, faseId)
        {
            combo.up('periodoFechaPanelFiltros').fireEvent('faseSelected', faseId);
        }
    }
});