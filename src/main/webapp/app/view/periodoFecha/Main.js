Ext.define('pho.view.periodoFecha.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.periodoFechaMain',
    title : 'Gestió: Dates Periodes',
    requires : [ 'pho.view.periodoFecha.ViewModel', 'pho.view.periodoFecha.Grid', 'pho.view.periodoFecha.PanelFiltros' ],

    padding: 10,
    viewModel :
    {
        type : 'periodoFechaViewModel'
    },

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'periodoFechaPanelFiltros',
        height : 60
    },
    {
        xtype : 'periodoFechaGrid',
        flex : 1,
        scrollable : true
    } ]
});
