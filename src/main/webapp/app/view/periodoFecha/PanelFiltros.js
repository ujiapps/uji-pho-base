Ext.define('pho.view.periodoFecha.PanelFiltros',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.periodoFechaPanelFiltros',
    border : 0,
    padding : 10,
    requires : [ 'pho.view.periodoFecha.PanelFiltrosController', 'pho.view.periodoFecha.ComboFase' ],
    controller : 'periodoFechaPanelFiltrosController',

    layout : 'anchor',
    items : [
    {
        xtype : 'periodoFechaComboFase'
    } ],

    listeners :
    {
        faseSelected : 'onFaseSelected'
    }
});
