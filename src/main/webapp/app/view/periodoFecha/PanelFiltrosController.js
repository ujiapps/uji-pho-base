Ext.define('pho.view.periodoFecha.PanelFiltrosController',
{
    extend : 'Ext.app.ViewController',
    alias : 'controller.periodoFechaPanelFiltrosController',

    onFaseSelected : function(faseId)
    {
        if (faseId == null)
        {
            return;
        }

        var panel = this.getView();
        var store = this.getStore('periodoFechasStore');

        panel.setLoading(true);
        store.setUrl(faseId);

        store.load(
        {
            callback : function(records)
            {
                panel.setLoading(false);
            }
        });
    }
});
