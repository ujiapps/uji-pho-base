Ext.define('pho.view.alumno.ViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.alumnoViewModel',
        requires: ['pho.model.Curso', 'pho.model.Alumno'],
        stores:
            {
                fasesStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'pho.model.Fase',
                        url: '/pho/rest/fases',
                        sorters: ['nombre'],
                        autoLoad: true
                    }),

                matriculasStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'pho.model.Matricula',
                        url: '/pho/rest/matriculas',
                        sorters: ['nombrePersona'],
                        autoLoad: true
                    })
            }
    });
