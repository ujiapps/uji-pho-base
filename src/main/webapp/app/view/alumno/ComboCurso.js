Ext.define('pho.view.alumno.ComboCurso',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.alumnoComboCurso',
    fieldLabel : 'Curs',
    requires: [ 'pho.model.Curso' ],
    showClearIcon : true,
    emptyText : 'Sel·lecciona un curs',
    labelWidth : 70,
    width: 280,
    padding : 5,
    displayField: 'id',

    bind: {
        store : '{cursosStore}'
    },

    listeners :
    {
        change : function(combo, cursoId)
        {
            combo.up('alumnoMain').fireEvent('cursoSelected', cursoId);
        }
    }
});