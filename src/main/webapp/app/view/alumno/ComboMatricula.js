Ext.define('pho.view.alumno.ComboMatricula',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.comboMatricula',
    fieldLabel : 'Alumne',
    showClearIcon : true,
    emptyText : 'Sel·lecciona un alumne',
    labelWidth : 70,
    width : 450,
    padding : 5,

    displayField: 'nombrePersona',
    valueField: 'personaId',

    bind: {
        store: '{matriculasStore}'
    },


    listeners :
    {
        change : function(combo, matriculaId)
        {
            combo.up('alumnoMain').fireEvent('matriculaSelected', matriculaId);
        }
    }
});