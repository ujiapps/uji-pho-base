Ext.define('pho.view.alumno.Main',
    {
        extend: 'Ext.panel.Panel',

        alias: 'widget.alumnoMain',
        requires: ['pho.view.alumno.ComboCurso', 'pho.view.alumno.ComboFase', 'pho.view.alumno.ComboMatricula', 'pho.view.alumno.ViewModel', 'pho.view.alumno.MainController'],
        title: 'Previsualització de taulers',
        controller: 'alumnoMainController',

        viewModel:
            {
                type: 'alumnoViewModel'
            },
        padding: 10,

        layout:
            {
                type: 'vbox',
            },

        items: [
            {
                xtype: 'alumnoComboFase',
                reference: 'fase'
            },
            {
                xtype: 'comboMatricula',
                reference: 'alumno',
            },
            {
                xtype: 'panel',
                layout: 'hbox',
                border: 0,
                items: [

                    {
                        xtype: 'button',
                        iconCls: 'fa fa-table',
                        margin: '0 0 0 80',
                        text: 'Simular tauler matrícula',
                        handler: 'onSimularTablero',
                        bind:
                            {
                                disabled: '{!alumno.selection}'
                            }
                    },
                    {
                        xtype: 'button',
                        iconCls: 'fa fa-table',
                        margin: '0 0 0 10',
                        text: 'Gestió d\'exclusions',
                        handler: 'onGestionExclusiones',
                        bind:
                            {
                                disabled: '{!alumno.selection}'
                            }
                    }]
            }],
        listeners: {
            alumnoSelected: 'alumnoSelected'
        }
    });
