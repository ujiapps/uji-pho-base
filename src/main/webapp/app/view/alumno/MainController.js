Ext.define('pho.view.alumno.MainController',
    {
        extend: 'Ext.app.ViewController',
        alias: 'controller.alumnoMainController',

        onFaseSelected: function (faseId) {
            var vm = this.getViewModel();
            var panel = this.getView();

            if (faseId === null) return;

            var matriculasStore = vm.getStore('matriculasStore');
            matriculasStore.load();
        },

        onGestionExclusiones: function () {
            var view = this.getView();
            var alumnoId = view.down('comboMatricula').getValue();
            var comboFase = view.down('alumnoComboFase');

            var faseId = comboFase.getValue()

            if (faseId) {
                return window.open('/pho/rest/publicacion/tablero/admin?personaId=' + alumnoId + '&faseId=' + faseId);
            }

            window.open('/pho/rest/publicacion/tablero/admin?personaId=' + alumnoId);
        },

        onSimularTablero: function () {
            var view = this.getView();
            var alumnoId = view.down('comboMatricula').getValue();
            var comboFase = view.down('alumnoComboFase');

            var faseId = comboFase.getValue()

            if (faseId) {
                return window.open('/pho/rest/publicacion/tablero?personaId=' + alumnoId + '&faseId=' + faseId);
            }

            window.open('/pho/rest/publicacion/tablero?personaId=' + alumnoId);
        }

    });
