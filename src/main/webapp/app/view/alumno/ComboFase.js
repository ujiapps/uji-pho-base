Ext.define('pho.view.alumno.ComboFase',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.alumnoComboFase',
    reference: 'alumnoComboFase',
    fieldLabel : 'Fase',
    showClearIcon : true,
    emptyText: 'Sel·lecciona una fase',
    labelWidth : 70,
    width : 450,
    padding : 5,

    bind :
    {
        store : '{fasesStore}'
    },

    listeners :
    {
        change : function(combo, faseId)
        {
            combo.up('alumnoMain').fireEvent('faseSelected', faseId);
        }
    }
});