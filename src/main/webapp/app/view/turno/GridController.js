Ext.define('pho.view.turno.GridController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.turnoGridController',

    onLoad: function() {
        var vm = this.getViewModel();
        var store = vm.get('turnosStore')

        store.reload();
    },

    onBuscarAlumno : function(field, searchString)
    {
        var vm = this.getViewModel();
        var store = vm.get('turnosStore')

        if (!searchString)
        {
            store.clearFilter();
            return;
        }

        var filter = new Ext.util.Filter(
        {
            id : 'nombrePersona',
            property : 'nombrePersona',
            value : searchString,
            anyMatch : true
        });

        store.addFilter(filter);

    },

    onChangeDate : function()
    {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        var panel = this.getView().up('panel');

        if (!record)
        {
            return;
        }

        var fecha = record.get('fecha');
        var minutos = (fecha.getMinutes() < 10 ? '0' : '') + fecha.getMinutes()

        this.modal = panel.add(
        {
            xtype : 'formFechaTurno',
            viewModel :
            {
                data :
                {
                    title : 'Canviar la data del torn ' + record.get('faseId') + ' de ' + record.get('_persona').nombre,
                    store : grid.getStore(),
                    record : record,
                    fecha : fecha,
                    hora : fecha,
                    minutos : minutos
                }
            }
        });

        this.modal.show();

    }
});
