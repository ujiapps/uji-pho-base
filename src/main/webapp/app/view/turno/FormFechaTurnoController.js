Ext.define('pho.view.turno.FormFechaTurnoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.formFechaTurnoController',

    onClose: function () {
        var win = Ext.WindowManager.getActive();
        if (win) {
            win.destroy();
        }
    },

    onSaveRecord: function() {
        var vm = this.getViewModel(),
            view = this.getView(),
            form = Ext.ComponentQuery.query('form[name=fechaTurno]')[0];

        var record = vm.get('record');
        var store = vm.get('store');

        if (form.isValid()) {
            view.setLoading(true);
            var data = form.getValues();

            var fecha = Ext.Date.parseDate(data.fecha + ' ' + data.hora + ':' + data.minutos, 'd/m/Y H:i');
            record.set('fecha', fecha);
            store.sync({
                success: function () {
                    store.reload();
                    this.onClose();
                },
                failure: function () {
                    view.setLoading(false);
                },
                scope: this
            })
        }

    }
});