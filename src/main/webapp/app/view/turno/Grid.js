Ext.define('pho.view.turno.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.turnoGrid',
    requires : [ 'pho.view.turno.GridController' ],
    controller : 'turnoGridController',
    allowEdit : false,
    bind :
    {
        store : '{turnosStore}'
    },
    title : 'Turno',
    tbar : [
    {
        xtype : 'textfield',
        emptyText : 'Buscar alumne',
        name : 'buscarAlumne',
        flex : 1,
        listeners :
        {
            change : 'onBuscarAlumno'
        }
    }, '->',
    {
        xtype : 'button',
        iconCls : 'fa fa-calendar',
        text : 'Canviar data',
        handler : 'onChangeDate'
    } ],

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        header : 'Alumne',
        dataIndex : 'nombrePersona',
        editable : false,
        allowBlank : false,
        flex : 1
    },
    {
        header : 'Fase',
        dataIndex : 'nombreFase',
        flex : 1
    },
    {
        dataIndex : 'fecha',
        text : 'Data',
        xtype : 'datecolumn',
        align : 'center',
        width : 150,
        renderer : Ext.util.Format.dateRenderer('d/m/Y H:i')
    } ],
    listeners :
    {
        activate : 'onLoad',
        itemdblclick : 'onChangeDate'
    }
});