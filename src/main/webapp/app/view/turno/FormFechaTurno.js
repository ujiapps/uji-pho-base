Ext.define('pho.view.turno.FormFechaTurno',
{
    extend : 'Ext.window.Window',
    xtype : 'formFechaTurno',

    requires : [ 'pho.view.turno.FormFechaTurnoController' ],
    controller : 'formFechaTurnoController',

    bind :
    {
        title : '{title}'
    },
    width : 320,
    height : 270,
    modal : true,
    bodyPadding : 10,
    layout : 'fit',
    bbar : [ '->',
    {
        xtype : 'button',
        text : 'Guardar',
        handler : 'onSaveRecord'
    },
    {
        xtype : 'panel',
        html : '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
        listeners :
        {
            render : function(component)
            {
                component.getEl().on('click', 'onClose');
            }
        }
    } ],

    items : [
    {
        xtype : 'form',
        name : 'fechaTurno',
        border : 0,
        layout : 'anchor',
        items : [
        {
            xtype : 'fieldset',
            title : 'Data i hora',
            defaultType : 'textfield',
            defaults :
            {
                anchor : '100%'
            },

            items : [
            {
                allowBlank : false,
                xtype : 'datefield',
                name : 'fecha',
                emptyText : 'data',
                minValue: '{fecha}',
                format : 'd/m/Y',
                altFormats : 'd/m/Y H:i:s',
                bind : '{fecha}',
                flex : 1,
                padding : '0 10 0 0'
            },
            {
                xtype : 'fieldcontainer',
                layout : 'hbox',
                items : [
                {
                    allowBlank : false,
                    xtype : 'timefield',
                    name : 'hora',
                    emptyText : 'hora',
                    format : 'H',
                    increment: 60,
                    flex: 1,
                    altFormats : 'd/m/Y H:i:s',
                    bind : '{hora}',
                    padding : '0 10 0 0'
                },
                {
                    allowBlank : false,
                    xtype : 'combo',
                    name: 'minutos',
                    emptyText : 'minuts',
                    flex: 1,
                    store: [
                        '00', '01', '02', '03', '04', '05', '06', '07', '08', '09',
                        '10', '11', '12', '13', '14', '15', '16', '17', '18', '19',
                        '20', '21', '22', '23', '24', '25', '26', '27', '28', '29',
                        '30', '31', '32', '33', '34', '35', '36', '37', '38', '39',
                        '40', '41', '42', '43', '44', '45', '46', '47', '48', '49',
                        '50', '51', '52', '53', '54', '55', '56', '57', '58', '59'
                    ],
                    bind : '{minutos}',
                    padding : '0 10 0 0'
                } ]
            } ]
        } ]
    } ]
})
