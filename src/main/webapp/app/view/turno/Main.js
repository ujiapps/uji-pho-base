Ext.define('pho.view.turno.Main',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.turnoMain',
    title : 'Gestió: Turno',
    requires : [ 'pho.view.turno.ViewModel' , 'pho.view.turno.Grid', 'pho.view.turno.FormFechaTurno'],
    layout : 'fit',
    padding : 10,

    viewModel :'turnoViewModel',

    items : [
    {
        xtype : 'turnoGrid'
    } ]
});
