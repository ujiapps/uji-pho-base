Ext.define('pho.view.turno.ViewModel',
{
    extend : 'Ext.app.ViewModel',
    alias : 'viewmodel.turnoViewModel',
    requires : [ 'pho.model.Turno' ],
    stores :
    {
        turnosStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Turno',
            url : '/pho/rest/turnos',
            sorters : [ 'nombreFase', 'fecha' ]
        })
    }
});
