Ext.define('pho.view.oferta.ComboFase',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.ofertaComboFase',
    reference : 'ofertaComboFase',
    fieldLabel : 'Fase',
    showClearIcon : true,
    emptyText : 'Sel·lecciona una fase',
    labelWidth : 70,
    width : 450,
    padding : 5,
    bind :
    {
        store : '{fasesStore}'
    }
});