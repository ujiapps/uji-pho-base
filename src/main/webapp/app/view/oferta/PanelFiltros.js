Ext.define('pho.view.oferta.PanelFiltros',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.ofertaPanelFiltros',
    border : 0,
    padding : 10,
    requires : [ 'pho.view.oferta.PanelFiltrosController', 'pho.view.oferta.ComboFase' ],
    controller : 'ofertaPanelFiltrosController',

    layout : 'hbox',
    items : [
    {
        xtype : 'ofertaComboFase'
    },
    {
        xtype : 'button',
        margin: 5,
        text : 'Generar oferta',
        handler : 'onGenerarOferta',
        bind :
        {
            disabled : '{!faseSelected}'
        }
    } ],

    listeners :
    {
        faseSelected : 'onFaseSelected'
    }
});
