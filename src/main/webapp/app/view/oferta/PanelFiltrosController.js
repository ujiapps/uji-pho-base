Ext.define('pho.view.oferta.PanelFiltrosController',
{
    extend : 'Ext.app.ViewController',
    alias : 'controller.ofertaPanelFiltrosController',

    onGenerarOferta : function()
    {
        var panel = this.getView();
        var combo = panel.down('ofertaComboFase');
        var faseId = combo.getValue();
        if (faseId == null)
        {
            return;
        }

        panel.setLoading(true);
        Ext.Ajax.request(
        {
            url : '/pho/rest/command/generar-oferta/' + faseId,
            method: 'PUT',
            success : function()
            {
                Ext.Msg.alert('Success', 'La oferta se ha generat correctament');
                panel.setLoading(false);
            },
            timeout: 90000,
            failure : function()
            {
                alert('Error al generar l\'oferta');
                panel.setLoading(false);
            },
            scope : this
        });
    }
});
