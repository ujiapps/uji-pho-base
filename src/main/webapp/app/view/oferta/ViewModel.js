Ext.define('pho.view.oferta.ViewModel',
{
    extend : 'Ext.app.ViewModel',
    alias : 'viewmodel.ofertaViewModel',
    requires : [ 'pho.model.Fase' ],
    formulas :
    {
        faseSelected : function(get)
        {
            return get('ofertaComboFase.selection');
        }
    },
    stores :
    {
        fasesStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'pho.model.Fase',
            url : '/pho/rest/fases',
            autoLoad : true,
            sorters : [
            {
                property : 'nombre',
                direction : 'ASC'
            } ]
        })
    }
});
