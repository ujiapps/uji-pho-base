Ext.define('pho.view.oferta.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.ofertaMain',
    title : 'Generació de l\'oferta',
    requires : [ 'pho.view.oferta.ViewModel', 'pho.view.oferta.PanelFiltros' ],
    layout : 'fit',

    viewModel :
    {
        type : 'ofertaViewModel'
    },

    items : [
    {
        xtype : 'ofertaPanelFiltros'
    } ]
});
