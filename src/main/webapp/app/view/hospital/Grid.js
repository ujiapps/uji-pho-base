Ext.define('pho.view.hospital.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.hospitalGrid',

    store : Ext.create('Ext.ux.uji.data.Store',
    {
        model : 'pho.model.Hospital',
        url : '/pho/rest/hospitales',
        sorters: ['orden']
    }),

    title : 'Hospitals',

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden: true
    },
    {
        text : 'Codi',
        align: 'center',
        dataIndex : 'codigo',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    },
    {
        text : 'Nom',
        dataIndex : 'nombre',
        flex : 1,
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    },
    {
        text : 'Ordre',
        dataIndex : 'orden',
        align: 'center',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    } ]
});
