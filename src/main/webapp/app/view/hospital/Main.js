Ext.define('pho.view.hospital.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.hospitalMain',
    title : 'Gestió d\'hospitals',
    requires : [ 'pho.view.hospital.Grid' ],
    layout: 'fit',
    padding: 10,

    items : [
    {
        xtype : 'hospitalGrid'
    } ]
});
