Ext.Loader.setPath('Ext.ux', '//static.uji.es/js/extjs/ext-6.2.1/packages/ux/classic/src');
Ext.Loader.setPath('Ext.ux.uji', '//static.uji.es/js/extjs/uji-commons-extjs-widgets/src/ux/uji');
Ext.Loader.setPath('pho', 'app');

Ext.require('Ext.ux.TabCloseMenu');
Ext.require('Ext.ux.form.SearchField');
Ext.require('Ext.ux.uji.ApplicationViewport');
Ext.require('Ext.ux.uji.grid.ForeignColumn');
Ext.require('Ext.ux.uji.data.Store');
Ext.require('Ext.ux.uji.data.Model');
Ext.require('Ext.ux.uji.data.identifier.None');
Ext.require('Ext.ux.uji.combo.Combo');
Ext.require('pho.view.dashboard.PanelDashboard');

Ext.ariaWarn = Ext.emptyFn;

Ext.define('Ext.ux.uji.AppConfig',
{
    alias : 'widget.AppConfig',
    singleton : true,
    appName : 'pho'
});

Ext.application(
{
    extend : 'Ext.app.Application',
    name : 'pho',
    title : 'pho',

    requires : [ 'pho.view.hospital.Main', 'pho.view.grupo.Main', 'pho.view.periodoFecha.Main', 'pho.view.alumno.Main', 'pho.view.bloque.Main', 'pho.view.asignatura.Main', 'pho.view.fase.Main', 'pho.view.turno.Main',
            'pho.view.limite.Main', 'pho.view.especialidad.Main', 'pho.view.oferta.Main', 'pho.model.Asignatura', 'pho.model.Bloque', 'pho.model.Fase', 'pho.model.FaseAsignatura', 'pho.model.Hospital', 'pho.model.Grupo',
            'pho.model.GrupoDetalle', 'pho.model.PeriodoFecha', 'pho.model.Lookup', 'pho.model.Curso', 'pho.model.Matricula', 'pho.model.Turno', 'pho.model.Limite', 'pho.model.Especialidad', 'pho.model.EspecialidadPeriodo', 'pho.model.Periodo' ],

    launch : function()
    {
        var viewport = Ext.create('Ext.ux.uji.ApplicationViewport',
        {
            codigoAplicacion : 'PHO',
            tituloAplicacion : 'Pràctiques hospitalàries',
            dashboard : true
        });
        document.getElementById('landing-loading').style.display = 'none';
    }
});
