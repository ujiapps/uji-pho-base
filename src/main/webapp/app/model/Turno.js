Ext.define('pho.model.Turno',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'personaId',
        type : 'number',
        useNull : false
    },
    {
        name : 'nombrePersona',
        mapping : '_persona.nombre'
    },
    {
        name : 'nombreFase',
        mapping : '_fase.nombre'
    },
    {
        name : 'fecha',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s',
        useNull : false
    },
    {
        name : 'faseId',
        type : 'number',
        useNull : false
    } ]
});