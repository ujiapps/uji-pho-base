Ext.define('pho.model.Asignatura',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'codigoAsignatura',
        type : 'string',
        useNull : false
    },
    {
        name : 'nombre',
        type : 'string',
        useNull : false
    },
    {
        name : 'curso',
        type : 'number',
        useNull : false
    },
    {
        name : 'semestre',
        type : 'string',
        useNull : false
    },
    {
        name : 'orden',
        type : 'number',
        useNull : false
    } ]
});
