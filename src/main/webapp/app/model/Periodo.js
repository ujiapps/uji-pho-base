Ext.define('pho.model.Periodo',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'nombre',
        type : 'string'
    },
    {
        name : 'semestre1',
        type : 'number'
    },
    {
        name : 'semestre2',
        type : 'number'
    },
    {
        name : 'seleccionado',
        type : 'boolean'
    } ]
});