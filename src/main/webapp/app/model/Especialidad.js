Ext.define('pho.model.Especialidad',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'nombre',
        type : 'string',
        useNull : false
    },
    {
        name : 'tipo',
        type : 'string',
        useNull : false
    },
    {
        name : 'bloqueId',
        type : 'number',
        useNull : false
    },
    {
        name : 'periodosObligatorios',
        type : 'number',
        useNull : false
    },
    {
        name : 'periodosOptativos',
        type : 'number',
        useNull : false
    },
    {
        name : 'semestre',
        type : 'string',
        useNull : false
    },
    {
        name : 'periodoInicialId',
        type : 'string',
        useNull : true
    },
    {
        name : 'periodoFinalId',
        type : 'string',
        useNull : true
    },
    {
        name : 'orden',
        type : 'number',
        useNull : false
    },
    {
        name : 'curso',
        type : 'number'
    },
    {
        name : 'numeroPeriodos',
        type : 'number',
        useNull : false
    },
    {
        name : 'comentarios',
        type : 'string'
    },
    {
        name : '_bloque',
        type : 'auto'
    },
    {
        name : 'nombreCurso',
        type : 'string',
        calculate : function(data)
        {
            return data.curso + ' - ' + data.nombre;
        }
    } ]
});