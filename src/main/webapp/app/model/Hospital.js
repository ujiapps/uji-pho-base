Ext.define('pho.model.Hospital',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'nombre',
        type : 'string',
        useNull : false
    },
    {
        name : 'codigo',
        type : 'string',
        useNull : false
    },
    {
        name : 'orden',
        type : 'number',
        useNull : false
    } ]
});
