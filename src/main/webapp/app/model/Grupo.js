Ext.define('pho.model.Grupo',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'nombre',
        type : 'string',
        useNull : false
    },
    {
        name : 'faseId',
        type : 'number',
        useNull : false
    } ]
});