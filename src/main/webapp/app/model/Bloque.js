Ext.define('pho.model.Bloque',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'nombre',
        type : 'string',
        useNull : false
    },
    {
        name : 'numeroObligatorias',
        type : 'number',
        useNull : false
    },
    {
        name : 'numeroOptativas',
        type : 'number',
        useNull : false
    },
    {
        name : 'asignaturaId',
        type : 'number',
        useNull : true
    },
    {
        name : 'orden',
        type : 'number'
    },
    {
        name : 'curso',
        type : 'number'
    },
    {
        name : 'codigoAsignatura',
        mapping : '_asignatura.codigoAsignatura'
    },
    {
        type : 'auto',
        name : '_asignatura'
    } ]
});
