Ext.define('pho.model.PeriodoFecha',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'cursoId',
        type : 'number'
    },
    {
        name : 'periodoId',
        type : 'number'
    },
    {
        name : 'faseId',
        type : 'number'
    },
    {
        name : 'fechaInicio',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'fechaFin',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    } ]
});