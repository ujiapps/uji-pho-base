Ext.define('pho.model.Fase',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'cursoId',
        type : 'number',
        useNull : false
    },
    {
        name : 'nombre',
        type : 'string',
        useNull : false
    },
    {
        name : 'asignaturas',
        type : 'string',
        useNull : false
    },
    {
        name : 'asignaturasIds',
        type : 'string',
        useNull : false
    },
    {
        name : 'fechaDesde',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'fechaHasta',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'

    },
    {
        name : 'tipo',
        type : 'string',
        useNull : false
    } ]
});