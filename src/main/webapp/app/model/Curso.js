Ext.define('pho.model.Curso',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'activo',
        type : 'boolean'
    } ]
});
