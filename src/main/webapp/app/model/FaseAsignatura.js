Ext.define('pho.model.FaseAsignatura',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'faseId',
        type : 'number',
        useNull : false
    },
    {
        name : 'asignaturaId',
        type : 'number',
        useNull : false
    } ]
});