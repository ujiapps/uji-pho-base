Ext.define('pho.model.GrupoDetalle',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'grupoId',
        type : 'number',
        useNull : false
    },
    {
        name : 'periodoId',
        type : 'number',
        useNull : false
    },
    {
        name : 'especialidadId',
        type : 'number',
        useNull : false
    } ]
});