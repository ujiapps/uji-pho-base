Ext.define('pho.model.Limite',
    {
        extend: 'Ext.ux.uji.data.Model',

        fields: [
            {
                name: 'id',
                type: 'number'
            },
            {
                name: 'cursoId',
                type: 'number',
                useNull: false
            },
            {
                name: 'hospitalId',
                type: 'number',
                useNull: false
            },
            {
                name: 'especialidadId',
                type: 'number',
                useNull: false
            },
            {
                name: 'plazas',
                type: 'number',
                useNull: false
            },
            {
                name: 'comentarios',
                type: 'string'
            },
            {
                name: 'nombreEspecialidad',
                mapping: '_especialidad.nombre'
            },
            {
                name: 'nombreHospital',
                mapping: '_hospital.nombre'
            },
            {
                name: '_especialidad',
                type: 'auto'
            },
            {
                name: 'curso',
                mapping: '_especialidad.curso'
            },
            {
                name: '_hospital',
                type: 'auto'
            }]
    });