Ext.define('pho.model.EspecialidadPeriodo',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'periodoId',
        type : 'number',
        useNull : false
    } ]
});