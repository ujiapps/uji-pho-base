Ext.define('pho.model.Matricula',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'cursoId',
        type : 'number'
    },
    {
        name : 'personaId',
        type : 'number'
    },
    {
        name : 'nombrePersona',
        mapping : '_persona.nombre'
    } ]
});
