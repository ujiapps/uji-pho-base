Ext.define('pho.model.Lookup',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [ 'id', 'nombre' ]
});
