Ext.define('pho.store.Limites',
{
    extend : 'Ext.data.Store',
    alias : 'store.limite',
    model : 'pho.model.Limites',

    proxy :
    {
        type : 'rest',
        url : '/pho/rest/limites',
        reader :
        {
            type : 'json',
            rootProperty : 'data'
        },
        writer :
        {
            type : 'json',
            writeAllFields : true
        }
    }
});
