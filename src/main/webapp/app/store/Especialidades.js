Ext.define('pho.store.Especialidades',
{
    extend : 'Ext.data.Store',
    alias : 'store.especialidad',
    model : 'pho.model.Especialidades',

    proxy :
    {
        type : 'rest',
        url : '/pho/rest/especialidades',
        reader :
        {
            type : 'json',
            rootProperty : 'data'
        },
        writer :
        {
            type : 'json',
            writeAllFields : true
        }
    }
});
