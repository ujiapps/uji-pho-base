Ext.define('pho.store.TiposAsignatura',
{
    extend : 'Ext.data.Store',
    alias : 'store.tiposAsignatura',
    model : 'pho.model.Lookup',

    data :
    {
        tipos : [
        {
            id : 'OB',
            nombre : 'Obligatoria'
        },
        {
            id : 'OP',
            nombre : 'Optativa',
            color : 'pendiente'
        } ]
    },

    proxy :
    {
        type : 'memory',
        reader :
        {
            type : 'json',
            rootProperty : 'tipos'
        }
    }
});