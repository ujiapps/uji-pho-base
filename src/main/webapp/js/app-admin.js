$.extend({
    getUrlVars: function () {
        var vars = [],
            hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function (name) {
        return $.getUrlVars()[name];
    }
});

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function getData() {
    return $.ajax({
        type: 'get',
        url: '/pho/rest/tablero/admin',
        data: {
            personaId: $.getUrlVar('personaId'),
            faseId: $.getUrlVar('faseId')
        },
        cache: false
    });
}

function getTemplates() {
    var tmpl = '/pho/templates/tablero.tmpl.html';
    return $.get(tmpl);
}

function fetchData() {
    var df = $.Deferred();
    $.when(getTemplates()).then(function (tmpl) {
        $.templates('master', tmpl);
        $.when(getData()).done(function (response) {
            df.resolve(processData(response));
        }).fail(function (response) {
            df.reject(response.responseJSON.message);
        });
    });
    return df;
}

function bindTooltips() {
    $('.tooltip').each(function (tooltip) {
        var content = $(this).data('info');

        if ($(this).data('warning')) {
            content += '<br />' + '<small>' + $(this).data('warning') + '</small>';
        }
        if ($(this).data('info')) {
            $(this).tooltipster({
                content: content,
                theme: 'tooltipster-light',
                contentAsHTML: true,
                trigger: 'custom',
                triggerOpen: {
                    click: true,
                    touchstart: true,
                    mouseenter: true
                },
                triggerClose: {
                    mouseleave: true,
                    touchleave: true
                }
            });
        }
    });
}

function bindHelpTooltips() {
    $('.help-tooltip').each(function (tooltip) {
        $(this).tooltipster({
            content: $('#help-content'),
            theme: 'tooltipster-light',
            contentAsHTML: true,
            trigger: 'custom',
            triggerOpen: {
                click: true,
                touchstart: true,
                mouseenter: true
            },
            triggerClose: {
                mouseleave: true,
                touchleave: true
            }
        })
    });
}

function redraw(data) {
    $("#container").html($.render.master(data));
    bindTooltips();
    bindEvents();
}

function showError(msg) {
    $("#container").html('<div class="error"><span>' + msg + '</span></div>');
}

function initAsignatura(asig) {
    return {
        id: asig.asignaturaId,
        nombre: asig.asignaturaNombre,
        orden: asig.ordenAsignatura,
        codigo: asig.codigoAsignatura,
        bloques: {}
    };
}

function initBloque(asig) {
    return {
        id: asig.bloqueId,
        nombre: asig.bloqueNombre,
        orden: asig.ordenBloque,
        especialidades: {}
    };
}

function initEspecialidad(asig) {
    return {
        id: asig.especialidadId,
        nombre: asig.especialidadNombre,
        orden: asig.ordenEspecialidad,
        hospitales: {}
    };
}

function initHospital(asig) {
    return {
        id: asig.hospitalId,
        nombre: asig.hospitalNombre,
        plazas: asig.plazas,
        ofertas: []
    };
}

function ordena(a, b) {
    return a.orden - b.orden;
}

function ordenaEspecialidades(especialidades) {
    var lista = Object.keys(especialidades).map(function (especialidad) {
        return especialidades[especialidad];
    });

    lista.sort(ordena);
    return especialidades;
}

function ordenaBloques(bloques) {
    var lista = Object.keys(bloques).map(function (bloque) {
        bloques[bloque].especialidades = ordenaEspecialidades(bloques[bloque].especialidades);
        return bloques[bloque];
    });

    lista.sort(ordena);
    return bloques;
}

function ordenaAsignaturas(asignaturas) {
    var lista = Object.keys(asignaturas).map(function (asig) {
        asignaturas[asig].bloques = ordenaBloques(asignaturas[asig].bloques);
        return asignaturas[asig];
    });

    lista.sort(ordena);
    return lista;
}

function processData(response) {
    var asignaturas = {}
    response.data.map(function (asig) {
        if (!asignaturas.hasOwnProperty(asig.asignaturaId)) {
            asignaturas[asig.asignaturaId] = initAsignatura(asig);
        }

        var bloques = asignaturas[asig.asignaturaId].bloques;
        if (!bloques.hasOwnProperty(asig.bloqueId)) {
            bloques[asig.bloqueId] = initBloque(asig);
        }

        var especialidades = bloques[asig.bloqueId].especialidades;
        if (!especialidades.hasOwnProperty(asig.especialidadId)) {
            especialidades[asig.especialidadId] = initEspecialidad(asig);
        }

        var hospitales = especialidades[asig.especialidadId].hospitales;
        if (!hospitales.hasOwnProperty(asig.hospitalId)) {
            hospitales[asig.hospitalId] = initHospital(asig);
        }

        if (parseInt(asig.plazas) > parseInt(hospitales[asig.hospitalId].plazas)) {
            hospitales[asig.hospitalId].plazas = asig.plazas;
        }

        var ofertas = hospitales[asig.hospitalId].ofertas;

        ofertas.push({
            id: asig.id,
            unicoId: asig.unicoId,
            nombre: asig.ofertaNombre,
            titulo: asig.ofertaTooltip + ' (' + asig.plazas + ' places)',
            estado: asig.estado,
            periodoInicial: asig.primero,
            periodoFinal: asig.ultimo,
            faseNombre: asig.faseNombre,
            plazas: asig.plazas
        });
    });

    asignaturas = ordenaAsignaturas(asignaturas);

    return {
        asignaturas: asignaturas
    };
}

function showLoading() {
    var over = '<div id="overlay">' + '<img id="loading" src="/pho/img/spinner3.gif">' + '</div>';
    $(over).appendTo('body');
}

function hideLoading() {
    $('#overlay').remove();
}

function loadDataAndRedraw() {
    showLoading();
    $.when(fetchData()).then(function (data) {
        redraw(data);
        hideLoading();
    }).fail(function (data) {
        showError(data);
        hideLoading();
    });
}

var unicosId = [];

function bindEvents() {
    $('.periodo.seleccionado, .periodo.libre').on('mouseover', function () {
        $(this).css('opacity', '0.7');
        var id = $(this).data('id');
        $('.periodo.seleccionado, .periodo.libre').each(function () {
            if ($(this).data('id') === id) {
                $(this).css('opacity', '0.7');
            }
        })
    });

    $('.periodo.seleccionado, .periodo.libre').on('mouseout', function () {
        $(this).css('opacity', '1');
        var id = $(this).data('id');
        $('.periodo.seleccionado, .periodo.libre').each(function () {
            if ($(this).data('id') === id) {
                $(this).css('opacity', '1');
            }
        })
    });
}

function getQueryEliminarOferta(unicosId) {
    var sql = "";
    unicosId.forEach(function (valor) {
        sql += "INSERT INTO PHO_OFERTA_ELIMINADA (UNICO_ID) VALUES  ('" + valor + "');\n";
    });
    return sql;
}

$(document).ready(function () {
    $('.refrescar-panel').on('click', function (e) {
        e.preventDefault();
        loadDataAndRedraw();
        unicosIds = [];
    });

    $('body').on('click', '.reset-oferta-eliminada', function (e) {
        e.preventDefault();
        var faseId = getUrlParameter('faseId');
        if (confirm('Aquesta acció resetejarà l\'oferta i generarà de nou el tabler. Esteu segur?')) {
            showLoading();
            $.ajax({
                type: 'DELETE',
                url: '/pho/rest/ofertaeliminada/reset/' + faseId,
                contentType: 'application/json',
                cache: false
            }).done(function (response) {
                hideLoading();
                alert("Oferta eliminada correctament");
                loadDataAndRedraw();
                unicosIds = [];
            }).error(function (response) {
                hideLoading();
                alert("Ha hagut un error");
            });
        }
    });

    $('.capturar-tablero').on('click', function (e) {
        html2canvas(document.getElementById("container")).then(function (canvas) {
            var link = document.createElement("a");
            var url = canvas.toDataURL('png').replace(/^data:image\/[^;]/, 'data:application/octet-stream');
            link.href = url;
            link.download = "screenshot.png";
            document.body.append(link);
            link.click();
            document.body.removeChild(link);
        });
    });

    $('body').on('click', '.guardar-oferta-eliminada', function (e) {
        e.preventDefault();
        var faseId = getUrlParameter('faseId');
        var preservarNumeroPlazas = $('#p_preservar_plazas').val();
        if (preservarNumeroPlazas == 0 || confirm('Vas a deixar ' + preservarNumeroPlazas +  ' places en els períodes seleccionats. Vols continuar?')) {
            $.ajax({
                type: 'POST',
                url: '/pho/rest/ofertaeliminada/' + faseId,
                data: JSON.stringify({
                    unicosIds: unicosId,
                    preservarNumeroPlazas: preservarNumeroPlazas
                }),
                contentType: 'application/json',
                cache: false
            }).done(function (response) {
                alert("Dades guardades");
                loadDataAndRedraw();
                unicosIds = [];
            }).error(function (response) {
                alert("Ha hagut un error");
            });
        }
    });

    $('body').on('click', '.generar-oferta', function (e) {
        e.preventDefault();
        var faseId = getUrlParameter('faseId');
        $.ajax({
            type: 'PUT',
            url: '/pho/rest/command/generar-oferta/' + faseId,
            contentType: 'application/json',
            cache: false
        }).done(function (response) {
            alert("Oferta generada corrrectament");
            loadDataAndRedraw();
            unicosIds = [];
        }).error(function (response) {
            alert("Ha hagut un error");
        });
    });

    $('body').on('click', '.toggle-plazas', function (e) {
        e.preventDefault();
        $(".plazas-hospital").toggleClass("hidden");
        $(".plazas-periodo").toggleClass("hidden");
    });

    $('body').on('click', '.periodo.libre', function () {
        var unicoId = $(this).data('id');
        var index = unicosId.indexOf(unicoId);
        if (index == -1) {
            unicosId.push(unicoId);
            $("[data-id='" + unicoId + "']").removeClass("libre").addClass("deshabilitado");
            console.log("DELETE FROM PHO_OFERTA WHERE UNICO_ID IN ('" + unicosId.join('\',\'') + "');");
            console.log("--------\n" + getQueryEliminarOferta(unicosId) + "--------\n");
        }
    });

    $('body').on('click', '.periodo.deshabilitado', function () {
        var unicoId = $(this).data('id');
        var index = unicosId.indexOf(unicoId);
        if (index > -1) {
            unicosId.splice(index, 1);
            $("[data-id='" + unicoId + "']").removeClass("deshabilitado").addClass("libre");
            console.log("DELETE FROM PHO_OFERTA WHERE UNICO_ID IN ('" + unicosId.join('\',\'') + "');");
            console.log("--------\n" + getQueryEliminarOferta(unicosId) + "--------\n");
        }
    });

    loadDataAndRedraw();
    bindHelpTooltips();
});