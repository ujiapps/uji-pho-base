$.extend({
    getUrlVars: function () {
        var vars = [],
            hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function (name) {
        return $.getUrlVars()[name];
    }
});

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function getData() {
    return $.ajax({
        type: 'get',
        url: '/pho/rest/tablero',
        data: {personaId: getUrlParameter("personaId"), faseId: getUrlParameter("faseId")},
        cache: false
    });
}

function getTemplates() {
    var tmpl = '/pho/templates/tablero.tmpl.html';
    return $.get(tmpl);
}

function fetchData() {
    var df = $.Deferred();
    $.when(getTemplates()).then(function (tmpl) {
        $.templates('master', tmpl);
        $.when(getData()).done(function (response) {
            df.resolve(processData(response));
        }).fail(function (response) {
            df.reject(response.responseJSON.message);
        });
    });
    return df;
}

function bindTooltips() {
    $('.tooltip').each(function (tooltip) {
        var content = $(this).data('info');

        if ($(this).data('warning')) {
            content += '<br />' + '<small>' + $(this).data('warning') + '</small>';
        }
        if ($(this).data('info')) {
            $(this).tooltipster(
                {
                    content: content,
                    theme: 'tooltipster-light',
                    contentAsHTML: true,
                    trigger: 'custom',
                    triggerOpen: {
                        click: true,
                        touchstart: true,
                        mouseenter: true
                    },
                    triggerClose: {
                        mouseleave: true,
                        touchleave: true
                    }
                });
        }
    });
}

function bindHelpTooltips() {
    $('.help-tooltip').each(function (tooltip) {
        $(this).tooltipster(
            {
                content: $('#help-content'),
                theme: 'tooltipster-light',
                contentAsHTML: true,
                trigger: 'custom',
                triggerOpen: {
                    click: true,
                    touchstart: true,
                    mouseenter: true
                },
                triggerClose: {
                    mouseleave: true,
                    touchleave: true
                }
            }
        )
    });
}

function redraw(data) {
    $("#container").html($.render.master(data));
    bindTooltips();
    bindEvents();
}

function showError(msg) {
    $("#container").html('<div class="error"><span>' + msg + '</span></div>');
}

function initAsignatura(asig) {
    return {
        id: asig.asignaturaId,
        nombre: asig.asignaturaNombre,
        orden: asig.ordenAsignatura,
        codigo: asig.codigoAsignatura,
        bloques: {}
    };
}

function initBloque(asig) {
    return {
        id: asig.bloqueId,
        nombre: asig.bloqueNombre,
        orden: asig.ordenBloque,
        especialidades: {}
    };
}

function initEspecialidad(asig) {
    return {
        id: asig.especialidadId,
        nombre: asig.especialidadNombre,
        orden: asig.ordenEspecialidad,
        comentariosEspecialidad: asig.comentariosEspecialidad,
        hospitales: {}
    };
}

function initHospital(asig) {
    return {
        id: asig.hospitalId,
        nombre: asig.hospitalNombre,
        ofertas: []
    };
}

function ordena(a, b) {
    return a.orden - b.orden;
}

function ordenaEspecialidades(especialidades) {
    var lista = Object.keys(especialidades).map(function (especialidad) {
        return especialidades[especialidad];
    });

    lista.sort(ordena);
    return especialidades;
}

function ordenaBloques(bloques) {
    var lista = Object.keys(bloques).map(function (bloque) {
        bloques[bloque].especialidades = ordenaEspecialidades(bloques[bloque].especialidades);
        return bloques[bloque];
    });

    lista.sort(ordena);
    return bloques;
}

function ordenaAsignaturas(asignaturas) {
    var lista = Object.keys(asignaturas).map(function (asig) {
        asignaturas[asig].bloques = ordenaBloques(asignaturas[asig].bloques);
        return asignaturas[asig];
    });

    lista.sort(ordena);
    return lista;
}

function processData(response) {
    var asignaturas = {}
    response.data.map(function (asig) {
        if (!asignaturas.hasOwnProperty(asig.asignaturaId)) {
            asignaturas[asig.asignaturaId] = initAsignatura(asig);
        }

        var bloques = asignaturas[asig.asignaturaId].bloques;
        if (!bloques.hasOwnProperty(asig.bloqueId)) {
            bloques[asig.bloqueId] = initBloque(asig);
        }

        var especialidades = bloques[asig.bloqueId].especialidades;
        if (!especialidades.hasOwnProperty(asig.especialidadId)) {
            especialidades[asig.especialidadId] = initEspecialidad(asig);
        }

        var hospitales = especialidades[asig.especialidadId].hospitales;
        if (!hospitales.hasOwnProperty(asig.hospitalId)) {
            hospitales[asig.hospitalId] = initHospital(asig);
        }

        var ofertas = hospitales[asig.hospitalId].ofertas;

        ofertas.push(
            {
                id: asig.id,
                unicoId: asig.unicoId,
                nombre: asig.ofertaNombre,
                titulo: asig.ofertaTooltip,
                estado: asig.estado,
                periodoInicial: asig.primero,
                periodoFinal: asig.ultimo,
                faseNombre: asig.faseNombre,
                comentariosLimites: asig.comentariosLimites
            });
    });

    asignaturas = ordenaAsignaturas(asignaturas);

    return {
        asignaturas: asignaturas
    };
}

function showLoading() {
    var over = '<div id="overlay">' + '<img id="loading" src="/pho/img/spinner3.gif">' + '</div>';
    $(over).appendTo('body');
}

function hideLoading() {
    $('#overlay').remove();
}

function actualizaEstadoMatricula() {
    $('#notificacion').html('<img src="/pho/img/spinner_small.gif" />');
    $.ajax(
        {
            type: 'get',
            url: '/pho/rest/estadomatricula',
            contentType: 'application/json',
            data: {faseId: getUrlParameter("faseId"), personaId: getUrlParameter("personaId")},
            cache: false
        }).done(function (response) {
        if (!response.data.total) {
            $('#notificacion').removeClass().addClass('warning').html('Sense informació d\'estat disponible');
        } else if (response.data.seleccionados == response.data.total) {
            $('#notificacion').removeClass().addClass('ok').html('Matrícula completada!');
        } else {
            $('#notificacion').removeClass().addClass('warning').html('Seleccionades: ' + response.data.seleccionados + ' / ' + response.data.total);
        }
    }).error(function (response) {
        $('#notificacion').removeClass().addClass('warning').html('Sense informació d\'estat disponible');
    });
}

function loadDataAndRedraw() {
    showLoading();
    $.when(fetchData()).then(function (data) {
        redraw(data);
        hideLoading();
    }).fail(function (data) {
        showError(data);
        hideLoading();
    });
}

function toggle(id, seleccionado) {
    $.ajax(
        {
            type: 'put',
            url: '/pho/rest/oferta/' + id,
            data: JSON.stringify({
                seleccionado: seleccionado,
                alumnoId: getUrlParameter('personaId'),
                faseId: getUrlParameter("faseId")
            }),
            contentType: 'application/json',
            cache: false
        }).done(function (response) {
        redraw(processData(response));
        actualizaEstadoMatricula();
        hideLoading();
    }).error(function (response) {
        alert(response.responseJSON.message);
        hideLoading();
        loadDataAndRedraw();
    });
}

function bindEvents() {
    $('.periodo.libre, .periodo.seleccionado').on('click', function () {
        showLoading();
        var el = $(this);
        var seleccionado = el.hasClass('seleccionado');
        if (el.data('comentariosLimites') && !seleccionado) {
            swal({
                title: 'Atenció',
                text: el.data('comentarios-limites'),
                buttons: ['Cancel·lar', 'Acceptar']
            }).then(function (res) {
                res ? toggle(el.data('id'), seleccionado) : hideLoading();
            });
        } else {
            toggle(el.data('id'), seleccionado);
        }
    });

    $('.periodo.seleccionado, .periodo.libre').on('mouseover', function () {
        $(this).css('opacity', '0.7');
        var id = $(this).data('id');
        $('.periodo.seleccionado, .periodo.libre').each(function () {
            if ($(this).data('id') === id) {
                $(this).css('opacity', '0.7');
            }
        })
    });

    $('.periodo.seleccionado, .periodo.libre').on('mouseout', function () {
        $(this).css('opacity', '1');
        var id = $(this).data('id');
        $('.periodo.seleccionado, .periodo.libre').each(function () {
            if ($(this).data('id') === id) {
                $(this).css('opacity', '1');
            }
        })
    });
}

$(document).ready(function () {
    $('.refrescar-panel').on('click', function (e) {
        e.preventDefault();
        loadDataAndRedraw();
        actualizaEstadoMatricula();
    });

    loadDataAndRedraw();
    actualizaEstadoMatricula();
    bindHelpTooltips();
});
