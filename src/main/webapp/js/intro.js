function zeroFill(number, width)
{
    width -= number.toString().length;
    if (width > 0)
    {
        return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
    }
    return number + '';
}

function convertJSONDateString2Date(str)
{
    var ds = str;
    ds = ds.replace(/-/g, '/');
    ds = ds.replace('T', ' ');
    ds = ds.replace(/(\+[0-9]{2})(\:)([0-9]{2}$)/, ' UTC\$1\$3');
    return new Date(ds);
}

function setReloj(selectorReloj, cadenaFechaTurno, selectorBoton)
{
    if (!cadenaFechaTurno)
    {
        return;
    }

    var fechaActual = new Date();
    var fechaTurno = new Date(cadenaFechaTurno);

    if (isNaN(fechaTurno.getTime()))
    {
        fechaTurno = convertJSONDateString2Date(cadenaFechaTurno);
    }
    var segundosTotales = (fechaTurno.getTime() - fechaActual.getTime()) / 1000;

    if (segundosTotales > 24 * 60 * 60)
    {
        return;
    }

    var interval = setInterval(function()
    {
        fechaActual = new Date();
        var segundosTotales = (fechaTurno.getTime() - fechaActual.getTime()) / 1000;

        if (segundosTotales <= 0)
        {
            clearInterval(interval);
            $(selectorBoton).show();
            return;
        }

        var horas = Math.floor(segundosTotales / 3600);
        var minutos = Math.floor((segundosTotales / 60) % 60);
        var segundos = Math.floor(segundosTotales % 60);
        $(selectorReloj).html('Falten ' + zeroFill(horas, 2) + ':' + zeroFill(minutos, 2) + ':' + zeroFill(segundos, 2) + ' hores');
    }, 1000);
}

(function() {
    var horaTurno = document.getElementById("horaTurno").innerHTML;
    setReloj('#reloj', horaTurno, '#boton');
})();