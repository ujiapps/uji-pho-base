<%@page import="es.uji.commons.sso.User" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
<head>
    <title>Pràctiques hospitalàries</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="//static.uji.es/js/extjs/ext-6.2.1/build/classic/theme-triton/resources/theme-triton-all.css">
    <link rel="stylesheet" type="text/css" href="//static.uji.es/js/extjs/ext-6.2.1/build/packages/font-awesome/resources/font-awesome-all.css">
    <link rel="stylesheet" type="text/css" href="/pho/css/estilos.css">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script type="text/javascript" src="//static.uji.es/js/extjs/ext-6.2.1/build/ext-all.js"></script>
    <%
        User user = (User) session.getAttribute(User.SESSION_USER);
    %>

    <script type="text/javascript" src="app/Application.js?v=<%=new SimpleDateFormat ("yyyyMMddhhmmss").format(new Date())%>"></script>

    <script type="text/javascript">
        var login = '<%=user.getName()%>';
        var perId = <%=user.getId()%>;
    </script>
</head>
<body>
    <div id="landing-loading">
        <img src="img/gears.gif"/>
        <p>Carregant aplicació...</p>
    </div>
</body>
</html>