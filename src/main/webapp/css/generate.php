<?php

$size = 24;

for($i=1;$i<=10;$i++)
{
    $x = ($size * $i) - 8;
    echo "div.tablero div.bloque div.grid div.periodo.tam-{$i} { width: {$x}px; }\n";
}

echo "\n";

for($i=1;$i<=32;$i++)
{
    $x = ($i-1) * $size;
    echo "div.tablero div.bloque div.grid div.periodo.col-{$i} { left: {$x}px; }\n";
}
