create or replace force view UJI_PRACTICASHOSPITALARIAS.PHO_VW_ESTADO_MATRICULA(PERSONA_ID, FASE_ID, SELECCIONADOS, TOTAL) as
     select persona_id_mostrar as persona_id, fase_id, count(distinct decode(persona_id, null, null, bloque_id)) seleccionados, count(distinct bloque_id) total
       from pho_vw_oferta o, pho_fases f
      where fase_id = f.id
        and trunc(sysdate) between fecha_Desde and fecha_hasta
   group by fase_id, persona_id_mostrar;

