ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ADD (CURSO NUMBER);
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES ADD (CURSO NUMBER);

CREATE OR REPLACE package UJI_PRACTICASHOSPITALARIAS.pack_generar is
   function generar_oferta_fase(faseId in number)
      return number;

   procedure generar_oferta_fase_1(faseId in NUMBER, borrar_Datos in varchar2);

   procedure generar_oferta_fase_2;

   procedure generar_oferta_fase_3;

   procedure generar_turnos(fase_id number, fecha date, hora_inicial varchar2, incremento_turno NUMBER);

   procedure borrar_no_asignados(faseId in number);

   procedure generar_muestra_fase_1;

   CURSO_ACADEMICO   number(4) := 2017;
end;
/

CREATE OR REPLACE package body UJI_PRACTICASHOSPITALARIAS.pack_generar is
   cursor matriculados_old is
      select p.id persona_id, nota_media
      from pho_ext_personas p
      where p.id in (select persona_id
                     from PHO_EXT_ASIGNATURAS_CURSADAS ac
                     where ac.curso_academico_id = CURSO_ACADEMICO)
      order by 2 desc;

   cursor matriculados
   (
      p_fase   in number
   ) is
      select persona_id, nota, orden
      from (select persona_id,
                   count(distinct ac.asignatura_id)                num_asignaturas,
                   nota,
                   count(distinct ac.asignatura_id) * 100 + nota   orden
            from pho_ext_asignaturas_cursadas     ac
                 join pho_asignaturas a on a.codigo_asignatura = ac.asignatura_id
                 join pho_fases_asignaturas fa on fa.asignatura_id = a.id
            where     ac.curso_academico_id = CURSO_ACADEMICO
                  and fase_id = p_fase
            group by persona_id, nota)
      order by orden desc;


   procedure actualizar_grupos(p_fase in number) is
      v_primero   number;

      cursor lista(p_fase in number) is
         select o.id oferta_id, g.id grupo_id
         from pho_grupos g, pho_grupos_Detalle d, pho_oferta o
         where     g.id = d.grupo_id
               and g.fase_id = p_fase
               and g.fase_id = o.fase_id
               and d.especialidad_id = o.especialidad_id
               and o.bloque_unido = 1
               and d.periodo_id between o.periodo_inicial_id and o.periodo_final_id;
   begin
      for x in lista(p_fase) loop
         select grupo_id
         into v_primero
         from pho_oferta
         where id = x.oferta_id;

         if v_primero is null then
            update pho_oferta
            set grupo_id = x.grupo_id
            where id = x.oferta_id;
         else
            update pho_oferta
            set grupo2_id = x.grupo_id
            where id = x.oferta_id;
         end if;

         commit;
      end loop;
   end;

   procedure generar_turnos(fase_id number, fecha date, hora_inicial varchar2, incremento_turno number) is
      fecha_inicio_matricula   date;
      incremento               number;
      fase_actual_id           number;
   begin
      select id
      into fase_actual_id
      from pho_fases f
      where     f.id = fase_id
            and f.curso_academico_id = CURSO_ACADEMICO;

      delete from pho_turnos t
      where t.fase_id = fase_actual_id;

      fecha_inicio_matricula := nvl(fecha, sysdate);
      incremento := 0;

      for matricula in matriculados(fase_id) loop
         insert into pho_turnos(ID, PERSONA_ID, FECHA, FASE_ID)
            values
                   (
                      (select nvl(max(id) + 1, 1)
                       from pho_turnos),
                      matricula.persona_id,
                      to_date(to_char(fecha_inicio_matricula, 'DD/MM/YYYY') || ' ' || hora_inicial,
                      'DD/MM/YYYY HH24:MI') + (incremento / (24 * 60)),
                      fase_actual_id
                   );

         incremento := incremento + incremento_turno;
      end loop;

      update pho_fases
      set fecha_Desde = trunc(fecha_inicio_matricula), fecha_hasta = trunc(fecha_inicio_matricula) + 1
      where     id = fase_actual_id
            and curso_academico_id = CURSO_ACADEMICO;

      update pho_fases
      set fecha_Desde = trunc(fecha_inicio_matricula) + 1, fecha_hasta = trunc(fecha_inicio_matricula) + 300
      where     id = 3
            and curso_academico_id = CURSO_ACADEMICO;

      commit;
   end;

   procedure refrescar_flags(p_fase in number) is
   begin
      for matricula in matriculados(p_fase) loop
         if p_Fase = 1 then
            pack_pho_practicas.refrescar_flag(matricula.persona_id, 1);
            pack_pho_practicas.refrescar_flag(matricula.persona_id, 2);
         elsif p_fase >= 4 then
            pack_pho_practicas.refrescar_flag(matricula.persona_id, p_fase);
         end if;
      end loop;
   end;

   function generar_oferta_fase(faseId in number)
      return number is
      v_id    number;
      v_id2   number;

      cursor lista is
         select b.*,
                c.id                                            curso_academico_id,
                decode(bloque_unido, 'S', 1, 0)                 unido,
                p1                                              p_primero,
                nvl(p6, nvl(p5, nvl(p4, nvl(p3, nvl(p2, p1))))) p_ultimo
         from pho_vw_oferta_base b, pho_cursos c
         where     c.activo = 1
               and fase_id = faseId;
   begin
      if faseId = 1 then
         generar_oferta_fase_1(faseId, 'S');
      elsif faseId = 2 then
         generar_oferta_fase_2;
      elsif faseId = 3 then
         generar_oferta_fase_3;
      elsif faseId in (4, 7) then
         generar_oferta_fase_1(faseId, 'S');
      else                                    -- Mantenemos la oferta de quinto cuando se realiza la matrícula de cuarto
         generar_oferta_fase_1(faseId, 'N');
      end if;

      actualizar_grupos(faseId);
      return 1;
   end;

   procedure generar_oferta_fase_1(faseId in number, borrar_datos in varchar2) is
      v_id    number;
      v_id2   number;

      cursor lista is
         select b.*,
                c.id                                            curso_academico_id,
                decode(bloque_unido, 'S', 1, 0)                 unido,
                p1                                              p_primero,
                nvl(p6, nvl(p5, nvl(p4, nvl(p3, nvl(p2, p1))))) p_ultimo
         from pho_vw_oferta_base b, pho_cursos c
         where     c.activo = 1
               and fase_id = faseId;
   begin
      if borrar_datos = 'S' then
         delete pho_ofertas_periodos;

         delete pho_oferta;
      end if;

      refrescar_flags(faseId);

      for x in lista loop
         for p in 1 .. x.plazas loop
            v_id := hibernate_sequence.nextval;

            if x.unido = 1 then
               insert into pho_oferta(ID,
                                      CURSO_ACADEMICO_ID,
                                      ASIGNATURA_ID,
                                      BLOQUE_ID,
                                      ESPECIALIDAD_ID,
                                      FASE_ID,
                                      HOSPITAL_ID,
                                      PERIODO_INICIAL_ID,
                                      PERIODO_FINAL_ID,
                                      PERSONA_ID,
                                      FECHA,
                                      UNICO_ID,
                                      ORDEN_1,
                                      ORDEN_2,
                                      ORDEN_3,
                                      bloque_unido)
               values (v_id,
                       x.curso_Academico_id,
                       x.asignatura_id,
                       x.bloque_id,
                       x.ESPECIALIDAD_ID,
                       x.FASE_ID,
                       x.HOSPITAL_ID,
                       x.p_primero,
                       x.p_ultimo,
                       null,
                       null,
                       x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.hospital_id ||
                       '_' || x.grupo_id,
                       nvl(x.ORDEN_1, 9999),
                       nvl(x.ORDEN_2, 9999),
                       nvl(x.ORDEN_3, 9999),
                       x.unido);
            else
               insert into pho_oferta(ID,
                                      CURSO_ACADEMICO_ID,
                                      ASIGNATURA_ID,
                                      BLOQUE_ID,
                                      ESPECIALIDAD_ID,
                                      FASE_ID,
                                      HOSPITAL_ID,
                                      PERIODO_INICIAL_ID,
                                      PERIODO_FINAL_ID,
                                      PERSONA_ID,
                                      FECHA,
                                      UNICO_ID,
                                      ORDEN_1,
                                      ORDEN_2,
                                      ORDEN_3,
                                      bloque_unido)
               values (v_id,
                       x.curso_Academico_id,
                       x.asignatura_id,
                       x.bloque_id,
                       x.ESPECIALIDAD_ID,
                       x.FASE_ID,
                       x.HOSPITAL_ID,
                       x.p_primero,
                       x.p_ultimo,
                       null,
                       null,
                       x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.hospital_id ||
                       '_' || x.grupo_id,
                       nvl(x.ORDEN_1, 9999),
                       nvl(x.ORDEN_2, 9999),
                       nvl(x.ORDEN_3, 9999),
                       x.unido);

               if x.p1 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p1);
               end if;

               if x.p2 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p2);
               end if;

               if x.p3 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p3);
               end if;

               if x.p4 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p4);
               end if;

               if x.p5 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p5);
               end if;

               if x.p6 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p6);
               end if;
            end if;
         end loop;

         null;
      end loop;

      commit;
   end;

   procedure generar_oferta_fase_2 is
      v_id       number;
      v_id2      number;
      v_error    number;
      v_libres   number;

      cursor lista is
         select xxx.*, plazas - ocupacion libres, c.id curso_academico_id, decode(bloque_unido, 'S', 1, 0) unido
         from pho_Vw_oferta_base_2 xxx, pho_cursos c
         where     c.activo = 1
               and fase_id = 2                                                                --and    asignatura_id = 1
                              --and    bloque_id = 2
                              --and    especialidad_id = 3
   ;
   begin
      borrar_no_asignados(1);
      refrescar_flags(1);
      v_error := 1;

      for x in lista loop
         v_error := 11;
         v_libres := x.libres;

         for p in 1 .. v_libres loop
            --dbms_output.put_line ('P: ' || p);
            v_error := 2;
            v_id := hibernate_sequence.nextval;
            v_error := 3;

            if x.unido = 1 then
               v_error := 4;

               begin
                  insert into pho_oferta(ID,
                                         CURSO_ACADEMICO_ID,
                                         ASIGNATURA_ID,
                                         BLOQUE_ID,
                                         ESPECIALIDAD_ID,
                                         FASE_ID,
                                         HOSPITAL_ID,
                                         PERIODO_INICIAL_ID,
                                         PERIODO_FINAL_ID,
                                         PERSONA_ID,
                                         FECHA,
                                         UNICO_ID,
                                         ORDEN_1,
                                         ORDEN_2,
                                         ORDEN_3,
                                         bloque_unido)
                     values
                            (
                               v_id,
                               x.curso_Academico_id,
                               x.asignatura_id,
                               x.bloque_id,
                               x.ESPECIALIDAD_ID,
                               x.FASE_ID,
                               x.HOSPITAL_ID,
                               x.primero,
                               x.ultimo,
                               null,
                               null,
                               x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.
                               hospital_id || '_' || x.grupo_id,
                               nvl(x.ORDEN_1, 9999),
                               nvl(x.ORDEN_2, 9999),
                               nvl(x.ORDEN_3, 9999),
                               x.unido
                            );
               exception
                  when others then
                     dbms_output.put_line(v_error || ' - ' || sqlerrm || x.asignatura_id || ' ' || x.bloque_id || ' '
                                          || x.especialidad_id || ' ' || x.hospital_id || ' - ' || x.fase_id);
               end;
            else
               v_error := 5;

               insert into pho_oferta(ID,
                                      CURSO_ACADEMICO_ID,
                                      ASIGNATURA_ID,
                                      BLOQUE_ID,
                                      ESPECIALIDAD_ID,
                                      FASE_ID,
                                      HOSPITAL_ID,
                                      PERIODO_INICIAL_ID,
                                      PERIODO_FINAL_ID,
                                      PERSONA_ID,
                                      FECHA,
                                      UNICO_ID,
                                      ORDEN_1,
                                      ORDEN_2,
                                      ORDEN_3,
                                      bloque_unido)
               values (v_id,
                       x.curso_Academico_id,
                       x.asignatura_id,
                       x.bloque_id,
                       x.ESPECIALIDAD_ID,
                       x.FASE_ID,
                       x.HOSPITAL_ID,
                       x.primero,
                       x.ultimo,
                       null,
                       null,
                       x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.hospital_id ||
                       '_' || x.grupo_id,
                       nvl(x.ORDEN_1, 9999),
                       nvl(x.ORDEN_2, 9999),
                       nvl(x.ORDEN_3, 9999),
                       x.unido);

               v_error := 6;

               if x.p1 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p1);
               end if;

               if x.p2 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p2);
               end if;

               if x.p3 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p3);
               end if;

               if x.p4 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p4);
               end if;

               if x.p5 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p5);
               end if;

               if x.p6 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p6);
               end if;
            end if;
         end loop;

         null;
         commit;
      end loop;
   exception
      when others then
         dbms_output.put_line(v_error || ' - ' || sqlerrm);
   end;

   procedure generar_oferta_fase_3 is
      v_id       number;
      v_id2      number;
      v_error    number;
      v_libres   number;

      cursor lista is
         select xxx.*, plazas - ocupacion libres, c.id curso_academico_id, decode(bloque_unido, 'S', 1, 0) unido
         from pho_Vw_oferta_base_2 xxx, pho_cursos c
         where     c.activo = 1
               and fase_id = 2                                                                --and    asignatura_id = 1
                              --and    bloque_id = 2
                              --and    especialidad_id = 3
   ;
   begin
      borrar_no_asignados(2);
      refrescar_flags(1);
      v_error := 1;

      for x in lista loop
         v_error := 11;
         v_libres := x.libres;

         for p in 1 .. 30 loop
            --dbms_output.put_line ('P: ' || p);
            v_error := 2;
            v_id := hibernate_sequence.nextval;
            v_error := 3;

            if x.unido = 1 then
               v_error := 4;

               begin
                  insert into pho_oferta(ID,
                                         CURSO_ACADEMICO_ID,
                                         ASIGNATURA_ID,
                                         BLOQUE_ID,
                                         ESPECIALIDAD_ID,
                                         FASE_ID,
                                         HOSPITAL_ID,
                                         PERIODO_INICIAL_ID,
                                         PERIODO_FINAL_ID,
                                         PERSONA_ID,
                                         FECHA,
                                         UNICO_ID,
                                         ORDEN_1,
                                         ORDEN_2,
                                         ORDEN_3,
                                         bloque_unido)
                     values
                            (
                               v_id,
                               x.curso_Academico_id,
                               x.asignatura_id,
                               x.bloque_id,
                               x.ESPECIALIDAD_ID,
                               3,
                               x.HOSPITAL_ID,
                               x.primero,
                               x.ultimo,
                               null,
                               null,
                               x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.
                               hospital_id || '_' || x.grupo_id,
                               nvl(x.ORDEN_1, 9999),
                               nvl(x.ORDEN_2, 9999),
                               nvl(x.ORDEN_3, 9999),
                               x.unido
                            );
               exception
                  when others then
                     dbms_output.put_line(v_error || ' - ' || sqlerrm || x.asignatura_id || ' ' || x.bloque_id || ' '
                                          || x.especialidad_id || ' ' || x.hospital_id || ' - ' || x.fase_id);
               end;
            else
               v_error := 5;

               insert into pho_oferta(ID,
                                      CURSO_ACADEMICO_ID,
                                      ASIGNATURA_ID,
                                      BLOQUE_ID,
                                      ESPECIALIDAD_ID,
                                      FASE_ID,
                                      HOSPITAL_ID,
                                      PERIODO_INICIAL_ID,
                                      PERIODO_FINAL_ID,
                                      PERSONA_ID,
                                      FECHA,
                                      UNICO_ID,
                                      ORDEN_1,
                                      ORDEN_2,
                                      ORDEN_3,
                                      bloque_unido)
               values (v_id,
                       x.curso_Academico_id,
                       x.asignatura_id,
                       x.bloque_id,
                       x.ESPECIALIDAD_ID,
                       3,
                       x.HOSPITAL_ID,
                       x.primero,
                       x.ultimo,
                       null,
                       null,
                       x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.hospital_id ||
                       '_' || x.grupo_id,
                       nvl(x.ORDEN_1, 9999),
                       nvl(x.ORDEN_2, 9999),
                       nvl(x.ORDEN_3, 9999),
                       x.unido);

               v_error := 6;

               if x.p1 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p1);
               end if;

               if x.p2 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p2);
               end if;

               if x.p3 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p3);
               end if;

               if x.p4 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p4);
               end if;

               if x.p5 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p5);
               end if;

               if x.p6 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p6);
               end if;
            end if;
         end loop;

         null;
         commit;
      end loop;
   exception
      when others then
         dbms_output.put_line(v_error || ' - ' || sqlerrm);
   end;

   procedure borrar_no_asignados(faseId in number) is
   begin
      delete pho_ofertas_periodos
      where oferta_id in (select id
                          from pho_oferta
                          where     fase_id = faseId
                                and persona_id is null);

      delete pho_oferta
      where     fase_id = faseId
            and persona_id is null;

      commit;
   end;

   procedure generar_muestra_Fase_1 is
   begin
      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '1_1_1_1_1'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '1_2_21_1_3'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '4_5_35_5_5'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '2_3_31_2_2'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '3_4_32_1_3'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '5_6_33_1_4'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '5_7_34_8_17'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      commit;
   end;

   procedure notificar_turnos_Fase_1 is
      cursor lista is
         select distinct persona_id, busca_cuenta(persona_id) email, busca_nombre_persona(persona_id) nombre
         from pho_turnos
         where rownum <= 1;

      v_asunto   varchar2(2000);
      v_texto    varchar2(4000);
      v_para     varchar2(200);
      v_sep      varchar2(10) := '<br />';
   begin
      v_asunto := 'Turno de matrícula de las prácticas clínicas de sexto curso del grado en medicina';

      for x in lista loop
         v_para := x.email;
         v_para := 'ferrerq@uji.es';
         v_texto :=
            'Estimado/Estimada ' || x.nombre || v_Sep || v_sep ||
            'En el siguiente enlace podrás consultar tus turnos para la matrícula de las prácticas clínicas de sexto curso.'
            || v_Sep || v_Sep || '<a href="http://ujiapps.uji.es/pho">http://ujiapps.uji.es/pho</a>' || v_Sep || v_sep
            || v_sep || v_Sep || 'Atentamente' || v_Sep || v_sep || 'Universitat Jaume I';
         gri_www.euji_envios_html.mail(p_from          => 'no_reply@uji.es',
                                       p_to            => v_para,
                                       p_subject       => v_Asunto,
                                       p_texto         => v_Texto,
                                       p_permite_dup   => 'S',
                                       p_formato       => 'HTM');
      end loop;
   end;
end;
/

CREATE OR REPLACE package UJI_PRACTICASHOSPITALARIAS.pack_pho_practicas is
   procedure asignar(p_persona_id        in number,
                     p_fase_id           in number,
                     p_oferta_id         in number,
                     p_periodo_inicial   in number,
                     p_periodo_final     in number,
                     p_bloque_unido      in number);

   procedure desasignar(p_persona_id        in number,
                        p_fase_id           in number,
                        p_oferta_id         in number,
                        p_periodo_inicial   in number,
                        p_periodo_final     in number,
                        p_bloque_unido      in number);

   function periodos_disponibles(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number;

   function un_periodo_disponible(p_persona_id in number, p_fase_id in number, p_periodo in number)
      return number;

   function especialidad_disponible(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number;

   function bloque_disponible(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number;

   function grupo_seleccionado(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number;

   function pho_buscar_ultimo_cons(p_inicial   in number,
                                   p1          in number,
                                   p2          in number,
                                   p3          in number,
                                   p4          in number,
                                   p5          in number,
                                   p6          in number)
      return number;

   procedure refrescar_flag(p_persona_id in number, p_Fase_id in number);

   function plazas_ocupadas(p_asignatura     in number,
                            p_bloque         in number,
                            p_especialidad   in number,
                            p_hospital       in number,
                            p_periodo        in number)
      return number;

   function oferta_plazas(p_asignatura     in number,
                          p_bloque         in number,
                          p_especialidad   in number,
                          p_hospital       in number,
                          p_periodo        in number)
      return number;
end;
/

CREATE OR REPLACE package body UJI_PRACTICASHOSPITALARIAS.pack_pho_practicas is
   procedure asignar_periodo(p_persona_id in number, p_fase_id in number, p_periodo_id in number) is
      v_flag      varchar2(32);
      v_antes     varchar2(100);
      v_despues   varchar2(100);
      v_aux       number;
      v_curso     number;
   begin
      select curso_academico_id
      into v_curso
      from pho_fases
      where id = p_fase_id;

      begin
         select flags
         into v_flag
         from pho_matriculas
         where     persona_id = p_persona_id
               and curso_academico_id = v_curso;
      exception
         when no_Data_found then
            select nvl(max(id), 0) + 1
            into v_aux
            from pho_matriculas;

            insert into pho_matriculas(id, persona_id, curso_academico_id)
            values (v_aux, p_persona_id, v_curso);

            select flags
            into v_flag
            from pho_matriculas
            where     persona_id = p_persona_id
                  and curso_academico_id = v_curso;
      end;

      v_antes := substr(v_flag, 1, p_periodo_id - 1);
      v_despues := substr(v_flag, p_periodo_id + 1);

      update pho_matriculas
      set flags = v_antes || '1' || v_Despues
      where     persona_id = p_persona_id
            and curso_academico_id = v_curso;
   end;

   procedure desasignar_periodo(p_persona_id in number, p_fase_id in number, p_periodo_id in number) is
      v_flag      varchar2(32);
      v_antes     varchar2(100);
      v_despues   varchar2(100);
      v_aux       number;
      v_curso     number;
   begin
      select curso_academico_id
      into v_curso
      from pho_fases
      where id = p_fase_id;

      begin
         select flags
         into v_flag
         from pho_matriculas
         where     persona_id = p_persona_id
               and curso_academico_id = v_curso;
      exception
         when no_Data_found then
            select nvl(max(id), 0) + 1
            into v_aux
            from pho_matriculas;

            insert into pho_matriculas(id, persona_id, curso_academico_id)
            values (v_aux, p_persona_id, v_curso);

            select flags
            into v_flag
            from pho_matriculas
            where     persona_id = p_persona_id
                  and curso_academico_id = v_curso;
      end;

      v_antes := substr(v_flag, 1, p_periodo_id - 1);
      v_despues := substr(v_flag, p_periodo_id + 1);

      update pho_matriculas
      set flags = v_antes || '0' || v_Despues
      where     persona_id = p_persona_id
            and curso_academico_id = v_curso;
   end;

   procedure asignar(p_persona_id        in number,
                     p_fase_id           in number,
                     p_oferta_id         in number,
                     p_periodo_inicial   in number,
                     p_periodo_final     in number,
                     p_bloque_unido      in number) is
      cursor lista_periodos(p_oferta_id in number) is
         select periodo_id
         from pho_ofertas_periodos
         where oferta_id = p_oferta_id
         order by periodo_id;
   begin
      if p_bloque_unido = 1 then
         for x in p_periodo_inicial .. p_periodo_final loop
            asignar_periodo(p_persona_id, p_fase_id, x);
         end loop;
      else
         for x in lista_periodos(p_oferta_id) loop
            asignar_periodo(p_persona_id, p_fase_id, x.periodo_id);
         end loop;
      end if;
   end;

   procedure desasignar(p_persona_id        in number,
                        p_fase_id           in number,
                        p_oferta_id         in number,
                        p_periodo_inicial   in number,
                        p_periodo_final     in number,
                        p_bloque_unido      in number) is
      cursor lista_periodos(p_oferta_id in number) is
         select periodo_id
         from pho_ofertas_periodos
         where oferta_id = p_oferta_id
         order by periodo_id;
   begin
      if p_bloque_unido = 1 then
         for x in p_periodo_inicial .. p_periodo_final loop
            desasignar_periodo(p_persona_id, p_fase_id, x);
         end loop;
      else
         for x in lista_periodos(p_oferta_id) loop
            desasignar_periodo(p_persona_id, p_fase_id, x.periodo_id);
         end loop;
      end if;
   end;

   function periodos_disponibles(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number is
      v_oferta_id      number;
      v_bloque_unido   number;
      v_inicial        number;
      v_final          number;
      v_rdo            number;
      v_flag           varchar2(32);
      v_error          number;
      sin_oferta       exception;

      cursor lista_periodos(p_oferta_id in number) is
         select periodo_id
         from pho_ofertas_periodos
         where oferta_id = p_oferta_id
         order by periodo_id;
   begin
      v_error := -10;

      begin
         select distinct min(id), bloque_unido, periodo_inicial_id, periodo_final_id
         into v_oferta_id, v_bloque_unido, v_inicial, v_final
         from pho_oferta
         where     unico_id = p_unico
               and fase_id = p_fase_id
         group by bloque_unido, periodo_inicial_id, periodo_final_id, grupo_id;
      exception
         when others then
            raise sin_oferta;
      end;

      v_error := -20;

      select flags
      into v_flag
      from pho_matriculas
      where     persona_id = p_persona_id
            and curso_academico_id = (select curso_academico_id
                                      from pho_Fases
                                      where id = p_fase_id);

      v_error := -30;
      v_rdo := 0;

      if v_bloque_unido = 1 then
         for x in v_inicial .. v_final loop
            if v_rdo = 0 then
               v_rdo := substr(v_flag, x, 1);
            end if;
         end loop;
      else
         for x in lista_periodos(v_oferta_id) loop
            if v_rdo = 0 then
               v_rdo := substr(v_flag, x.periodo_id, 1);
            end if;
         end loop;
      end if;

      v_error := -40;

      if v_rdo = 1 then
         return (0);
      else
         return (1);
      end if;
   exception
      when sin_oferta then
         return (0);
      when others then
         return (v_error);
   end;


   function un_periodo_disponible(p_persona_id in number, p_fase_id in number, p_periodo in number)
      return number is
      v_flag    varchar2(32);
      v_valor   varchar2(1);
   begin
      select flags
      into v_flag
      from pho_matriculas
      where persona_id = p_persona_id;

      v_valor := substr(v_flag, p_periodo, 1);

      if v_valor = '1' then
         return (0);
      else
         return (1);
      end if;
   end;

   function especialidad_disponible(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number is
      v_especialidad   number;
      v_matriculada    number;
      v_rdo            number;
      v_numero         number;
      v_bloque         number;
   begin
      select distinct especialidad_id, bloque_id
      into v_especialidad, v_bloque
      from pho_oferta
      where unico_id = p_unico;


      select numero_obligatorias
      into v_numero
      from pho_bloques
      where id = v_bloque;

      --select decode(count(*), 0, 0, 1)
      select count(*)
      into v_matriculada
      from pho_oferta
      where     persona_id = p_persona_id
            and especialidad_id = v_especialidad;

      if v_matriculada = 0 then
         v_rdo := 1;
      else
         v_rdo := 0;
      end if;

      /*
      if v_matriculada < v_numero then
         v_rdo := 1;
      else
         v_rdo := 0;
      end if;
      */

      return (v_rdo);
   end;

   function bloque_disponible(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number is
      v_bloque      number;
      v_maximo      number;
      v_asignados   number;
      v_rdo         number;
   begin
      select distinct bloque_id
      into v_bloque
      from pho_oferta
      where unico_id = p_unico;

      select decode(f.tipo,
                    'OB', numero_obligatorias,
                    'OB/OP', decode(f.id, 1, numero_obligatorias, numero_optativas),
                    numero_optativas)
      into v_maximo
      from pho_bloques b, pho_fases f
      where     b.id = v_bloque
            and f.id = p_Fase_id;

      select count(*)
      into v_asignados
      from pho_oferta
      where     persona_id = p_persona_id
            and bloque_id = v_bloque
            and fase_id = p_Fase_id;

      if v_asignados < v_maximo then
         v_rdo := 1;
      else
         v_rdo := 0;
      end if;

      return (v_rdo);
   end;

   function grupo_seleccionado(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number is
      v_grupo   number;
   begin
      select distinct grupo_id
      into v_grupo
      from pho_oferta
      where     persona_id = p_persona_id
            and fase_id = p_fase_id
            and grupo_id is not null;

      return (v_grupo);
   exception
      when no_Data_found then
         return (-1);
   end;

   function pho_buscar_ultimo_cons(p_inicial   in number,
                                   p1          in number,
                                   p2          in number,
                                   p3          in number,
                                   p4          in number,
                                   p5          in number,
                                   p6          in number)
      return number is
      v_rdo     number;
      v_aux     number;
      v_ult     number;
      valores   euji_util.ident_arr2;
   begin
      valores(1) := p1;
      valores(2) := p2;
      valores(3) := p3;
      valores(4) := p4;
      valores(5) := p5;
      valores(6) := p6;
      v_ult := 0;

      for x in 1 .. 6 loop
         if valores(x) > 0 then
            v_ult := v_ult + 1;
         end if;
      end loop;

      for x in 1 .. v_ult loop
         if valores(x) < p_inicial then
            null;
            null;
         elsif valores(x) = p_inicial then
            v_rdo := valores(x);
         else
            if valores(x) = v_rdo + 1 then
               v_rdo := valores(x);
            end if;
         end if;
      end loop;

      if v_ult = 0 then
         v_rdo := -1;
      end if;

      return (v_rdo);
   end;

   procedure refrescar_flag(p_persona_id in number, p_Fase_id in number) is
      v_flag    varchar2(32) := '00000000000000000000000000000000';
      v_aux     number;
      v_curso   number;

      cursor lista_matricula is
         select fase_id, primero, ultimo, grupo_id
         from pho_vw_oferta
         where     persona_id = p_persona_id
               and fase_id <= p_FAse_id;

      function asignar_flag(p_flag in varchar2, p_periodo_id in number)
         return varchar2 is
         v_flag      varchar2(32);
         v_antes     varchar2(100);
         v_despues   varchar2(100);
         v_valor     varchar2(1);
      begin
         v_flag := p_flag;
         v_antes := substr(v_flag, 1, p_periodo_id - 1);
         v_despues := substr(v_flag, p_periodo_id + 1);
         v_valor := substr(v_flag, p_periodo_id, 1);
         v_valor := to_char(to_number(v_valor) + 1);

         if to_number(v_valor) > 1 then
            v_valor := 1;
         end if;

         return (v_antes || v_Valor || v_Despues);
      end;
   begin
      select curso_academico_id
      into v_curso
      from pho_fases
      where id = p_fase_id;

      select count(*)
      into v_aux
      from pho_matriculas
      where     persona_id = p_persona_id
            and curso_academico_id = v_curso;

      if v_aux = 0 then
         select nvl(max(id), 0) + 1
         into v_aux
         from pho_matriculas;

         insert into pho_matriculas(ID, PERSONA_ID, CURSO_ACADEMICO_ID)
         values (v_aux, p_persona_id, v_curso);

         commit;
      end if;

      for x in lista_matricula loop
         for p in x.primero .. x.ultimo loop
            v_flag := asignar_flag(v_flag, p);
         end loop;
      end loop;

      update pho_matriculas
      set flags = v_flag
      where     persona_id = p_persona_id
            and curso_academico_id = (select curso_academico_id
                                      from pho_fases
                                      where id = p_fase_id);

      commit;
   end;

   function plazas_ocupadas(p_asignatura     in number,
                            p_bloque         in number,
                            p_especialidad   in number,
                            p_hospital       in number,
                            p_periodo        in number)
      return number is
      v_rdo   number;
   begin
      select sum(plazas)
      into v_Rdo
      from (select asignatura_id,
                   bloque_id,
                   especialidad_id,
                   hospital_id,
                   p.id,
                   count(*)   plazas
            from pho_oferta o, pho_periodos p
            where     bloque_unido = 1
                  and persona_id is not null
                  and p.id between periodo_inicial_id and periodo_final_id
                  and asignatura_id = p_asignatura
                  and bloque_id = p_bloque
                  and especialidad_id = p_especialidad
                  and hospital_id = p_hospital
                  and p.id = p_periodo
            group by asignatura_id, bloque_id, especialidad_id, hospital_id, p.id
            union all
            select asignatura_id,
                   bloque_id,
                   especialidad_id,
                   hospital_id,
                   p.id,
                   count(*)   plazas
            from pho_oferta o, pho_ofertas_periodos op, pho_periodos p
            where     bloque_unido = 0
                  and persona_id is not null
                  and o.id = op.oferta_id
                  and op.periodo_id = p.id
                  and asignatura_id = p_asignatura
                  and bloque_id = p_bloque
                  and especialidad_id = p_especialidad
                  and hospital_id = p_hospital
                  and p.id = p_periodo
            group by asignatura_id, bloque_id, especialidad_id, hospital_id, p.id);

      return (nvl(v_rdo, 0));
   end;

   function oferta_plazas(p_asignatura     in number,
                          p_bloque         in number,
                          p_especialidad   in number,
                          p_hospital       in number,
                          p_periodo        in number)
      return number is
      v_rdo   number;
   begin
      select sum(plazas)
      into v_Rdo
      from (select asignatura_id,
                   bloque_id,
                   especialidad_id,
                   hospital_id,
                   p.id,
                   count(*)   plazas
            from pho_oferta o, pho_periodos p
            where     bloque_unido = 1
                  and p.id between periodo_inicial_id and periodo_final_id
                  and asignatura_id = p_asignatura
                  and bloque_id = p_bloque
                  and especialidad_id = p_especialidad
                  and hospital_id = p_hospital
                  and p.id = p_periodo
            group by asignatura_id, bloque_id, especialidad_id, hospital_id, p.id
            union all
            select asignatura_id,
                   bloque_id,
                   especialidad_id,
                   hospital_id,
                   p.id,
                   count(*)   plazas
            from pho_oferta o, pho_ofertas_periodos op, pho_periodos p
            where     bloque_unido = 0
                  and o.id = op.oferta_id
                  and op.periodo_id = p.id
                  and asignatura_id = p_asignatura
                  and bloque_id = p_bloque
                  and especialidad_id = p_especialidad
                  and hospital_id = p_hospital
                  and p.id = p_periodo
            group by asignatura_id, bloque_id, especialidad_id, hospital_id, p.id);

      return (nvl(v_rdo, 0));
   end;
end;
/

CREATE OR REPLACE function UJI_PRACTICASHOSPITALARIAS.pho_buscar_ultimo_cons (
   p_inicial   in   number,
   p1          in   number,
   p2          in   number,
   p3          in   number,
   p4          in   number,
   p5          in   number,
   p6          in   number
)
   return number is
   v_rdo     number;
   v_aux     number;
   v_ult     number;
   valores   euji_util.ident_arr2;
begin
   valores (1) := p1;
   valores (2) := p2;
   valores (3) := p3;
   valores (4) := p4;
   valores (5) := p5;
   valores (6) := p6;
   v_ult := 0;

   for x in 1 .. 6 loop
      if valores (x) > 0 then
         v_ult := v_ult + 1;
      end if;
   end loop;

   for x in 1 .. v_ult loop
      if valores (x) < p_inicial then
         null;
         null;
      elsif valores (x) = p_inicial then
         v_rdo := valores (x);
      else
         if valores (x) = v_rdo + 1 then
            v_rdo := valores (x);
         end if;
      end if;
   end loop;

   if v_ult = 0 then
      v_rdo := -1;
   end if;

   return (v_rdo);
end;
/

create or replace force view UJI_PRACTICASHOSPITALARIAS.PHO_EXT_ASIGNATURAS_CURSADAS
(
   ID
 , ESTUDIO_ID
 , CURSO_ACADEMICO_ID
 , ASIGNATURA_ID
 , NOTA
 , PERSONA_ID
)
   bequeath definer as
   select to_number(mat_exp_per_id || mat_exp_tit_id) id, mat_exp_tit_id estudio_id, mat_curso_aca curso_academico_id, asi_id asignatura_id, round(media_alumno, 8) nota, mat_exp_per_id persona_id
     from exp_asi_cursadas ac, gra_Exp.exp_orden_mat o, pho_cursos c
    where mat_curso_aca = c.id
      and c.activo = 1
      and mat_exp_tit_id = 229
      and mat_exp_per_id = per_id
      and mat_exp_tit_id = tit_id
      and asi_id in (select asi_id
                       from gra_pod.pod_asi_cursos
                      where curso_aca = mat_curso_aca
                        and cur_tit_id = mat_exp_tit_id
                        and cur_id >= 4
                        and caracter in ('LC', 'OB', 'OP', 'TR'))
   union all
   select "ID", "ESTUDIO_ID", "CURSO_ACADEMICO_ID", "ASIGNATURA_ID", "NOTA", "PERSONA_ID" from pho_extra_asignaturas_cursadas;

create or replace force view UJI_PRACTICASHOSPITALARIAS.PHO_EXT_PERSONAS
(
   ID
 , NOMBRE
 , IDENTIFICACION
 , CUENTA
 , TURNO_ID
 , TURNO_FECHA
 , NOTA_MEDIA
)
   bequeath definer as
   select p.id, nombre || ' ' || apellido1 || ' ' || apellido2 nombre, identificacion, busca_cuenta(p.id) cuenta, t.id turno_id, to_date(t.dia || '/' || t.mes || '/' || t.ano || ' ' || t.hora || ':' || t.minutos, 'dd/mm/yyyy hh24:mi') turno_fecha, decode(p.id,  436980, 7.61,  433388, 7.85,  nvl(gra_exp.pack_Exp.media_exp(p.id, 229), 0)) nota_media
     from per_personas p, gra_Exp.exp_matriz_turnos t, gra.orden_mat_2016 o
    where p.id = t.per_id
      and t.tit_id = 229
      and t.tit_id = exp_tit_id
      and t.per_id = exp_per_id
   union all
   select "ID", "NOMBRE", "IDENTIFICACION", "CUENTA", "TURNO_ID", "TURNO_FECHA", "NOTA_MEDIA" from pho_Extra_personas;

DROP VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_ESTADO_MATRICULA;

create or replace force view UJI_PRACTICASHOSPITALARIAS.PHO_VW_ESTADO_MATRICULA
(
   PERSONA_ID
 , FASE_ID
 , SELECCIONADOS
 , TOTAL
)
   bequeath definer as
     select persona_id_mostrar as persona_id, fase_id, count(distinct decode(persona_id, null, null, decode(f.seleccion_totales, 'b', bloque_id, especialidad_id))) seleccionados, count(distinct decode(f.seleccion_totales, 'B', bloque_id, especialidad_id)) total
       from pho_vw_oferta o, pho_fases f
      where fase_id = f.id
        and trunc(sysdate) between fecha_Desde and fecha_hasta
   group by fase_id, persona_id_mostrar;


CREATE OR REPLACE FORCE VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA
(ID, UNICO_ID, ASIGNATURA_ID, ASIGNATURA, CODIGO_ASIGNATURA,
 BLOQUE_ID, BLOQUE, ESPECIALIDAD_ID, ESPECIALIDAD, FASE_ID,
 FASE, FASE_TIPO, GRUPO_ID, TAMAÑO, PRIMERO,
 ULTIMO, P1, P2, P3, P4,
 P5, P6, CONSECUTIVOS, PLAZAS, HOSPITAL_ID,
 HOSPITAL, CODIGO, PERSONA_ID, FECHA, FECHA_INICIO,
 FECHA_FIN, PERSONA_ID_MOSTRAR, ES_SELECCIONABLE, FASE_ACTUAL_ID, ORDEN_1,
 ORDEN_2, ORDEN_3)
BEQUEATH DEFINER
AS
select "ID",
          "UNICO_ID",
          "ASIGNATURA_ID",
          "ASIGNATURA",
          "CODIGO_ASIGNATURA",
          "BLOQUE_ID",
          "BLOQUE",
          "ESPECIALIDAD_ID",
          "ESPECIALIDAD",
          "FASE_ID",
          "FASE",
          "FASE_TIPO",
          "GRUPO_ID",
          "TAMAÑO",
          "PRIMERO",
          "ULTIMO",
          "P1",
          "P2",
          "P3",
          "P4",
          "P5",
          "P6",
          "CONSECUTIVOS",
          "PLAZAS",
          "HOSPITAL_ID",
          "HOSPITAL",
          "CODIGO",
          "PERSONA_ID",
          "FECHA",
          "FECHA_INICIO",
          "FECHA_FIN",
          "PERSONA_ID_MOSTRAR",
          "ES_SELECCIONABLE",
          "FASE_ACTUAL_ID",
          "ORDEN_1",
          "ORDEN_2",
          "ORDEN_3"
   from (select xxxx."ID",
                xxxx."UNICO_ID",
                xxxx."ASIGNATURA_ID",
                xxxx."ASIGNATURA",
                xxxx."CODIGO_ASIGNATURA",
                xxxx."BLOQUE_ID",
                xxxx."BLOQUE",
                xxxx."ESPECIALIDAD_ID",
                xxxx."ESPECIALIDAD",
                xxxx."FASE_ID",
                xxxx."FASE",
                xxxx."FASE_TIPO",
                xxxx."GRUPO_ID",
                xxxx."TAMAÑO",
                xxxx."PRIMERO",
                xxxx."ULTIMO",
                xxxx."P1",
                xxxx."P2",
                xxxx."P3",
                xxxx."P4",
                xxxx."P5",
                xxxx."P6",
                xxxx."CONSECUTIVOS",
                xxxx."PLAZAS",
                xxxx."HOSPITAL_ID",
                xxxx."HOSPITAL",
                xxxx."CODIGO",
                xxxx."PERSONA_ID",
                xxxx."FECHA",
                xxxx."FECHA_INICIO",
                xxxx."FECHA_FIN",
                xxxx."PERSONA_ID_MOSTRAR",
                xxxx."ES_SELECCIONABLE",
                xxxx."FASE_ACTUAL_ID",
                xxxx."ORDEN_1",
                xxxx."ORDEN_2",
                xxxx."ORDEN_3"
         from (select ID,
                      UNICO_ID,
                      ASIGNATURA_ID,
                      ASIGNATURA,
                      CODIGO_ASIGNATURA,
                      BLOQUE_ID,
                      BLOQUE,
                      ESPECIALIDAD_ID,
                      ESPECIALIDAD,
                      FASE_ID,
                      FASE,
                      FASE_TIPO,
                      GRUPO_ID,
                      TAMAÑO,
                      PRIMERO,
                      ULTIMO,
                      P1,
                      P2,
                      P3,
                      P4,
                      P5,
                      P6,
                      CONSECUTIVOS,
                      PLAZAS,
                      HOSPITAL_ID,
                      HOSPITAL,
                      CODIGO,
                      PERSONA_ID,
                      FECHA,
                      FECHA_INICIO,
                      FECHA_FIN,
                      PERSONA_ID_MOSTRAR,
                      ES_SELECCIONABLE,
                      FASE_ACTUAL_ID,
                      orden_1,
                      orden_2,
                      orden_3
               from (select "ID",
                            "UNICO_ID",
                            "ASIGNATURA_ID",
                            "ASIGNATURA",
                            "CODIGO_ASIGNATURA",
                            "BLOQUE_ID",
                            "BLOQUE",
                            "ESPECIALIDAD_ID",
                            "ESPECIALIDAD",
                            "FASE_ID",
                            "FASE",
                            "FASE_TIPO",
                            "GRUPO_ID",
                            "TAMAÑO",
                            "PRIMERO",
                            "ULTIMO",
                            "P1",
                            "P2",
                            "P3",
                            "P4",
                            "P5",
                            "P6",
                            "CONSECUTIVOS",
                            "PLAZAS",
                            "HOSPITAL_ID",
                            "HOSPITAL",
                            "CODIGO",
                            "PERSONA_ID",
                            "FECHA",
                            "FECHA_INICIO",
                            "FECHA_FIN",
                            "PERSONA_ID_MOSTRAR",
                            "ES_SELECCIONABLE",
                            "FASE_ACTUAL_ID",
                            row_number() over(partition by persona_id_mostrar, id order by id, persona_id)       orden,
                            orden_1,
                            orden_2,
                            orden_3
                     from (select xx."ID",
                                  xx."UNICO_ID",
                                  xx."ASIGNATURA_ID",
                                  xx."ASIGNATURA",
                                  xx."CODIGO_ASIGNATURA",
                                  xx."BLOQUE_ID",
                                  xx."BLOQUE",
                                  xx."ESPECIALIDAD_ID",
                                  xx."ESPECIALIDAD",
                                  xx."FASE_ID",
                                  xx."FASE",
                                  xx."FASE_TIPO",
                                  xx."GRUPO_ID",
                                  xx."TAMAÑO",
                                  xx."PRIMERO",
                                  xx."ULTIMO",
                                  xx."P1",
                                  xx."P2",
                                  xx."P3",
                                  xx."P4",
                                  xx."P5",
                                  xx."P6",
                                  xx."CONSECUTIVOS",
                                  xx."PLAZAS",
                                  xx."HOSPITAL_ID",
                                  xx."HOSPITAL",
                                  xx."CODIGO",
                                  xx."PERSONA_ID",
                                  xx."FECHA",
                                  xx."FECHA_INICIO",
                                  xx."FECHA_FIN",
                                  ac.persona_id         persona_id_mostrar,
                                  decode
                                  (
                                     grupo_id,
                                     null, decode
                                           (
                                              xx.persona_id,
                                              ac.persona_id, 1,
                                              decode
                                              (
                                                 pack_pho_practicas.bloque_disponible(ac.persona_id,
                                                                                      fase_id,
                                                                                      unico_id),
                                                 1, decode
                                                    (
                                                       pack_pho_practicas.especialidad_disponible
                                                       (
                                                          ac.persona_id,
                                                          1,
                                                          unico_id
                                                       ),
                                                       1, decode
                                                          (
                                                             pack_pho_practicas.periodos_disponibles
                                                             (
                                                                ac.persona_id,
                                                                fase_id,
                                                                unico_id
                                                             ),
                                                             1, 1,
                                                             0
                                                          ),
                                                       0
                                                    ),
                                                 0
                                              )
                                           ),
                                     decode
                                     (
                                        xx.persona_id,
                                        ac.persona_id, 1,
                                        decode
                                        (
                                           pack_pho_practicas.grupo_seleccionado(ac.persona_id,
                                                                                 fase_id,
                                                                                 unico_id),
                                           -1, decode
                                               (
                                                  pack_pho_practicas.un_periodo_disponible(ac.persona_id,
                                                                                           fase_id,
                                                                                           primero),
                                                  1, 1,
                                                  0
                                               ),
                                           grupo_id, decode
                                                     (
                                                        pack_pho_practicas.un_periodo_disponible(ac.persona_id,
                                                                                                 fase_id,
                                                                                                 primero),
                                                        1, decode
                                                           (
                                                              pack_pho_practicas.especialidad_disponible
                                                              (
                                                                 ac.persona_id,
                                                                 1,
                                                                 unico_id
                                                              ),
                                                              1, 1,
                                                              0
                                                           ),
                                                        0
                                                     ),
                                           0
                                        )
                                     )
                                  )
                                     es_seleccionable,
                                  (select id
                                   from pho_Fases
                                   where trunc(sysdate) between fecha_desde and fecha_hasta - 1)
                                     fase_actual_id,
                                  orden_1,
                                  orden_2,
                                  orden_3
                           from (select x.unico_id || '_' || x.periodo_inicial_id               id,
                                        x.unico_id,
                                        x.asignatura_id,
                                        a.nombre                                                asignatura,
                                        a.codigo_asignatura,
                                        x.bloque_id,
                                        b.nombre                                                bloque,
                                        x.especialidad_id,
                                        e.nombre                                                especialidad,
                                        x.fase_id,
                                        f.nombre                                                fase,
                                        --decode(x.fase_id,1,null,2,null,3,null,x.grupo_id)       grupo_id,
                                        x.grupo_id grupo_id,
                                        x.periodo_final_id - x.periodo_inicial_id + 1           tamaño,
                                        x.periodo_inicial_id                                    primero,
                                        x.periodo_final_id                                      ultimo,
                                        null                                                    p1,
                                        null                                                    p2,
                                        null                                                    p3,
                                        null                                                    p4,
                                        null                                                    p5,
                                        null                                                    p6,
                                        x.bloque_unido                                          consecutivos,
                                        plazas,
                                        x.hospital_id,
                                        h.nombre                                                hospital,
                                        h.codigo,
                                        x.persona_id,
                                        x.fecha,
                                        x.fecha_inicio,
                                        x.fecha_fin,
                                        orden_1,
                                        orden_2,
                                        orden_3,
                                        f.tipo                                                  fase_tipo
                                 from (select o.curso_academico_id,
                                              o.asignatura_id,
                                              o.bloque_id,
                                              o.especialidad_id,
                                              o.fase_id,
                                              o.hospital_id,
                                              o.periodo_inicial_id,
                                              o.periodo_final_id,
                                              o.unico_id,
                                              o.bloque_unido,
                                              o.orden_1,
                                              o.orden_2,
                                              o.orden_3,
                                              p.fecha_inicio,
                                              p2.fecha_fin,
                                              persona_id,
                                              o.fecha,
                                              o.grupo_id             grupo_id,
                                              count(*)               plazas
                                       from pho_oferta o, pho_periodos_fechas p, pho_periodos_fechas p2
                                       where     o.bloque_unido = 1
                                             and o.persona_id is null
                                             and o.periodo_inicial_id = p.periodo_id
                                             and o.curso_academico_id = p.curso_Aca_id
                                             and o.fase_id = p.fase_id
                                             and o.periodo_final_id = p2.periodo_id
                                             and o.curso_academico_id = p2.curso_Aca_id
                                             and o.fase_id = p2.fase_id
                                       group by o.curso_academico_id,
                                                o.asignatura_id,
                                                o.bloque_id,
                                                o.especialidad_id,
                                                o.fase_id,
                                                o.hospital_id,
                                                o.periodo_inicial_id,
                                                o.periodo_final_id,
                                                o.unico_id,
                                                o.bloque_unido,
                                                o.orden_1,
                                                o.orden_2,
                                                o.orden_3,
                                                p.fecha_inicio,
                                                p2.fecha_fin,
                                                o.persona_id,
                                                o.fecha,
                                                o.grupo_id
                                       union all
                                       select o.curso_academico_id,
                                              o.asignatura_id,
                                              o.bloque_id,
                                              o.especialidad_id,
                                              o.fase_id,
                                              o.hospital_id,
                                              o.periodo_inicial_id,
                                              o.periodo_final_id,
                                              o.unico_id,
                                              o.bloque_unido,
                                              o.orden_1,
                                              o.orden_2,
                                              o.orden_3,
                                              p.fecha_inicio,
                                              p2.fecha_fin,
                                              persona_id,
                                              o.fecha,
                                              o.grupo_id             grupo_id,
                                              count(*)               plazas
                                       from pho_oferta o, pho_periodos_fechas p, pho_periodos_fechas p2
                                       where     o.bloque_unido = 1
                                             and o.persona_id is not null
                                             and o.periodo_inicial_id = p.periodo_id
                                             and o.curso_academico_id = p.curso_Aca_id
                                             and o.fase_id = p.fase_id
                                             and o.periodo_final_id = p2.periodo_id
                                             and o.curso_academico_id = p2.curso_Aca_id
                                             and o.fase_id = p2.fase_id
                                       group by o.curso_academico_id,
                                                o.asignatura_id,
                                                o.bloque_id,
                                                o.especialidad_id,
                                                o.fase_id,
                                                o.hospital_id,
                                                o.periodo_inicial_id,
                                                o.periodo_final_id,
                                                o.unico_id,
                                                o.bloque_unido,
                                                o.orden_1,
                                                o.orden_2,
                                                o.orden_3,
                                                p.fecha_inicio,
                                                p2.fecha_fin,
                                                o.persona_id,
                                                o.fecha,
                                                o.grupo_id
                                       union all
                                       select xx.curso_academico_id,
                                              xx.asignatura_id,
                                              xx.bloque_id,
                                              xx.especialidad_id,
                                              xx.fase_id,
                                              xx.hospital_id,
                                              p.id             periodo_inicial_id,
                                              pack_pho_practicas.pho_buscar_ultimo_cons(p.id,
                                                                                        p1,
                                                                                        p2,
                                                                                        p3,
                                                                                        p4,
                                                                                        p5,
                                                                                        p6)
                                                 periodo_final_id,
                                              xx.unico_id,
                                              xx.bloque_unido,
                                              xx.orden_1,
                                              xx.orden_2,
                                              xx.orden_3,
                                              xx.fecha_inicio,
                                              xx.fecha_fin,
                                              xx.persona_id,
                                              xx.fecha,
                                              xx.grupo_id,
                                              xx.plazas
                                       from (select x.curso_academico_id,
                                                    x.asignatura_id,
                                                    x.bloque_id,
                                                    x.especialidad_id,
                                                    x.fase_id,
                                                    x.hospital_id,
                                                    x.periodo_inicial_id,
                                                    x.periodo_final_id,
                                                    x.unico_id,
                                                    x.bloque_unido,
                                                    x.orden_1,
                                                    x.orden_2,
                                                    x.orden_3,
                                                    x.fecha_inicio,
                                                    x.fecha_fin,
                                                    x.persona_id,
                                                    x.fecha,
                                                    x.plazas,
                                                    x.grupo_id,
                                                    min(decode(orden, 1, periodo_id, null))               p1,
                                                    min(decode(orden, 2, periodo_id, null))               p2,
                                                    min(decode(orden, 3, periodo_id, null))               p3,
                                                    min(decode(orden, 4, periodo_id, null))               p4,
                                                    min(decode(orden, 5, periodo_id, null))               p5,
                                                    min(decode(orden, 6, periodo_id, null))               p6
                                             from (select o.curso_academico_id,
                                                          o.asignatura_id,
                                                          o.bloque_id,
                                                          o.especialidad_id,
                                                          o.fase_id,
                                                          o.hospital_id,
                                                          o.periodo_inicial_id,
                                                          o.periodo_final_id,
                                                          o.unico_id,
                                                          o.bloque_unido,
                                                          o.orden_1,
                                                          o.orden_2,
                                                          o.orden_3,
                                                          p.fecha_inicio,
                                                          p2.fecha_fin,
                                                          persona_id,
                                                          fecha,
                                                          o.grupo_id                 grupo_id,
                                                          count(*)                   plazas,
                                                          op.periodo_id,
                                                          row_number()
                                                             over(partition by o.curso_academico_id,
                                                                               o.asignatura_id,
                                                                               o.bloque_id,
                                                                               o.especialidad_id,
                                                                               o.fase_id,
                                                                               o.hospital_id,
                                                                               o.periodo_inicial_id,
                                                                               o.periodo_final_id,
                                                                               o.unico_id,
                                                                               o.bloque_unido,
                                                                               o.orden_1,
                                                                               o.orden_2,
                                                                               o.orden_3
                                                                  order by op.periodo_id)
                                                             orden
                                                   from pho_oferta                             o,
                                                        pho_periodos_fechas                    p,
                                                        pho_periodos_fechas                    p2,
                                                        pho_ofertas_periodos                   op
                                                   where     o.bloque_unido = 0
                                                         and o.persona_id is null
                                                         and o.periodo_inicial_id = p.periodo_id
                                                         and o.curso_academico_id = p.curso_Aca_id
                                                         and o.fase_id = p.fase_id
                                                         and o.periodo_final_id = p2.periodo_id
                                                         and o.curso_academico_id = p2.curso_Aca_id
                                                         and o.fase_id = p2.fase_id
                                                         and o.id = op.oferta_id
                                                   group by o.curso_academico_id,
                                                            o.asignatura_id,
                                                            o.bloque_id,
                                                            o.especialidad_id,
                                                            o.fase_id,
                                                            o.hospital_id,
                                                            o.periodo_inicial_id,
                                                            o.periodo_final_id,
                                                            o.unico_id,
                                                            o.bloque_unido,
                                                            o.orden_1,
                                                            o.orden_2,
                                                            o.orden_3,
                                                            p.fecha_inicio,
                                                            p2.fecha_fin,
                                                            o.persona_id,
                                                            o.fecha,
                                                            o.grupo_id,
                                                            op.periodo_id) x
                                             group by x.curso_academico_id,
                                                      x.asignatura_id,
                                                      x.bloque_id,
                                                      x.especialidad_id,
                                                      x.fase_id,
                                                      x.hospital_id,
                                                      x.periodo_inicial_id,
                                                      x.periodo_final_id,
                                                      x.unico_id,
                                                      x.bloque_unido,
                                                      x.orden_1,
                                                      x.orden_2,
                                                      x.orden_3,
                                                      x.fecha_inicio,
                                                      x.fecha_fin,
                                                      x.persona_id,
                                                      x.fecha,
                                                      x.grupo_id,
                                                      x.plazas) xx,
                                            (select id, id - 1 periodo_ant, nombre
                                             from pho_periodos) p
                                       where (   p1 = p.id
                                              or (    p2 = p.id
                                                  and p1 != p.periodo_ant)
                                              or (    p3 = p.id
                                                  and p2 != p.periodo_ant)
                                              or (    p4 = p.id
                                                  and p3 != p.periodo_ant)
                                              or (    p5 = p.id
                                                  and p4 != p.periodo_ant)
                                              or (    p6 = p.id
                                                  and p5 != p.periodo_ant))
                                       union all
                                       select xx.curso_academico_id,
                                              xx.asignatura_id,
                                              xx.bloque_id,
                                              xx.especialidad_id,
                                              xx.fase_id,
                                              xx.hospital_id,
                                              p.id             periodo_inicial_id,
                                              pack_pho_practicas.pho_buscar_ultimo_cons(p.id,
                                                                                        p1,
                                                                                        p2,
                                                                                        p3,
                                                                                        p4,
                                                                                        p5,
                                                                                        p6)
                                                 periodo_final_id,
                                              xx.unico_id,
                                              xx.bloque_unido,
                                              xx.orden_1,
                                              xx.orden_2,
                                              xx.orden_3,
                                              xx.fecha_inicio,
                                              xx.fecha_fin,
                                              xx.persona_id,
                                              xx.fecha,
                                              xx.grupo_id,
                                              xx.plazas
                                       from (select x.curso_academico_id,
                                                    x.asignatura_id,
                                                    x.bloque_id,
                                                    x.especialidad_id,
                                                    x.fase_id,
                                                    x.hospital_id,
                                                    x.periodo_inicial_id,
                                                    x.periodo_final_id,
                                                    x.unico_id,
                                                    x.bloque_unido,
                                                    x.orden_1,
                                                    x.orden_2,
                                                    x.orden_3,
                                                    x.fecha_inicio,
                                                    x.fecha_fin,
                                                    x.persona_id,
                                                    x.fecha,
                                                    x.plazas,
                                                    x.grupo_id,
                                                    min(decode(orden, 1, periodo_id, null))               p1,
                                                    min(decode(orden, 2, periodo_id, null))               p2,
                                                    min(decode(orden, 3, periodo_id, null))               p3,
                                                    min(decode(orden, 4, periodo_id, null))               p4,
                                                    min(decode(orden, 5, periodo_id, null))               p5,
                                                    min(decode(orden, 6, periodo_id, null))               p6
                                             from (select o.curso_academico_id,
                                                          o.asignatura_id,
                                                          o.bloque_id,
                                                          o.especialidad_id,
                                                          o.fase_id,
                                                          o.hospital_id,
                                                          o.periodo_inicial_id,
                                                          o.periodo_final_id,
                                                          o.unico_id,
                                                          o.bloque_unido,
                                                          o.orden_1,
                                                          o.orden_2,
                                                          o.orden_3,
                                                          p.fecha_inicio,
                                                          p2.fecha_fin,
                                                          persona_id,
                                                          fecha,
                                                          o.grupo_id                 grupo_id,
                                                          count(*)                   plazas,
                                                          op.periodo_id,
                                                          row_number()
                                                             over(partition by o.curso_academico_id,
                                                                               o.asignatura_id,
                                                                               o.bloque_id,
                                                                               o.especialidad_id,
                                                                               o.fase_id,
                                                                               o.hospital_id,
                                                                               o.periodo_inicial_id,
                                                                               o.periodo_final_id,
                                                                               o.unico_id,
                                                                               o.bloque_unido,
                                                                               o.orden_1,
                                                                               o.orden_2,
                                                                               o.orden_3,
                                                                               persona_id
                                                                  order by op.periodo_id)
                                                             orden
                                                   from pho_oferta                             o,
                                                        pho_periodos_fechas                    p,
                                                        pho_periodos_fechas                    p2,
                                                        pho_ofertas_periodos                   op
                                                   where     o.bloque_unido = 0
                                                         and o.persona_id is not null
                                                         and o.periodo_inicial_id = p.periodo_id
                                                         and o.curso_academico_id = p.curso_Aca_id
                                                         and o.fase_id = p.fase_id
                                                         and o.periodo_final_id = p2.periodo_id
                                                         and o.curso_academico_id = p2.curso_Aca_id
                                                         and o.fase_id = p2.fase_id
                                                         and o.id = op.oferta_id
                                                   group by o.curso_academico_id,
                                                            o.asignatura_id,
                                                            o.bloque_id,
                                                            o.especialidad_id,
                                                            o.fase_id,
                                                            o.hospital_id,
                                                            o.periodo_inicial_id,
                                                            o.periodo_final_id,
                                                            o.unico_id,
                                                            o.bloque_unido,
                                                            o.orden_1,
                                                            o.orden_2,
                                                            o.orden_3,
                                                            p.fecha_inicio,
                                                            p2.fecha_fin,
                                                            o.persona_id,
                                                            o.fecha,
                                                            o.grupo_id,
                                                            op.periodo_id) x
                                             group by x.curso_academico_id,
                                                      x.asignatura_id,
                                                      x.bloque_id,
                                                      x.especialidad_id,
                                                      x.fase_id,
                                                      x.hospital_id,
                                                      x.periodo_inicial_id,
                                                      x.periodo_final_id,
                                                      x.unico_id,
                                                      x.bloque_unido,
                                                      x.orden_1,
                                                      x.orden_2,
                                                      x.orden_3,
                                                      x.fecha_inicio,
                                                      x.fecha_fin,
                                                      x.persona_id,
                                                      x.fecha,
                                                      x.grupo_id,
                                                      x.plazas) xx,
                                            (select id, id - 1 periodo_ant, nombre
                                             from pho_periodos) p
                                       where (   p1 = p.id
                                              or (    p2 = p.id
                                                  and p1 != p.periodo_ant)
                                              or (    p3 = p.id
                                                  and p2 != p.periodo_ant)
                                              or (    p4 = p.id
                                                  and p3 != p.periodo_ant)
                                              or (    p5 = p.id
                                                  and p4 != p.periodo_ant)
                                              or (    p6 = p.id
                                                  and p5 != p.periodo_ant))) x,
                                      pho_asignaturas                   a,
                                      pho_bloques                       b,
                                      pho_especialidades                e,
                                      pho_fases                         f,
                                      pho_hospitales                    h,
                                      pho_fases_asignaturas             fa
                                 where     x.asignatura_id = a.id
                                       and x.bloque_id = b.id
                                       and x.especialidad_id = e.id
                                       and x.fase_id = f.id
                                       and x.fase_id = fa.fase_id
                                       and x.asignatura_id = fa.asignatura_id
                                       and x.hospital_id = h.id) xx,
                                pho_ext_asignaturas_cursadas           ac
                           where     (   xx.persona_id = ac.persona_id
                                      or xx.persona_id is null)
                                 and xx.codigo_asignatura = ac.asignatura_id)) xxx
               where orden = 1) xxxx
         where (   fase_id = 1
                or (    fase_id = 2
                    and not exists
                           (select 1
                            from pho_oferta o
                            where     o.persona_id = xxxx.persona_id_mostrar
                                  and o.especialidad_id = xxxx.especialidad_id
                                  and fase_id = 1))
                or (    fase_id = 3
                    and not exists
                           (select 1
                            from pho_oferta o
                            where     o.persona_id = xxxx.persona_id_mostrar
                                  and o.especialidad_id = xxxx.especialidad_id
                                  and fase_id in (1, 2)))
                or fase_id > 3));



create or replace force view UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA_BASE
(
   ASIGNATURA_ID
 , ASIGNATURA
 , CODIGO_ASIGNATURA
 , BLOQUE_ID
 , BLOQUE
 , ESPECIALIDAD_ID
 , ESPECIALIDAD
 , FASE_ID
 , FASE
 , GRUPO_ID
 , TAMAÑO
 , PRIMERO
 , ULTIMO
 , P1
 , P2
 , P3
 , P4
 , P5
 , P6
 , CONSECUTIVOS
 , PLAZAS
 , HOSPITAL_ID
 , HOSPITAL
 , CODIGO
 , BLOQUE_UNIDO
 , ORDEN_1
 , ORDEN_2
 , ORDEN_3
)
   bequeath definer as
   select x."ASIGNATURA_ID", x."ASIGNATURA", x.codigo_asignatura, x."BLOQUE_ID", x."BLOQUE", x."ESPECIALIDAD_ID", x."ESPECIALIDAD", x."FASE_ID", x."FASE", x."GRUPO_ID", x."TAMAÑO", x."PRIMERO", x."ULTIMO", x."P1", x."P2", x."P3", x."P4", x."P5", x."P6", x."CONSECUTIVOS", x."PLAZAS", x."HOSPITAL_ID", x."HOSPITAL", x."CODIGO", decode(tamaño,  1, 'S',  2, decode(p1, p2 - 1, 'S', 'N'),  4, decode(p1, p2 - 1, decode(p2, p3 - 1, decode(p3, p4 - 1, 'S', 'N'), 'N'), 'N'),  'X') bloque_unido, orden_1, orden_2, orden_3
     from (select o.*, l.plazas, h.id hospital_id, h.nombre hospital, codigo
             from (  select asignatura_id, asignatura, codigo_asignatura, bloque_id, bloque, especialidad_id, especialidad, fase_id, fase, grupo_id, periodos_obligatorios tamaño, min(num) primero, max(num) ultimo, min(decode(modulo, 1, periodo_id, null)) p1, min(decode(modulo, 2, periodo_id, null)) p2, min(decode(modulo, 3, periodo_id, null)) p3, min(decode(modulo, 4, periodo_id, null)) p4, min(decode(modulo, 5, periodo_id, null)) p5, min(decode(modulo, 6, periodo_id, null)) p6, 'S' consecutivos, orden_1, orden_2, orden_3
                       from (select xxx.*, count(*) over (partition by asignatura_id, fase_id, bloque_id, especialidad_id, grupo_id) cuantos
                               from (select x.*, trunc((x.num - 1) / periodos_obligatorios) + 1 grupo_id, mod(num - 1, periodos_obligatorios) + 1 modulo
                                       from (select a.id asignatura_id, a.nombre asignatura, a.codigo_asignatura, b.id bloque_id, b.nombre bloque, numero_obligatorias, numero_optativas, e.id especialidad_id, e.nombre especialidad, f.id fase_id, f.nombre fase, periodos_obligatorios, p.id periodo_id, a.orden orden_1, b.orden orden_2, e.orden orden_3, row_number() over(partition by a.id, f.id, b.id, e.id order by p.id) num
                                               from pho_asignaturas     a
                                                  , pho_bloques         b
                                                  , pho_especialidades  e
                                                  , pho_fases           f
                                                  , pho_periodos        p
                                                  , pho_fases_asignaturas fa
                                              where a.id = b.asignatura_id
                                                and b.id = e.bloque_id
                                                --AND f.id = 1
                                                and f.id not in (2, 3)
                                                and f.id = fa.fase_id
                                                and a.id = fa.asignatura_id
                                                and e.tipo like '%' || f.tipo || '%'
                                                and periodo_inicial_id is not null
                                                and p.id between periodo_inicial_id and periodo_final_id --and    e.id = 5
                                                                                                        ) x) xxx)
                      where periodos_obligatorios = cuantos
                   group by asignatura_id
                          , asignatura
                          , codigo_asignatura
                          , bloque_id
                          , bloque
                          , especialidad_id
                          , especialidad
                          , fase_id
                          , fase
                          , grupo_id
                          , periodos_obligatorios
                          , orden_1
                          , orden_2
                          , orden_3) o
                , pho_limites    l
                , pho_hospitales h
            where o.especialidad_id = l.especialidad_id
              and hospital_id = h.id
           union all
           select o.*, l.plazas, h.id hospital_id, h.nombre hospital, codigo
             from (  select asignatura_id, asignatura, codigo_asignatura, bloque_id, bloque, especialidad_id, especialidad, fase_id, fase, grupo_id, periodos_obligatorios tamaño, min(num) primero, max(num) ultimo, min(decode(modulo, 1, periodo_id, null)) p1, min(decode(modulo, 2, periodo_id, null)) p2, min(decode(modulo, 3, periodo_id, null)) p3, min(decode(modulo, 4, periodo_id, null)) p4, min(decode(modulo, 5, periodo_id, null)) p5, min(decode(modulo, 6, periodo_id, null)) p6, 'N' consecutivos, orden_1, orden_2, orden_3
                       from (select xxx.*, count(*) over (partition by asignatura_id, fase_id, bloque_id, especialidad_id, grupo_id) cuantos
                               from (select x.*, trunc((x.num - 1) / periodos_obligatorios) + 1 grupo_id, mod(num - 1, periodos_obligatorios) + 1 modulo
                                       from (select a.id asignatura_id, a.nombre asignatura, a.codigo_asignatura, b.id bloque_id, b.nombre bloque, numero_obligatorias, numero_optativas, e.id especialidad_id, e.nombre especialidad, periodos_obligatorios, p.id periodo_id, f.id fase_id, f.nombre fase, a.orden orden_1, b.orden orden_2, e.orden orden_3, row_number() over(partition by a.id, f.id, b.id, e.id order by p.id) num
                                               from pho_asignaturas     a
                                                  , pho_bloques         b
                                                  , pho_especialidades  e
                                                  , pho_fases           f
                                                  , pho_periodos        p
                                                  , pho_Fases_asignaturas fa
                                              where a.id = b.asignatura_id
                                                and b.id = e.bloque_id
                                                --and f.id= 1
                                                and f.id not in (2, 3)
                                                and f.id = fa.fase_id
                                                and a.id = fa.asignatura_id
                                                and f.tipo = 'OB'
                                                and e.tipo like '%' || f.tipo || '%'
                                                and periodo_inicial_id is null
                                                and p.id in (select periodo_id
                                                               from pho_especialidades_periodos
                                                              where especialidad_id = e.id) --and    e.id in (1,2)
                                                                                           ) x) xxx)
                      where periodos_obligatorios = cuantos
                   group by asignatura_id
                          , asignatura
                          , codigo_asignatura
                          , bloque_id
                          , bloque
                          , especialidad_id
                          , especialidad
                          , fase_id
                          , fase
                          , grupo_id
                          , periodos_obligatorios
                          , orden_1
                          , orden_2
                          , orden_3) o
                , pho_limites    l
                , pho_hospitales h
            where o.especialidad_id = l.especialidad_id
              and hospital_id = h.id) x;

create or replace force view UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA_PREVIA
(
   ID
 , UNICO_ID
 , ASIGNATURA_ID
 , ASIGNATURA
 , BLOQUE_ID
 , BLOQUE
 , ESPECIALIDAD_ID
 , ESPECIALIDAD
 , FASE_ID
 , FASE
 , GRUPO_ID
 , TAMAÑO
 , PRIMERO
 , ULTIMO
 , P1
 , P2
 , P3
 , P4
 , P5
 , P6
 , CONSECUTIVOS
 , PLAZAS
 , HOSPITAL_ID
 , HOSPITAL
 , CODIGO
)
   bequeath definer as
   with base as
           (select asignatura_id || '_' || bloque_id || '_' || especialidad_id || '_' || hospital_id || '_' || grupo_id unico_id, b.*
              from (select o.*, l.plazas, h.id hospital_id, h.nombre hospital, codigo
                      from (  select asignatura_id, asignatura, bloque_id, bloque, especialidad_id, especialidad, fase_id, fase, grupo_id, periodos_obligatorios tamaño, min(num) primero, max(num) ultimo, min(decode(modulo, 1, periodo_id, null)) p1, min(decode(modulo, 2, periodo_id, null)) p2, min(decode(modulo, 3, periodo_id, null)) p3, min(decode(modulo, 4, periodo_id, null)) p4, min(decode(modulo, 5, periodo_id, null)) p5, min(decode(modulo, 6, periodo_id, null)) p6, 'S' consecutivos
                                from (select xxx.*, count(*) over (partition by asignatura_id, bloque_id, especialidad_id, grupo_id) cuantos
                                        from (select x.*, trunc((x.num - 1) / periodos_obligatorios) + 1 grupo_id, mod(num - 1, periodos_obligatorios) + 1 modulo
                                                from (select a.id asignatura_id, a.nombre asignatura, b.id bloque_id, b.nombre bloque, numero_obligatorias, numero_optativas, e.id especialidad_id, e.nombre especialidad, f.id fase_id, f.nombre fase, periodos_obligatorios, p.id periodo_id, row_number() over(partition by a.id, b.id, e.id order by p.id) num
                                                        from pho_asignaturas     a
                                                           , pho_bloques         b
                                                           , pho_especialidades  e
                                                           , pho_fases           f
                                                           , pho_periodos        p
                                                           , pho_fases_asignaturas fa
                                                       where a.id = b.asignatura_id
                                                         and b.id = e.bloque_id
                                                         and f.id = 1
                                                         and f.id = fa.fase_id
                                                         and a.id = fa.asignatura_id
                                                         and f.tipo = e.tipo
                                                         and periodo_inicial_id is not null
                                                         and p.id between periodo_inicial_id and periodo_final_id --and    e.id = 5
                                                                                                                 ) x) xxx)
                               where periodos_obligatorios = cuantos
                            group by asignatura_id
                                   , asignatura
                                   , bloque_id
                                   , bloque
                                   , especialidad_id
                                   , especialidad
                                   , fase_id
                                   , fase
                                   , grupo_id
                                   , periodos_obligatorios) o
                         , pho_limites    l
                         , pho_hospitales h
                     where o.especialidad_id = l.especialidad_id
                       and hospital_id = h.id
                    union all
                    select o.*, l.plazas, h.id hospital_id, h.nombre hospital, codigo
                      from (  select asignatura_id, asignatura, bloque_id, bloque, especialidad_id, especialidad, fase_id, fase, grupo_id, periodos_obligatorios tamaño, min(num) primero, max(num) ultimo, min(decode(modulo, 1, periodo_id, null)) p1, min(decode(modulo, 2, periodo_id, null)) p2, min(decode(modulo, 3, periodo_id, null)) p3, min(decode(modulo, 4, periodo_id, null)) p4, min(decode(modulo, 5, periodo_id, null)) p5, min(decode(modulo, 6, periodo_id, null)) p6, 'N' consecutivos
                                from (select xxx.*, count(*) over (partition by asignatura_id, bloque_id, especialidad_id, grupo_id) cuantos
                                        from (select x.*, trunc((x.num - 1) / periodos_obligatorios) + 1 grupo_id, mod(num - 1, periodos_obligatorios) + 1 modulo
                                                from (select a.id asignatura_id, a.nombre asignatura, b.id bloque_id, b.nombre bloque, numero_obligatorias, numero_optativas, e.id especialidad_id, e.nombre especialidad, periodos_obligatorios, p.id periodo_id, f.id fase_id, f.nombre fase, row_number() over(partition by a.id, b.id, e.id order by p.id) num
                                                        from pho_asignaturas     a
                                                           , pho_bloques         b
                                                           , pho_especialidades  e
                                                           , pho_fases           f
                                                           , pho_periodos        p
                                                           , pho_Fases_asignaturas fa
                                                       where a.id = b.asignatura_id
                                                         and b.id = e.bloque_id
                                                         and f.id = 1
                                                         and f.id = fa.fase_id
                                                         and a.id = fa.asignatura_id
                                                         and f.tipo = e.tipo
                                                         and periodo_inicial_id is null
                                                         and p.id in (select periodo_id
                                                                        from pho_especialidades_periodos
                                                                       where especialidad_id = e.id) --and    e.id in (1,2)
                                                                                                    ) x) xxx)
                               where periodos_obligatorios = cuantos
                            group by asignatura_id
                                   , asignatura
                                   , bloque_id
                                   , bloque
                                   , especialidad_id
                                   , especialidad
                                   , fase_id
                                   , fase
                                   , grupo_id
                                   , periodos_obligatorios) o
                         , pho_limites    l
                         , pho_hospitales h
                     where o.especialidad_id = l.especialidad_id
                       and hospital_id = h.id) b)
   select b.unico_id || '_' || p.id id, b."UNICO_ID", b."ASIGNATURA_ID", b."ASIGNATURA", b."BLOQUE_ID", b."BLOQUE", b."ESPECIALIDAD_ID", b."ESPECIALIDAD", b.fase_id, b.fase, b."GRUPO_ID", pack_pho_practicas.pho_buscar_ultimo_cons(p.id, p1, p2, p3, p4, p5, p6) - p.id + 1 "TAMAÑO", p.id "PRIMERO", pack_pho_practicas.pho_buscar_ultimo_cons(p.id, p1, p2, p3, p4, p5, p6)"ULTIMO", b."P1", b."P2", b."P3", b."P4", b."P5", b."P6", b."CONSECUTIVOS", b."PLAZAS", b."HOSPITAL_ID", b."HOSPITAL", b."CODIGO"
     from base b
        , (select id, id - 1 periodo_ant, nombre
             from pho_periodos) p
    where (p1 = p.id
        or (p2 = p.id
        and p1 != p.periodo_ant)
        or (p3 = p.id
        and p2 != p.periodo_ant)
        or (p4 = p.id
        and p3 != p.periodo_ant)
        or (p5 = p.id
        and p4 != p.periodo_ant)
        or (p6 = p.id
        and p5 != p.periodo_ant));
