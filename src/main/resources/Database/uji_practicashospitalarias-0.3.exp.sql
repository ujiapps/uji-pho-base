drop table PHO_ASIGNATURAS cascade constraints;
drop table PHO_BLOQUES cascade constraints;
drop table PHO_CURSOS cascade constraints;
drop table PHO_ESPECIALIDADES cascade constraints;
drop table PHO_ESPECIALIDADES_PERIODOS cascade constraints;
drop table PHO_EXT_ASIGNATURAS_CURSADAS cascade constraints;
drop table PHO_EXT_PERSONAS cascade constraints;
drop table PHO_FASES cascade constraints;
drop table PHO_HOSPITALES cascade constraints;
drop table PHO_LIMITES cascade constraints;
drop table PHO_MATRICULAS cascade constraints;
drop table PHO_OFERTA cascade constraints;
drop table PHO_OFERTAS_PERIODOS cascade constraints;
drop table PHO_PERIODOS cascade constraints;
drop table PHO_PERIODOS_FECHAS cascade constraints;
drop table PHO_TURNOS cascade constraints;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ASIGNATURAS
  (
    ID                VARCHAR2 (10 BYTE) NOT NULL ,
    NOMBRE            VARCHAR2 (1000 BYTE) NOT NULL ,
    ESTUDIO_ID        NUMBER NOT NULL ,
    ESTUDIO_NOMBRE    VARCHAR2 (1000 BYTE) NOT NULL ,
    CURSO             NUMBER NOT NULL ,
    SEMESTRE          VARCHAR2 (10 BYTE) NOT NULL ,
    orden             NUMBER ,
    CODIGO_ASIGNATURA VARCHAR2 (10 BYTE)
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ASIGNATURAS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_ASIGNATURAS
  (
    ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ASIGNATURAS ADD CONSTRAINT PHO_ASIGNATURAS_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES
  (
    ID                  NUMBER NOT NULL ,
    ASIGNATURA_ID       VARCHAR2 (10 BYTE) NOT NULL ,
    NOMBRE              VARCHAR2 (100 BYTE) NOT NULL ,
    NUMERO_OBLIGATORIAS NUMBER NOT NULL ,
    NUMERO_OPTATIVAS    NUMBER NOT NULL ,
    orden               NUMBER
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES_ASI_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES
  (
    ASIGNATURA_ID ASC
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES
  (
    ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES ADD CONSTRAINT PHO_BLOQUES_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS
  (
    ID     NUMBER NOT NULL ,
    ACTIVO NUMBER NOT NULL
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS
  (
    ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS ADD CONSTRAINT PHO_CURSOS_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES
  (
    ID                    NUMBER NOT NULL ,
    NOMBRE                VARCHAR2 (100 BYTE) NOT NULL ,
    TIPO                  VARCHAR2 (10 BYTE) NOT NULL ,
    BLOQUE_ID             NUMBER NOT NULL ,
    PERIODOS_OBLIGATORIOS NUMBER NOT NULL ,
    PERIODOS_OPTATIVOS    NUMBER NOT NULL ,
    SEMESTRE              VARCHAR2 (10 BYTE) NOT NULL ,
    PERIODO_INICIAL_ID    NUMBER ,
    PERIODO_FINAL_ID      NUMBER ,
    orden                 NUMBER
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_BLOQUE_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES
  (
    BLOQUE_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES__IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES
  (
    PERIODO_INICIAL_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES__IDXv1 ON UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES
  (
    PERIODO_FINAL_ID ASC
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES
  (
    ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ADD CONSTRAINT PHO_ESPECIALIDADES_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS
  (
    ID              NUMBER NOT NULL ,
    ESPECIALIDAD_ID NUMBER NOT NULL ,
    PERIODO_ID      NUMBER NOT NULL
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS
  (
    ID ASC
  )
  ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERI_UN ON UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS
  (
    ESPECIALIDAD_ID ASC , PERIODO_ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS ADD CONSTRAINT PHO_ESPECIALIDADES_PERIODOS_PK PRIMARY KEY ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS ADD CONSTRAINT PHO_ESPECIALIDADES_PERI_UN UNIQUE ( ESPECIALIDAD_ID , PERIODO_ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_EXT_ASIGNATURAS_CURSADAS
  (
    ID                 NUMBER NOT NULL ,
    ESTUDIO_ID         NUMBER NOT NULL ,
    CURSO_ACADEMICO_ID NUMBER NOT NULL ,
    ASIGNATURA_ID      VARCHAR2 (10 BYTE) NOT NULL ,
    NOTA               NUMBER ,
    PERSONA_ID         NUMBER NOT NULL
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_EXT_ASIGNATURAS_CUR_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_EXT_ASIGNATURAS_CURSADAS
  (
    ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_EXT_ASIGNATURAS_CURSADAS ADD CONSTRAINT PHO_EXT_ASIGNATURAS_CUR_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_EXT_PERSONAS
  (
    ID             NUMBER NOT NULL ,
    NOMBRE         VARCHAR2 (100 BYTE) NOT NULL ,
    IDENTIFICACION VARCHAR2 (100 BYTE) NOT NULL ,
    CUENTA         VARCHAR2 (100 BYTE) ,
    TURNO_ID       NUMBER ,
    TURNO_FECHA    DATE ,
    NOTA_MEDIA     NUMBER
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_EXT_PERSONAS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_EXT_PERSONAS
  (
    ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_EXT_PERSONAS ADD CONSTRAINT PHO_EXT_PERSONAS_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_FASES
  (
    ID                 NUMBER NOT NULL ,
    CURSO_ACADEMICO_ID NUMBER NOT NULL ,
    NOMBRE             VARCHAR2 (100 BYTE) NOT NULL ,
    FECHA_DESDE        DATE ,
    FECHA_HASTA        DATE ,
    TIPO               VARCHAR2 (10 BYTE) NOT NULL
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_FASES_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_FASES
  (
    ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_FASES ADD CONSTRAINT PHO_FASES_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_HOSPITALES
  (
    ID     NUMBER NOT NULL ,
    NOMBRE VARCHAR2 (100 BYTE) NOT NULL ,
    CODIGO VARCHAR2 (10 BYTE) NOT NULL ,
    orden  NUMBER DEFAULT 9999 NOT NULL
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_HOSPITALES_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_HOSPITALES
  (
    ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_HOSPITALES ADD CONSTRAINT PHO_HOSPITALES_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES
  (
    ID                 NUMBER NOT NULL ,
    CURSO_ACADEMICO_ID NUMBER NOT NULL ,
    HOSPITAL_ID        NUMBER NOT NULL ,
    ESPECIALIDAD_ID    NUMBER NOT NULL ,
    PLAZAS             NUMBER NOT NULL
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES_CURSO_ACA_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES
  (
    CURSO_ACADEMICO_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES_HOS_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES
  (
    HOSPITAL_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES_ESP_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES
  (
    ESPECIALIDAD_ID ASC
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES
  (
    ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES ADD CONSTRAINT PHO_LIMITES_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_MATRICULAS
  (
    ID                 NUMBER NOT NULL ,
    PERSONA_ID         NUMBER NOT NULL ,
    FLAGS              VARCHAR2 (24 BYTE) DEFAULT '000000000000000000000000' NOT NULL ,
    CURSO_ACADEMICO_ID NUMBER NOT NULL
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_MATRICULAS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_MATRICULAS
  (
    ID ASC
  )
  ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_MATRICULAS__UN ON UJI_PRACTICASHOSPITALARIAS.PHO_MATRICULAS
  (
    PERSONA_ID ASC , CURSO_ACADEMICO_ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_MATRICULAS ADD CONSTRAINT PHO_MATRICULAS_PK PRIMARY KEY ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_MATRICULAS ADD CONSTRAINT PHO_MATRICULAS__UN UNIQUE ( PERSONA_ID , CURSO_ACADEMICO_ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
  (
    ID                 NUMBER NOT NULL ,
    CURSO_ACADEMICO_ID NUMBER NOT NULL ,
    ASIGNATURA_ID      VARCHAR2 (10) NOT NULL ,
    BLOQUE_ID          NUMBER NOT NULL ,
    ESPECIALIDAD_ID    NUMBER NOT NULL ,
    FASE_ID            NUMBER NOT NULL ,
    HOSPITAL_ID        NUMBER NOT NULL ,
    PERIODO_INICIAL_ID NUMBER NOT NULL ,
    PERIODO_FINAL_ID   NUMBER NOT NULL ,
    PERSONA_ID         NUMBER ,
    FECHA              DATE ,
    unico_id           VARCHAR2 (200) NOT NULL ,
    orden_1            NUMBER NOT NULL ,
    orden_2            NUMBER NOT NULL ,
    orden_3            NUMBER NOT NULL ,
    BLOQUE_UNIDO       NUMBER NOT NULL
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
  (
    CURSO_ACADEMICO_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv1 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
  (
    ASIGNATURA_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv2 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
  (
    BLOQUE_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv3 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
  (
    ESPECIALIDAD_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv4 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
  (
    FASE_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv6 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
  (
    HOSPITAL_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv7 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
  (
    PERIODO_INICIAL_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv8 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
  (
    PERIODO_FINAL_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv9 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
  (
    PERSONA_ID ASC
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
  (
    ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS
  (
    ID         NUMBER NOT NULL ,
    NOMBRE     VARCHAR2 (100 BYTE) NOT NULL ,
    SEMESTRE_1 NUMBER NOT NULL ,
    SEMESTRE_2 NUMBER NOT NULL
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS
  (
    ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ADD CONSTRAINT PHO_PERIODOS_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS
  (
    ID           NUMBER NOT NULL ,
    CURSO_ACA_ID NUMBER NOT NULL ,
    PERIODO_ID   NUMBER NOT NULL ,
    FECHA_INICIO DATE NOT NULL ,
    FECHA_FIN    DATE NOT NULL
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS_CUR_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS
  (
    CURSO_ACA_ID ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS_PER_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS
  (
    PERIODO_ID ASC
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS
  (
    ID ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS ADD CONSTRAINT PHO_PERIODOS_FECHAS_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS
  (
    ID         NUMBER NOT NULL ,
    PERSONA_ID NUMBER NOT NULL ,
    FECHA      DATE NOT NULL ,
    FASE_ID    NUMBER NOT NULL
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS_PER_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS
  (
    PERSONA_ID ASC
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS
  (
    ID ASC
  )
  ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS_FASES_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS
    (
      FASE_ID ASC
    ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS ADD CONSTRAINT PHO_TURNOS_PK PRIMARY KEY ( ID ) ;


CREATE TABLE UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos
  (
    id         NUMBER NOT NULL ,
    oferta_id  NUMBER NOT NULL ,
    periodo_id NUMBER NOT NULL
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos_of_IDX ON UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos
  (
    oferta_id ASC
  ) ;
CREATE INDEX UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos_per_IDX ON UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos
  (
    periodo_id ASC
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTAS_PERIODOS_PK ON UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos
  (
    id ASC
  )
  ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos ADD CONSTRAINT pho_ofertas_periodos_PK PRIMARY KEY ( id ) ;


ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES ADD CONSTRAINT PHO_BLOQUES_PHO_ASI_FK FOREIGN KEY ( ASIGNATURA_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_ASIGNATURAS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ADD CONSTRAINT PHO_ESPECIALIDADES_BLOQ_FK FOREIGN KEY ( BLOQUE_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ADD CONSTRAINT PHO_ESPECIALIDADES_PERI_FK FOREIGN KEY ( PERIODO_INICIAL_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ADD CONSTRAINT PHO_ESPECIALIDADES_PER_2_FK FOREIGN KEY ( PERIODO_FINAL_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS ADD CONSTRAINT PHO_ESP_PERI_ESP_FK FOREIGN KEY ( ESPECIALIDAD_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS ADD CONSTRAINT PHO_ESP_PER_PERI_FK FOREIGN KEY ( PERIODO_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_FASES ADD CONSTRAINT PHO_FASES_PHO_CURSOS_FK FOREIGN KEY ( CURSO_ACADEMICO_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES ADD CONSTRAINT PHO_LIMITES_CURSOS_FK FOREIGN KEY ( CURSO_ACADEMICO_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES ADD CONSTRAINT PHO_LIMITES_ESPE_FK FOREIGN KEY ( ESPECIALIDAD_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES ADD CONSTRAINT PHO_LIMITES_HOSP_FK FOREIGN KEY ( HOSPITAL_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_HOSPITALES ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_MATRICULAS ADD CONSTRAINT PHO_MATRICULAS_PHO_CURSOS_FK FOREIGN KEY ( CURSO_ACADEMICO_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_ASIGNATURAS_FK FOREIGN KEY ( ASIGNATURA_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_ASIGNATURAS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_CURSOS_FK FOREIGN KEY ( CURSO_ACADEMICO_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_ESP_FK FOREIGN KEY ( ESPECIALIDAD_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_FASES_FK FOREIGN KEY ( FASE_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_FASES ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_HOSPITALES_FK FOREIGN KEY ( HOSPITAL_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_HOSPITALES ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_PERIODOS_FK FOREIGN KEY ( PERIODO_INICIAL_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_PERIODOS_FK2 FOREIGN KEY ( PERIODO_FINAL_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS ADD CONSTRAINT PHO_PERIOD_FECHAS_CURS_FK FOREIGN KEY ( CURSO_ACA_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS ADD CONSTRAINT PHO_PERIOD_FECHAS_PER_FK FOREIGN KEY ( PERIODO_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS ADD CONSTRAINT PHO_TURNOS_FASES_FK FOREIGN KEY ( FASE_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_FASES ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos ADD CONSTRAINT pho_ofertas_periodos_OF_FK FOREIGN KEY ( oferta_id ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ( ID ) ;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos ADD CONSTRAINT pho_ofertas_periodos_PER_FK FOREIGN KEY ( periodo_id ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;

CREATE OR REPLACE VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA ( ID, UNICO_ID, ASIGNATURA_ID, ASIGNATURA, CODIGO_ASIGNATURA, BLOQUE_ID, BLOQUE, ESPECIALIDAD_ID, ESPECIALIDAD, FASE_ID, FASE, GRUPO_ID, TAMA�O, PRIMERO, ULTIMO, P1, P2, P3, P4, P5, P6, CONSECUTIVOS, PLAZAS, HOSPITAL_ID, HOSPITAL, CODIGO, PERSONA_ID, FECHA, FECHA_INICIO, FECHA_FIN, PERSONA_ID_MOSTRAR, ES_SELECCIONABLE, FASE_ACTUAL_ID, ORDEN_1, ORDEN_2, ORDEN_3 ) AS
select xxxx."ID",xxxx."UNICO_ID",xxxx."ASIGNATURA_ID",xxxx."ASIGNATURA",xxxx."CODIGO_ASIGNATURA",xxxx."BLOQUE_ID",xxxx."BLOQUE",xxxx."ESPECIALIDAD_ID",xxxx."ESPECIALIDAD",xxxx."FASE_ID",xxxx."FASE",xxxx."GRUPO_ID",xxxx."TAMA�O",xxxx."PRIMERO",xxxx."ULTIMO",xxxx."P1",xxxx."P2",xxxx."P3",xxxx."P4",xxxx."P5",xxxx."P6",xxxx."CONSECUTIVOS",xxxx."PLAZAS",xxxx."HOSPITAL_ID",xxxx."HOSPITAL",xxxx."CODIGO",xxxx."PERSONA_ID",xxxx."FECHA",xxxx."FECHA_INICIO",xxxx."FECHA_FIN",xxxx."PERSONA_ID_MOSTRAR",xxxx."ES_SELECCIONABLE",xxxx."FASE_ACTUAL_ID",xxxx."ORDEN_1",xxxx."ORDEN_2",xxxx."ORDEN_3"
from   (select ID, UNICO_ID, ASIGNATURA_ID, ASIGNATURA, CODIGO_ASIGNATURA, BLOQUE_ID, BLOQUE, ESPECIALIDAD_ID,
               ESPECIALIDAD, FASE_ID, FASE, GRUPO_ID, TAMA�O, PRIMERO, ULTIMO, P1, P2, P3, P4, P5, P6, CONSECUTIVOS,
               PLAZAS, HOSPITAL_ID, HOSPITAL, CODIGO, PERSONA_ID, FECHA, FECHA_INICIO, FECHA_FIN, PERSONA_ID_MOSTRAR,
               ES_SELECCIONABLE, FASE_ACTUAL_ID, orden_1, orden_2, orden_3
        from   (select "ID", "UNICO_ID", "ASIGNATURA_ID", "ASIGNATURA", "CODIGO_ASIGNATURA", "BLOQUE_ID", "BLOQUE",
                       "ESPECIALIDAD_ID", "ESPECIALIDAD", "FASE_ID", "FASE", "GRUPO_ID", "TAMA�O", "PRIMERO", "ULTIMO",
                       "P1", "P2", "P3", "P4", "P5", "P6", "CONSECUTIVOS", "PLAZAS", "HOSPITAL_ID", "HOSPITAL",
                       "CODIGO", "PERSONA_ID", "FECHA", "FECHA_INICIO", "FECHA_FIN", "PERSONA_ID_MOSTRAR",
                       "ES_SELECCIONABLE", "FASE_ACTUAL_ID",
                       row_number () over (partition by persona_id_mostrar, id order by id,
                        persona_id) orden, orden_1, orden_2, orden_3
                from   (select xx."ID", xx."UNICO_ID", xx."ASIGNATURA_ID", xx."ASIGNATURA", xx."CODIGO_ASIGNATURA",
                               xx."BLOQUE_ID", xx."BLOQUE", xx."ESPECIALIDAD_ID", xx."ESPECIALIDAD", xx."FASE_ID",
                               xx."FASE", xx."GRUPO_ID", xx."TAMA�O", xx."PRIMERO", xx."ULTIMO", xx."P1", xx."P2",
                               xx."P3", xx."P4", xx."P5", xx."P6", xx."CONSECUTIVOS", xx."PLAZAS", xx."HOSPITAL_ID",
                               xx."HOSPITAL", xx."CODIGO", xx."PERSONA_ID", xx."FECHA", xx."FECHA_INICIO",
                               xx."FECHA_FIN", ac.persona_id persona_id_mostrar,
                               decode
                                  (xx.persona_id,
                                   ac.persona_id, 1,
                                   decode (pack_pho_practicas.bloque_disponible (ac.persona_id, fase_id, unico_id),
                                           1, decode (pack_pho_practicas.especialidad_disponible (ac.persona_id, 1,
                                                                                                  unico_id),
                                                      1, decode
                                                               (pack_pho_practicas.periodos_disponibles (ac.persona_id,
                                                                                                         fase_id,
                                                                                                         unico_id),
                                                                1, 1,
                                                                0
                                                               ),
                                                      0
                                                     ),
                                           0
                                          )
                                  ) es_seleccionable,
                               (select id
                                from   pho_Fases
                                where  trunc (sysdate) between fecha_desde and fecha_hasta) fase_actual_id, orden_1,
                               orden_2, orden_3
                        from   (select x.unico_id || '_' || x.periodo_inicial_id id, x.unico_id, x.asignatura_id,
                                       a.nombre asignatura, a.codigo_asignatura, x.bloque_id, b.nombre bloque,
                                       x.especialidad_id, e.nombre especialidad, x.fase_id, f.nombre fase,
                                       null grupo_id, x.periodo_final_id - x.periodo_inicial_id + 1 tama�o,
                                       x.periodo_inicial_id primero, x.periodo_final_id ultimo, null p1, null p2,
                                       null p3, null p4, null p5, null p6, x.bloque_unido consecutivos, plazas,
                                       x.hospital_id, h.nombre hospital, h.codigo, x.persona_id, x.fecha,
                                       x.fecha_inicio, x.fecha_fin, orden_1, orden_2, orden_3
                                from   (select   o.curso_academico_id, o.asignatura_id, o.bloque_id, o.especialidad_id,
                                                 o.fase_id, o.hospital_id, o.periodo_inicial_id, o.periodo_final_id,
                                                 o.unico_id, o.bloque_unido, o.orden_1, o.orden_2, o.orden_3,
                                                 p.fecha_inicio, p2.fecha_fin, persona_id, o.fecha, count (*) plazas
                                        from     pho_oferta o,
                                                 pho_periodos_fechas p,
                                                 pho_periodos_fechas p2
                                        where    o.bloque_unido = 1
                                        and      o.persona_id is null
                                        and      o.periodo_inicial_id = p.periodo_id
                                        and      o.curso_academico_id = p.curso_Aca_id
                                        and      o.periodo_final_id = p2.periodo_id
                                        and      o.curso_academico_id = p2.curso_Aca_id
                                        group by o.curso_academico_id,
                                                 o.asignatura_id,
                                                 o.bloque_id,
                                                 o.especialidad_id,
                                                 o.fase_id,
                                                 o.hospital_id,
                                                 o.periodo_inicial_id,
                                                 o.periodo_final_id,
                                                 o.unico_id,
                                                 o.bloque_unido,
                                                 o.orden_1,
                                                 o.orden_2,
                                                 o.orden_3,
                                                 p.fecha_inicio,
                                                 p2.fecha_fin,
                                                 o.persona_id,
                                                 o.fecha
                                        union all
                                        select   o.curso_academico_id, o.asignatura_id, o.bloque_id, o.especialidad_id,
                                                 o.fase_id, o.hospital_id, o.periodo_inicial_id, o.periodo_final_id,
                                                 o.unico_id, o.bloque_unido, o.orden_1, o.orden_2, o.orden_3,
                                                 p.fecha_inicio, p2.fecha_fin, persona_id, o.fecha, count (*) plazas
                                        from     pho_oferta o,
                                                 pho_periodos_fechas p,
                                                 pho_periodos_fechas p2
                                        where    o.bloque_unido = 1
                                        and      o.persona_id is not null
                                        and      o.periodo_inicial_id = p.periodo_id
                                        and      o.curso_academico_id = p.curso_Aca_id
                                        and      o.periodo_final_id = p2.periodo_id
                                        and      o.curso_academico_id = p2.curso_Aca_id
                                        group by o.curso_academico_id,
                                                 o.asignatura_id,
                                                 o.bloque_id,
                                                 o.especialidad_id,
                                                 o.fase_id,
                                                 o.hospital_id,
                                                 o.periodo_inicial_id,
                                                 o.periodo_final_id,
                                                 o.unico_id,
                                                 o.bloque_unido,
                                                 o.orden_1,
                                                 o.orden_2,
                                                 o.orden_3,
                                                 p.fecha_inicio,
                                                 p2.fecha_fin,
                                                 o.persona_id,
                                                 o.fecha
                                        union all
                                        select xx.curso_academico_id, xx.asignatura_id, xx.bloque_id,
                                               xx.especialidad_id, xx.fase_id, xx.hospital_id, p.id periodo_inicial_id,
                                               pack_pho_practicas.pho_buscar_ultimo_cons (p.id, p1,
                                                                                          p2, p3, p4,
                                                                                          p5, p6) periodo_final_id,
                                               xx.unico_id, xx.bloque_unido, xx.orden_1, xx.orden_2, xx.orden_3,
                                               xx.fecha_inicio, xx.fecha_fin, xx.persona_id, xx.fecha, xx.plazas
                                        from   (select   x.curso_academico_id, x.asignatura_id, x.bloque_id,
                                                         x.especialidad_id, x.fase_id, x.hospital_id,
                                                         x.periodo_inicial_id, x.periodo_final_id, x.unico_id,
                                                         x.bloque_unido, x.orden_1, x.orden_2, x.orden_3,
                                                         x.fecha_inicio, x.fecha_fin, x.persona_id, x.fecha, x.plazas,
                                                         min (decode (orden, 1, periodo_id, null)) p1,
                                                         min (decode (orden, 2, periodo_id, null)) p2,
                                                         min (decode (orden, 3, periodo_id, null)) p3,
                                                         min (decode (orden, 4, periodo_id, null)) p4,
                                                         min (decode (orden, 5, periodo_id, null)) p5,
                                                         min (decode (orden, 6, periodo_id, null)) p6
                                                from     (select   o.curso_academico_id, o.asignatura_id, o.bloque_id,
                                                                   o.especialidad_id, o.fase_id, o.hospital_id,
                                                                   o.periodo_inicial_id, o.periodo_final_id, o.unico_id,
                                                                   o.bloque_unido, o.orden_1, o.orden_2, o.orden_3,
                                                                   p.fecha_inicio, p2.fecha_fin, persona_id, fecha,
                                                                   count (*) plazas, op.periodo_id,
                                                                   row_number () over (partition by o.curso_academico_id, o.asignatura_id, o.bloque_id, o.especialidad_id, o.fase_id, o.hospital_id, o.periodo_inicial_id, o.periodo_final_id, o.unico_id, o.bloque_unido, o.orden_1, o.orden_2, o.orden_3 order by op.periodo_id)
                                                                                                                  orden
                                                          from     pho_oferta o,
                                                                   pho_periodos_fechas p,
                                                                   pho_periodos_fechas p2,
                                                                   pho_ofertas_periodos op
                                                          where    o.bloque_unido = 0
                                                          and      o.persona_id is null
                                                          and      o.periodo_inicial_id = p.periodo_id
                                                          and      o.curso_academico_id = p.curso_Aca_id
                                                          and      o.periodo_final_id = p2.periodo_id
                                                          and      o.curso_academico_id = p2.curso_Aca_id
                                                          and      o.id = op.oferta_id
                                                          group by o.curso_academico_id,
                                                                   o.asignatura_id,
                                                                   o.bloque_id,
                                                                   o.especialidad_id,
                                                                   o.fase_id,
                                                                   o.hospital_id,
                                                                   o.periodo_inicial_id,
                                                                   o.periodo_final_id,
                                                                   o.unico_id,
                                                                   o.bloque_unido,
                                                                   o.orden_1,
                                                                   o.orden_2,
                                                                   o.orden_3,
                                                                   p.fecha_inicio,
                                                                   p2.fecha_fin,
                                                                   o.persona_id,
                                                                   o.fecha,
                                                                   op.periodo_id) x
                                                group by x.curso_academico_id,
                                                         x.asignatura_id,
                                                         x.bloque_id,
                                                         x.especialidad_id,
                                                         x.fase_id,
                                                         x.hospital_id,
                                                         x.periodo_inicial_id,
                                                         x.periodo_final_id,
                                                         x.unico_id,
                                                         x.bloque_unido,
                                                         x.orden_1,
                                                         x.orden_2,
                                                         x.orden_3,
                                                         x.fecha_inicio,
                                                         x.fecha_fin,
                                                         x.persona_id,
                                                         x.fecha,
                                                         x.plazas) xx,
                                               (select id, id - 1 periodo_ant, nombre
                                                from   pho_periodos) p
                                        where  (   p1 = p.id
                                                or (    p2 = p.id
                                                    and p1 != p.periodo_ant)
                                                or (    p3 = p.id
                                                    and p2 != p.periodo_ant)
                                                or (    p4 = p.id
                                                    and p3 != p.periodo_ant)
                                                or (    p5 = p.id
                                                    and p4 != p.periodo_ant)
                                                or (    p6 = p.id
                                                    and p5 != p.periodo_ant)
                                               )
                                        union all
                                        select xx.curso_academico_id, xx.asignatura_id, xx.bloque_id,
                                               xx.especialidad_id, xx.fase_id, xx.hospital_id, p.id periodo_inicial_id,
                                               pack_pho_practicas.pho_buscar_ultimo_cons (p.id, p1,
                                                                                          p2, p3, p4,
                                                                                          p5, p6) periodo_final_id,
                                               xx.unico_id, xx.bloque_unido, xx.orden_1, xx.orden_2, xx.orden_3,
                                               xx.fecha_inicio, xx.fecha_fin, xx.persona_id, xx.fecha, xx.plazas
                                        from   (select   x.curso_academico_id, x.asignatura_id, x.bloque_id,
                                                         x.especialidad_id, x.fase_id, x.hospital_id,
                                                         x.periodo_inicial_id, x.periodo_final_id, x.unico_id,
                                                         x.bloque_unido, x.orden_1, x.orden_2, x.orden_3,
                                                         x.fecha_inicio, x.fecha_fin, x.persona_id, x.fecha, x.plazas,
                                                         min (decode (orden, 1, periodo_id, null)) p1,
                                                         min (decode (orden, 2, periodo_id, null)) p2,
                                                         min (decode (orden, 3, periodo_id, null)) p3,
                                                         min (decode (orden, 4, periodo_id, null)) p4,
                                                         min (decode (orden, 5, periodo_id, null)) p5,
                                                         min (decode (orden, 6, periodo_id, null)) p6
                                                from     (select   o.curso_academico_id, o.asignatura_id, o.bloque_id,
                                                                   o.especialidad_id, o.fase_id, o.hospital_id,
                                                                   o.periodo_inicial_id, o.periodo_final_id, o.unico_id,
                                                                   o.bloque_unido, o.orden_1, o.orden_2, o.orden_3,
                                                                   p.fecha_inicio, p2.fecha_fin, persona_id, fecha,
                                                                   count (*) plazas, op.periodo_id,
                                                                   row_number () over (partition by o.curso_academico_id, o.asignatura_id, o.bloque_id, o.especialidad_id, o.fase_id, o.hospital_id, o.periodo_inicial_id, o.periodo_final_id, o.unico_id, o.bloque_unido, o.orden_1, o.orden_2, o.orden_3 order by op.periodo_id)
                                                                                                                  orden
                                                          from     pho_oferta o,
                                                                   pho_periodos_fechas p,
                                                                   pho_periodos_fechas p2,
                                                                   pho_ofertas_periodos op
                                                          where    o.bloque_unido = 0
                                                          and      o.persona_id is not null
                                                          and      o.periodo_inicial_id = p.periodo_id
                                                          and      o.curso_academico_id = p.curso_Aca_id
                                                          and      o.periodo_final_id = p2.periodo_id
                                                          and      o.curso_academico_id = p2.curso_Aca_id
                                                          and      o.id = op.oferta_id
                                                          group by o.curso_academico_id,
                                                                   o.asignatura_id,
                                                                   o.bloque_id,
                                                                   o.especialidad_id,
                                                                   o.fase_id,
                                                                   o.hospital_id,
                                                                   o.periodo_inicial_id,
                                                                   o.periodo_final_id,
                                                                   o.unico_id,
                                                                   o.bloque_unido,
                                                                   o.orden_1,
                                                                   o.orden_2,
                                                                   o.orden_3,
                                                                   p.fecha_inicio,
                                                                   p2.fecha_fin,
                                                                   o.persona_id,
                                                                   o.fecha,
                                                                   op.periodo_id) x
                                                group by x.curso_academico_id,
                                                         x.asignatura_id,
                                                         x.bloque_id,
                                                         x.especialidad_id,
                                                         x.fase_id,
                                                         x.hospital_id,
                                                         x.periodo_inicial_id,
                                                         x.periodo_final_id,
                                                         x.unico_id,
                                                         x.bloque_unido,
                                                         x.orden_1,
                                                         x.orden_2,
                                                         x.orden_3,
                                                         x.fecha_inicio,
                                                         x.fecha_fin,
                                                         x.persona_id,
                                                         x.fecha,
                                                         x.plazas) xx,
                                               (select id, id - 1 periodo_ant, nombre
                                                from   pho_periodos) p
                                        where  (   p1 = p.id
                                                or (    p2 = p.id
                                                    and p1 != p.periodo_ant)
                                                or (    p3 = p.id
                                                    and p2 != p.periodo_ant)
                                                or (    p4 = p.id
                                                    and p3 != p.periodo_ant)
                                                or (    p5 = p.id
                                                    and p4 != p.periodo_ant)
                                                or (    p6 = p.id
                                                    and p5 != p.periodo_ant)
                                               )) x,
                                       pho_asignaturas a,
                                       pho_bloques b,
                                       pho_especialidades e,
                                       pho_fases f,
                                       pho_hospitales h
                                where  x.asignatura_id = a.id
                                and    x.bloque_id = b.id
                                and    x.especialidad_id = e.id
                                and    x.fase_id = f.id
                                and    x.hospital_id = h.id) xx,
                               pho_ext_asignaturas_cursadas ac
                        where  (   xx.persona_id = ac.persona_id
                                or xx.persona_id is null)
                        and    xx.codigo_asignatura = ac.asignatura_id)) xxx
        where  orden = 1) xxxx
where  (   fase_id = 1
        or (    fase_id = 2
            and not exists (
                   select 1
                   from   pho_oferta o
                   where  o.persona_id = xxxx.persona_id_mostrar
                   and    o.especialidad_id = xxxx.especialidad_id
                   and    fase_id = 1)
           )
       ) 
;





CREATE OR REPLACE VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_ESTADO_MATRICULA ( PERSONA_ID, FASE_ID, SELECCIONADOS, TOTAL ) AS
select   persona_id_mostrar as persona_id, fase_id,
            count (distinct decode (persona_id, null, null, bloque_id)) seleccionados, count (distinct bloque_id) total
   from     pho_vw_oferta o,
            pho_fases f
   where    fase_id = f.id
   and      trunc (sysdate) between fecha_Desde and fecha_hasta
   group by fase_id,
            persona_id_mostrar 
;





CREATE OR REPLACE VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA_BASE ( ASIGNATURA_ID, ASIGNATURA, CODIGO_ASIGNATURA, BLOQUE_ID, BLOQUE, ESPECIALIDAD_ID, ESPECIALIDAD, FASE_ID, FASE, GRUPO_ID, TAMA�O, PRIMERO, ULTIMO, P1, P2, P3, P4, P5, P6, CONSECUTIVOS, PLAZAS, HOSPITAL_ID, HOSPITAL, CODIGO, BLOQUE_UNIDO, ORDEN_1, ORDEN_2, ORDEN_3 ) AS
select x."ASIGNATURA_ID", x."ASIGNATURA", x.codigo_asignatura, x."BLOQUE_ID", x."BLOQUE", x."ESPECIALIDAD_ID",
          x."ESPECIALIDAD", x."FASE_ID", x."FASE", x."GRUPO_ID", x."TAMA�O", x."PRIMERO", x."ULTIMO", x."P1", x."P2",
          x."P3", x."P4", x."P5", x."P6", x."CONSECUTIVOS", x."PLAZAS", x."HOSPITAL_ID", x."HOSPITAL", x."CODIGO",
          decode (tama�o,
                  1, 'S',
                  2, decode (p1, p2 - 1, 'S', 'N'),
                  4, decode (p1, p2 - 1, decode (p2, p3 - 1, decode (p3, p4 - 1, 'S', 'N'), 'N'), 'N'),
                  'X'
                 ) bloque_unido,
          orden_1, orden_2, orden_3
   from   (select o.*, l.plazas, h.id hospital_id, h.nombre hospital, codigo
           from   (select   asignatura_id, asignatura, codigo_asignatura, bloque_id, bloque, especialidad_id,
                            especialidad, fase_id, fase, grupo_id, periodos_obligatorios tama�o, min (num) primero,
                            max (num) ultimo, min (decode (modulo, 1, periodo_id, null)) p1,
                            min (decode (modulo, 2, periodo_id, null)) p2, min (decode (modulo,
                                                                                        3, periodo_id,
                                                                                        null
                                                                                       )) p3,
                            min (decode (modulo, 4, periodo_id, null)) p4, min (decode (modulo,
                                                                                        5, periodo_id,
                                                                                        null
                                                                                       )) p5,
                            min (decode (modulo, 6, periodo_id, null)) p6, 'S' consecutivos, orden_1, orden_2, orden_3
                   from     (select xxx.*,
                                    count (*) over (partition by asignatura_id, bloque_id, especialidad_id, grupo_id)
                                                                                                                cuantos
                             from   (select x.*, trunc ((x.num - 1) / periodos_obligatorios) + 1 grupo_id,
                                            mod (num - 1, periodos_obligatorios) + 1 modulo
                                     from   (select a.id asignatura_id, a.nombre asignatura, a.codigo_asignatura,
                                                    b.id bloque_id, b.nombre bloque, numero_obligatorias,
                                                    numero_optativas, e.id especialidad_id, e.nombre especialidad,
                                                    f.id fase_id, f.nombre fase, periodos_obligatorios, p.id periodo_id,
                                                    a.orden orden_1, b.orden orden_2, e.orden orden_3,
                                                    row_number () over (partition by a.id, b.id, e.id order by p.id)
                                                                                                                    num
                                             from   pho_asignaturas a,
                                                    pho_bloques b,
                                                    pho_especialidades e,
                                                    pho_fases f,
                                                    pho_periodos p
                                             where  a.id = b.asignatura_id
                                             and    b.id = e.bloque_id
                                             and    f.id = 1
                                             and    e.tipo like '%'||f.tipo||'%'
                                             and    periodo_inicial_id is not null
                                             and    p.id between periodo_inicial_id and periodo_final_id
                                                                                                        --and    e.id = 5
                                            ) x) xxx)
                   where    periodos_obligatorios = cuantos
                   group by asignatura_id,
                            asignatura,
                            codigo_asignatura,
                            bloque_id,
                            bloque,
                            especialidad_id,
                            especialidad,
                            fase_id,
                            fase,
                            grupo_id,
                            periodos_obligatorios,
                            orden_1,
                            orden_2,
                            orden_3) o,
                  pho_limites l,
                  pho_hospitales h
           where  o.especialidad_id = l.especialidad_id
           and    hospital_id = h.id
           union all
           select o.*, l.plazas, h.id hospital_id, h.nombre hospital, codigo
           from   (select   asignatura_id, asignatura, codigo_asignatura, bloque_id, bloque, especialidad_id,
                            especialidad, fase_id, fase, grupo_id, periodos_obligatorios tama�o, min (num) primero,
                            max (num) ultimo, min (decode (modulo, 1, periodo_id, null)) p1,
                            min (decode (modulo, 2, periodo_id, null)) p2, min (decode (modulo,
                                                                                        3, periodo_id,
                                                                                        null
                                                                                       )) p3,
                            min (decode (modulo, 4, periodo_id, null)) p4, min (decode (modulo,
                                                                                        5, periodo_id,
                                                                                        null
                                                                                       )) p5,
                            min (decode (modulo, 6, periodo_id, null)) p6, 'N' consecutivos, orden_1, orden_2, orden_3
                   from     (select xxx.*,
                                    count (*) over (partition by asignatura_id, bloque_id, especialidad_id, grupo_id)
                                                                                                                cuantos
                             from   (select x.*, trunc ((x.num - 1) / periodos_obligatorios) + 1 grupo_id,
                                            mod (num - 1, periodos_obligatorios) + 1 modulo
                                     from   (select a.id asignatura_id, a.nombre asignatura, a.codigo_asignatura,
                                                    b.id bloque_id, b.nombre bloque, numero_obligatorias,
                                                    numero_optativas, e.id especialidad_id, e.nombre especialidad,
                                                    periodos_obligatorios, p.id periodo_id, f.id fase_id, f.nombre fase,
                                                    a.orden orden_1, b.orden orden_2, e.orden orden_3,
                                                    row_number () over (partition by a.id, b.id, e.id order by p.id)
                                                                                                                    num
                                             from   pho_asignaturas a,
                                                    pho_bloques b,
                                                    pho_especialidades e,
                                                    pho_fases f,
                                                    pho_periodos p
                                             where  a.id = b.asignatura_id
                                             and    b.id = e.bloque_id
                                             and    f.id = 1
                                             and    e.tipo like '%'||f.tipo||'%'
                                             and    periodo_inicial_id is null
                                             and    p.id in (select periodo_id
                                                             from   pho_especialidades_periodos
                                                             where  especialidad_id = e.id)
                                                                                           --and    e.id in (1,2)
                                            ) x) xxx)
                   where    periodos_obligatorios = cuantos
                   group by asignatura_id,
                            asignatura,
                            codigo_asignatura,
                            bloque_id,
                            bloque,
                            especialidad_id,
                            especialidad,
                            fase_id,
                            fase,
                            grupo_id,
                            periodos_obligatorios,
                            orden_1,
                            orden_2,
                            orden_3) o,
                  pho_limites l,
                  pho_hospitales h
           where  o.especialidad_id = l.especialidad_id
           and    hospital_id = h.id) x 
;





CREATE OR REPLACE VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA_BASE_2 ( ASIGNATURA_ID, ASIGNATURA, CODIGO_ASIGNATURA, BLOQUE_ID, BLOQUE, ESPECIALIDAD_ID, ESPECIALIDAD, FASE_ID, FASE, GRUPO_ID, TAMA�O, PRIMERO, ULTIMO, P1, P2, P3, P4, P5, P6, CONSECUTIVOS, PLAZAS, OCUPACION, HOSPITAL_ID, HOSPITAL, CODIGO, BLOQUE_UNIDO, ORDEN_1, ORDEN_2, ORDEN_3, UNICO_ID ) AS
select x."ASIGNATURA_ID", x."ASIGNATURA", x.codigo_asignatura, x."BLOQUE_ID", x."BLOQUE",
                        x."ESPECIALIDAD_ID", x."ESPECIALIDAD", x."FASE_ID", x."FASE", x."GRUPO_ID", x."TAMA�O",
                        
                        --x."PRIMERO", x."ULTIMO",
                        x.p1 primero, nvl (x.p6, nvl (x.p5, nvl (x.p4, nvl (x.p3, nvl (x.p2, x.p1))))) ULTIMO, x."P1",
                        x."P2", x."P3", x."P4", x."P5", x."P6", x."CONSECUTIVOS", x."PLAZAS",
                        pack_pho_practicas.plazas_ocupadas (x.asignatura_id, x.bloque_id, x.especialidad_id,
                                                            x.hospital_id, x.p1) ocupacion,
                        x."HOSPITAL_ID", x."HOSPITAL", x."CODIGO",
                        decode (tama�o,
                                1, 'S',
                                2, decode (p1, p2 - 1, 'S', 'N'),
                                4, decode (p1, p2 - 1, decode (p2, p3 - 1, decode (p3, p4 - 1, 'S', 'N'), 'N'), 'N'),
                                'X'
                               ) bloque_unido,
                        orden_1, orden_2, orden_3,
                        x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_'
                        || x.hospital_id || '_' || x.grupo_id unico_id
                 from   (select o.*, l.plazas, h.id hospital_id, h.nombre hospital, codigo
                         from   (select   asignatura_id, asignatura, codigo_asignatura, bloque_id, bloque,
                                          especialidad_id, especialidad, fase_id, fase, grupo_id, periodos tama�o,
                                          min (num) primero, max (num) ultimo,
                                          min (decode (modulo, 1, periodo_id, null)) p1,
                                          min (decode (modulo, 2, periodo_id, null)) p2,
                                          min (decode (modulo, 3, periodo_id, null)) p3,
                                          min (decode (modulo, 4, periodo_id, null)) p4,
                                          min (decode (modulo, 5, periodo_id, null)) p5,
                                          min (decode (modulo, 6, periodo_id, null)) p6, 'S' consecutivos, orden_1,
                                          orden_2, orden_3
                                 from     (select xxx.*,
                                                  count (*) over (partition by asignatura_id, bloque_id, especialidad_id, grupo_id)
                                                                                                                cuantos
                                           from   (select x.*, trunc ((x.num - 1) / periodos_optativos) + 1 grupo_id,
                                                          mod (num - 1, periodos_optativos) + 1 modulo,
                                                          periodos_optativos periodos
                                                   from   (select a.id asignatura_id, a.nombre asignatura,
                                                                  a.codigo_asignatura, b.id bloque_id, b.nombre bloque,
                                                                  numero_obligatorias, numero_optativas,
                                                                  e.id especialidad_id, e.nombre especialidad,
                                                                  f.id fase_id, f.nombre fase, f.tipo tipo,
                                                                  periodos_obligatorios, periodos_optativos,
                                                                  p.id periodo_id, a.orden orden_1, b.orden orden_2,
                                                                  nvl (e.orden, 999) orden_3,
                                                                  row_number () over (partition by a.id, b.id, e.id order by p.id)
                                                                                                                    num
                                                           from   pho_asignaturas a,
                                                                  pho_bloques b,
                                                                  pho_especialidades e,
                                                                  pho_fases f,
                                                                  pho_periodos p
                                                           where  a.id = b.asignatura_id
                                                           and    b.id = e.bloque_id
                                                           and    f.id = 2
                                                           and    e.tipo like '%' || f.tipo || '%'
                                                           and    periodo_inicial_id is not null
                                                           and    p.id between periodo_inicial_id and periodo_final_id
                                                                                                                      --and    e.id = 5
                                                          ) x) xxx)
                                 where    periodos = cuantos
                                 group by asignatura_id,
                                          asignatura,
                                          codigo_asignatura,
                                          bloque_id,
                                          bloque,
                                          especialidad_id,
                                          especialidad,
                                          fase_id,
                                          fase,
                                          grupo_id,
                                          periodos,
                                          orden_1,
                                          orden_2,
                                          orden_3) o,
                                pho_limites l,
                                pho_hospitales h
                         where  o.especialidad_id = l.especialidad_id
                         and    hospital_id = h.id
                         union all
                         select o.*, l.plazas, h.id hospital_id, h.nombre hospital, codigo
                         from   (select   asignatura_id, asignatura, codigo_asignatura, bloque_id, bloque,
                                          especialidad_id, especialidad, fase_id, fase, grupo_id, periodos tama�o,
                                          min (num) primero, max (num) ultimo,
                                          min (decode (modulo, 1, periodo_id, null)) p1,
                                          min (decode (modulo, 2, periodo_id, null)) p2,
                                          min (decode (modulo, 3, periodo_id, null)) p3,
                                          min (decode (modulo, 4, periodo_id, null)) p4,
                                          min (decode (modulo, 5, periodo_id, null)) p5,
                                          min (decode (modulo, 6, periodo_id, null)) p6, 'N' consecutivos, orden_1,
                                          orden_2, orden_3
                                 from     (select xxx.*,
                                                  count (*) over (partition by asignatura_id, bloque_id, especialidad_id, grupo_id)
                                                                                                                cuantos
                                           from   (select x.*,
                                                          decode (periodos_optativos,
                                                                  0, 0,
                                                                  trunc ((x.num - 1) / periodos_optativos) + 1
                                                                 ) grupo_id,
                                                          mod (num - 1, periodos_optativos) + 1 modulo,
                                                          periodos_optativos periodos
                                                   from   (select a.id asignatura_id, a.nombre asignatura,
                                                                  a.codigo_asignatura, b.id bloque_id, b.nombre bloque,
                                                                  numero_obligatorias, numero_optativas,
                                                                  e.id especialidad_id, e.nombre especialidad,
                                                                  periodos_obligatorios, periodos_optativos,
                                                                  p.id periodo_id, f.id fase_id, f.nombre fase, f.tipo,
                                                                  a.orden orden_1, nvl (b.orden, 999) orden_2,
                                                                  nvl (e.orden, 999) orden_3,
                                                                  row_number () over (partition by a.id, b.id, e.id order by p.id)
                                                                                                                    num
                                                           from   pho_asignaturas a,
                                                                  pho_bloques b,
                                                                  pho_especialidades e,
                                                                  pho_fases f,
                                                                  pho_periodos p
                                                           where  a.id = b.asignatura_id
                                                           and    b.id = e.bloque_id
                                                           and    f.id = 2
                                                           and    e.tipo like '%' || f.tipo || '%'
                                                           and    periodo_inicial_id is null
                                                           and    p.id in (select periodo_id
                                                                           from   pho_especialidades_periodos
                                                                           where  especialidad_id = e.id)
                                                                                                         --and    e.id in (1,2)
                                                          ) x) xxx)
                                 where    periodos_optativos = cuantos
                                 group by asignatura_id,
                                          asignatura,
                                          codigo_asignatura,
                                          bloque_id,
                                          bloque,
                                          especialidad_id,
                                          especialidad,
                                          fase_id,
                                          fase,
                                          grupo_id,
                                          periodos,
                                          orden_1,
                                          orden_2,
                                          orden_3) o,
                                pho_limites l,
                                pho_hospitales h
                         where  o.especialidad_id = l.especialidad_id
                         and    hospital_id = h.id) x 
;





CREATE OR REPLACE VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA_PREVIA ( ID, UNICO_ID, ASIGNATURA_ID, ASIGNATURA, BLOQUE_ID, BLOQUE, ESPECIALIDAD_ID, ESPECIALIDAD, FASE_ID, FASE, GRUPO_ID, TAMA�O, PRIMERO, ULTIMO, P1, P2, P3, P4, P5, P6, CONSECUTIVOS, PLAZAS, HOSPITAL_ID, HOSPITAL, CODIGO ) AS
with base as
        (select asignatura_id || '_' || bloque_id || '_' || especialidad_id || '_' || hospital_id || '_'
                || grupo_id unico_id,
                b.*
         from   (select o.*, l.plazas, h.id hospital_id, h.nombre hospital, codigo
                 from   (select   asignatura_id, asignatura, bloque_id, bloque, especialidad_id, especialidad, fase_id,
                                  fase, grupo_id, periodos_obligatorios tama�o, min (num) primero, max (num) ultimo,
                                  min (decode (modulo, 1, periodo_id, null)) p1,
                                  min (decode (modulo, 2, periodo_id, null)) p2,
                                  min (decode (modulo, 3, periodo_id, null)) p3,
                                  min (decode (modulo, 4, periodo_id, null)) p4,
                                  min (decode (modulo, 5, periodo_id, null)) p5,
                                  min (decode (modulo, 6, periodo_id, null)) p6, 'S' consecutivos
                         from     (select xxx.*,
                                          count (*) over (partition by asignatura_id, bloque_id, especialidad_id, grupo_id)
                                                                                                                cuantos
                                   from   (select x.*, trunc ((x.num - 1) / periodos_obligatorios) + 1 grupo_id,
                                                  mod (num - 1, periodos_obligatorios) + 1 modulo
                                           from   (select a.id asignatura_id, a.nombre asignatura, b.id bloque_id,
                                                          b.nombre bloque, numero_obligatorias, numero_optativas,
                                                          e.id especialidad_id, e.nombre especialidad, f.id fase_id,
                                                          f.nombre fase, periodos_obligatorios, p.id periodo_id,
                                                          row_number () over (partition by a.id, b.id, e.id order by p.id)
                                                                                                                    num
                                                   from   pho_asignaturas a,
                                                          pho_bloques b,
                                                          pho_especialidades e,
                                                          pho_fases f,
                                                          pho_periodos p
                                                   where  a.id = b.asignatura_id
                                                   and    b.id = e.bloque_id
                                                   and    f.id = 1
                                                   and    f.tipo = e.tipo
                                                   and    periodo_inicial_id is not null
                                                   and    p.id between periodo_inicial_id and periodo_final_id
                                                                                                              --and    e.id = 5
                                                  ) x) xxx)
                         where    periodos_obligatorios = cuantos
                         group by asignatura_id,
                                  asignatura,
                                  bloque_id,
                                  bloque,
                                  especialidad_id,
                                  especialidad,
                                  fase_id,
                                  fase,
                                  grupo_id,
                                  periodos_obligatorios) o,
                        pho_limites l,
                        pho_hospitales h
                 where  o.especialidad_id = l.especialidad_id
                 and    hospital_id = h.id
                 union all
                 select o.*, l.plazas, h.id hospital_id, h.nombre hospital, codigo
                 from   (select   asignatura_id, asignatura, bloque_id, bloque, especialidad_id, especialidad, fase_id,
                                  fase, grupo_id, periodos_obligatorios tama�o, min (num) primero, max (num) ultimo,
                                  min (decode (modulo, 1, periodo_id, null)) p1,
                                  min (decode (modulo, 2, periodo_id, null)) p2,
                                  min (decode (modulo, 3, periodo_id, null)) p3,
                                  min (decode (modulo, 4, periodo_id, null)) p4,
                                  min (decode (modulo, 5, periodo_id, null)) p5,
                                  min (decode (modulo, 6, periodo_id, null)) p6, 'N' consecutivos
                         from     (select xxx.*,
                                          count (*) over (partition by asignatura_id, bloque_id, especialidad_id, grupo_id)
                                                                                                                cuantos
                                   from   (select x.*, trunc ((x.num - 1) / periodos_obligatorios) + 1 grupo_id,
                                                  mod (num - 1, periodos_obligatorios) + 1 modulo
                                           from   (select a.id asignatura_id, a.nombre asignatura, b.id bloque_id,
                                                          b.nombre bloque, numero_obligatorias, numero_optativas,
                                                          e.id especialidad_id, e.nombre especialidad,
                                                          periodos_obligatorios, p.id periodo_id, f.id fase_id,
                                                          f.nombre fase,
                                                          row_number () over (partition by a.id, b.id, e.id order by p.id)
                                                                                                                    num
                                                   from   pho_asignaturas a,
                                                          pho_bloques b,
                                                          pho_especialidades e,
                                                          pho_fases f,
                                                          pho_periodos p
                                                   where  a.id = b.asignatura_id
                                                   and    b.id = e.bloque_id
                                                   and    f.id = 1
                                                   and    f.tipo = e.tipo
                                                   and    periodo_inicial_id is null
                                                   and    p.id in (select periodo_id
                                                                   from   pho_especialidades_periodos
                                                                   where  especialidad_id = e.id)
                                                                                                 --and    e.id in (1,2)
                                                  ) x) xxx)
                         where    periodos_obligatorios = cuantos
                         group by asignatura_id,
                                  asignatura,
                                  bloque_id,
                                  bloque,
                                  especialidad_id,
                                  especialidad,
                                  fase_id,
                                  fase,
                                  grupo_id,
                                  periodos_obligatorios) o,
                        pho_limites l,
                        pho_hospitales h
                 where  o.especialidad_id = l.especialidad_id
                 and    hospital_id = h.id) b)
   select b.unico_id || '_' || p.id id, b."UNICO_ID", b."ASIGNATURA_ID", b."ASIGNATURA", b."BLOQUE_ID", b."BLOQUE",
          b."ESPECIALIDAD_ID", b."ESPECIALIDAD", b.fase_id, b.fase, b."GRUPO_ID",
          pack_pho_practicas.pho_buscar_ultimo_cons (p.id, p1, p2, p3, p4, p5, p6) - p.id + 1 "TAMA�O", p.id "PRIMERO",
          pack_pho_practicas.pho_buscar_ultimo_cons (p.id, p1, p2, p3, p4, p5, p6) "ULTIMO", b."P1", b."P2", b."P3",
          b."P4", b."P5", b."P6", b."CONSECUTIVOS", b."PLAZAS", b."HOSPITAL_ID", b."HOSPITAL", b."CODIGO"
   from   base b,
          (select id, id - 1 periodo_ant, nombre
           from   pho_periodos) p
   where  (   p1 = p.id
           or (    p2 = p.id
               and p1 != p.periodo_ant)
           or (    p3 = p.id
               and p2 != p.periodo_ant)
           or (    p4 = p.id
               and p3 != p.periodo_ant)
           or (    p5 = p.id
               and p4 != p.periodo_ant)
           or (    p6 = p.id
               and p5 != p.periodo_ant)
          ) 
;






-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                            16
-- CREATE INDEX                            40
-- ALTER TABLE                             41
-- CREATE VIEW                              5
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- TSDP POLICY                              0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0


CREATE OR REPLACE package UJI_PRACTICASHOSPITALARIAS.pack_generar is
   procedure generar_fase_1;

   procedure generar_fase_2;

   procedure borrar_no_asignados;

   procedure generar_muestra_fase_1;
end;

CREATE OR REPLACE package body UJI_PRACTICASHOSPITALARIAS.pack_generar is
   procedure generar_fase_1 is
      v_id    number;
      v_id2   number;

      cursor lista is
         select b.*, c.id curso_academico_id, decode (bloque_unido, 'S', 1, 0) unido, p1 p_primero,
                nvl (p6, nvl (p5, nvl (p4, nvl (p3, nvl (p2, p1))))) p_ultimo
         from   pho_vw_oferta_base b,
                pho_cursos c
         where  c.activo = 1
         and    fase_id = 1
                           /*and    (   (    bloque_id = 2
                                       and especialidad_id = 21)
                                   or (    bloque_id = 1
                                       and especialidad_id = 1))
                                       */
      ;
   begin
      delete      pho_ofertas_periodos;

      delete      pho_oferta;

      update pho_turnos
      set fecha = trunc (sysdate) + 8 / 24
      where  fase_id = 1;

      update pho_turnos
      set fecha = trunc (sysdate) + 1 + 8 / 24
      where  fase_id = 2;

      pack_pho_practicas.refrescar_flag (65394, 1);
      pack_pho_practicas.refrescar_flag (65394, 2);
      pack_pho_practicas.refrescar_flag (591555, 1);
      pack_pho_practicas.refrescar_flag (591555, 2);

      update pho_fases
      set fecha_Desde = trunc (sysdate),
          fecha_hasta = trunc (sysdate) + 1
      where  id = 1;

      update pho_fases
      set fecha_Desde = trunc (sysdate) + 2,
          fecha_hasta = trunc (sysdate) + 3
      where  id = 2;

      for x in lista loop
         --for p in 1 .. x.plazas loop
         for p in 1 .. 1 loop
            v_id := hibernate_sequence.nextval;

            if x.unido = 1 then
               insert into pho_oferta
                           (ID, CURSO_ACADEMICO_ID, ASIGNATURA_ID, BLOQUE_ID, ESPECIALIDAD_ID, FASE_ID,
                            HOSPITAL_ID, PERIODO_INICIAL_ID, PERIODO_FINAL_ID, PERSONA_ID, FECHA,
                            UNICO_ID,
                            ORDEN_1, ORDEN_2, ORDEN_3, bloque_unido
                           )
               values      (v_id, x.curso_Academico_id, x.asignatura_id, x.bloque_id, x.ESPECIALIDAD_ID, x.FASE_ID,
                            x.HOSPITAL_ID, x.p_primero, x.p_ultimo, null, null,
                            x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.hospital_id
                            || '_' || x.grupo_id,
                            nvl (x.ORDEN_1, 9999), nvl (x.ORDEN_2, 9999), nvl (x.ORDEN_3, 9999), x.unido
                           );
            else
               insert into pho_oferta
                           (ID, CURSO_ACADEMICO_ID, ASIGNATURA_ID, BLOQUE_ID, ESPECIALIDAD_ID, FASE_ID,
                            HOSPITAL_ID, PERIODO_INICIAL_ID, PERIODO_FINAL_ID, PERSONA_ID, FECHA,
                            UNICO_ID,
                            ORDEN_1, ORDEN_2, ORDEN_3, bloque_unido
                           )
               values      (v_id, x.curso_Academico_id, x.asignatura_id, x.bloque_id, x.ESPECIALIDAD_ID, x.FASE_ID,
                            x.HOSPITAL_ID, x.p_primero, x.p_ultimo, null, null,
                            x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.hospital_id
                            || '_' || x.grupo_id,
                            nvl (x.ORDEN_1, 9999), nvl (x.ORDEN_2, 9999), nvl (x.ORDEN_3, 9999), x.unido
                           );

               if x.p1 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values      (v_id2, v_id, x.p1);
               end if;

               if x.p2 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values      (v_id2, v_id, x.p2);
               end if;

               if x.p3 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values      (v_id2, v_id, x.p3);
               end if;

               if x.p4 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values      (v_id2, v_id, x.p4);
               end if;

               if x.p5 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values      (v_id2, v_id, x.p5);
               end if;

               if x.p6 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values      (v_id2, v_id, x.p6);
               end if;
            end if;
         end loop;

         null;
      end loop;

      commit;
   end;

   procedure generar_fase_2 is
      v_id       number;
      v_id2      number;
      v_error    number;
      v_libres   number;

      cursor lista is
         select xxx.*, plazas - ocupacion libres, c.id curso_academico_id, decode (bloque_unido, 'S', 1, 0) unido
         from   pho_Vw_oferta_base_2 xxx,
                pho_cursos c
         where  c.activo = 1
         and    fase_id = 2
                           --and    asignatura_id = 1
                           --and    bloque_id = 2
                                               --and    especialidad_id = 3
      ;
   begin
      borrar_no_asignados;

      update pho_turnos
      set fecha = trunc (sysdate) - 2 + 8 / 24
      where  fase_id = 1;

      update pho_turnos
      set fecha = trunc (sysdate) + 8 / 24
      where  fase_id = 2;

      pack_pho_practicas.refrescar_flag (65394, 1);
      pack_pho_practicas.refrescar_flag (65394, 2);
      pack_pho_practicas.refrescar_flag (591555, 1);
      pack_pho_practicas.refrescar_flag (591555, 2);

      update pho_fases
      set fecha_Desde = trunc (sysdate) - 2,
          fecha_hasta = trunc (sysdate) - 1
      where  id = 1;

      update pho_fases
      set fecha_Desde = trunc (sysdate),
          fecha_hasta = trunc (sysdate) + 1
      where  id = 2;

      v_error := 1;

      for x in lista loop
         v_error := 11;
         v_libres := x.libres;

         if v_libres > 1 then
            v_libres := 1;
         end if;

         for p in 1 .. v_libres loop
            --dbms_output.put_line ('P: ' || p);
            v_error := 2;
            v_id := hibernate_sequence.nextval;
            v_error := 3;

            if x.unido = 1 then
               v_error := 4;

               begin
                  insert into pho_oferta
                              (ID, CURSO_ACADEMICO_ID, ASIGNATURA_ID, BLOQUE_ID, ESPECIALIDAD_ID, FASE_ID,
                               HOSPITAL_ID, PERIODO_INICIAL_ID, PERIODO_FINAL_ID, PERSONA_ID, FECHA,
                               UNICO_ID,
                               ORDEN_1, ORDEN_2, ORDEN_3, bloque_unido
                              )
                  values      (v_id, x.curso_Academico_id, x.asignatura_id, x.bloque_id, x.ESPECIALIDAD_ID, x.FASE_ID,
                               x.HOSPITAL_ID, x.primero, x.ultimo, null, null,
                               x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.hospital_id
                               || '_' || x.grupo_id,
                               nvl (x.ORDEN_1, 9999), nvl (x.ORDEN_2, 9999), nvl (x.ORDEN_3, 9999), x.unido
                              );
               exception
                  when others then
                     dbms_output.put_line (v_error || ' - ' || sqlerrm || x.asignatura_id || ' ' || x.bloque_id || ' '
                                           || x.especialidad_id || ' ' || x.hospital_id || ' - ' || x.fase_id);
               end;
            else
               v_error := 5;

               insert into pho_oferta
                           (ID, CURSO_ACADEMICO_ID, ASIGNATURA_ID, BLOQUE_ID, ESPECIALIDAD_ID, FASE_ID,
                            HOSPITAL_ID, PERIODO_INICIAL_ID, PERIODO_FINAL_ID, PERSONA_ID, FECHA,
                            UNICO_ID,
                            ORDEN_1, ORDEN_2, ORDEN_3, bloque_unido
                           )
               values      (v_id, x.curso_Academico_id, x.asignatura_id, x.bloque_id, x.ESPECIALIDAD_ID, x.FASE_ID,
                            x.HOSPITAL_ID, x.primero, x.ultimo, null, null,
                            x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.hospital_id
                            || '_' || x.grupo_id,
                            nvl (x.ORDEN_1, 9999), nvl (x.ORDEN_2, 9999), nvl (x.ORDEN_3, 9999), x.unido
                           );

               v_error := 6;

               if x.p1 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values      (v_id2, v_id, x.p1);
               end if;

               if x.p2 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values      (v_id2, v_id, x.p2);
               end if;

               if x.p3 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values      (v_id2, v_id, x.p3);
               end if;

               if x.p4 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values      (v_id2, v_id, x.p4);
               end if;

               if x.p5 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values      (v_id2, v_id, x.p5);
               end if;

               if x.p6 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values      (v_id2, v_id, x.p6);
               end if;
            end if;
         end loop;

         null;
         commit;
      end loop;
   exception
      when others then
         dbms_output.put_line (v_error || ' - ' || sqlerrm);
   end;

   procedure borrar_no_asignados is
   begin
      delete      pho_ofertas_periodos
      where       oferta_id in (select id
                                from   pho_oferta
                                where  persona_id is null);

      delete      pho_oferta
      where       persona_id is null;

      commit;
   end;

   procedure generar_muestra_Fase_1 is
   begin
      update pho_oferta o
      set persona_id = 591555
      where  unico_id = '1_1_1_1_1'
      and    persona_id is null
      and    id = (select min (id)
                   from   pho_oferta o2
                   where  persona_id is null
                   and    o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where  unico_id = '1_2_21_1_3'
      and    persona_id is null
      and    id = (select min (id)
                   from   pho_oferta o2
                   where  persona_id is null
                   and    o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where  unico_id = '4_5_35_5_5'
      and    persona_id is null
      and    id = (select min (id)
                   from   pho_oferta o2
                   where  persona_id is null
                   and    o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where  unico_id = '2_3_31_2_2'
      and    persona_id is null
      and    id = (select min (id)
                   from   pho_oferta o2
                   where  persona_id is null
                   and    o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where  unico_id = '3_4_32_1_3'
      and    persona_id is null
      and    id = (select min (id)
                   from   pho_oferta o2
                   where  persona_id is null
                   and    o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where  unico_id = '5_6_33_1_4'
      and    persona_id is null
      and    id = (select min (id)
                   from   pho_oferta o2
                   where  persona_id is null
                   and    o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where  unico_id = '5_7_34_8_17'
      and    persona_id is null
      and    id = (select min (id)
                   from   pho_oferta o2
                   where  persona_id is null
                   and    o.unico_id = o2.unico_id);

      commit;
   end;
end;


CREATE OR REPLACE package UJI_PRACTICASHOSPITALARIAS.pack_pho_practicas is
   procedure asignar (
      p_persona_id        in   number,
      p_fase_id           in   number,
      p_oferta_id         in   number,
      p_periodo_inicial   in   number,
      p_periodo_final     in   number,
      p_bloque_unido      in   number
   );

   procedure desasignar (
      p_persona_id        in   number,
      p_fase_id           in   number,
      p_oferta_id         in   number,
      p_periodo_inicial   in   number,
      p_periodo_final     in   number,
      p_bloque_unido      in   number
   );

   function periodos_disponibles (p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number;

   function especialidad_disponible (p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number;

   function bloque_disponible (p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number;

   function pho_buscar_ultimo_cons (
      p_inicial   in   number,
      p1          in   number,
      p2          in   number,
      p3          in   number,
      p4          in   number,
      p5          in   number,
      p6          in   number
   )
      return number;

   procedure refrescar_flag (p_persona_id in number, p_Fase_id in number);

   function plazas_ocupadas (
      p_asignatura     in   number,
      p_bloque         in   number,
      p_especialidad   in   number,
      p_hospital       in   number,
      p_periodo        in   number
   )
      return number;

   function oferta_plazas (
      p_asignatura     in   number,
      p_bloque         in   number,
      p_especialidad   in   number,
      p_hospital       in   number,
      p_periodo        in   number
   )
      return number;
end;



CREATE OR REPLACE package body UJI_PRACTICASHOSPITALARIAS.pack_pho_practicas is
   procedure asignar_periodo (p_persona_id in number, p_fase_id in number, p_periodo_id in number) is
      v_flag      varchar2 (24);
      v_antes     varchar2 (100);
      v_despues   varchar2 (100);
      v_aux       number;
      v_curso     number;
   begin
      select curso_academico_id
      into   v_curso
      from   pho_fases
      where  id = p_fase_id;

      begin
         select flags
         into   v_flag
         from   pho_matriculas
         where  persona_id = p_persona_id
         and    curso_academico_id = v_curso;
      exception
         when no_Data_found then
            select nvl (max (id), 0) + 1
            into   v_aux
            from   pho_matriculas;

            insert into pho_matriculas
                        (id, persona_id, curso_academico_id
                        )
            values      (v_aux, p_persona_id, v_curso
                        );

            select flags
            into   v_flag
            from   pho_matriculas
            where  persona_id = p_persona_id
            and    curso_academico_id = v_curso;
      end;

      v_antes := substr (v_flag, 1, p_periodo_id - 1);
      v_despues := substr (v_flag, p_periodo_id + 1);

      update pho_matriculas
      set flags = v_antes || '1' || v_Despues
      where  persona_id = p_persona_id
      and    curso_academico_id = v_curso;
   end;

   procedure desasignar_periodo (p_persona_id in number, p_fase_id in number, p_periodo_id in number) is
      v_flag      varchar2 (24);
      v_antes     varchar2 (100);
      v_despues   varchar2 (100);
      v_aux       number;
      v_curso     number;
   begin
      select curso_academico_id
      into   v_curso
      from   pho_fases
      where  id = p_fase_id;

      begin
         select flags
         into   v_flag
         from   pho_matriculas
         where  persona_id = p_persona_id
         and    curso_academico_id = v_curso;
      exception
         when no_Data_found then
            select nvl (max (id), 0) + 1
            into   v_aux
            from   pho_matriculas;

            insert into pho_matriculas
                        (id, persona_id, curso_academico_id
                        )
            values      (v_aux, p_persona_id, v_curso
                        );

            select flags
            into   v_flag
            from   pho_matriculas
            where  persona_id = p_persona_id
            and    curso_academico_id = v_curso;
      end;

      v_antes := substr (v_flag, 1, p_periodo_id - 1);
      v_despues := substr (v_flag, p_periodo_id + 1);

      update pho_matriculas
      set flags = v_antes || '0' || v_Despues
      where  persona_id = p_persona_id
      and    curso_academico_id = v_curso;
   end;

   procedure asignar (
      p_persona_id        in   number,
      p_fase_id           in   number,
      p_oferta_id         in   number,
      p_periodo_inicial   in   number,
      p_periodo_final     in   number,
      p_bloque_unido      in   number
   ) is
      cursor lista_periodos (p_oferta_id in number) is
         select   periodo_id
         from     pho_ofertas_periodos
         where    oferta_id = p_oferta_id
         order by periodo_id;
   begin
      if p_bloque_unido = 1 then
         for x in p_periodo_inicial .. p_periodo_final loop
            asignar_periodo (p_persona_id, p_fase_id, x);
         end loop;
      else
         for x in lista_periodos (p_oferta_id) loop
            asignar_periodo (p_persona_id, p_fase_id, x.periodo_id);
         end loop;
      end if;
   end;

   procedure desasignar (
      p_persona_id        in   number,
      p_fase_id           in   number,
      p_oferta_id         in   number,
      p_periodo_inicial   in   number,
      p_periodo_final     in   number,
      p_bloque_unido      in   number
   ) is
      cursor lista_periodos (p_oferta_id in number) is
         select   periodo_id
         from     pho_ofertas_periodos
         where    oferta_id = p_oferta_id
         order by periodo_id;
   begin
      if p_bloque_unido = 1 then
         for x in p_periodo_inicial .. p_periodo_final loop
            desasignar_periodo (p_persona_id, p_fase_id, x);
         end loop;
      else
         for x in lista_periodos (p_oferta_id) loop
            desasignar_periodo (p_persona_id, p_fase_id, x.periodo_id);
         end loop;
      end if;
   end;

   function periodos_disponibles (p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number is
      v_oferta_id      number;
      v_bloque_unido   number;
      v_inicial        number;
      v_final          number;
      v_rdo            number;
      v_flag           varchar2 (24);
      v_error          number;
      sin_oferta       exception;

      cursor lista_periodos (p_oferta_id in number) is
         select   periodo_id
         from     pho_ofertas_periodos
         where    oferta_id = p_oferta_id
         order by periodo_id;
   begin
      v_error := -10;

      begin
         select distinct min (id), bloque_unido, periodo_inicial_id, periodo_final_id
         into            v_oferta_id, v_bloque_unido, v_inicial, v_final
         from            pho_oferta
         where           unico_id = p_unico
         and             fase_id = p_fase_id
         group by        bloque_unido,
                         periodo_inicial_id,
                         periodo_final_id;
      exception
         when others then
            raise sin_oferta;
      end;

      v_error := -20;

      select flags
      into   v_flag
      from   pho_matriculas
      where  persona_id = p_persona_id
      and    curso_academico_id = (select curso_academico_id
                                   from   pho_Fases
                                   where  id = p_fase_id);

      v_error := -30;
      v_rdo := 0;

      if v_bloque_unido = 1 then
         for x in v_inicial .. v_final loop
            if v_rdo = 0 then
               v_rdo := substr (v_flag, x, 1);
            end if;
         end loop;
      else
         for x in lista_periodos (v_oferta_id) loop
            if v_rdo = 0 then
               v_rdo := substr (v_flag, x.periodo_id, 1);
            end if;
         end loop;
      end if;

      v_error := -40;

      if v_rdo = 1 then
         return (0);
      else
         return (1);
      end if;
   exception
      when sin_oferta then
         return (0);
      when others then
         return (v_error);
   end;

   function especialidad_disponible (p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number is
      v_especialidad   number;
      v_matriculada    number;
      v_rdo            number;
   begin
      select distinct especialidad_id
      into            v_especialidad
      from            pho_oferta
      where           unico_id = p_unico;

      select decode (count (*), 0, 0, 1)
      into   v_matriculada
      from   pho_oferta
      where  persona_id = p_persona_id
      and    especialidad_id = v_especialidad;

      if v_matriculada = 0 then
         v_rdo := 1;
      else
         v_rdo := 0;
      end if;

      return (v_rdo);
   end;

   function bloque_disponible (p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number is
      v_bloque      number;
      v_maximo      number;
      v_asignados   number;
      v_rdo         number;
   begin
      select distinct bloque_id
      into            v_bloque
      from            pho_oferta
      where           unico_id = p_unico;

      select decode (f.tipo,
                     'OB', numero_obligatorias,
                     'OB/OP', decode (f.id, 1, numero_obligatorias, numero_optativas),
                     numero_optativas
                    )
      into   v_maximo
      from   pho_bloques b,
             pho_fases f
      where  b.id = v_bloque
      and    f.id = p_Fase_id;

      select count (*)
      into   v_asignados
      from   pho_oferta
      where  persona_id = p_persona_id
      and    bloque_id = v_bloque
      and    fase_id = p_Fase_id;

      if v_asignados < v_maximo then
         v_rdo := 1;
      else
         v_rdo := 0;
      end if;

      return (v_rdo);
   end;

   function pho_buscar_ultimo_cons (
      p_inicial   in   number,
      p1          in   number,
      p2          in   number,
      p3          in   number,
      p4          in   number,
      p5          in   number,
      p6          in   number
   )
      return number is
      v_rdo     number;
      v_aux     number;
      v_ult     number;
      valores   euji_util.ident_arr2;
   begin
      valores (1) := p1;
      valores (2) := p2;
      valores (3) := p3;
      valores (4) := p4;
      valores (5) := p5;
      valores (6) := p6;
      v_ult := 0;

      for x in 1 .. 6 loop
         if valores (x) > 0 then
            v_ult := v_ult + 1;
         end if;
      end loop;

      for x in 1 .. v_ult loop
         if valores (x) < p_inicial then
            null;
            null;
         elsif valores (x) = p_inicial then
            v_rdo := valores (x);
         else
            if valores (x) = v_rdo + 1 then
               v_rdo := valores (x);
            end if;
         end if;
      end loop;

      if v_ult = 0 then
         v_rdo := -1;
      end if;

      return (v_rdo);
   end;

   procedure refrescar_flag (p_persona_id in number, p_Fase_id in number) is
      v_flag    varchar2 (24) := '000000000000000000000000';
      v_aux     number;
      v_curso   number;

      cursor lista_matricula is
         select fase_id, primero, ultimo
         from   pho_vw_oferta
         where  persona_id = p_persona_id
         and    fase_id <= p_FAse_id;

      function asignar_flag (p_flag in varchar2, p_periodo_id in number)
         return varchar2 is
         v_flag      varchar2 (24);
         v_antes     varchar2 (100);
         v_despues   varchar2 (100);
         v_valor     varchar2 (1);
      begin
         v_flag := p_flag;
         v_antes := substr (v_flag, 1, p_periodo_id - 1);
         v_despues := substr (v_flag, p_periodo_id + 1);
         v_valor := substr (v_flag, p_periodo_id, 1);
         v_valor := to_char (to_number (v_valor) + 1);
         return (v_antes || v_Valor || v_Despues);
      end;
   begin
      select curso_academico_id
      into   v_curso
      from   pho_fases
      where  id = p_fase_id;

      select count (*)
      into   v_aux
      from   pho_matriculas
      where  persona_id = p_persona_id
      and    curso_academico_id = v_curso;

      if v_aux = 0 then
         select nvl (max (id), 0) + 1
         into   v_aux
         from   pho_matriculas;

         insert into pho_matriculas
                     (ID, PERSONA_ID, CURSO_ACADEMICO_ID
                     )
         values      (v_aux, p_persona_id, v_curso
                     );

         commit;
      end if;

      for x in lista_matricula loop
         for p in x.primero .. x.ultimo loop
            v_flag := asignar_flag (v_flag, p);
         end loop;
      end loop;

      update pho_matriculas
      set flags = v_flag
      where  persona_id = p_persona_id
      and    curso_academico_id = (select curso_academico_id
                                   from   pho_fases
                                   where  id = p_fase_id);

      commit;
   end;

   function plazas_ocupadas (
      p_asignatura     in   number,
      p_bloque         in   number,
      p_especialidad   in   number,
      p_hospital       in   number,
      p_periodo        in   number
   )
      return number is
      v_rdo   number;
   begin
      select sum (plazas)
      into   v_Rdo
      from   (select   asignatura_id, bloque_id, especialidad_id, hospital_id, p.id, count (*) plazas
              from     pho_oferta o,
                       pho_periodos p
              where    bloque_unido = 1
              and      persona_id is not null
              and      p.id between periodo_inicial_id and periodo_final_id
              and      asignatura_id = p_asignatura
              and      bloque_id = p_bloque
              and      especialidad_id = p_especialidad
              and      hospital_id = p_hospital
              and      p.id = p_periodo
              group by asignatura_id,
                       bloque_id,
                       especialidad_id,
                       hospital_id,
                       p.id
              union all
              select   asignatura_id, bloque_id, especialidad_id, hospital_id, p.id, count (*) plazas
              from     pho_oferta o,
                       pho_ofertas_periodos op,
                       pho_periodos p
              where    bloque_unido = 0
              and      persona_id is not null
              and      o.id = op.oferta_id
              and      op.periodo_id = p.id
              and      asignatura_id = p_asignatura
              and      bloque_id = p_bloque
              and      especialidad_id = p_especialidad
              and      hospital_id = p_hospital
              and      p.id = p_periodo
              group by asignatura_id,
                       bloque_id,
                       especialidad_id,
                       hospital_id,
                       p.id);

      return (nvl (v_rdo, 0));
   end;

   function oferta_plazas (
      p_asignatura     in   number,
      p_bloque         in   number,
      p_especialidad   in   number,
      p_hospital       in   number,
      p_periodo        in   number
   )
      return number is
      v_rdo   number;
   begin
      select sum (plazas)
      into   v_Rdo
      from   (select   asignatura_id, bloque_id, especialidad_id, hospital_id, p.id, count (*) plazas
              from     pho_oferta o,
                       pho_periodos p
              where    bloque_unido = 1
              and      p.id between periodo_inicial_id and periodo_final_id
              and      asignatura_id = p_asignatura
              and      bloque_id = p_bloque
              and      especialidad_id = p_especialidad
              and      hospital_id = p_hospital
              and      p.id = p_periodo
              group by asignatura_id,
                       bloque_id,
                       especialidad_id,
                       hospital_id,
                       p.id
              union all
              select   asignatura_id, bloque_id, especialidad_id, hospital_id, p.id, count (*) plazas
              from     pho_oferta o,
                       pho_ofertas_periodos op,
                       pho_periodos p
              where    bloque_unido = 0
              and      o.id = op.oferta_id
              and      op.periodo_id = p.id
              and      asignatura_id = p_asignatura
              and      bloque_id = p_bloque
              and      especialidad_id = p_especialidad
              and      hospital_id = p_hospital
              and      p.id = p_periodo
              group by asignatura_id,
                       bloque_id,
                       especialidad_id,
                       hospital_id,
                       p.id);

      return (nvl (v_rdo, 0));
   end;
end;



DROP SEQUENCE UJI_PRACTICASHOSPITALARIAS.HIBERNATE_SEQUENCE;

CREATE SEQUENCE UJI_PRACTICASHOSPITALARIAS.HIBERNATE_SEQUENCE
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;
  
  
  
CREATE OR REPLACE TRIGGER UJI_PRACTICASHOSPITALARIAS.pho_oferta_trg
   BEFORE UPDATE OF PERSONA_ID
   ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA    REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
BEGIN
   if     :new.persona_id is null
      and :old.persona_id is not null then
      :new.fecha := null;
      pack_pho_practicas.desasignar (:old.persona_id, :new.fase_id, :new.id, :new.periodo_inicial_id,
                                     :new.periodo_final_id, :new.bloque_unido);
   end if;

   if     :new.persona_id is not null
      and :old.persona_id is null then
      :new.fecha := sysdate;
      pack_pho_practicas.asignar (:new.persona_id, :new.fase_id, :new.id, :new.periodo_inicial_id,
                                  :new.periodo_final_id, :new.bloque_unido);
   end if;

   if     :new.persona_id is not null
      and :old.persona_id is not null
      and :new.persona_id <> :old.persona_id then
      RAISE_application_error (-20001, 'Una oferta assignada no es pot reassignar a un altra persona.');
   end if;
END;


ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
 ADD (control  VARCHAR2(200))
;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
 ADD (control2  VARCHAR2(200))
;


update pho_oferta
set control = curso_academico_id || '_' || especialidad_id || '_'
      || nvl (to_char (persona_id), 'n' || to_char (id)),
   control2 =
   curso_academico_id || '_' || bloque_id || '_' || fase_id || '_'
      || nvl (to_char (persona_id), 'n' || to_char (id));


ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
MODIFY(CONTROL  NOT NULL)
;

ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
MODIFY(CONTROL2  NOT NULL)
;

CREATE OR REPLACE TRIGGER UJI_PRACTICASHOSPITALARIAS.pho_ofertas_ins_trg
   BEFORE INSERT
   ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
BEGIN
   :new.control :=
      :new.curso_academico_id || '_' || :new.especialidad_id || '_'
      || nvl (to_char (:new.persona_id), 'n' || to_char (:new.id));
   :new.control2 :=
      :new.curso_academico_id || '_' || :new.bloque_id || '_' || :new.fase_id || '_'
      || nvl (to_char (:new.persona_id), 'n' || to_char (:new.id));
END;

CREATE OR REPLACE TRIGGER UJI_PRACTICASHOSPITALARIAS.pho_oferta_trg
   BEFORE UPDATE OF PERSONA_ID
   ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
BEGIN
   :new.control :=
      :new.curso_academico_id || '_' || :new.especialidad_id || '_'
      || nvl (to_char (:new.persona_id), 'n' || to_char (:new.id));
   :new.control2 :=
      :new.curso_academico_id || '_' || :new.bloque_id || '_' || :new.fase_id || '_'
      || nvl (to_char (:new.persona_id), 'n' || to_char (:new.id));

   if     :new.persona_id is null
      and :old.persona_id is not null then
      :new.fecha := null;
      pack_pho_practicas.desasignar (:old.persona_id, :new.fase_id, :new.id, :new.periodo_inicial_id,
                                     :new.periodo_final_id, :new.bloque_unido);
   end if;

   if     :new.persona_id is not null
      and :old.persona_id is null then
      :new.fecha := sysdate;
      pack_pho_practicas.asignar (:new.persona_id, :new.fase_id, :new.id, :new.periodo_inicial_id,
                                  :new.periodo_final_id, :new.bloque_unido);
   end if;

   if     :new.persona_id is not null
      and :old.persona_id is not null
      and :new.persona_id <> :old.persona_id then
      RAISE_application_error (-20001, 'Una oferta assignada no es pot reassignar a un altra persona.');
   end if;
END;


ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD (
  CONSTRAINT PHO_OFERTA_BLQ_FASE_UK
 UNIQUE (CONTROL2));

  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD (
  CONSTRAINT PHO_OFERTA_ESP_UK
 UNIQUE (CONTROL));

CREATE OR REPLACE VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA ( ID, UNICO_ID, ASIGNATURA_ID, ASIGNATURA, CODIGO_ASIGNATURA, BLOQUE_ID, BLOQUE, ESPECIALIDAD_ID, ESPECIALIDAD, FASE_ID, FASE, FASE_TIPO, GRUPO_ID, TAMAÑO, PRIMERO, ULTIMO, P1, P2, P3, P4, P5, P6, CONSECUTIVOS, PLAZAS, HOSPITAL_ID, HOSPITAL, CODIGO, PERSONA_ID, FECHA, FECHA_INICIO, FECHA_FIN, PERSONA_ID_MOSTRAR, ES_SELECCIONABLE, FASE_ACTUAL_ID, ORDEN_1, ORDEN_2, ORDEN_3 )
AS
  SELECT xxxx."ID",
    xxxx."UNICO_ID",
    xxxx."ASIGNATURA_ID",
    xxxx."ASIGNATURA",
    xxxx."CODIGO_ASIGNATURA",
    xxxx."BLOQUE_ID",
    xxxx."BLOQUE",
    xxxx."ESPECIALIDAD_ID",
    xxxx."ESPECIALIDAD",
    xxxx."FASE_ID",
    xxxx."FASE",
    xxxx."FASE_TIPO",
    xxxx."GRUPO_ID",
    xxxx."TAMAÑO",
    xxxx."PRIMERO",
    xxxx."ULTIMO",
    xxxx."P1",
    xxxx."P2",
    xxxx."P3",
    xxxx."P4",
    xxxx."P5",
    xxxx."P6",
    xxxx."CONSECUTIVOS",
    xxxx."PLAZAS",
    xxxx."HOSPITAL_ID",
    xxxx."HOSPITAL",
    xxxx."CODIGO",
    xxxx."PERSONA_ID",
    xxxx."FECHA",
    xxxx."FECHA_INICIO",
    xxxx."FECHA_FIN",
    xxxx."PERSONA_ID_MOSTRAR",
    xxxx."ES_SELECCIONABLE",
    xxxx."FASE_ACTUAL_ID",
    xxxx."ORDEN_1",
    xxxx."ORDEN_2",
    xxxx."ORDEN_3"
  FROM
    (SELECT ID,
      UNICO_ID,
      ASIGNATURA_ID,
      ASIGNATURA,
      CODIGO_ASIGNATURA,
      BLOQUE_ID,
      BLOQUE,
      ESPECIALIDAD_ID,
      ESPECIALIDAD,
      FASE_ID,
      FASE,
      FASE_TIPO,
      GRUPO_ID,
      TAMAÑO,
      PRIMERO,
      ULTIMO,
      P1,
      P2,
      P3,
      P4,
      P5,
      P6,
      CONSECUTIVOS,
      PLAZAS,
      HOSPITAL_ID,
      HOSPITAL,
      CODIGO,
      PERSONA_ID,
      FECHA,
      FECHA_INICIO,
      FECHA_FIN,
      PERSONA_ID_MOSTRAR,
      ES_SELECCIONABLE,
      FASE_ACTUAL_ID,
      orden_1,
      orden_2,
      orden_3
    FROM
      (SELECT "ID",
        "UNICO_ID",
        "ASIGNATURA_ID",
        "ASIGNATURA",
        "CODIGO_ASIGNATURA",
        "BLOQUE_ID",
        "BLOQUE",
        "ESPECIALIDAD_ID",
        "ESPECIALIDAD",
        "FASE_ID",
        "FASE",
        "FASE_TIPO",
        "GRUPO_ID",
        "TAMAÑO",
        "PRIMERO",
        "ULTIMO",
        "P1",
        "P2",
        "P3",
        "P4",
        "P5",
        "P6",
        "CONSECUTIVOS",
        "PLAZAS",
        "HOSPITAL_ID",
        "HOSPITAL",
        "CODIGO",
        "PERSONA_ID",
        "FECHA",
        "FECHA_INICIO",
        "FECHA_FIN",
        "PERSONA_ID_MOSTRAR",
        "ES_SELECCIONABLE",
        "FASE_ACTUAL_ID",
        row_number () over (partition BY persona_id_mostrar, id order by id, persona_id) orden,
        orden_1,
        orden_2,
        orden_3
      FROM
        (SELECT xx."ID",
          xx."UNICO_ID",
          xx."ASIGNATURA_ID",
          xx."ASIGNATURA",
          xx."CODIGO_ASIGNATURA",
          xx."BLOQUE_ID",
          xx."BLOQUE",
          xx."ESPECIALIDAD_ID",
          xx."ESPECIALIDAD",
          xx."FASE_ID",
          xx."FASE",
          xx."FASE_TIPO",
          xx."GRUPO_ID",
          xx."TAMAÑO",
          xx."PRIMERO",
          xx."ULTIMO",
          xx."P1",
          xx."P2",
          xx."P3",
          xx."P4",
          xx."P5",
          xx."P6",
          xx."CONSECUTIVOS",
          xx."PLAZAS",
          xx."HOSPITAL_ID",
          xx."HOSPITAL",
          xx."CODIGO",
          xx."PERSONA_ID",
          xx."FECHA",
          xx."FECHA_INICIO",
          xx."FECHA_FIN",
          ac.persona_id persona_id_mostrar,
          DECODE (xx.persona_id, ac.persona_id, 1, DECODE (pack_pho_practicas.bloque_disponible (ac.persona_id, fase_id, unico_id), 1, DECODE (pack_pho_practicas.especialidad_disponible (ac.persona_id, 1, unico_id), 1, DECODE (pack_pho_practicas.periodos_disponibles (ac.persona_id, fase_id, unico_id), 1, 1, 0 ), 0 ), 0 ) ) es_seleccionable,
          (SELECT id
          FROM pho_Fases
          WHERE TRUNC (sysdate) BETWEEN fecha_desde AND fecha_hasta
          ) fase_actual_id,
          orden_1,
          orden_2,
          orden_3
        FROM
          (SELECT x.unico_id
            || '_'
            || x.periodo_inicial_id id,
            x.unico_id,
            x.asignatura_id,
            a.nombre asignatura,
            a.codigo_asignatura,
            x.bloque_id,
            b.nombre bloque,
            x.especialidad_id,
            e.nombre especialidad,
            x.fase_id,
            f.nombre fase,
            NULL grupo_id,
            x.periodo_final_id - x.periodo_inicial_id + 1 tamaño,
            x.periodo_inicial_id primero,
            x.periodo_final_id ultimo,
            NULL p1,
            NULL p2,
            NULL p3,
            NULL p4,
            NULL p5,
            NULL p6,
            x.bloque_unido consecutivos,
            plazas,
            x.hospital_id,
            h.nombre hospital,
            h.codigo,
            x.persona_id,
            x.fecha,
            x.fecha_inicio,
            x.fecha_fin,
            orden_1,
            orden_2,
            orden_3,
            f.tipo fase_tipo
          FROM
            (SELECT o.curso_academico_id,
              o.asignatura_id,
              o.bloque_id,
              o.especialidad_id,
              o.fase_id,
              o.hospital_id,
              o.periodo_inicial_id,
              o.periodo_final_id,
              o.unico_id,
              o.bloque_unido,
              o.orden_1,
              o.orden_2,
              o.orden_3,
              p.fecha_inicio,
              p2.fecha_fin,
              persona_id,
              o.fecha,
              COUNT (*) plazas
            FROM pho_oferta o,
              pho_periodos_fechas p,
              pho_periodos_fechas p2
            WHERE o.bloque_unido     = 1
            AND o.persona_id        IS NULL
            AND o.periodo_inicial_id = p.periodo_id
            AND o.curso_academico_id = p.curso_Aca_id
            AND o.periodo_final_id   = p2.periodo_id
            AND o.curso_academico_id = p2.curso_Aca_id
            GROUP BY o.curso_academico_id,
              o.asignatura_id,
              o.bloque_id,
              o.especialidad_id,
              o.fase_id,
              o.hospital_id,
              o.periodo_inicial_id,
              o.periodo_final_id,
              o.unico_id,
              o.bloque_unido,
              o.orden_1,
              o.orden_2,
              o.orden_3,
              p.fecha_inicio,
              p2.fecha_fin,
              o.persona_id,
              o.fecha
            UNION ALL
            SELECT o.curso_academico_id,
              o.asignatura_id,
              o.bloque_id,
              o.especialidad_id,
              o.fase_id,
              o.hospital_id,
              o.periodo_inicial_id,
              o.periodo_final_id,
              o.unico_id,
              o.bloque_unido,
              o.orden_1,
              o.orden_2,
              o.orden_3,
              p.fecha_inicio,
              p2.fecha_fin,
              persona_id,
              o.fecha,
              COUNT (*) plazas
            FROM pho_oferta o,
              pho_periodos_fechas p,
              pho_periodos_fechas p2
            WHERE o.bloque_unido     = 1
            AND o.persona_id        IS NOT NULL
            AND o.periodo_inicial_id = p.periodo_id
            AND o.curso_academico_id = p.curso_Aca_id
            AND o.periodo_final_id   = p2.periodo_id
            AND o.curso_academico_id = p2.curso_Aca_id
            GROUP BY o.curso_academico_id,
              o.asignatura_id,
              o.bloque_id,
              o.especialidad_id,
              o.fase_id,
              o.hospital_id,
              o.periodo_inicial_id,
              o.periodo_final_id,
              o.unico_id,
              o.bloque_unido,
              o.orden_1,
              o.orden_2,
              o.orden_3,
              p.fecha_inicio,
              p2.fecha_fin,
              o.persona_id,
              o.fecha
            UNION ALL
            SELECT xx.curso_academico_id,
              xx.asignatura_id,
              xx.bloque_id,
              xx.especialidad_id,
              xx.fase_id,
              xx.hospital_id,
              p.id periodo_inicial_id,
              pack_pho_practicas.pho_buscar_ultimo_cons (p.id, p1, p2, p3, p4, p5, p6) periodo_final_id,
              xx.unico_id,
              xx.bloque_unido,
              xx.orden_1,
              xx.orden_2,
              xx.orden_3,
              xx.fecha_inicio,
              xx.fecha_fin,
              xx.persona_id,
              xx.fecha,
              xx.plazas
            FROM
              (SELECT x.curso_academico_id,
                x.asignatura_id,
                x.bloque_id,
                x.especialidad_id,
                x.fase_id,
                x.hospital_id,
                x.periodo_inicial_id,
                x.periodo_final_id,
                x.unico_id,
                x.bloque_unido,
                x.orden_1,
                x.orden_2,
                x.orden_3,
                x.fecha_inicio,
                x.fecha_fin,
                x.persona_id,
                x.fecha,
                x.plazas,
                MIN (DECODE (orden, 1, periodo_id, NULL)) p1,
                MIN (DECODE (orden, 2, periodo_id, NULL)) p2,
                MIN (DECODE (orden, 3, periodo_id, NULL)) p3,
                MIN (DECODE (orden, 4, periodo_id, NULL)) p4,
                MIN (DECODE (orden, 5, periodo_id, NULL)) p5,
                MIN (DECODE (orden, 6, periodo_id, NULL)) p6
              FROM
                (SELECT o.curso_academico_id,
                  o.asignatura_id,
                  o.bloque_id,
                  o.especialidad_id,
                  o.fase_id,
                  o.hospital_id,
                  o.periodo_inicial_id,
                  o.periodo_final_id,
                  o.unico_id,
                  o.bloque_unido,
                  o.orden_1,
                  o.orden_2,
                  o.orden_3,
                  p.fecha_inicio,
                  p2.fecha_fin,
                  persona_id,
                  fecha,
                  COUNT (*) plazas,
                  op.periodo_id,
                  row_number () over (partition BY o.curso_academico_id, o.asignatura_id, o.bloque_id, o.especialidad_id, o.fase_id, o.hospital_id, o.periodo_inicial_id, o.periodo_final_id, o.unico_id, o.bloque_unido, o.orden_1, o.orden_2, o.orden_3 order by op.periodo_id) orden
                FROM pho_oferta o,
                  pho_periodos_fechas p,
                  pho_periodos_fechas p2,
                  pho_ofertas_periodos op
                WHERE o.bloque_unido     = 0
                AND o.persona_id        IS NULL
                AND o.periodo_inicial_id = p.periodo_id
                AND o.curso_academico_id = p.curso_Aca_id
                AND o.periodo_final_id   = p2.periodo_id
                AND o.curso_academico_id = p2.curso_Aca_id
                AND o.id                 = op.oferta_id
                GROUP BY o.curso_academico_id,
                  o.asignatura_id,
                  o.bloque_id,
                  o.especialidad_id,
                  o.fase_id,
                  o.hospital_id,
                  o.periodo_inicial_id,
                  o.periodo_final_id,
                  o.unico_id,
                  o.bloque_unido,
                  o.orden_1,
                  o.orden_2,
                  o.orden_3,
                  p.fecha_inicio,
                  p2.fecha_fin,
                  o.persona_id,
                  o.fecha,
                  op.periodo_id
                ) x
              GROUP BY x.curso_academico_id,
                x.asignatura_id,
                x.bloque_id,
                x.especialidad_id,
                x.fase_id,
                x.hospital_id,
                x.periodo_inicial_id,
                x.periodo_final_id,
                x.unico_id,
                x.bloque_unido,
                x.orden_1,
                x.orden_2,
                x.orden_3,
                x.fecha_inicio,
                x.fecha_fin,
                x.persona_id,
                x.fecha,
                x.plazas
              ) xx,
              (SELECT id, id - 1 periodo_ant, nombre FROM pho_periodos
              ) p
            WHERE ( p1 = p.id
            OR ( p2    = p.id
            AND p1    != p.periodo_ant)
            OR ( p3    = p.id
            AND p2    != p.periodo_ant)
            OR ( p4    = p.id
            AND p3    != p.periodo_ant)
            OR ( p5    = p.id
            AND p4    != p.periodo_ant)
            OR ( p6    = p.id
            AND p5    != p.periodo_ant) )
            UNION ALL
            SELECT xx.curso_academico_id,
              xx.asignatura_id,
              xx.bloque_id,
              xx.especialidad_id,
              xx.fase_id,
              xx.hospital_id,
              p.id periodo_inicial_id,
              pack_pho_practicas.pho_buscar_ultimo_cons (p.id, p1, p2, p3, p4, p5, p6) periodo_final_id,
              xx.unico_id,
              xx.bloque_unido,
              xx.orden_1,
              xx.orden_2,
              xx.orden_3,
              xx.fecha_inicio,
              xx.fecha_fin,
              xx.persona_id,
              xx.fecha,
              xx.plazas
            FROM
              (SELECT x.curso_academico_id,
                x.asignatura_id,
                x.bloque_id,
                x.especialidad_id,
                x.fase_id,
                x.hospital_id,
                x.periodo_inicial_id,
                x.periodo_final_id,
                x.unico_id,
                x.bloque_unido,
                x.orden_1,
                x.orden_2,
                x.orden_3,
                x.fecha_inicio,
                x.fecha_fin,
                x.persona_id,
                x.fecha,
                x.plazas,
                MIN (DECODE (orden, 1, periodo_id, NULL)) p1,
                MIN (DECODE (orden, 2, periodo_id, NULL)) p2,
                MIN (DECODE (orden, 3, periodo_id, NULL)) p3,
                MIN (DECODE (orden, 4, periodo_id, NULL)) p4,
                MIN (DECODE (orden, 5, periodo_id, NULL)) p5,
                MIN (DECODE (orden, 6, periodo_id, NULL)) p6
              FROM
                (SELECT o.curso_academico_id,
                  o.asignatura_id,
                  o.bloque_id,
                  o.especialidad_id,
                  o.fase_id,
                  o.hospital_id,
                  o.periodo_inicial_id,
                  o.periodo_final_id,
                  o.unico_id,
                  o.bloque_unido,
                  o.orden_1,
                  o.orden_2,
                  o.orden_3,
                  p.fecha_inicio,
                  p2.fecha_fin,
                  persona_id,
                  fecha,
                  COUNT (*) plazas,
                  op.periodo_id,
                  row_number () over (partition BY o.curso_academico_id, o.asignatura_id, o.bloque_id, o.especialidad_id, o.fase_id, o.hospital_id, o.periodo_inicial_id, o.periodo_final_id, o.unico_id, o.bloque_unido, o.orden_1, o.orden_2, o.orden_3 order by op.periodo_id) orden
                FROM pho_oferta o,
                  pho_periodos_fechas p,
                  pho_periodos_fechas p2,
                  pho_ofertas_periodos op
                WHERE o.bloque_unido     = 0
                AND o.persona_id        IS NOT NULL
                AND o.periodo_inicial_id = p.periodo_id
                AND o.curso_academico_id = p.curso_Aca_id
                AND o.periodo_final_id   = p2.periodo_id
                AND o.curso_academico_id = p2.curso_Aca_id
                AND o.id                 = op.oferta_id
                GROUP BY o.curso_academico_id,
                  o.asignatura_id,
                  o.bloque_id,
                  o.especialidad_id,
                  o.fase_id,
                  o.hospital_id,
                  o.periodo_inicial_id,
                  o.periodo_final_id,
                  o.unico_id,
                  o.bloque_unido,
                  o.orden_1,
                  o.orden_2,
                  o.orden_3,
                  p.fecha_inicio,
                  p2.fecha_fin,
                  o.persona_id,
                  o.fecha,
                  op.periodo_id
                ) x
              GROUP BY x.curso_academico_id,
                x.asignatura_id,
                x.bloque_id,
                x.especialidad_id,
                x.fase_id,
                x.hospital_id,
                x.periodo_inicial_id,
                x.periodo_final_id,
                x.unico_id,
                x.bloque_unido,
                x.orden_1,
                x.orden_2,
                x.orden_3,
                x.fecha_inicio,
                x.fecha_fin,
                x.persona_id,
                x.fecha,
                x.plazas
              ) xx,
              (SELECT id, id - 1 periodo_ant, nombre FROM pho_periodos
              ) p
            WHERE ( p1 = p.id
            OR ( p2    = p.id
            AND p1    != p.periodo_ant)
            OR ( p3    = p.id
            AND p2    != p.periodo_ant)
            OR ( p4    = p.id
            AND p3    != p.periodo_ant)
            OR ( p5    = p.id
            AND p4    != p.periodo_ant)
            OR ( p6    = p.id
            AND p5    != p.periodo_ant) )
            ) x,
            pho_asignaturas a,
            pho_bloques b,
            pho_especialidades e,
            pho_fases f,
            pho_hospitales h
          WHERE x.asignatura_id = a.id
          AND x.bloque_id       = b.id
          AND x.especialidad_id = e.id
          AND x.fase_id         = f.id
          AND x.hospital_id     = h.id
          ) xx,
          pho_ext_asignaturas_cursadas ac
        WHERE ( xx.persona_id    = ac.persona_id
        OR xx.persona_id        IS NULL)
        AND xx.codigo_asignatura = ac.asignatura_id
        )
      ) xxx
    WHERE orden = 1
    ) xxxx
  WHERE ( fase_id = 1
  OR ( fase_id    = 2
  AND NOT EXISTS
    (SELECT 1
    FROM pho_oferta o
    WHERE o.persona_id    = xxxx.persona_id_mostrar
    AND o.especialidad_id = xxxx.especialidad_id
    AND fase_id           = 1
    ) ) ) ;
 

/* Formatted on 10/06/2016 11:44 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA (ID,
                                                                       UNICO_ID,
                                                                       ASIGNATURA_ID,
                                                                       ASIGNATURA,
                                                                       CODIGO_ASIGNATURA,
                                                                       BLOQUE_ID,
                                                                       BLOQUE,
                                                                       ESPECIALIDAD_ID,
                                                                       ESPECIALIDAD,
                                                                       FASE_ID,
                                                                       FASE,
                                                                       FASE_TIPO,
                                                                       GRUPO_ID,
                                                                       TAMAÑO,
                                                                       PRIMERO,
                                                                       ULTIMO,
                                                                       P1,
                                                                       P2,
                                                                       P3,
                                                                       P4,
                                                                       P5,
                                                                       P6,
                                                                       CONSECUTIVOS,
                                                                       PLAZAS,
                                                                       HOSPITAL_ID,
                                                                       HOSPITAL,
                                                                       CODIGO,
                                                                       PERSONA_ID,
                                                                       FECHA,
                                                                       FECHA_INICIO,
                                                                       FECHA_FIN,
                                                                       PERSONA_ID_MOSTRAR,
                                                                       ES_SELECCIONABLE,
                                                                       FASE_ACTUAL_ID,
                                                                       ORDEN_1,
                                                                       ORDEN_2,
                                                                       ORDEN_3
                                                                      ) AS
   select "ID", "UNICO_ID", "ASIGNATURA_ID", "ASIGNATURA", "CODIGO_ASIGNATURA", "BLOQUE_ID", "BLOQUE",
          "ESPECIALIDAD_ID", "ESPECIALIDAD", "FASE_ID", "FASE", "FASE_TIPO", "GRUPO_ID", "TAMAÑO", "PRIMERO", "ULTIMO",
          "P1", "P2", "P3", "P4", "P5", "P6", "CONSECUTIVOS", "PLAZAS", "HOSPITAL_ID", "HOSPITAL", "CODIGO",
          "PERSONA_ID", "FECHA", "FECHA_INICIO", "FECHA_FIN", "PERSONA_ID_MOSTRAR", "ES_SELECCIONABLE",
          "FASE_ACTUAL_ID", "ORDEN_1", "ORDEN_2", "ORDEN_3"
   from   (select xxxx."ID", xxxx."UNICO_ID", xxxx."ASIGNATURA_ID", xxxx."ASIGNATURA", xxxx."CODIGO_ASIGNATURA",
                  xxxx."BLOQUE_ID", xxxx."BLOQUE", xxxx."ESPECIALIDAD_ID", xxxx."ESPECIALIDAD", xxxx."FASE_ID",
                  xxxx."FASE", xxxx."FASE_TIPO", xxxx."GRUPO_ID", xxxx."TAMAÑO", xxxx."PRIMERO", xxxx."ULTIMO",
                  xxxx."P1", xxxx."P2", xxxx."P3", xxxx."P4", xxxx."P5", xxxx."P6", xxxx."CONSECUTIVOS", xxxx."PLAZAS",
                  xxxx."HOSPITAL_ID", xxxx."HOSPITAL", xxxx."CODIGO", xxxx."PERSONA_ID", xxxx."FECHA",
                  xxxx."FECHA_INICIO", xxxx."FECHA_FIN", xxxx."PERSONA_ID_MOSTRAR", xxxx."ES_SELECCIONABLE",
                  xxxx."FASE_ACTUAL_ID", xxxx."ORDEN_1", xxxx."ORDEN_2", xxxx."ORDEN_3"
           from   (select ID, UNICO_ID, ASIGNATURA_ID, ASIGNATURA, CODIGO_ASIGNATURA, BLOQUE_ID, BLOQUE,
                          ESPECIALIDAD_ID, ESPECIALIDAD, FASE_ID, FASE, FASE_TIPO, GRUPO_ID, TAMAÑO, PRIMERO, ULTIMO,
                          P1, P2, P3, P4, P5, P6, CONSECUTIVOS, PLAZAS, HOSPITAL_ID, HOSPITAL, CODIGO, PERSONA_ID,
                          FECHA, FECHA_INICIO, FECHA_FIN, PERSONA_ID_MOSTRAR, ES_SELECCIONABLE, FASE_ACTUAL_ID, orden_1,
                          orden_2, orden_3
                   from   (select "ID", "UNICO_ID", "ASIGNATURA_ID", "ASIGNATURA", "CODIGO_ASIGNATURA", "BLOQUE_ID",
                                  "BLOQUE", "ESPECIALIDAD_ID", "ESPECIALIDAD", "FASE_ID", "FASE", "FASE_TIPO",
                                  "GRUPO_ID", "TAMAÑO", "PRIMERO", "ULTIMO", "P1", "P2", "P3", "P4", "P5", "P6",
                                  "CONSECUTIVOS", "PLAZAS", "HOSPITAL_ID", "HOSPITAL", "CODIGO", "PERSONA_ID", "FECHA",
                                  "FECHA_INICIO", "FECHA_FIN", "PERSONA_ID_MOSTRAR", "ES_SELECCIONABLE",
                                  "FASE_ACTUAL_ID",
                                  row_number () over (partition by persona_id_mostrar, id order by id,
                                   persona_id) orden, orden_1, orden_2, orden_3
                           from   (select xx."ID", xx."UNICO_ID", xx."ASIGNATURA_ID", xx."ASIGNATURA",
                                          xx."CODIGO_ASIGNATURA", xx."BLOQUE_ID", xx."BLOQUE", xx."ESPECIALIDAD_ID",
                                          xx."ESPECIALIDAD", xx."FASE_ID", xx."FASE", xx."FASE_TIPO", xx."GRUPO_ID",
                                          xx."TAMAÑO", xx."PRIMERO", xx."ULTIMO", xx."P1", xx."P2", xx."P3", xx."P4",
                                          xx."P5", xx."P6", xx."CONSECUTIVOS", xx."PLAZAS", xx."HOSPITAL_ID",
                                          xx."HOSPITAL", xx."CODIGO", xx."PERSONA_ID", xx."FECHA", xx."FECHA_INICIO",
                                          xx."FECHA_FIN", ac.persona_id persona_id_mostrar,
                                          decode
                                             (xx.persona_id,
                                              ac.persona_id, 1,
                                              decode
                                                 (pack_pho_practicas.bloque_disponible (ac.persona_id, fase_id,
                                                                                        unico_id),
                                                  1, decode
                                                      (pack_pho_practicas.especialidad_disponible (ac.persona_id, 1,
                                                                                                   unico_id),
                                                       1, decode
                                                               (pack_pho_practicas.periodos_disponibles (ac.persona_id,
                                                                                                         fase_id,
                                                                                                         unico_id),
                                                                1, 1,
                                                                0
                                                               ),
                                                       0
                                                      ),
                                                  0
                                                 )
                                             ) es_seleccionable,
                                          (select id
                                           from   pho_Fases
                                           where  trunc (sysdate) between fecha_desde and fecha_hasta) fase_actual_id,
                                          orden_1, orden_2, orden_3
                                   from   (select x.unico_id || '_' || x.periodo_inicial_id id, x.unico_id,
                                                  x.asignatura_id, a.nombre asignatura, a.codigo_asignatura,
                                                  x.bloque_id, b.nombre bloque, x.especialidad_id,
                                                  e.nombre especialidad, x.fase_id, f.nombre fase, null grupo_id,
                                                  x.periodo_final_id - x.periodo_inicial_id + 1 tamaño,
                                                  x.periodo_inicial_id primero, x.periodo_final_id ultimo, null p1,
                                                  null p2, null p3, null p4, null p5, null p6,
                                                  x.bloque_unido consecutivos, plazas, x.hospital_id, h.nombre hospital,
                                                  h.codigo, x.persona_id, x.fecha, x.fecha_inicio, x.fecha_fin, orden_1,
                                                  orden_2, orden_3, f.tipo fase_tipo
                                           from   (select   o.curso_academico_id, o.asignatura_id, o.bloque_id,
                                                            o.especialidad_id, o.fase_id, o.hospital_id,
                                                            o.periodo_inicial_id, o.periodo_final_id, o.unico_id,
                                                            o.bloque_unido, o.orden_1, o.orden_2, o.orden_3,
                                                            p.fecha_inicio, p2.fecha_fin, persona_id, o.fecha,
                                                            count (*) plazas
                                                   from     pho_oferta o,
                                                            pho_periodos_fechas p,
                                                            pho_periodos_fechas p2
                                                   where    o.bloque_unido = 1
                                                   and      o.persona_id is null
                                                   and      o.periodo_inicial_id = p.periodo_id
                                                   and      o.curso_academico_id = p.curso_Aca_id
                                                   and      o.periodo_final_id = p2.periodo_id
                                                   and      o.curso_academico_id = p2.curso_Aca_id
                                                   group by o.curso_academico_id,
                                                            o.asignatura_id,
                                                            o.bloque_id,
                                                            o.especialidad_id,
                                                            o.fase_id,
                                                            o.hospital_id,
                                                            o.periodo_inicial_id,
                                                            o.periodo_final_id,
                                                            o.unico_id,
                                                            o.bloque_unido,
                                                            o.orden_1,
                                                            o.orden_2,
                                                            o.orden_3,
                                                            p.fecha_inicio,
                                                            p2.fecha_fin,
                                                            o.persona_id,
                                                            o.fecha
                                                   union all
                                                   select   o.curso_academico_id, o.asignatura_id, o.bloque_id,
                                                            o.especialidad_id, o.fase_id, o.hospital_id,
                                                            o.periodo_inicial_id, o.periodo_final_id, o.unico_id,
                                                            o.bloque_unido, o.orden_1, o.orden_2, o.orden_3,
                                                            p.fecha_inicio, p2.fecha_fin, persona_id, o.fecha,
                                                            count (*) plazas
                                                   from     pho_oferta o,
                                                            pho_periodos_fechas p,
                                                            pho_periodos_fechas p2
                                                   where    o.bloque_unido = 1
                                                   and      o.persona_id is not null
                                                   and      o.periodo_inicial_id = p.periodo_id
                                                   and      o.curso_academico_id = p.curso_Aca_id
                                                   and      o.periodo_final_id = p2.periodo_id
                                                   and      o.curso_academico_id = p2.curso_Aca_id
                                                   group by o.curso_academico_id,
                                                            o.asignatura_id,
                                                            o.bloque_id,
                                                            o.especialidad_id,
                                                            o.fase_id,
                                                            o.hospital_id,
                                                            o.periodo_inicial_id,
                                                            o.periodo_final_id,
                                                            o.unico_id,
                                                            o.bloque_unido,
                                                            o.orden_1,
                                                            o.orden_2,
                                                            o.orden_3,
                                                            p.fecha_inicio,
                                                            p2.fecha_fin,
                                                            o.persona_id,
                                                            o.fecha
                                                   union all
                                                   select xx.curso_academico_id, xx.asignatura_id, xx.bloque_id,
                                                          xx.especialidad_id, xx.fase_id, xx.hospital_id,
                                                          p.id periodo_inicial_id,
                                                          pack_pho_practicas.pho_buscar_ultimo_cons
                                                                                                (p.id,
                                                                                                 p1,
                                                                                                 p2,
                                                                                                 p3,
                                                                                                 p4,
                                                                                                 p5,
                                                                                                 p6) periodo_final_id,
                                                          xx.unico_id, xx.bloque_unido, xx.orden_1, xx.orden_2,
                                                          xx.orden_3, xx.fecha_inicio, xx.fecha_fin, xx.persona_id,
                                                          xx.fecha, xx.plazas
                                                   from   (select   x.curso_academico_id, x.asignatura_id, x.bloque_id,
                                                                    x.especialidad_id, x.fase_id, x.hospital_id,
                                                                    x.periodo_inicial_id, x.periodo_final_id,
                                                                    x.unico_id, x.bloque_unido, x.orden_1, x.orden_2,
                                                                    x.orden_3, x.fecha_inicio, x.fecha_fin,
                                                                    x.persona_id, x.fecha, x.plazas,
                                                                    min (decode (orden, 1, periodo_id, null)) p1,
                                                                    min (decode (orden, 2, periodo_id, null)) p2,
                                                                    min (decode (orden, 3, periodo_id, null)) p3,
                                                                    min (decode (orden, 4, periodo_id, null)) p4,
                                                                    min (decode (orden, 5, periodo_id, null)) p5,
                                                                    min (decode (orden, 6, periodo_id, null)) p6
                                                           from     (select   o.curso_academico_id, o.asignatura_id,
                                                                              o.bloque_id, o.especialidad_id, o.fase_id,
                                                                              o.hospital_id, o.periodo_inicial_id,
                                                                              o.periodo_final_id, o.unico_id,
                                                                              o.bloque_unido, o.orden_1, o.orden_2,
                                                                              o.orden_3, p.fecha_inicio, p2.fecha_fin,
                                                                              persona_id, fecha, count (*) plazas,
                                                                              op.periodo_id,
                                                                              row_number () over (partition by o.curso_academico_id, o.asignatura_id, o.bloque_id, o.especialidad_id, o.fase_id, o.hospital_id, o.periodo_inicial_id, o.periodo_final_id, o.unico_id, o.bloque_unido, o.orden_1, o.orden_2, o.orden_3 order by op.periodo_id)
                                                                                                                  orden
                                                                     from     pho_oferta o,
                                                                              pho_periodos_fechas p,
                                                                              pho_periodos_fechas p2,
                                                                              pho_ofertas_periodos op
                                                                     where    o.bloque_unido = 0
                                                                     and      o.persona_id is null
                                                                     and      o.periodo_inicial_id = p.periodo_id
                                                                     and      o.curso_academico_id = p.curso_Aca_id
                                                                     and      o.periodo_final_id = p2.periodo_id
                                                                     and      o.curso_academico_id = p2.curso_Aca_id
                                                                     and      o.id = op.oferta_id
                                                                     group by o.curso_academico_id,
                                                                              o.asignatura_id,
                                                                              o.bloque_id,
                                                                              o.especialidad_id,
                                                                              o.fase_id,
                                                                              o.hospital_id,
                                                                              o.periodo_inicial_id,
                                                                              o.periodo_final_id,
                                                                              o.unico_id,
                                                                              o.bloque_unido,
                                                                              o.orden_1,
                                                                              o.orden_2,
                                                                              o.orden_3,
                                                                              p.fecha_inicio,
                                                                              p2.fecha_fin,
                                                                              o.persona_id,
                                                                              o.fecha,
                                                                              op.periodo_id) x
                                                           group by x.curso_academico_id,
                                                                    x.asignatura_id,
                                                                    x.bloque_id,
                                                                    x.especialidad_id,
                                                                    x.fase_id,
                                                                    x.hospital_id,
                                                                    x.periodo_inicial_id,
                                                                    x.periodo_final_id,
                                                                    x.unico_id,
                                                                    x.bloque_unido,
                                                                    x.orden_1,
                                                                    x.orden_2,
                                                                    x.orden_3,
                                                                    x.fecha_inicio,
                                                                    x.fecha_fin,
                                                                    x.persona_id,
                                                                    x.fecha,
                                                                    x.plazas) xx,
                                                          (select id, id - 1 periodo_ant, nombre
                                                           from   pho_periodos) p
                                                   where  (   p1 = p.id
                                                           or (    p2 = p.id
                                                               and p1 != p.periodo_ant)
                                                           or (    p3 = p.id
                                                               and p2 != p.periodo_ant)
                                                           or (    p4 = p.id
                                                               and p3 != p.periodo_ant)
                                                           or (    p5 = p.id
                                                               and p4 != p.periodo_ant)
                                                           or (    p6 = p.id
                                                               and p5 != p.periodo_ant)
                                                          )
                                                   union all
                                                   select xx.curso_academico_id, xx.asignatura_id, xx.bloque_id,
                                                          xx.especialidad_id, xx.fase_id, xx.hospital_id,
                                                          p.id periodo_inicial_id,
                                                          pack_pho_practicas.pho_buscar_ultimo_cons
                                                                                                (p.id,
                                                                                                 p1,
                                                                                                 p2,
                                                                                                 p3,
                                                                                                 p4,
                                                                                                 p5,
                                                                                                 p6) periodo_final_id,
                                                          xx.unico_id, xx.bloque_unido, xx.orden_1, xx.orden_2,
                                                          xx.orden_3, xx.fecha_inicio, xx.fecha_fin, xx.persona_id,
                                                          xx.fecha, xx.plazas
                                                   from   (select   x.curso_academico_id, x.asignatura_id, x.bloque_id,
                                                                    x.especialidad_id, x.fase_id, x.hospital_id,
                                                                    x.periodo_inicial_id, x.periodo_final_id,
                                                                    x.unico_id, x.bloque_unido, x.orden_1, x.orden_2,
                                                                    x.orden_3, x.fecha_inicio, x.fecha_fin,
                                                                    x.persona_id, x.fecha, x.plazas,
                                                                    min (decode (orden, 1, periodo_id, null)) p1,
                                                                    min (decode (orden, 2, periodo_id, null)) p2,
                                                                    min (decode (orden, 3, periodo_id, null)) p3,
                                                                    min (decode (orden, 4, periodo_id, null)) p4,
                                                                    min (decode (orden, 5, periodo_id, null)) p5,
                                                                    min (decode (orden, 6, periodo_id, null)) p6
                                                           from     (select   o.curso_academico_id, o.asignatura_id,
                                                                              o.bloque_id, o.especialidad_id, o.fase_id,
                                                                              o.hospital_id, o.periodo_inicial_id,
                                                                              o.periodo_final_id, o.unico_id,
                                                                              o.bloque_unido, o.orden_1, o.orden_2,
                                                                              o.orden_3, p.fecha_inicio, p2.fecha_fin,
                                                                              persona_id, fecha, count (*) plazas,
                                                                              op.periodo_id,
                                                                              row_number () over (partition by o.curso_academico_id, o.asignatura_id, o.bloque_id, o.especialidad_id, o.fase_id, o.hospital_id, o.periodo_inicial_id, o.periodo_final_id, o.unico_id, o.bloque_unido, o.orden_1, o.orden_2, o.orden_3, persona_id order by op.periodo_id)
                                                                                                                  orden
                                                                     from     pho_oferta o,
                                                                              pho_periodos_fechas p,
                                                                              pho_periodos_fechas p2,
                                                                              pho_ofertas_periodos op
                                                                     where    o.bloque_unido = 0
                                                                     and      o.persona_id is not null
                                                                     and      o.periodo_inicial_id = p.periodo_id
                                                                     and      o.curso_academico_id = p.curso_Aca_id
                                                                     and      o.periodo_final_id = p2.periodo_id
                                                                     and      o.curso_academico_id = p2.curso_Aca_id
                                                                     and      o.id = op.oferta_id
                                                                     group by o.curso_academico_id,
                                                                              o.asignatura_id,
                                                                              o.bloque_id,
                                                                              o.especialidad_id,
                                                                              o.fase_id,
                                                                              o.hospital_id,
                                                                              o.periodo_inicial_id,
                                                                              o.periodo_final_id,
                                                                              o.unico_id,
                                                                              o.bloque_unido,
                                                                              o.orden_1,
                                                                              o.orden_2,
                                                                              o.orden_3,
                                                                              p.fecha_inicio,
                                                                              p2.fecha_fin,
                                                                              o.persona_id,
                                                                              o.fecha,
                                                                              op.periodo_id) x
                                                           group by x.curso_academico_id,
                                                                    x.asignatura_id,
                                                                    x.bloque_id,
                                                                    x.especialidad_id,
                                                                    x.fase_id,
                                                                    x.hospital_id,
                                                                    x.periodo_inicial_id,
                                                                    x.periodo_final_id,
                                                                    x.unico_id,
                                                                    x.bloque_unido,
                                                                    x.orden_1,
                                                                    x.orden_2,
                                                                    x.orden_3,
                                                                    x.fecha_inicio,
                                                                    x.fecha_fin,
                                                                    x.persona_id,
                                                                    x.fecha,
                                                                    x.plazas) xx,
                                                          (select id, id - 1 periodo_ant, nombre
                                                           from   pho_periodos) p
                                                   where  (   p1 = p.id
                                                           or (    p2 = p.id
                                                               and p1 != p.periodo_ant)
                                                           or (    p3 = p.id
                                                               and p2 != p.periodo_ant)
                                                           or (    p4 = p.id
                                                               and p3 != p.periodo_ant)
                                                           or (    p5 = p.id
                                                               and p4 != p.periodo_ant)
                                                           or (    p6 = p.id
                                                               and p5 != p.periodo_ant)
                                                          )) x,
                                                  pho_asignaturas a,
                                                  pho_bloques b,
                                                  pho_especialidades e,
                                                  pho_fases f,
                                                  pho_hospitales h
                                           where  x.asignatura_id = a.id
                                           and    x.bloque_id = b.id
                                           and    x.especialidad_id = e.id
                                           and    x.fase_id = f.id
                                           and    x.hospital_id = h.id) xx,
                                          pho_ext_asignaturas_cursadas ac
                                   where  (   xx.persona_id = ac.persona_id
                                           or xx.persona_id is null)
                                   and    xx.codigo_asignatura = ac.asignatura_id)) xxx
                   where  orden = 1) xxxx
           where  (   fase_id = 1
                   or (    fase_id = 2
                       and not exists (
                              select 1
                              from   pho_oferta o
                              where  o.persona_id = xxxx.persona_id_mostrar
                              and    o.especialidad_id = xxxx.especialidad_id
                              and    fase_id = 1)
                      )
                  ));

 