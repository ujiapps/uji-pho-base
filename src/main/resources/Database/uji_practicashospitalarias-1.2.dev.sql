ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ADD (CURSO NUMBER);
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES ADD (CURSO NUMBER);

CREATE OR REPLACE package UJI_PRACTICASHOSPITALARIAS.pack_generar is
   function generar_oferta_fase(faseId in number)
      return number;

   procedure generar_oferta_fase_1(faseId in NUMBER, borrar_Datos in varchar2);

   procedure generar_oferta_fase_2;

   procedure generar_oferta_fase_3;

   procedure generar_turnos(fase_id number, fecha date, hora_inicial varchar2, incremento_turno NUMBER);

   procedure borrar_no_asignados(faseId in number);

   procedure generar_muestra_fase_1;

   CURSO_ACADEMICO   number(4) := 2017;
end;
/

CREATE OR REPLACE package body UJI_PRACTICASHOSPITALARIAS.pack_generar is
   cursor matriculados_old is
      select p.id persona_id, nota_media
      from pho_ext_personas p
      where p.id in (select persona_id
                     from PHO_EXT_ASIGNATURAS_CURSADAS ac
                     where ac.curso_academico_id = CURSO_ACADEMICO)
      order by 2 desc;

   cursor matriculados
   (
      p_fase   in number
   ) is
      select persona_id, nota, orden
      from (select persona_id,
                   count(distinct ac.asignatura_id)                num_asignaturas,
                   nota,
                   count(distinct ac.asignatura_id) * 100 + nota   orden
            from pho_ext_asignaturas_cursadas     ac
                 join pho_asignaturas a on a.codigo_asignatura = ac.asignatura_id
                 join pho_fases_asignaturas fa on fa.asignatura_id = a.id
            where     ac.curso_academico_id = CURSO_ACADEMICO
                  and fase_id = p_fase
            group by persona_id, nota)
      order by orden desc;


   procedure actualizar_grupos(p_fase in number) is
      v_primero   number;

      cursor lista(p_fase in number) is
         select o.id oferta_id, g.id grupo_id
         from pho_grupos g, pho_grupos_Detalle d, pho_oferta o
         where     g.id = d.grupo_id
               and g.fase_id = p_fase
               and g.fase_id = o.fase_id
               and d.especialidad_id = o.especialidad_id
               and o.bloque_unido = 1
               and d.periodo_id between o.periodo_inicial_id and o.periodo_final_id;
   begin
      for x in lista(p_fase) loop
         select grupo_id
         into v_primero
         from pho_oferta
         where id = x.oferta_id;

         if v_primero is null then
            update pho_oferta
            set grupo_id = x.grupo_id
            where id = x.oferta_id;
         else
            update pho_oferta
            set grupo2_id = x.grupo_id
            where id = x.oferta_id;
         end if;

         commit;
      end loop;
   end;

   procedure generar_turnos(fase_id number, fecha date, hora_inicial varchar2, incremento_turno number) is
      fecha_inicio_matricula   date;
      incremento               number;
      fase_actual_id           number;
   begin
      select id
      into fase_actual_id
      from pho_fases f
      where     f.id = fase_id
            and f.curso_academico_id = CURSO_ACADEMICO;

      delete from pho_turnos t
      where t.fase_id = fase_actual_id;

      fecha_inicio_matricula := nvl(fecha, sysdate);
      incremento := 0;

      for matricula in matriculados(fase_id) loop
         insert into pho_turnos(ID, PERSONA_ID, FECHA, FASE_ID)
            values
                   (
                      (select nvl(max(id) + 1, 1)
                       from pho_turnos),
                      matricula.persona_id,
                      to_date(to_char(fecha_inicio_matricula, 'DD/MM/YYYY') || ' ' || hora_inicial,
                      'DD/MM/YYYY HH24:MI') + (incremento / (24 * 60)),
                      fase_actual_id
                   );

         incremento := incremento + incremento_turno;
      end loop;

      update pho_fases
      set fecha_Desde = trunc(fecha_inicio_matricula), fecha_hasta = trunc(fecha_inicio_matricula) + 1
      where     id = fase_actual_id
            and curso_academico_id = CURSO_ACADEMICO;

      update pho_fases
      set fecha_Desde = trunc(fecha_inicio_matricula) + 1, fecha_hasta = trunc(fecha_inicio_matricula) + 300
      where     id = 3
            and curso_academico_id = CURSO_ACADEMICO;

      commit;
   end;

   procedure refrescar_flags(p_fase in number) is
   begin
      for matricula in matriculados(p_fase) loop
         if p_Fase = 1 then
            pack_pho_practicas.refrescar_flag(matricula.persona_id, 1);
            pack_pho_practicas.refrescar_flag(matricula.persona_id, 2);
         elsif p_fase >= 4 then
            pack_pho_practicas.refrescar_flag(matricula.persona_id, p_fase);
         end if;
      end loop;
   end;

   function generar_oferta_fase(faseId in number)
      return number is
      v_id    number;
      v_id2   number;

      cursor lista is
         select b.*,
                c.id                                            curso_academico_id,
                decode(bloque_unido, 'S', 1, 0)                 unido,
                p1                                              p_primero,
                nvl(p6, nvl(p5, nvl(p4, nvl(p3, nvl(p2, p1))))) p_ultimo
         from pho_vw_oferta_base b, pho_cursos c
         where     c.activo = 1
               and fase_id = faseId;
   begin
      if faseId = 1 then
         generar_oferta_fase_1(faseId, 'S');
      elsif faseId = 2 then
         generar_oferta_fase_2;
      elsif faseId = 3 then
         generar_oferta_fase_3;
      elsif faseId in (4, 7) then
         generar_oferta_fase_1(faseId, 'S');
      else                                    -- Mantenemos la oferta de quinto cuando se realiza la matrícula de cuarto
         generar_oferta_fase_1(faseId, 'N');
      end if;

      actualizar_grupos(faseId);
      return 1;
   end;

   procedure generar_oferta_fase_1(faseId in number, borrar_datos in varchar2) is
      v_id    number;
      v_id2   number;

      cursor lista is
         select b.*,
                c.id                                            curso_academico_id,
                decode(bloque_unido, 'S', 1, 0)                 unido,
                p1                                              p_primero,
                nvl(p6, nvl(p5, nvl(p4, nvl(p3, nvl(p2, p1))))) p_ultimo
         from pho_vw_oferta_base b, pho_cursos c
         where     c.activo = 1
               and fase_id = faseId;
   begin
      if borrar_datos = 'S' then
         delete pho_ofertas_periodos;

         delete pho_oferta;
      end if;

      refrescar_flags(faseId);

      for x in lista loop
         for p in 1 .. x.plazas loop
            v_id := hibernate_sequence.nextval;

            if x.unido = 1 then
               insert into pho_oferta(ID,
                                      CURSO_ACADEMICO_ID,
                                      ASIGNATURA_ID,
                                      BLOQUE_ID,
                                      ESPECIALIDAD_ID,
                                      FASE_ID,
                                      HOSPITAL_ID,
                                      PERIODO_INICIAL_ID,
                                      PERIODO_FINAL_ID,
                                      PERSONA_ID,
                                      FECHA,
                                      UNICO_ID,
                                      ORDEN_1,
                                      ORDEN_2,
                                      ORDEN_3,
                                      bloque_unido)
               values (v_id,
                       x.curso_Academico_id,
                       x.asignatura_id,
                       x.bloque_id,
                       x.ESPECIALIDAD_ID,
                       x.FASE_ID,
                       x.HOSPITAL_ID,
                       x.p_primero,
                       x.p_ultimo,
                       null,
                       null,
                       x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.hospital_id ||
                       '_' || x.grupo_id,
                       nvl(x.ORDEN_1, 9999),
                       nvl(x.ORDEN_2, 9999),
                       nvl(x.ORDEN_3, 9999),
                       x.unido);
            else
               insert into pho_oferta(ID,
                                      CURSO_ACADEMICO_ID,
                                      ASIGNATURA_ID,
                                      BLOQUE_ID,
                                      ESPECIALIDAD_ID,
                                      FASE_ID,
                                      HOSPITAL_ID,
                                      PERIODO_INICIAL_ID,
                                      PERIODO_FINAL_ID,
                                      PERSONA_ID,
                                      FECHA,
                                      UNICO_ID,
                                      ORDEN_1,
                                      ORDEN_2,
                                      ORDEN_3,
                                      bloque_unido)
               values (v_id,
                       x.curso_Academico_id,
                       x.asignatura_id,
                       x.bloque_id,
                       x.ESPECIALIDAD_ID,
                       x.FASE_ID,
                       x.HOSPITAL_ID,
                       x.p_primero,
                       x.p_ultimo,
                       null,
                       null,
                       x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.hospital_id ||
                       '_' || x.grupo_id,
                       nvl(x.ORDEN_1, 9999),
                       nvl(x.ORDEN_2, 9999),
                       nvl(x.ORDEN_3, 9999),
                       x.unido);

               if x.p1 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p1);
               end if;

               if x.p2 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p2);
               end if;

               if x.p3 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p3);
               end if;

               if x.p4 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p4);
               end if;

               if x.p5 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p5);
               end if;

               if x.p6 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p6);
               end if;
            end if;
         end loop;

         null;
      end loop;

      commit;
   end;

   procedure generar_oferta_fase_2 is
      v_id       number;
      v_id2      number;
      v_error    number;
      v_libres   number;

      cursor lista is
         select xxx.*, plazas - ocupacion libres, c.id curso_academico_id, decode(bloque_unido, 'S', 1, 0) unido
         from pho_Vw_oferta_base_2 xxx, pho_cursos c
         where     c.activo = 1
               and fase_id = 2                                                                --and    asignatura_id = 1
                              --and    bloque_id = 2
                              --and    especialidad_id = 3
   ;
   begin
      borrar_no_asignados(1);
      refrescar_flags(1);
      v_error := 1;

      for x in lista loop
         v_error := 11;
         v_libres := x.libres;

         for p in 1 .. v_libres loop
            --dbms_output.put_line ('P: ' || p);
            v_error := 2;
            v_id := hibernate_sequence.nextval;
            v_error := 3;

            if x.unido = 1 then
               v_error := 4;

               begin
                  insert into pho_oferta(ID,
                                         CURSO_ACADEMICO_ID,
                                         ASIGNATURA_ID,
                                         BLOQUE_ID,
                                         ESPECIALIDAD_ID,
                                         FASE_ID,
                                         HOSPITAL_ID,
                                         PERIODO_INICIAL_ID,
                                         PERIODO_FINAL_ID,
                                         PERSONA_ID,
                                         FECHA,
                                         UNICO_ID,
                                         ORDEN_1,
                                         ORDEN_2,
                                         ORDEN_3,
                                         bloque_unido)
                     values
                            (
                               v_id,
                               x.curso_Academico_id,
                               x.asignatura_id,
                               x.bloque_id,
                               x.ESPECIALIDAD_ID,
                               x.FASE_ID,
                               x.HOSPITAL_ID,
                               x.primero,
                               x.ultimo,
                               null,
                               null,
                               x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.
                               hospital_id || '_' || x.grupo_id,
                               nvl(x.ORDEN_1, 9999),
                               nvl(x.ORDEN_2, 9999),
                               nvl(x.ORDEN_3, 9999),
                               x.unido
                            );
               exception
                  when others then
                     dbms_output.put_line(v_error || ' - ' || sqlerrm || x.asignatura_id || ' ' || x.bloque_id || ' '
                                          || x.especialidad_id || ' ' || x.hospital_id || ' - ' || x.fase_id);
               end;
            else
               v_error := 5;

               insert into pho_oferta(ID,
                                      CURSO_ACADEMICO_ID,
                                      ASIGNATURA_ID,
                                      BLOQUE_ID,
                                      ESPECIALIDAD_ID,
                                      FASE_ID,
                                      HOSPITAL_ID,
                                      PERIODO_INICIAL_ID,
                                      PERIODO_FINAL_ID,
                                      PERSONA_ID,
                                      FECHA,
                                      UNICO_ID,
                                      ORDEN_1,
                                      ORDEN_2,
                                      ORDEN_3,
                                      bloque_unido)
               values (v_id,
                       x.curso_Academico_id,
                       x.asignatura_id,
                       x.bloque_id,
                       x.ESPECIALIDAD_ID,
                       x.FASE_ID,
                       x.HOSPITAL_ID,
                       x.primero,
                       x.ultimo,
                       null,
                       null,
                       x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.hospital_id ||
                       '_' || x.grupo_id,
                       nvl(x.ORDEN_1, 9999),
                       nvl(x.ORDEN_2, 9999),
                       nvl(x.ORDEN_3, 9999),
                       x.unido);

               v_error := 6;

               if x.p1 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p1);
               end if;

               if x.p2 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p2);
               end if;

               if x.p3 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p3);
               end if;

               if x.p4 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p4);
               end if;

               if x.p5 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p5);
               end if;

               if x.p6 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p6);
               end if;
            end if;
         end loop;

         null;
         commit;
      end loop;
   exception
      when others then
         dbms_output.put_line(v_error || ' - ' || sqlerrm);
   end;

   procedure generar_oferta_fase_3 is
      v_id       number;
      v_id2      number;
      v_error    number;
      v_libres   number;

      cursor lista is
         select xxx.*, plazas - ocupacion libres, c.id curso_academico_id, decode(bloque_unido, 'S', 1, 0) unido
         from pho_Vw_oferta_base_2 xxx, pho_cursos c
         where     c.activo = 1
               and fase_id = 2                                                                --and    asignatura_id = 1
                              --and    bloque_id = 2
                              --and    especialidad_id = 3
   ;
   begin
      borrar_no_asignados(2);
      refrescar_flags(1);
      v_error := 1;

      for x in lista loop
         v_error := 11;
         v_libres := x.libres;

         for p in 1 .. 30 loop
            --dbms_output.put_line ('P: ' || p);
            v_error := 2;
            v_id := hibernate_sequence.nextval;
            v_error := 3;

            if x.unido = 1 then
               v_error := 4;

               begin
                  insert into pho_oferta(ID,
                                         CURSO_ACADEMICO_ID,
                                         ASIGNATURA_ID,
                                         BLOQUE_ID,
                                         ESPECIALIDAD_ID,
                                         FASE_ID,
                                         HOSPITAL_ID,
                                         PERIODO_INICIAL_ID,
                                         PERIODO_FINAL_ID,
                                         PERSONA_ID,
                                         FECHA,
                                         UNICO_ID,
                                         ORDEN_1,
                                         ORDEN_2,
                                         ORDEN_3,
                                         bloque_unido)
                     values
                            (
                               v_id,
                               x.curso_Academico_id,
                               x.asignatura_id,
                               x.bloque_id,
                               x.ESPECIALIDAD_ID,
                               3,
                               x.HOSPITAL_ID,
                               x.primero,
                               x.ultimo,
                               null,
                               null,
                               x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.
                               hospital_id || '_' || x.grupo_id,
                               nvl(x.ORDEN_1, 9999),
                               nvl(x.ORDEN_2, 9999),
                               nvl(x.ORDEN_3, 9999),
                               x.unido
                            );
               exception
                  when others then
                     dbms_output.put_line(v_error || ' - ' || sqlerrm || x.asignatura_id || ' ' || x.bloque_id || ' '
                                          || x.especialidad_id || ' ' || x.hospital_id || ' - ' || x.fase_id);
               end;
            else
               v_error := 5;

               insert into pho_oferta(ID,
                                      CURSO_ACADEMICO_ID,
                                      ASIGNATURA_ID,
                                      BLOQUE_ID,
                                      ESPECIALIDAD_ID,
                                      FASE_ID,
                                      HOSPITAL_ID,
                                      PERIODO_INICIAL_ID,
                                      PERIODO_FINAL_ID,
                                      PERSONA_ID,
                                      FECHA,
                                      UNICO_ID,
                                      ORDEN_1,
                                      ORDEN_2,
                                      ORDEN_3,
                                      bloque_unido)
               values (v_id,
                       x.curso_Academico_id,
                       x.asignatura_id,
                       x.bloque_id,
                       x.ESPECIALIDAD_ID,
                       3,
                       x.HOSPITAL_ID,
                       x.primero,
                       x.ultimo,
                       null,
                       null,
                       x.asignatura_id || '_' || x.bloque_id || '_' || x.especialidad_id || '_' || x.hospital_id ||
                       '_' || x.grupo_id,
                       nvl(x.ORDEN_1, 9999),
                       nvl(x.ORDEN_2, 9999),
                       nvl(x.ORDEN_3, 9999),
                       x.unido);

               v_error := 6;

               if x.p1 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p1);
               end if;

               if x.p2 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p2);
               end if;

               if x.p3 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p3);
               end if;

               if x.p4 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p4);
               end if;

               if x.p5 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p5);
               end if;

               if x.p6 is not null then
                  v_id2 := hibernate_sequence.nextval;

                  insert into pho_ofertas_periodos
                  values (v_id2, v_id, x.p6);
               end if;
            end if;
         end loop;

         null;
         commit;
      end loop;
   exception
      when others then
         dbms_output.put_line(v_error || ' - ' || sqlerrm);
   end;

   procedure borrar_no_asignados(faseId in number) is
   begin
      delete pho_ofertas_periodos
      where oferta_id in (select id
                          from pho_oferta
                          where     fase_id = faseId
                                and persona_id is null);

      delete pho_oferta
      where     fase_id = faseId
            and persona_id is null;

      commit;
   end;

   procedure generar_muestra_Fase_1 is
   begin
      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '1_1_1_1_1'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '1_2_21_1_3'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '4_5_35_5_5'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '2_3_31_2_2'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '3_4_32_1_3'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '5_6_33_1_4'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      update pho_oferta o
      set persona_id = 591555
      where     unico_id = '5_7_34_8_17'
            and persona_id is null
            and id = (select min(id)
                      from pho_oferta o2
                      where     persona_id is null
                            and o.unico_id = o2.unico_id);

      commit;
   end;

   procedure notificar_turnos_Fase_1 is
      cursor lista is
         select distinct persona_id, busca_cuenta(persona_id) email, busca_nombre_persona(persona_id) nombre
         from pho_turnos
         where rownum <= 1;

      v_asunto   varchar2(2000);
      v_texto    varchar2(4000);
      v_para     varchar2(200);
      v_sep      varchar2(10) := '<br />';
   begin
      v_asunto := 'Turno de matrícula de las prácticas clínicas de sexto curso del grado en medicina';

      for x in lista loop
         v_para := x.email;
         v_para := 'ferrerq@uji.es';
         v_texto :=
            'Estimado/Estimada ' || x.nombre || v_Sep || v_sep ||
            'En el siguiente enlace podrás consultar tus turnos para la matrícula de las prácticas clínicas de sexto curso.'
            || v_Sep || v_Sep || '<a href="http://ujiapps.uji.es/pho">http://ujiapps.uji.es/pho</a>' || v_Sep || v_sep
            || v_sep || v_Sep || 'Atentamente' || v_Sep || v_sep || 'Universitat Jaume I';
         gri_www.euji_envios_html.mail(p_from          => 'no_reply@uji.es',
                                       p_to            => v_para,
                                       p_subject       => v_Asunto,
                                       p_texto         => v_Texto,
                                       p_permite_dup   => 'S',
                                       p_formato       => 'HTM');
      end loop;
   end;
end;
/

CREATE OR REPLACE package UJI_PRACTICASHOSPITALARIAS.pack_pho_practicas is
   procedure asignar(p_persona_id        in number,
                     p_fase_id           in number,
                     p_oferta_id         in number,
                     p_periodo_inicial   in number,
                     p_periodo_final     in number,
                     p_bloque_unido      in number);

   procedure desasignar(p_persona_id        in number,
                        p_fase_id           in number,
                        p_oferta_id         in number,
                        p_periodo_inicial   in number,
                        p_periodo_final     in number,
                        p_bloque_unido      in number);

   function periodos_disponibles(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number;

   function un_periodo_disponible(p_persona_id in number, p_fase_id in number, p_periodo in number)
      return number;

   function especialidad_disponible(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number;

   function bloque_disponible(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number;

   function grupo_seleccionado(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number;

   function pho_buscar_ultimo_cons(p_inicial   in number,
                                   p1          in number,
                                   p2          in number,
                                   p3          in number,
                                   p4          in number,
                                   p5          in number,
                                   p6          in number)
      return number;

   procedure refrescar_flag(p_persona_id in number, p_Fase_id in number);

   function plazas_ocupadas(p_asignatura     in number,
                            p_bloque         in number,
                            p_especialidad   in number,
                            p_hospital       in number,
                            p_periodo        in number)
      return number;

   function oferta_plazas(p_asignatura     in number,
                          p_bloque         in number,
                          p_especialidad   in number,
                          p_hospital       in number,
                          p_periodo        in number)
      return number;
end;
/

CREATE OR REPLACE package body UJI_PRACTICASHOSPITALARIAS.pack_pho_practicas is
   procedure asignar_periodo(p_persona_id in number, p_fase_id in number, p_periodo_id in number) is
      v_flag      varchar2(32);
      v_antes     varchar2(100);
      v_despues   varchar2(100);
      v_aux       number;
      v_curso     number;
   begin
      select curso_academico_id
      into v_curso
      from pho_fases
      where id = p_fase_id;

      begin
         select flags
         into v_flag
         from pho_matriculas
         where     persona_id = p_persona_id
               and curso_academico_id = v_curso;
      exception
         when no_Data_found then
            select nvl(max(id), 0) + 1
            into v_aux
            from pho_matriculas;

            insert into pho_matriculas(id, persona_id, curso_academico_id)
            values (v_aux, p_persona_id, v_curso);

            select flags
            into v_flag
            from pho_matriculas
            where     persona_id = p_persona_id
                  and curso_academico_id = v_curso;
      end;

      v_antes := substr(v_flag, 1, p_periodo_id - 1);
      v_despues := substr(v_flag, p_periodo_id + 1);

      update pho_matriculas
      set flags = v_antes || '1' || v_Despues
      where     persona_id = p_persona_id
            and curso_academico_id = v_curso;
   end;

   procedure desasignar_periodo(p_persona_id in number, p_fase_id in number, p_periodo_id in number) is
      v_flag      varchar2(32);
      v_antes     varchar2(100);
      v_despues   varchar2(100);
      v_aux       number;
      v_curso     number;
   begin
      select curso_academico_id
      into v_curso
      from pho_fases
      where id = p_fase_id;

      begin
         select flags
         into v_flag
         from pho_matriculas
         where     persona_id = p_persona_id
               and curso_academico_id = v_curso;
      exception
         when no_Data_found then
            select nvl(max(id), 0) + 1
            into v_aux
            from pho_matriculas;

            insert into pho_matriculas(id, persona_id, curso_academico_id)
            values (v_aux, p_persona_id, v_curso);

            select flags
            into v_flag
            from pho_matriculas
            where     persona_id = p_persona_id
                  and curso_academico_id = v_curso;
      end;

      v_antes := substr(v_flag, 1, p_periodo_id - 1);
      v_despues := substr(v_flag, p_periodo_id + 1);

      update pho_matriculas
      set flags = v_antes || '0' || v_Despues
      where     persona_id = p_persona_id
            and curso_academico_id = v_curso;
   end;

   procedure asignar(p_persona_id        in number,
                     p_fase_id           in number,
                     p_oferta_id         in number,
                     p_periodo_inicial   in number,
                     p_periodo_final     in number,
                     p_bloque_unido      in number) is
      cursor lista_periodos(p_oferta_id in number) is
         select periodo_id
         from pho_ofertas_periodos
         where oferta_id = p_oferta_id
         order by periodo_id;
   begin
      if p_bloque_unido = 1 then
         for x in p_periodo_inicial .. p_periodo_final loop
            asignar_periodo(p_persona_id, p_fase_id, x);
         end loop;
      else
         for x in lista_periodos(p_oferta_id) loop
            asignar_periodo(p_persona_id, p_fase_id, x.periodo_id);
         end loop;
      end if;
   end;

   procedure desasignar(p_persona_id        in number,
                        p_fase_id           in number,
                        p_oferta_id         in number,
                        p_periodo_inicial   in number,
                        p_periodo_final     in number,
                        p_bloque_unido      in number) is
      cursor lista_periodos(p_oferta_id in number) is
         select periodo_id
         from pho_ofertas_periodos
         where oferta_id = p_oferta_id
         order by periodo_id;
   begin
      if p_bloque_unido = 1 then
         for x in p_periodo_inicial .. p_periodo_final loop
            desasignar_periodo(p_persona_id, p_fase_id, x);
         end loop;
      else
         for x in lista_periodos(p_oferta_id) loop
            desasignar_periodo(p_persona_id, p_fase_id, x.periodo_id);
         end loop;
      end if;
   end;

   function periodos_disponibles(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number is
      v_oferta_id      number;
      v_bloque_unido   number;
      v_inicial        number;
      v_final          number;
      v_rdo            number;
      v_flag           varchar2(32);
      v_error          number;
      sin_oferta       exception;

      cursor lista_periodos(p_oferta_id in number) is
         select periodo_id
         from pho_ofertas_periodos
         where oferta_id = p_oferta_id
         order by periodo_id;
   begin
      v_error := -10;

      begin
         select distinct min(id), bloque_unido, periodo_inicial_id, periodo_final_id
         into v_oferta_id, v_bloque_unido, v_inicial, v_final
         from pho_oferta
         where     unico_id = p_unico
               and fase_id = p_fase_id
         group by bloque_unido, periodo_inicial_id, periodo_final_id, grupo_id;
      exception
         when others then
            raise sin_oferta;
      end;

      v_error := -20;

      select flags
      into v_flag
      from pho_matriculas
      where     persona_id = p_persona_id
            and curso_academico_id = (select curso_academico_id
                                      from pho_Fases
                                      where id = p_fase_id);

      v_error := -30;
      v_rdo := 0;

      if v_bloque_unido = 1 then
         for x in v_inicial .. v_final loop
            if v_rdo = 0 then
               v_rdo := substr(v_flag, x, 1);
            end if;
         end loop;
      else
         for x in lista_periodos(v_oferta_id) loop
            if v_rdo = 0 then
               v_rdo := substr(v_flag, x.periodo_id, 1);
            end if;
         end loop;
      end if;

      v_error := -40;

      if v_rdo = 1 then
         return (0);
      else
         return (1);
      end if;
   exception
      when sin_oferta then
         return (0);
      when others then
         return (v_error);
   end;


   function un_periodo_disponible(p_persona_id in number, p_fase_id in number, p_periodo in number)
      return number is
      v_flag    varchar2(32);
      v_valor   varchar2(1);
   begin
      select flags
      into v_flag
      from pho_matriculas
      where persona_id = p_persona_id;

      v_valor := substr(v_flag, p_periodo, 1);

      if v_valor = '1' then
         return (0);
      else
         return (1);
      end if;
   end;

   function especialidad_disponible(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number is
      v_especialidad   number;
      v_matriculada    number;
      v_rdo            number;
      v_numero         number;
      v_bloque         number;
   begin
      select distinct especialidad_id, bloque_id
      into v_especialidad, v_bloque
      from pho_oferta
      where unico_id = p_unico;


      select numero_obligatorias
      into v_numero
      from pho_bloques
      where id = v_bloque;

      --select decode(count(*), 0, 0, 1)
      select count(*)
      into v_matriculada
      from pho_oferta
      where     persona_id = p_persona_id
            and especialidad_id = v_especialidad;

      if v_matriculada = 0 then
         v_rdo := 1;
      else
         v_rdo := 0;
      end if;

      /*
      if v_matriculada < v_numero then
         v_rdo := 1;
      else
         v_rdo := 0;
      end if;
      */

      return (v_rdo);
   end;

   function bloque_disponible(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number is
      v_bloque      number;
      v_maximo      number;
      v_asignados   number;
      v_rdo         number;
   begin
      select distinct bloque_id
      into v_bloque
      from pho_oferta
      where unico_id = p_unico;

      select decode(f.tipo,
                    'OB', numero_obligatorias,
                    'OB/OP', decode(f.id, 1, numero_obligatorias, numero_optativas),
                    numero_optativas)
      into v_maximo
      from pho_bloques b, pho_fases f
      where     b.id = v_bloque
            and f.id = p_Fase_id;

      select count(*)
      into v_asignados
      from pho_oferta
      where     persona_id = p_persona_id
            and bloque_id = v_bloque
            and fase_id = p_Fase_id;

      if v_asignados < v_maximo then
         v_rdo := 1;
      else
         v_rdo := 0;
      end if;

      return (v_rdo);
   end;

   function grupo_seleccionado(p_persona_id in number, p_fase_id in number, p_unico in varchar2)
      return number is
      v_grupo   number;
   begin
      select distinct grupo_id
      into v_grupo
      from pho_oferta
      where     persona_id = p_persona_id
            and fase_id = p_fase_id
            and grupo_id is not null;

      return (v_grupo);
   exception
      when no_Data_found then
         return (-1);
   end;

   function pho_buscar_ultimo_cons(p_inicial   in number,
                                   p1          in number,
                                   p2          in number,
                                   p3          in number,
                                   p4          in number,
                                   p5          in number,
                                   p6          in number)
      return number is
      v_rdo     number;
      v_aux     number;
      v_ult     number;
      valores   euji_util.ident_arr2;
   begin
      valores(1) := p1;
      valores(2) := p2;
      valores(3) := p3;
      valores(4) := p4;
      valores(5) := p5;
      valores(6) := p6;
      v_ult := 0;

      for x in 1 .. 6 loop
         if valores(x) > 0 then
            v_ult := v_ult + 1;
         end if;
      end loop;

      for x in 1 .. v_ult loop
         if valores(x) < p_inicial then
            null;
            null;
         elsif valores(x) = p_inicial then
            v_rdo := valores(x);
         else
            if valores(x) = v_rdo + 1 then
               v_rdo := valores(x);
            end if;
         end if;
      end loop;

      if v_ult = 0 then
         v_rdo := -1;
      end if;

      return (v_rdo);
   end;

   procedure refrescar_flag(p_persona_id in number, p_Fase_id in number) is
      v_flag    varchar2(32) := '00000000000000000000000000000000';
      v_aux     number;
      v_curso   number;

      cursor lista_matricula is
         select fase_id, primero, ultimo, grupo_id
         from pho_vw_oferta
         where     persona_id = p_persona_id
               and fase_id <= p_FAse_id;

      function asignar_flag(p_flag in varchar2, p_periodo_id in number)
         return varchar2 is
         v_flag      varchar2(32);
         v_antes     varchar2(100);
         v_despues   varchar2(100);
         v_valor     varchar2(1);
      begin
         v_flag := p_flag;
         v_antes := substr(v_flag, 1, p_periodo_id - 1);
         v_despues := substr(v_flag, p_periodo_id + 1);
         v_valor := substr(v_flag, p_periodo_id, 1);
         v_valor := to_char(to_number(v_valor) + 1);

         if to_number(v_valor) > 1 then
            v_valor := 1;
         end if;

         return (v_antes || v_Valor || v_Despues);
      end;
   begin
      select curso_academico_id
      into v_curso
      from pho_fases
      where id = p_fase_id;

      select count(*)
      into v_aux
      from pho_matriculas
      where     persona_id = p_persona_id
            and curso_academico_id = v_curso;

      if v_aux = 0 then
         select nvl(max(id), 0) + 1
         into v_aux
         from pho_matriculas;

         insert into pho_matriculas(ID, PERSONA_ID, CURSO_ACADEMICO_ID)
         values (v_aux, p_persona_id, v_curso);

         commit;
      end if;

      for x in lista_matricula loop
         for p in x.primero .. x.ultimo loop
            v_flag := asignar_flag(v_flag, p);
         end loop;
      end loop;

      update pho_matriculas
      set flags = v_flag
      where     persona_id = p_persona_id
            and curso_academico_id = (select curso_academico_id
                                      from pho_fases
                                      where id = p_fase_id);

      commit;
   end;

   function plazas_ocupadas(p_asignatura     in number,
                            p_bloque         in number,
                            p_especialidad   in number,
                            p_hospital       in number,
                            p_periodo        in number)
      return number is
      v_rdo   number;
   begin
      select sum(plazas)
      into v_Rdo
      from (select asignatura_id,
                   bloque_id,
                   especialidad_id,
                   hospital_id,
                   p.id,
                   count(*)   plazas
            from pho_oferta o, pho_periodos p
            where     bloque_unido = 1
                  and persona_id is not null
                  and p.id between periodo_inicial_id and periodo_final_id
                  and asignatura_id = p_asignatura
                  and bloque_id = p_bloque
                  and especialidad_id = p_especialidad
                  and hospital_id = p_hospital
                  and p.id = p_periodo
            group by asignatura_id, bloque_id, especialidad_id, hospital_id, p.id
            union all
            select asignatura_id,
                   bloque_id,
                   especialidad_id,
                   hospital_id,
                   p.id,
                   count(*)   plazas
            from pho_oferta o, pho_ofertas_periodos op, pho_periodos p
            where     bloque_unido = 0
                  and persona_id is not null
                  and o.id = op.oferta_id
                  and op.periodo_id = p.id
                  and asignatura_id = p_asignatura
                  and bloque_id = p_bloque
                  and especialidad_id = p_especialidad
                  and hospital_id = p_hospital
                  and p.id = p_periodo
            group by asignatura_id, bloque_id, especialidad_id, hospital_id, p.id);

      return (nvl(v_rdo, 0));
   end;

   function oferta_plazas(p_asignatura     in number,
                          p_bloque         in number,
                          p_especialidad   in number,
                          p_hospital       in number,
                          p_periodo        in number)
      return number is
      v_rdo   number;
   begin
      select sum(plazas)
      into v_Rdo
      from (select asignatura_id,
                   bloque_id,
                   especialidad_id,
                   hospital_id,
                   p.id,
                   count(*)   plazas
            from pho_oferta o, pho_periodos p
            where     bloque_unido = 1
                  and p.id between periodo_inicial_id and periodo_final_id
                  and asignatura_id = p_asignatura
                  and bloque_id = p_bloque
                  and especialidad_id = p_especialidad
                  and hospital_id = p_hospital
                  and p.id = p_periodo
            group by asignatura_id, bloque_id, especialidad_id, hospital_id, p.id
            union all
            select asignatura_id,
                   bloque_id,
                   especialidad_id,
                   hospital_id,
                   p.id,
                   count(*)   plazas
            from pho_oferta o, pho_ofertas_periodos op, pho_periodos p
            where     bloque_unido = 0
                  and o.id = op.oferta_id
                  and op.periodo_id = p.id
                  and asignatura_id = p_asignatura
                  and bloque_id = p_bloque
                  and especialidad_id = p_especialidad
                  and hospital_id = p_hospital
                  and p.id = p_periodo
            group by asignatura_id, bloque_id, especialidad_id, hospital_id, p.id);

      return (nvl(v_rdo, 0));
   end;
end;
/

CREATE OR REPLACE function UJI_PRACTICASHOSPITALARIAS.pho_buscar_ultimo_cons (
   p_inicial   in   number,
   p1          in   number,
   p2          in   number,
   p3          in   number,
   p4          in   number,
   p5          in   number,
   p6          in   number
)
   return number is
   v_rdo     number;
   v_aux     number;
   v_ult     number;
   valores   euji_util.ident_arr2;
begin
   valores (1) := p1;
   valores (2) := p2;
   valores (3) := p3;
   valores (4) := p4;
   valores (5) := p5;
   valores (6) := p6;
   v_ult := 0;

   for x in 1 .. 6 loop
      if valores (x) > 0 then
         v_ult := v_ult + 1;
      end if;
   end loop;

   for x in 1 .. v_ult loop
      if valores (x) < p_inicial then
         null;
         null;
      elsif valores (x) = p_inicial then
         v_rdo := valores (x);
      else
         if valores (x) = v_rdo + 1 then
            v_rdo := valores (x);
         end if;
      end if;
   end loop;

   if v_ult = 0 then
      v_rdo := -1;
   end if;

   return (v_rdo);
end;
/
