CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ASIGNATURAS
  (
    ID                VARCHAR2 (10 BYTE) NOT NULL ,
    NOMBRE            VARCHAR2 (1000 BYTE) NOT NULL ,
    ESTUDIO_ID        NUMBER NOT NULL ,
    ESTUDIO_NOMBRE    VARCHAR2 (1000 BYTE) NOT NULL ,
    CURSO             NUMBER NOT NULL ,
    SEMESTRE          VARCHAR2 (10 BYTE) NOT NULL ,
    orden             NUMBER ,
    CODIGO_ASIGNATURA VARCHAR2 (10 BYTE)
  ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ASIGNATURAS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_ASIGNATURAS
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ASIGNATURAS ADD CONSTRAINT PHO_ASIGNATURAS_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES
    (
      ID                  NUMBER NOT NULL ,
      ASIGNATURA_ID       VARCHAR2 (10 BYTE) NOT NULL ,
      NOMBRE              VARCHAR2 (100 BYTE) NOT NULL ,
      NUMERO_OBLIGATORIAS NUMBER NOT NULL ,
      NUMERO_OPTATIVAS    NUMBER NOT NULL ,
      orden               NUMBER
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES_ASI_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES
    (
      ASIGNATURA_ID ASC
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES ADD CONSTRAINT PHO_BLOQUES_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS
    (
      ID     NUMBER NOT NULL ,
      ACTIVO NUMBER NOT NULL
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS ADD CONSTRAINT PHO_CURSOS_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES
    (
      ID                    NUMBER NOT NULL ,
      NOMBRE                VARCHAR2 (100 BYTE) NOT NULL ,
      TIPO                  VARCHAR2 (10 BYTE) NOT NULL ,
      BLOQUE_ID             NUMBER NOT NULL ,
      PERIODOS_OBLIGATORIOS NUMBER NOT NULL ,
      PERIODOS_OPTATIVOS    NUMBER NOT NULL ,
      SEMESTRE              VARCHAR2 (10 BYTE) NOT NULL ,
      PERIODO_INICIAL_ID    NUMBER ,
      PERIODO_FINAL_ID      NUMBER ,
      orden                 NUMBER
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_BLOQUE_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES
    (
      BLOQUE_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES__IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES
    (
      PERIODO_INICIAL_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES__IDXv1 ON UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES
    (
      PERIODO_FINAL_ID ASC
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ADD CONSTRAINT PHO_ESPECIALIDADES_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS
    (
      ID              NUMBER NOT NULL ,
      ESPECIALIDAD_ID NUMBER NOT NULL ,
      PERIODO_ID      NUMBER NOT NULL
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS
  (
    ID ASC
  )
  ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERI_UN ON UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS
  (
    ESPECIALIDAD_ID ASC , PERIODO_ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS ADD CONSTRAINT PHO_ESPECIALIDADES_PERIODOS_PK PRIMARY KEY ( ID ) ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS ADD CONSTRAINT PHO_ESPECIALIDADES_PERI_UN UNIQUE ( ESPECIALIDAD_ID , PERIODO_ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_EXT_ASIGNATURAS_CURSADAS
    (
      ID                 NUMBER NOT NULL ,
      ESTUDIO_ID         NUMBER NOT NULL ,
      CURSO_ACADEMICO_ID NUMBER NOT NULL ,
      ASIGNATURA_ID      VARCHAR2 (10 BYTE) NOT NULL ,
      NOTA               NUMBER ,
      PERSONA_ID         NUMBER NOT NULL
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_EXT_ASIGNATURAS_CUR_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_EXT_ASIGNATURAS_CURSADAS
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_EXT_ASIGNATURAS_CURSADAS ADD CONSTRAINT PHO_EXT_ASIGNATURAS_CUR_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_EXT_PERSONAS
    (
      ID             NUMBER NOT NULL ,
      NOMBRE         VARCHAR2 (100 BYTE) NOT NULL ,
      IDENTIFICACION VARCHAR2 (100 BYTE) NOT NULL ,
      CUENTA         VARCHAR2 (100 BYTE) ,
      TURNO_ID       NUMBER ,
      TURNO_FECHA    DATE ,
      NOTA_MEDIA     NUMBER
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_EXT_PERSONAS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_EXT_PERSONAS
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_EXT_PERSONAS ADD CONSTRAINT PHO_EXT_PERSONAS_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_FASES
    (
      ID                 NUMBER NOT NULL ,
      CURSO_ACADEMICO_ID NUMBER NOT NULL ,
      NOMBRE             VARCHAR2 (100 BYTE) NOT NULL ,
      FECHA_DESDE        DATE ,
      FECHA_HASTA        DATE ,
      TIPO               VARCHAR2 (10 BYTE) NOT NULL
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_FASES_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_FASES
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_FASES ADD CONSTRAINT PHO_FASES_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_HOSPITALES
    (
      ID     NUMBER NOT NULL ,
      NOMBRE VARCHAR2 (100 BYTE) NOT NULL ,
      CODIGO VARCHAR2 (10 BYTE) NOT NULL ,
      orden  NUMBER
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_HOSPITALES_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_HOSPITALES
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_HOSPITALES ADD CONSTRAINT PHO_HOSPITALES_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES
    (
      ID                 NUMBER NOT NULL ,
      CURSO_ACADEMICO_ID NUMBER NOT NULL ,
      HOSPITAL_ID        NUMBER NOT NULL ,
      ESPECIALIDAD_ID    NUMBER NOT NULL ,
      PLAZAS             NUMBER NOT NULL
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES_CURSO_ACA_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES
    (
      CURSO_ACADEMICO_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES_HOS_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES
    (
      HOSPITAL_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES_ESP_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES
    (
      ESPECIALIDAD_ID ASC
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES ADD CONSTRAINT PHO_LIMITES_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
    (
      ID                 NUMBER NOT NULL ,
      CURSO_ACADEMICO_ID NUMBER NOT NULL ,
      ASIGNATURA_ID      VARCHAR2 (10) NOT NULL ,
      BLOQUE_ID          NUMBER NOT NULL ,
      ESPECIALIDAD_ID    NUMBER NOT NULL ,
      FASE_ID            NUMBER NOT NULL ,
      HOSPITAL_ID        NUMBER NOT NULL ,
      PERIODO_INICIAL_ID NUMBER NOT NULL ,
      PERIODO_FINAL_ID   NUMBER NOT NULL ,
      PERSONA_ID         NUMBER ,
      FECHA              DATE ,
      unico_id           VARCHAR2 (200) NOT NULL ,
      orden_1            NUMBER NOT NULL ,
      orden_2            NUMBER NOT NULL ,
      orden_3            NUMBER NOT NULL ,
      BLOQUE_UNIDO       NUMBER NOT NULL
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
    (
      CURSO_ACADEMICO_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv1 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
    (
      ASIGNATURA_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv2 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
    (
      BLOQUE_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv3 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
    (
      ESPECIALIDAD_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv4 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
    (
      FASE_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv6 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
    (
      HOSPITAL_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv7 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
    (
      PERIODO_INICIAL_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv8 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
    (
      PERIODO_FINAL_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA__IDXv9 ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
    (
      PERSONA_ID ASC
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS
    (
      ID         NUMBER NOT NULL ,
      NOMBRE     VARCHAR2 (100 BYTE) NOT NULL ,
      SEMESTRE_1 NUMBER NOT NULL ,
      SEMESTRE_2 NUMBER NOT NULL
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ADD CONSTRAINT PHO_PERIODOS_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS
    (
      ID           NUMBER NOT NULL ,
      CURSO_ACA_ID NUMBER NOT NULL ,
      PERIODO_ID   NUMBER NOT NULL ,
      FECHA_INICIO DATE NOT NULL ,
      FECHA_FIN    DATE NOT NULL
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS_CUR_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS
    (
      CURSO_ACA_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS_PER_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS
    (
      PERIODO_ID ASC
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS ADD CONSTRAINT PHO_PERIODOS_FECHAS_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS
    (
      ID         NUMBER NOT NULL ,
      PERSONA_ID NUMBER NOT NULL ,
      FASE_ID    NUMBER NOT NULL ,
      FECHA      DATE NOT NULL ,
      FLAGS      VARCHAR2 (24 BYTE) DEFAULT '000000000000000000000000' NOT NULL
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS_FAS_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS
    (
      FASE_ID ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS_PER_IDX ON UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS
    (
      PERSONA_ID ASC
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS_PK ON UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS ADD CONSTRAINT PHO_TURNOS_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos
    (
      id         NUMBER NOT NULL ,
      oferta_id  NUMBER NOT NULL ,
      periodo_id NUMBER NOT NULL
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos_of_IDX ON UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos
    (
      oferta_id ASC
    ) ;
  CREATE INDEX UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos_per_IDX ON UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos
    (
      periodo_id ASC
    ) ;
CREATE UNIQUE INDEX UJI_PRACTICASHOSPITALARIAS.PHO_OFERTAS_PERIODOS_PK ON UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos
  (
    id ASC
  )
  ;
  ALTER TABLE UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos ADD CONSTRAINT pho_ofertas_periodos_PK PRIMARY KEY ( id ) ;
CREATE OR REPLACE VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA ( ID, UNICO_ID, ASIGNATURA_ID, ASIGNATURA, CODIGO_ASIGNATURA, BLOQUE_ID, BLOQUE, ESPECIALIDAD_ID, ESPECIALIDAD, FASE_ID, FASE, GRUPO_ID, TAMA�O, PRIMERO, ULTIMO, P1, P2, P3, P4, P5, P6, CONSECUTIVOS, PLAZAS, HOSPITAL_ID, HOSPITAL, CODIGO, PERSONA_ID, FECHA, FECHA_INICIO, FECHA_FIN, PERSONA_ID_MOSTRAR, ES_SELECCIONABLE, FASE_ACTUAL_ID )
AS
  SELECT xx."ID",
    xx."UNICO_ID",
    xx."ASIGNATURA_ID",
    xx."ASIGNATURA",
    xx."CODIGO_ASIGNATURA",
    xx."BLOQUE_ID",
    xx."BLOQUE",
    xx."ESPECIALIDAD_ID",
    xx."ESPECIALIDAD",
    xx."FASE_ID",
    xx."FASE",
    xx."GRUPO_ID",
    xx."TAMA�O",
    xx."PRIMERO",
    xx."ULTIMO",
    xx."P1",
    xx."P2",
    xx."P3",
    xx."P4",
    xx."P5",
    xx."P6",
    xx."CONSECUTIVOS",
    xx."PLAZAS",
    xx."HOSPITAL_ID",
    xx."HOSPITAL",
    xx."CODIGO",
    xx."PERSONA_ID",
    xx."FECHA",
    xx."FECHA_INICIO",
    xx."FECHA_FIN",
    ac.persona_id persona_id_mostrar,
    DECODE(xx.persona_id, ac.persona_id,1,DECODE(pack_pho_practicas.bloque_disponible(ac.persona_id, fase_id, unico_id), 1, DECODE(pack_pho_practicas.periodos_disponibles(ac.persona_id, fase_id, unico_id),1,1,0),0)) es_seleccionable,
    (SELECT id
    FROM pho_Fases
    WHERE TRUNC(sysdate) BETWEEN fecha_desde AND fecha_hasta
    ) fase_actual_id
  FROM
    (SELECT x.unico_id
      || '_'
      || x.periodo_inicial_id id,
      x.unico_id,
      x.asignatura_id,
      a.nombre asignatura,
      a.codigo_asignatura,
      x.bloque_id,
      b.nombre bloque,
      x.especialidad_id,
      e.nombre especialidad,
      x.fase_id,
      f.nombre fase,
      NULL grupo_id,
      x.periodo_final_id - x.periodo_inicial_id + 1 tama�o,
      x.periodo_inicial_id primero,
      x.periodo_final_id ultimo,
      NULL p1,
      NULL p2,
      NULL p3,
      NULL p4,
      NULL p5,
      NULL p6,
      x.bloque_unido consecutivos,
      plazas,
      x.hospital_id,
      h.nombre hospital,
      h.codigo,
      x.persona_id,
      x.fecha,
      x.fecha_inicio,
      x.fecha_fin
    FROM
      (SELECT o.curso_academico_id,
        o.asignatura_id,
        o.bloque_id,
        o.especialidad_id,
        o.fase_id,
        o.hospital_id,
        o.periodo_inicial_id,
        o.periodo_final_id,
        o.unico_id,
        o.bloque_unido,
        o.orden_1,
        o.orden_2,
        o.orden_3,
        p.fecha_inicio,
        p2.fecha_fin,
        persona_id,
        o.fecha,
        COUNT (*) plazas
      FROM pho_oferta o,
        pho_periodos_fechas p,
        pho_periodos_fechas p2
      WHERE o.bloque_unido     = 1
      AND o.persona_id        IS NULL
      AND o.periodo_inicial_id = p.periodo_id
      AND o.curso_academico_id = p.curso_Aca_id
      AND o.periodo_final_id   = p2.periodo_id
      AND o.curso_academico_id = p2.curso_Aca_id
      GROUP BY o.curso_academico_id,
        o.asignatura_id,
        o.bloque_id,
        o.especialidad_id,
        o.fase_id,
        o.hospital_id,
        o.periodo_inicial_id,
        o.periodo_final_id,
        o.unico_id,
        o.bloque_unido,
        o.orden_1,
        o.orden_2,
        o.orden_3,
        p.fecha_inicio,
        p2.fecha_fin,
        o.persona_id,
        o.fecha
      UNION ALL
      SELECT o.curso_academico_id,
        o.asignatura_id,
        o.bloque_id,
        o.especialidad_id,
        o.fase_id,
        o.hospital_id,
        o.periodo_inicial_id,
        o.periodo_final_id,
        o.unico_id,
        o.bloque_unido,
        o.orden_1,
        o.orden_2,
        o.orden_3,
        p.fecha_inicio,
        p2.fecha_fin,
        persona_id,
        o.fecha,
        COUNT (*) plazas
      FROM pho_oferta o,
        pho_periodos_fechas p,
        pho_periodos_fechas p2
      WHERE o.bloque_unido     = 1
      AND o.persona_id        IS NOT NULL
      AND o.periodo_inicial_id = p.periodo_id
      AND o.curso_academico_id = p.curso_Aca_id
      AND o.periodo_final_id   = p2.periodo_id
      AND o.curso_academico_id = p2.curso_Aca_id
      GROUP BY o.curso_academico_id,
        o.asignatura_id,
        o.bloque_id,
        o.especialidad_id,
        o.fase_id,
        o.hospital_id,
        o.periodo_inicial_id,
        o.periodo_final_id,
        o.unico_id,
        o.bloque_unido,
        o.orden_1,
        o.orden_2,
        o.orden_3,
        p.fecha_inicio,
        p2.fecha_fin,
        o.persona_id,
        o.fecha
      UNION ALL
      SELECT xx.curso_academico_id,
        xx.asignatura_id,
        xx.bloque_id,
        xx.especialidad_id,
        xx.fase_id,
        xx.hospital_id,
        p.id periodo_inicial_id,
        pho_buscar_ultimo_cons (p.id, p1, p2, p3, p4, p5, p6) periodo_final_id,
        xx.unico_id,
        xx.bloque_unido,
        xx.orden_1,
        xx.orden_2,
        xx.orden_3,
        xx.fecha_inicio,
        xx.fecha_fin,
        xx.persona_id,
        xx.fecha,
        xx.plazas
      FROM
        (SELECT x.curso_academico_id,
          x.asignatura_id,
          x.bloque_id,
          x.especialidad_id,
          x.fase_id,
          x.hospital_id,
          x.periodo_inicial_id,
          x.periodo_final_id,
          x.unico_id,
          x.bloque_unido,
          x.orden_1,
          x.orden_2,
          x.orden_3,
          x.fecha_inicio,
          x.fecha_fin,
          x.persona_id,
          x.fecha,
          x.plazas,
          MIN (DECODE (orden, 1, periodo_id, NULL)) p1,
          MIN (DECODE (orden, 2, periodo_id, NULL)) p2,
          MIN (DECODE (orden, 3, periodo_id, NULL)) p3,
          MIN (DECODE (orden, 4, periodo_id, NULL)) p4,
          MIN (DECODE (orden, 5, periodo_id, NULL)) p5,
          MIN (DECODE (orden, 6, periodo_id, NULL)) p6
        FROM
          (SELECT o.curso_academico_id,
            o.asignatura_id,
            o.bloque_id,
            o.especialidad_id,
            o.fase_id,
            o.hospital_id,
            o.periodo_inicial_id,
            o.periodo_final_id,
            o.unico_id,
            o.bloque_unido,
            o.orden_1,
            o.orden_2,
            o.orden_3,
            p.fecha_inicio,
            p2.fecha_fin,
            persona_id,
            fecha,
            COUNT (*) plazas,
            op.periodo_id,
            row_number () over (partition BY o.curso_academico_id, o.asignatura_id, o.bloque_id, o.especialidad_id, o.fase_id, o.hospital_id, o.periodo_inicial_id, o.periodo_final_id, o.unico_id, o.bloque_unido, o.orden_1, o.orden_2, o.orden_3 order by op.periodo_id) orden
          FROM pho_oferta o,
            pho_periodos_fechas p,
            pho_periodos_fechas p2,
            pho_ofertas_periodos op
          WHERE o.bloque_unido     = 0
          AND o.persona_id        IS NULL
          AND o.periodo_inicial_id = p.periodo_id
          AND o.curso_academico_id = p.curso_Aca_id
          AND o.periodo_final_id   = p2.periodo_id
          AND o.curso_academico_id = p2.curso_Aca_id
          AND o.id                 = op.oferta_id
          GROUP BY o.curso_academico_id,
            o.asignatura_id,
            o.bloque_id,
            o.especialidad_id,
            o.fase_id,
            o.hospital_id,
            o.periodo_inicial_id,
            o.periodo_final_id,
            o.unico_id,
            o.bloque_unido,
            o.orden_1,
            o.orden_2,
            o.orden_3,
            p.fecha_inicio,
            p2.fecha_fin,
            o.persona_id,
            o.fecha,
            op.periodo_id
          ) x
        GROUP BY x.curso_academico_id,
          x.asignatura_id,
          x.bloque_id,
          x.especialidad_id,
          x.fase_id,
          x.hospital_id,
          x.periodo_inicial_id,
          x.periodo_final_id,
          x.unico_id,
          x.bloque_unido,
          x.orden_1,
          x.orden_2,
          x.orden_3,
          x.fecha_inicio,
          x.fecha_fin,
          x.persona_id,
          x.fecha,
          x.plazas
        ) xx,
        (SELECT id, id - 1 periodo_ant, nombre FROM pho_periodos
        ) p
      WHERE ( p1 = p.id
      OR ( p2    = p.id
      AND p1    != p.periodo_ant)
      OR ( p3    = p.id
      AND p2    != p.periodo_ant)
      OR ( p4    = p.id
      AND p3    != p.periodo_ant)
      OR ( p5    = p.id
      AND p4    != p.periodo_ant)
      OR ( p6    = p.id
      AND p5    != p.periodo_ant) )
      UNION ALL
      SELECT xx.curso_academico_id,
        xx.asignatura_id,
        xx.bloque_id,
        xx.especialidad_id,
        xx.fase_id,
        xx.hospital_id,
        p.id periodo_inicial_id,
        pho_buscar_ultimo_cons (p.id, p1, p2, p3, p4, p5, p6) periodo_final_id,
        xx.unico_id,
        xx.bloque_unido,
        xx.orden_1,
        xx.orden_2,
        xx.orden_3,
        xx.fecha_inicio,
        xx.fecha_fin,
        xx.persona_id,
        xx.fecha,
        xx.plazas
      FROM
        (SELECT x.curso_academico_id,
          x.asignatura_id,
          x.bloque_id,
          x.especialidad_id,
          x.fase_id,
          x.hospital_id,
          x.periodo_inicial_id,
          x.periodo_final_id,
          x.unico_id,
          x.bloque_unido,
          x.orden_1,
          x.orden_2,
          x.orden_3,
          x.fecha_inicio,
          x.fecha_fin,
          x.persona_id,
          x.fecha,
          x.plazas,
          MIN (DECODE (orden, 1, periodo_id, NULL)) p1,
          MIN (DECODE (orden, 2, periodo_id, NULL)) p2,
          MIN (DECODE (orden, 3, periodo_id, NULL)) p3,
          MIN (DECODE (orden, 4, periodo_id, NULL)) p4,
          MIN (DECODE (orden, 5, periodo_id, NULL)) p5,
          MIN (DECODE (orden, 6, periodo_id, NULL)) p6
        FROM
          (SELECT o.curso_academico_id,
            o.asignatura_id,
            o.bloque_id,
            o.especialidad_id,
            o.fase_id,
            o.hospital_id,
            o.periodo_inicial_id,
            o.periodo_final_id,
            o.unico_id,
            o.bloque_unido,
            o.orden_1,
            o.orden_2,
            o.orden_3,
            p.fecha_inicio,
            p2.fecha_fin,
            persona_id,
            fecha,
            COUNT (*) plazas,
            op.periodo_id,
            row_number () over (partition BY o.curso_academico_id, o.asignatura_id, o.bloque_id, o.especialidad_id, o.fase_id, o.hospital_id, o.periodo_inicial_id, o.periodo_final_id, o.unico_id, o.bloque_unido, o.orden_1, o.orden_2, o.orden_3 order by op.periodo_id) orden
          FROM pho_oferta o,
            pho_periodos_fechas p,
            pho_periodos_fechas p2,
            pho_ofertas_periodos op
          WHERE o.bloque_unido     = 0
          AND o.persona_id        IS NOT NULL
          AND o.periodo_inicial_id = p.periodo_id
          AND o.curso_academico_id = p.curso_Aca_id
          AND o.periodo_final_id   = p2.periodo_id
          AND o.curso_academico_id = p2.curso_Aca_id
          AND o.id                 = op.oferta_id
          GROUP BY o.curso_academico_id,
            o.asignatura_id,
            o.bloque_id,
            o.especialidad_id,
            o.fase_id,
            o.hospital_id,
            o.periodo_inicial_id,
            o.periodo_final_id,
            o.unico_id,
            o.bloque_unido,
            o.orden_1,
            o.orden_2,
            o.orden_3,
            p.fecha_inicio,
            p2.fecha_fin,
            o.persona_id,
            o.fecha,
            op.periodo_id
          ) x
        GROUP BY x.curso_academico_id,
          x.asignatura_id,
          x.bloque_id,
          x.especialidad_id,
          x.fase_id,
          x.hospital_id,
          x.periodo_inicial_id,
          x.periodo_final_id,
          x.unico_id,
          x.bloque_unido,
          x.orden_1,
          x.orden_2,
          x.orden_3,
          x.fecha_inicio,
          x.fecha_fin,
          x.persona_id,
          x.fecha,
          x.plazas
        ) xx,
        (SELECT id, id - 1 periodo_ant, nombre FROM pho_periodos
        ) p
      WHERE ( p1 = p.id
      OR ( p2    = p.id
      AND p1    != p.periodo_ant)
      OR ( p3    = p.id
      AND p2    != p.periodo_ant)
      OR ( p4    = p.id
      AND p3    != p.periodo_ant)
      OR ( p5    = p.id
      AND p4    != p.periodo_ant)
      OR ( p6    = p.id
      AND p5    != p.periodo_ant) )
      ) x,
      pho_asignaturas a,
      pho_bloques b,
      pho_especialidades e,
      pho_fases f,
      pho_hospitales h
    WHERE x.asignatura_id = a.id
    AND x.bloque_id       = b.id
    AND x.especialidad_id = e.id
    AND x.fase_id         = f.id
    AND x.hospital_id     = h.id
    ) xx,
    pho_ext_asignaturas_cursadas ac
  WHERE ( xx.persona_id    = ac.persona_id
  OR xx.persona_id        IS NULL)
  AND xx.codigo_asignatura = ac.asignatura_id ;
CREATE OR REPLACE VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA_BASE ( ASIGNATURA_ID, ASIGNATURA, CODIGO_ASIGNATURA, BLOQUE_ID, BLOQUE, ESPECIALIDAD_ID, ESPECIALIDAD, FASE_ID, FASE, GRUPO_ID, TAMA�O, PRIMERO, ULTIMO, P1, P2, P3, P4, P5, P6, CONSECUTIVOS, PLAZAS, HOSPITAL_ID, HOSPITAL, CODIGO, BLOQUE_UNIDO, ORDEN_1, ORDEN_2, ORDEN_3 )
AS
  SELECT x."ASIGNATURA_ID",
    x."ASIGNATURA",
    x.codigo_asignatura,
    x."BLOQUE_ID",
    x."BLOQUE",
    x."ESPECIALIDAD_ID",
    x."ESPECIALIDAD",
    x."FASE_ID",
    x."FASE",
    x."GRUPO_ID",
    x."TAMA�O",
    x."PRIMERO",
    x."ULTIMO",
    x."P1",
    x."P2",
    x."P3",
    x."P4",
    x."P5",
    x."P6",
    x."CONSECUTIVOS",
    x."PLAZAS",
    x."HOSPITAL_ID",
    x."HOSPITAL",
    x."CODIGO",
    DECODE (tama�o, 1, 'S', 2, DECODE (p1, p2 - 1, 'S', 'N'), 4, DECODE (p1, p2 - 1, DECODE (p2, p3 - 1, DECODE (p3, p4 - 1, 'S', 'N'), 'N'), 'N'), 'X' ) bloque_unido,
    orden_1,
    orden_2,
    orden_3
  FROM
    (SELECT o.*,
      l.plazas,
      h.id hospital_id,
      h.nombre hospital,
      codigo
    FROM
      (SELECT asignatura_id,
        asignatura,
        codigo_asignatura,
        bloque_id,
        bloque,
        especialidad_id,
        especialidad,
        fase_id,
        fase,
        grupo_id,
        periodos_obligatorios tama�o,
        MIN (num) primero,
        MAX (num) ultimo,
        MIN (DECODE (modulo, 1, periodo_id, NULL)) p1,
        MIN (DECODE (modulo, 2, periodo_id, NULL )) p2,
        MIN (DECODE (modulo, 3, periodo_id, NULL)) p3,
        MIN (DECODE (modulo, 4, periodo_id, NULL )) p4,
        MIN (DECODE (modulo, 5, periodo_id, NULL)) p5,
        MIN (DECODE (modulo, 6, periodo_id, NULL )) p6,
        'S' consecutivos,
        orden_1,
        orden_2,
        orden_3
      FROM
        (SELECT xxx.*,
          COUNT (*) over (partition BY asignatura_id, bloque_id, especialidad_id, grupo_id) cuantos
        FROM
          (SELECT x.*,
            TRUNC ((x.num - 1) / periodos_obligatorios) + 1 grupo_id,
            mod (num      - 1, periodos_obligatorios) + 1 modulo
          FROM
            (SELECT a.id asignatura_id,
              a.nombre asignatura,
              a.codigo_asignatura,
              b.id bloque_id,
              b.nombre bloque,
              numero_obligatorias,
              numero_optativas,
              e.id especialidad_id,
              e.nombre especialidad,
              f.id fase_id,
              f.nombre fase,
              periodos_obligatorios,
              p.id periodo_id,
              a.orden orden_1,
              b.orden orden_2,
              e.orden orden_3,
              row_number () over (partition BY a.id, b.id, e.id order by p.id) num
            FROM pho_asignaturas a,
              pho_bloques b,
              pho_especialidades e,
              pho_fases f,
              pho_periodos p
            WHERE a.id              = b.asignatura_id
            AND b.id                = e.bloque_id
            AND f.id                = 1
            AND f.tipo              = e.tipo
            AND periodo_inicial_id IS NOT NULL
            AND p.id BETWEEN periodo_inicial_id AND periodo_final_id
              --and    e.id = 5
            ) x
          ) xxx
        )
      WHERE periodos_obligatorios = cuantos
      GROUP BY asignatura_id,
        asignatura,
        codigo_asignatura,
        bloque_id,
        bloque,
        especialidad_id,
        especialidad,
        fase_id,
        fase,
        grupo_id,
        periodos_obligatorios,
        orden_1,
        orden_2,
        orden_3
      ) o,
      pho_limites l,
      pho_hospitales h
    WHERE o.especialidad_id = l.especialidad_id
    AND hospital_id         = h.id
    UNION ALL
    SELECT o.*,
      l.plazas,
      h.id hospital_id,
      h.nombre hospital,
      codigo
    FROM
      (SELECT asignatura_id,
        asignatura,
        codigo_asignatura,
        bloque_id,
        bloque,
        especialidad_id,
        especialidad,
        fase_id,
        fase,
        grupo_id,
        periodos_obligatorios tama�o,
        MIN (num) primero,
        MAX (num) ultimo,
        MIN (DECODE (modulo, 1, periodo_id, NULL)) p1,
        MIN (DECODE (modulo, 2, periodo_id, NULL )) p2,
        MIN (DECODE (modulo, 3, periodo_id, NULL)) p3,
        MIN (DECODE (modulo, 4, periodo_id, NULL )) p4,
        MIN (DECODE (modulo, 5, periodo_id, NULL)) p5,
        MIN (DECODE (modulo, 6, periodo_id, NULL )) p6,
        'N' consecutivos,
        orden_1,
        orden_2,
        orden_3
      FROM
        (SELECT xxx.*,
          COUNT (*) over (partition BY asignatura_id, bloque_id, especialidad_id, grupo_id) cuantos
        FROM
          (SELECT x.*,
            TRUNC ((x.num - 1) / periodos_obligatorios) + 1 grupo_id,
            mod (num      - 1, periodos_obligatorios) + 1 modulo
          FROM
            (SELECT a.id asignatura_id,
              a.nombre asignatura,
              a.codigo_asignatura,
              b.id bloque_id,
              b.nombre bloque,
              numero_obligatorias,
              numero_optativas,
              e.id especialidad_id,
              e.nombre especialidad,
              periodos_obligatorios,
              p.id periodo_id,
              f.id fase_id,
              f.nombre fase,
              a.orden orden_1,
              b.orden orden_2,
              e.orden orden_3,
              row_number () over (partition BY a.id, b.id, e.id order by p.id) num
            FROM pho_asignaturas a,
              pho_bloques b,
              pho_especialidades e,
              pho_fases f,
              pho_periodos p
            WHERE a.id              = b.asignatura_id
            AND b.id                = e.bloque_id
            AND f.id                = 1
            AND f.tipo              = e.tipo
            AND periodo_inicial_id IS NULL
            AND p.id               IN
              (SELECT periodo_id
              FROM pho_especialidades_periodos
              WHERE especialidad_id = e.id
              )
              --and    e.id in (1,2)
            ) x
          ) xxx
        )
      WHERE periodos_obligatorios = cuantos
      GROUP BY asignatura_id,
        asignatura,
        codigo_asignatura,
        bloque_id,
        bloque,
        especialidad_id,
        especialidad,
        fase_id,
        fase,
        grupo_id,
        periodos_obligatorios,
        orden_1,
        orden_2,
        orden_3
      ) o,
      pho_limites l,
      pho_hospitales h
    WHERE o.especialidad_id = l.especialidad_id
    AND hospital_id         = h.id
    ) x ;
CREATE OR REPLACE VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA_PREVIA ( ID, UNICO_ID, ASIGNATURA_ID, ASIGNATURA, BLOQUE_ID, BLOQUE, ESPECIALIDAD_ID, ESPECIALIDAD, FASE_ID, FASE, GRUPO_ID, TAMA�O, PRIMERO, ULTIMO, P1, P2, P3, P4, P5, P6, CONSECUTIVOS, PLAZAS, HOSPITAL_ID, HOSPITAL, CODIGO )
          AS
WITH base AS
  (SELECT asignatura_id
    || '_'
    || bloque_id
    || '_'
    || especialidad_id
    || '_'
    || hospital_id
    || '_'
    || grupo_id unico_id,
    b.*
  FROM
    (SELECT o.*,
      l.plazas,
      h.id hospital_id,
      h.nombre hospital,
      codigo
    FROM
      (SELECT asignatura_id,
        asignatura,
        bloque_id,
        bloque,
        especialidad_id,
        especialidad,
        fase_id,
        fase,
        grupo_id,
        periodos_obligatorios tama�o,
        MIN (num) primero,
        MAX (num) ultimo,
        MIN (DECODE (modulo, 1, periodo_id, NULL)) p1,
        MIN (DECODE (modulo, 2, periodo_id, NULL)) p2,
        MIN (DECODE (modulo, 3, periodo_id, NULL)) p3,
        MIN (DECODE (modulo, 4, periodo_id, NULL)) p4,
        MIN (DECODE (modulo, 5, periodo_id, NULL)) p5,
        MIN (DECODE (modulo, 6, periodo_id, NULL)) p6,
        'S' consecutivos
      FROM
        (SELECT xxx.*,
          COUNT (*) over (partition BY asignatura_id, bloque_id, especialidad_id, grupo_id) cuantos
        FROM
          (SELECT x.*,
            TRUNC ((x.num - 1) / periodos_obligatorios) + 1 grupo_id,
            mod (num      - 1, periodos_obligatorios) + 1 modulo
          FROM
            (SELECT a.id asignatura_id,
              a.nombre asignatura,
              b.id bloque_id,
              b.nombre bloque,
              numero_obligatorias,
              numero_optativas,
              e.id especialidad_id,
              e.nombre especialidad,
              f.id fase_id,
              f.nombre fase ,
              periodos_obligatorios,
              p.id periodo_id,
              row_number () over (partition BY a.id, b.id, e.id order by p.id) num
            FROM pho_asignaturas a,
              pho_bloques b,
              pho_especialidades e,
              pho_fases f,
              pho_periodos p
            WHERE a.id              = b.asignatura_id
            AND b.id                = e.bloque_id
            AND f.id                = 1
            AND f.tipo              = e.tipo
            AND periodo_inicial_id IS NOT NULL
            AND p.id BETWEEN periodo_inicial_id AND periodo_final_id
              --and    e.id = 5
            ) x
          ) xxx
        )
      WHERE periodos_obligatorios = cuantos
      GROUP BY asignatura_id,
        asignatura,
        bloque_id,
        bloque,
        especialidad_id,
        especialidad,
        fase_id,
        fase,
        grupo_id,
        periodos_obligatorios
      ) o,
      pho_limites l,
      pho_hospitales h
    WHERE o.especialidad_id = l.especialidad_id
    AND hospital_id         = h.id
    UNION ALL
    SELECT o.*,
      l.plazas,
      h.id hospital_id,
      h.nombre hospital,
      codigo
    FROM
      (SELECT asignatura_id,
        asignatura,
        bloque_id,
        bloque,
        especialidad_id,
        especialidad,
        fase_id,
        fase,
        grupo_id,
        periodos_obligatorios tama�o,
        MIN (num) primero,
        MAX (num) ultimo,
        MIN (DECODE (modulo, 1, periodo_id, NULL)) p1,
        MIN (DECODE (modulo, 2, periodo_id, NULL)) p2,
        MIN (DECODE (modulo, 3, periodo_id, NULL)) p3,
        MIN (DECODE (modulo, 4, periodo_id, NULL)) p4,
        MIN (DECODE (modulo, 5, periodo_id, NULL)) p5,
        MIN (DECODE (modulo, 6, periodo_id, NULL)) p6,
        'N' consecutivos
      FROM
        (SELECT xxx.*,
          COUNT (*) over (partition BY asignatura_id, bloque_id, especialidad_id, grupo_id) cuantos
        FROM
          (SELECT x.*,
            TRUNC ((x.num - 1) / periodos_obligatorios) + 1 grupo_id,
            mod (num      - 1, periodos_obligatorios) + 1 modulo
          FROM
            (SELECT a.id asignatura_id,
              a.nombre asignatura,
              b.id bloque_id,
              b.nombre bloque,
              numero_obligatorias,
              numero_optativas,
              e.id especialidad_id,
              e.nombre especialidad,
              periodos_obligatorios,
              p.id periodo_id,
              f.id fase_id,
              f.nombre fase,
              row_number () over (partition BY a.id, b.id, e.id order by p.id) num
            FROM pho_asignaturas a,
              pho_bloques b,
              pho_especialidades e,
              pho_fases f,
              pho_periodos p
            WHERE a.id              = b.asignatura_id
            AND b.id                = e.bloque_id
            AND f.id                = 1
            AND f.tipo              = e.tipo
            AND periodo_inicial_id IS NULL
            AND p.id               IN
              (SELECT periodo_id
              FROM pho_especialidades_periodos
              WHERE especialidad_id = e.id
              )
              --and    e.id in (1,2)
            ) x
          ) xxx
        )
      WHERE periodos_obligatorios = cuantos
      GROUP BY asignatura_id,
        asignatura,
        bloque_id,
        bloque,
        especialidad_id,
        especialidad,
        fase_id,
        fase,
        grupo_id,
        periodos_obligatorios
      ) o,
      pho_limites l,
      pho_hospitales h
    WHERE o.especialidad_id = l.especialidad_id
    AND hospital_id         = h.id
    ) b
  )
SELECT b.unico_id
  || '_'
  || p.id id,
  b."UNICO_ID",
  b."ASIGNATURA_ID",
  b."ASIGNATURA",
  b."BLOQUE_ID",
  b."BLOQUE",
  b."ESPECIALIDAD_ID",
  b."ESPECIALIDAD",
  b.fase_id,
  b.fase,
  b."GRUPO_ID",
  pho_buscar_ultimo_cons (p.id, p1, p2, p3, p4, p5, p6) - p.id + 1 "TAMA�O",
  p.id "PRIMERO",
  pho_buscar_ultimo_cons (p.id, p1, p2, p3, p4, p5, p6) "ULTIMO",
  b."P1",
  b."P2",
  b."P3",
  b."P4",
  b."P5",
  b."P6",
  b."CONSECUTIVOS",
  b."PLAZAS",
  b."HOSPITAL_ID",
  b."HOSPITAL",
  b."CODIGO"
FROM base b,
  (SELECT id, id - 1 periodo_ant, nombre FROM pho_periodos
  ) p
WHERE ( p1 = p.id
OR ( p2    = p.id
AND p1    != p.periodo_ant)
OR ( p3    = p.id
AND p2    != p.periodo_ant)
OR ( p4    = p.id
AND p3    != p.periodo_ant)
OR ( p5    = p.id
AND p4    != p.periodo_ant)
OR ( p6    = p.id
AND p5    != p.periodo_ant) ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES ADD CONSTRAINT PHO_BLOQUES_PHO_ASI_FK FOREIGN KEY ( ASIGNATURA_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_ASIGNATURAS ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ADD CONSTRAINT PHO_ESPECIALIDADES_BLOQ_FK FOREIGN KEY ( BLOQUE_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_BLOQUES ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ADD CONSTRAINT PHO_ESPECIALIDADES_PERI_FK FOREIGN KEY ( PERIODO_INICIAL_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ADD CONSTRAINT PHO_ESPECIALIDADES_PER_2_FK FOREIGN KEY ( PERIODO_FINAL_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS ADD CONSTRAINT PHO_ESP_PERI_ESP_FK FOREIGN KEY ( ESPECIALIDAD_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES_PERIODOS ADD CONSTRAINT PHO_ESP_PER_PERI_FK FOREIGN KEY ( PERIODO_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_FASES ADD CONSTRAINT PHO_FASES_PHO_CURSOS_FK FOREIGN KEY ( CURSO_ACADEMICO_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES ADD CONSTRAINT PHO_LIMITES_CURSOS_FK FOREIGN KEY ( CURSO_ACADEMICO_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES ADD CONSTRAINT PHO_LIMITES_ESPE_FK FOREIGN KEY ( ESPECIALIDAD_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES ADD CONSTRAINT PHO_LIMITES_HOSP_FK FOREIGN KEY ( HOSPITAL_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_HOSPITALES ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_ASIGNATURAS_FK FOREIGN KEY ( ASIGNATURA_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_ASIGNATURAS ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_CURSOS_FK FOREIGN KEY ( CURSO_ACADEMICO_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_ESP_FK FOREIGN KEY ( ESPECIALIDAD_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_FASES_FK FOREIGN KEY ( FASE_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_FASES ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_HOSPITALES_FK FOREIGN KEY ( HOSPITAL_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_HOSPITALES ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_PERIODOS_FK FOREIGN KEY ( PERIODO_INICIAL_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ADD CONSTRAINT PHO_OFERTA_PERIODOS_FK2 FOREIGN KEY ( PERIODO_FINAL_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS ADD CONSTRAINT PHO_PERIOD_FECHAS_CURS_FK FOREIGN KEY ( CURSO_ACA_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_CURSOS ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS_FECHAS ADD CONSTRAINT PHO_PERIOD_FECHAS_PER_FK FOREIGN KEY ( PERIODO_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_TURNOS ADD CONSTRAINT PHO_TURNOS_PHO_FASES_FK FOREIGN KEY ( FASE_ID ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_FASES ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos ADD CONSTRAINT pho_ofertas_periodos_OF_FK FOREIGN KEY ( oferta_id ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_OFERTA ( ID ) ;
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.pho_ofertas_periodos ADD CONSTRAINT pho_ofertas_periodos_PER_FK FOREIGN KEY ( periodo_id ) REFERENCES UJI_PRACTICASHOSPITALARIAS.PHO_PERIODOS ( ID ) ;
