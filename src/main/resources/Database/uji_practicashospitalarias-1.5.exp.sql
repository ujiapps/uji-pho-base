ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_ESPECIALIDADES ADD (COMENTARIOS VARCHAR2(1000 Char));
ALTER TABLE UJI_PRACTICASHOSPITALARIAS.PHO_LIMITES ADD (COMENTARIOS VARCHAR2(1000 Char));

create or replace force view UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA_BASE
(
   ASIGNATURA_ID,
   ASIGNATURA,
   CODIGO_ASIGNATURA,
   BLOQUE_ID,
   BLOQUE,
   ESPECIALIDAD_ID,
   ESPECIALIDAD,
   FASE_ID,
   FASE,
   GRUPO_ID,
   TAMAÑO,
   PRIMERO,
   ULTIMO,
   P1,
   P2,
   P3,
   P4,
   P5,
   P6,
   CONSECUTIVOS,
   PLAZAS,
   HOSPITAL_ID,
   HOSPITAL,
   CODIGO,
   BLOQUE_UNIDO,
   ORDEN_1,
   ORDEN_2,
   ORDEN_3
)
   bequeath definer as
   select x."ASIGNATURA_ID", x."ASIGNATURA", x.codigo_asignatura, x."BLOQUE_ID", x."BLOQUE", x."ESPECIALIDAD_ID", x."ESPECIALIDAD", x."FASE_ID", x."FASE", x."GRUPO_ID", x."TAMAÑO", x."PRIMERO", x."ULTIMO", x."P1", x."P2", x."P3", x."P4", x."P5", x."P6", x."CONSECUTIVOS", x."PLAZAS", x."HOSPITAL_ID", x."HOSPITAL", x."CODIGO", decode(tamaño,  1, 'S',  2, decode(p1, p2 - 1, 'S', 'N'),  3, decode(p1, p2 - 1, decode(p2, p3 - 1, 'S', 'N'), 'N'),  4, decode(p1, p2 - 1, decode(p2, p3 - 1, decode(p3, p4 - 1, 'S', 'N'), 'N'), 'N'),  5, decode(p1, p2 - 1, decode(p2, p3 - 1, decode(p3, p4 - 1, decode(p4, p5 - 1, 'S', 'N'), 'N'), 'N'), 'N'),  'X') bloque_unido, orden_1, orden_2, orden_3
     from (select o.*, l.plazas, h.id hospital_id, h.nombre hospital, codigo
             from (  select asignatura_id, asignatura, codigo_asignatura, bloque_id, bloque, especialidad_id, especialidad, fase_id, fase, grupo_id, periodos_obligatorios tamaño, min(num) primero, max(num) ultimo, min(decode(modulo, 1, periodo_id, null)) p1, min(decode(modulo, 2, periodo_id, null)) p2, min(decode(modulo, 3, periodo_id, null)) p3, min(decode(modulo, 4, periodo_id, null)) p4, min(decode(modulo, 5, periodo_id, null)) p5, min(decode(modulo, 6, periodo_id, null)) p6, 'S' consecutivos, orden_1, orden_2, orden_3
                       from (select xxx.*, count(*) over (partition by asignatura_id, fase_id, bloque_id, especialidad_id, grupo_id) cuantos
                               from (select x.*, trunc((x.num - 1) / periodos_obligatorios) + 1 grupo_id, mod(num - 1, periodos_obligatorios) + 1 modulo
                                       from (select a.id asignatura_id, a.nombre asignatura, a.codigo_asignatura, b.id bloque_id, b.nombre bloque, numero_obligatorias, numero_optativas, e.id especialidad_id, e.nombre especialidad, f.id fase_id, f.nombre fase, periodos_obligatorios, p.id periodo_id, a.orden orden_1, b.orden orden_2, e.orden orden_3, row_number() over(partition by a.id, f.id, b.id, e.id order by p.id) num
                                               from pho_asignaturas a, pho_bloques b, pho_especialidades e, pho_fases f, pho_periodos p, pho_fases_asignaturas fa
                                              where a.id = b.asignatura_id and b.id = e.bloque_id --AND f.id = 1
                                                                                                 and f.id not in (2, 3) and f.id = fa.fase_id and a.id = fa.asignatura_id and e.tipo like
                                                       '%' || f.tipo || '%' and periodo_inicial_id is not null and p.id between periodo_inicial_id and periodo_final_id --and    e.id = 5
                                                                                                                                                                       ) x) xxx)
                      where periodos_obligatorios = cuantos
                   group by asignatura_id,
                            asignatura,
                            codigo_asignatura,
                            bloque_id,
                            bloque,
                            especialidad_id,
                            especialidad,
                            fase_id,
                            fase,
                            grupo_id,
                            periodos_obligatorios,
                            orden_1,
                            orden_2,
                            orden_3) o,
                  pho_limites    l,
                  pho_hospitales h
            where o.especialidad_id = l.especialidad_id and hospital_id = h.id
           union all
           select o.*, l.plazas, h.id hospital_id, h.nombre hospital, codigo
             from (  select asignatura_id, asignatura, codigo_asignatura, bloque_id, bloque, especialidad_id, especialidad, fase_id, fase, grupo_id, periodos_obligatorios tamaño, min(num) primero, max(num) ultimo, min(decode(modulo, 1, periodo_id, null)) p1, min(decode(modulo, 2, periodo_id, null)) p2, min(decode(modulo, 3, periodo_id, null)) p3, min(decode(modulo, 4, periodo_id, null)) p4, min(decode(modulo, 5, periodo_id, null)) p5, min(decode(modulo, 6, periodo_id, null)) p6, 'N' consecutivos, orden_1, orden_2, orden_3
                       from (select xxx.*, count(*) over (partition by asignatura_id, fase_id, bloque_id, especialidad_id, grupo_id) cuantos
                               from (select x.*, trunc((x.num - 1) / periodos_obligatorios) + 1 grupo_id, mod(num - 1, periodos_obligatorios) + 1 modulo
                                       from (select a.id asignatura_id, a.nombre asignatura, a.codigo_asignatura, b.id bloque_id, b.nombre bloque, numero_obligatorias, numero_optativas, e.id especialidad_id, e.nombre especialidad, periodos_obligatorios, p.id periodo_id, f.id fase_id, f.nombre fase, a.orden orden_1, b.orden orden_2, e.orden orden_3, row_number() over(partition by a.id, f.id, b.id, e.id order by p.id) num
                                               from pho_asignaturas a, pho_bloques b, pho_especialidades e, pho_fases f, pho_periodos p, pho_Fases_asignaturas fa
                                              where a.id = b.asignatura_id and b.id = e.bloque_id --and f.id= 1
                                                                                                 and f.id not in (2, 3) and f.id = fa.fase_id and a.id = fa.asignatura_id and f.tipo = 'OB' and e.tipo
                                                    like
                                                       '%' || f.tipo || '%' and periodo_inicial_id is null and p.id in (select periodo_id
                                                                                                                          from pho_especialidades_periodos
                                                                                                                         where especialidad_id = e.id) --and    e.id in (1,2)
                                                                                                                                                      ) x) xxx)
                      where periodos_obligatorios = cuantos
                   group by asignatura_id,
                            asignatura,
                            codigo_asignatura,
                            bloque_id,
                            bloque,
                            especialidad_id,
                            especialidad,
                            fase_id,
                            fase,
                            grupo_id,
                            periodos_obligatorios,
                            orden_1,
                            orden_2,
                            orden_3) o,
                  pho_limites    l,
                  pho_hospitales h
            where o.especialidad_id = l.especialidad_id and hospital_id = h.id) x;

CREATE OR REPLACE FORCE VIEW UJI_PRACTICASHOSPITALARIAS.PHO_VW_OFERTA
(ID, UNICO_ID, ASIGNATURA_ID, ASIGNATURA, CODIGO_ASIGNATURA,
 BLOQUE_ID, BLOQUE, ESPECIALIDAD_ID, ESPECIALIDAD, COMENTARIOS_ESPECIALIDAD,
 FASE_ID, FASE, FASE_TIPO, GRUPO_ID, TAMAÑO,
 PRIMERO, ULTIMO, P1, P2, P3,
 P4, P5, P6, CONSECUTIVOS, PLAZAS,
 HOSPITAL_ID, HOSPITAL, CODIGO, PERSONA_ID, FECHA,
 FECHA_INICIO, FECHA_FIN, PERSONA_ID_MOSTRAR, ES_SELECCIONABLE, FASE_ACTUAL_ID,
 ORDEN_1, ORDEN_2, ORDEN_3, COMENTARIOS_LIMITES)
BEQUEATH DEFINER
AS
SELECT x."ID",
           x."UNICO_ID",
           x."ASIGNATURA_ID",
           x."ASIGNATURA",
           x."CODIGO_ASIGNATURA",
           x."BLOQUE_ID",
           x."BLOQUE",
           x."ESPECIALIDAD_ID",
           x."ESPECIALIDAD",
           x."COMENTARIOS_ESPECIALIDAD",
           x."FASE_ID",
           x."FASE",
           x."FASE_TIPO",
           x."GRUPO_ID",
           x."TAMAÑO",
           x."PRIMERO",
           x."ULTIMO",
           x."P1",
           x."P2",
           x."P3",
           x."P4",
           x."P5",
           x."P6",
           x."CONSECUTIVOS",
           x."PLAZAS",
           x."HOSPITAL_ID",
           x."HOSPITAL",
           x."CODIGO",
           x."PERSONA_ID",
           x."FECHA",
           x."FECHA_INICIO",
           x."FECHA_FIN",
           x."PERSONA_ID_MOSTRAR",
           x."ES_SELECCIONABLE",
           x."FASE_ACTUAL_ID",
           x."ORDEN_1",
           x."ORDEN_2",
           x."ORDEN_3",
           l.comentarios AS comentarios
    FROM (SELECT xxxx."ID",
                 xxxx."UNICO_ID",
                 xxxx."ASIGNATURA_ID",
                 xxxx."ASIGNATURA",
                 xxxx."CODIGO_ASIGNATURA",
                 xxxx."BLOQUE_ID",
                 xxxx."BLOQUE",
                 xxxx."ESPECIALIDAD_ID",
                 xxxx."ESPECIALIDAD",
                 xxxx."COMENTARIOS_ESPECIALIDAD",
                 xxxx."FASE_ID",
                 xxxx."FASE",
                 xxxx."FASE_TIPO",
                 xxxx."GRUPO_ID",
                 xxxx."TAMAÑO",
                 xxxx."PRIMERO",
                 xxxx."ULTIMO",
                 xxxx."P1",
                 xxxx."P2",
                 xxxx."P3",
                 xxxx."P4",
                 xxxx."P5",
                 xxxx."P6",
                 xxxx."CONSECUTIVOS",
                 xxxx."PLAZAS",
                 xxxx."HOSPITAL_ID",
                 xxxx."HOSPITAL",
                 xxxx."CODIGO",
                 xxxx."PERSONA_ID",
                 xxxx."FECHA",
                 xxxx."FECHA_INICIO",
                 xxxx."FECHA_FIN",
                 xxxx."PERSONA_ID_MOSTRAR",
                 xxxx."ES_SELECCIONABLE",
                 xxxx."FASE_ACTUAL_ID",
                 xxxx."ORDEN_1",
                 xxxx."ORDEN_2",
                 xxxx."ORDEN_3"
          FROM (SELECT ID,
                       UNICO_ID,
                       ASIGNATURA_ID,
                       ASIGNATURA,
                       CODIGO_ASIGNATURA,
                       BLOQUE_ID,
                       BLOQUE,
                       ESPECIALIDAD_ID,
                       ESPECIALIDAD,
                       COMENTARIOS_ESPECIALIDAD,
                       FASE_ID,
                       FASE,
                       FASE_TIPO,
                       GRUPO_ID,
                       TAMAÑO,
                       PRIMERO,
                       ULTIMO,
                       P1,
                       P2,
                       P3,
                       P4,
                       P5,
                       P6,
                       CONSECUTIVOS,
                       PLAZAS,
                       HOSPITAL_ID,
                       HOSPITAL,
                       CODIGO,
                       PERSONA_ID,
                       FECHA,
                       FECHA_INICIO,
                       FECHA_FIN,
                       PERSONA_ID_MOSTRAR,
                       ES_SELECCIONABLE,
                       FASE_ACTUAL_ID,
                       orden_1,
                       orden_2,
                       orden_3
                FROM (SELECT "ID",
                             "UNICO_ID",
                             "ASIGNATURA_ID",
                             "ASIGNATURA",
                             "CODIGO_ASIGNATURA",
                             "BLOQUE_ID",
                             "BLOQUE",
                             "ESPECIALIDAD_ID",
                             "ESPECIALIDAD",
                             "COMENTARIOS_ESPECIALIDAD",
                             "FASE_ID",
                             "FASE",
                             "FASE_TIPO",
                             "GRUPO_ID",
                             "TAMAÑO",
                             "PRIMERO",
                             "ULTIMO",
                             "P1",
                             "P2",
                             "P3",
                             "P4",
                             "P5",
                             "P6",
                             "CONSECUTIVOS",
                             "PLAZAS",
                             "HOSPITAL_ID",
                             "HOSPITAL",
                             "CODIGO",
                             "PERSONA_ID",
                             "FECHA",
                             "FECHA_INICIO",
                             "FECHA_FIN",
                             "PERSONA_ID_MOSTRAR",
                             "ES_SELECCIONABLE",
                             "FASE_ACTUAL_ID",
                             ROW_NUMBER() OVER(PARTITION BY persona_id_mostrar, id ORDER BY id, persona_id) orden,
                             orden_1,
                             orden_2,
                             orden_3
                      FROM (SELECT xx."ID",
                                   xx."UNICO_ID",
                                   xx."ASIGNATURA_ID",
                                   xx."ASIGNATURA",
                                   xx."CODIGO_ASIGNATURA",
                                   xx."BLOQUE_ID",
                                   xx."BLOQUE",
                                   xx."ESPECIALIDAD_ID",
                                   xx."ESPECIALIDAD",
                                   xx."COMENTARIOS_ESPECIALIDAD",
                                   xx."FASE_ID",
                                   xx."FASE",
                                   xx."FASE_TIPO",
                                   xx."GRUPO_ID",
                                   xx."TAMAÑO",
                                   xx."PRIMERO",
                                   xx."ULTIMO",
                                   xx."P1",
                                   xx."P2",
                                   xx."P3",
                                   xx."P4",
                                   xx."P5",
                                   xx."P6",
                                   xx."CONSECUTIVOS",
                                   xx."PLAZAS",
                                   xx."HOSPITAL_ID",
                                   xx."HOSPITAL",
                                   xx."CODIGO",
                                   xx."PERSONA_ID",
                                   xx."FECHA",
                                   xx."FECHA_INICIO",
                                   xx."FECHA_FIN",
                                   ac.persona_id persona_id_mostrar,
                                   DECODE
                                   (
                                       grupo_id,
                                       NULL, DECODE
                                             (
                                                 xx.persona_id,
                                                 ac.persona_id, 1,
                                                 DECODE
                                                 (
                                                     pack_pho_practicas.bloque_disponible(ac.persona_id,
                                                                                          fase_id,
                                                                                          unico_id),
                                                     1, DECODE
                                                        (
                                                            pack_pho_practicas.especialidad_disponible(ac.persona_id,
                                                                                                       1,
                                                                                                       unico_id),
                                                            1, DECODE
                                                               (
                                                                   pack_pho_practicas.periodos_disponibles
                                                                   (
                                                                       ac.persona_id,
                                                                       fase_id,
                                                                       unico_id
                                                                   ),
                                                                   1, 1,
                                                                   0
                                                               ),
                                                            0
                                                        ),
                                                     0
                                                 )
                                             ),
                                       DECODE
                                       (
                                           xx.persona_id,
                                           ac.persona_id, 1,
                                           DECODE
                                           (
                                               pack_pho_practicas.grupo_seleccionado(ac.persona_id, fase_id, unico_id),
                                               -1, DECODE
                                                   (
                                                       pack_pho_practicas.un_periodo_disponible(ac.persona_id,
                                                                                                fase_id,
                                                                                                primero),
                                                       1, 1,
                                                       0
                                                   ),
                                               grupo_id, DECODE
                                                         (
                                                             pack_pho_practicas.un_periodo_disponible(ac.persona_id,
                                                                                                      fase_id,
                                                                                                      primero),
                                                             1, decode
                                                                (
                                                                    pack_pho_practicas.un_periodo_disponible
                                                                    (
                                                                        ac.persona_id,
                                                                        fase_id,
                                                                        ultimo
                                                                    ),
                                                                    1, DECODE
                                                                       (
                                                                           pack_pho_practicas.especialidad_disponible
                                                                           (
                                                                               ac.persona_id,
                                                                               1,
                                                                               unico_id
                                                                           ),
                                                                           1, 1,
                                                                           0
                                                                       ),
                                                                    0
                                                                ),
                                                             0
                                                         ),
                                               0
                                           )
                                       )
                                   )   es_seleccionable,
                                   (SELECT id
                                    FROM pho_Fases
                                    WHERE TRUNC(SYSDATE) BETWEEN fecha_desde AND fecha_hasta - 1) fase_actual_id,
                                   orden_1,
                                   orden_2,
                                   orden_3
                            FROM (SELECT x.unico_id || '_' || x.periodo_inicial_id id,
                                         x.unico_id,
                                         x.asignatura_id,
                                         a.nombre asignatura,
                                         a.codigo_asignatura,
                                         x.bloque_id,
                                         b.nombre bloque,
                                         x.especialidad_id,
                                         e.nombre especialidad,
                                         e.comentarios comentarios_especialidad,
                                         x.fase_id,
                                         f.nombre fase,
                                         --decode(x.fase_id,1,null,2,null,3,null,x.grupo_id)       grupo_id,
                                         x.grupo_id grupo_id,
                                         x.periodo_final_id - x.periodo_inicial_id + 1 tamaño,
                                         x.periodo_inicial_id primero,
                                         x.periodo_final_id ultimo,
                                         NULL p1,
                                         NULL p2,
                                         NULL p3,
                                         NULL p4,
                                         NULL p5,
                                         NULL p6,
                                         x.bloque_unido consecutivos,
                                         plazas,
                                         x.hospital_id,
                                         h.nombre hospital,
                                         h.codigo,
                                         x.persona_id,
                                         x.fecha,
                                         x.fecha_inicio,
                                         x.fecha_fin,
                                         orden_1,
                                         orden_2,
                                         orden_3,
                                         f.tipo fase_tipo
                                  FROM (SELECT o.curso_academico_id,
                                               o.asignatura_id,
                                               o.bloque_id,
                                               o.especialidad_id,
                                               o.fase_id,
                                               o.hospital_id,
                                               o.periodo_inicial_id,
                                               o.periodo_final_id,
                                               o.unico_id,
                                               o.bloque_unido,
                                               o.orden_1,
                                               o.orden_2,
                                               o.orden_3,
                                               p.fecha_inicio,
                                               p2.fecha_fin,
                                               persona_id,
                                               o.fecha,
                                               o.grupo_id grupo_id,
                                               COUNT(*) plazas
                                        FROM pho_oferta o, pho_periodos_fechas p, pho_periodos_fechas p2
                                        WHERE     o.bloque_unido = 1
                                              AND o.persona_id IS NULL
                                              AND o.periodo_inicial_id = p.periodo_id
                                              AND o.curso_academico_id = p.curso_Aca_id
                                              AND o.fase_id = p.fase_id
                                              AND o.periodo_final_id = p2.periodo_id
                                              AND o.curso_academico_id = p2.curso_Aca_id
                                              AND o.fase_id = p2.fase_id
                                        GROUP BY o.curso_academico_id,
                                                 o.asignatura_id,
                                                 o.bloque_id,
                                                 o.especialidad_id,
                                                 o.fase_id,
                                                 o.hospital_id,
                                                 o.periodo_inicial_id,
                                                 o.periodo_final_id,
                                                 o.unico_id,
                                                 o.bloque_unido,
                                                 o.orden_1,
                                                 o.orden_2,
                                                 o.orden_3,
                                                 p.fecha_inicio,
                                                 p2.fecha_fin,
                                                 o.persona_id,
                                                 o.fecha,
                                                 o.grupo_id
                                        UNION ALL
                                        SELECT o.curso_academico_id,
                                               o.asignatura_id,
                                               o.bloque_id,
                                               o.especialidad_id,
                                               o.fase_id,
                                               o.hospital_id,
                                               o.periodo_inicial_id,
                                               o.periodo_final_id,
                                               o.unico_id,
                                               o.bloque_unido,
                                               o.orden_1,
                                               o.orden_2,
                                               o.orden_3,
                                               p.fecha_inicio,
                                               p2.fecha_fin,
                                               persona_id,
                                               o.fecha,
                                               o.grupo_id grupo_id,
                                               COUNT(*) plazas
                                        FROM pho_oferta o, pho_periodos_fechas p, pho_periodos_fechas p2
                                        WHERE     o.bloque_unido = 1
                                              AND o.persona_id IS NOT NULL
                                              AND o.periodo_inicial_id = p.periodo_id
                                              AND o.curso_academico_id = p.curso_Aca_id
                                              AND o.fase_id = p.fase_id
                                              AND o.periodo_final_id = p2.periodo_id
                                              AND o.curso_academico_id = p2.curso_Aca_id
                                              AND o.fase_id = p2.fase_id
                                        GROUP BY o.curso_academico_id,
                                                 o.asignatura_id,
                                                 o.bloque_id,
                                                 o.especialidad_id,
                                                 o.fase_id,
                                                 o.hospital_id,
                                                 o.periodo_inicial_id,
                                                 o.periodo_final_id,
                                                 o.unico_id,
                                                 o.bloque_unido,
                                                 o.orden_1,
                                                 o.orden_2,
                                                 o.orden_3,
                                                 p.fecha_inicio,
                                                 p2.fecha_fin,
                                                 o.persona_id,
                                                 o.fecha,
                                                 o.grupo_id
                                        UNION ALL
                                        SELECT xx.curso_academico_id,
                                               xx.asignatura_id,
                                               xx.bloque_id,
                                               xx.especialidad_id,
                                               xx.fase_id,
                                               xx.hospital_id,
                                               p.id periodo_inicial_id,
                                               pack_pho_practicas.pho_buscar_ultimo_cons(p.id,
                                                                                         p1,
                                                                                         p2,
                                                                                         p3,
                                                                                         p4,
                                                                                         p5,
                                                                                         p6) periodo_final_id,
                                               xx.unico_id,
                                               xx.bloque_unido,
                                               xx.orden_1,
                                               xx.orden_2,
                                               xx.orden_3,
                                               xx.fecha_inicio,
                                               xx.fecha_fin,
                                               xx.persona_id,
                                               xx.fecha,
                                               xx.grupo_id,
                                               xx.plazas
                                        FROM (SELECT x.curso_academico_id,
                                                     x.asignatura_id,
                                                     x.bloque_id,
                                                     x.especialidad_id,
                                                     x.fase_id,
                                                     x.hospital_id,
                                                     x.periodo_inicial_id,
                                                     x.periodo_final_id,
                                                     x.unico_id,
                                                     x.bloque_unido,
                                                     x.orden_1,
                                                     x.orden_2,
                                                     x.orden_3,
                                                     x.fecha_inicio,
                                                     x.fecha_fin,
                                                     x.persona_id,
                                                     x.fecha,
                                                     x.plazas,
                                                     x.grupo_id,
                                                     MIN(DECODE(orden, 1, periodo_id, NULL)) p1,
                                                     MIN(DECODE(orden, 2, periodo_id, NULL)) p2,
                                                     MIN(DECODE(orden, 3, periodo_id, NULL)) p3,
                                                     MIN(DECODE(orden, 4, periodo_id, NULL)) p4,
                                                     MIN(DECODE(orden, 5, periodo_id, NULL)) p5,
                                                     MIN(DECODE(orden, 6, periodo_id, NULL)) p6
                                              FROM (SELECT o.curso_academico_id,
                                                           o.asignatura_id,
                                                           o.bloque_id,
                                                           o.especialidad_id,
                                                           o.fase_id,
                                                           o.hospital_id,
                                                           o.periodo_inicial_id,
                                                           o.periodo_final_id,
                                                           o.unico_id,
                                                           o.bloque_unido,
                                                           o.orden_1,
                                                           o.orden_2,
                                                           o.orden_3,
                                                           p.fecha_inicio,
                                                           p2.fecha_fin,
                                                           persona_id,
                                                           fecha,
                                                           o.grupo_id grupo_id,
                                                           COUNT(*) plazas,
                                                           op.periodo_id,
                                                           ROW_NUMBER()
                                                               OVER(PARTITION BY o.curso_academico_id,
                                                                                 o.asignatura_id,
                                                                                 o.bloque_id,
                                                                                 o.especialidad_id,
                                                                                 o.fase_id,
                                                                                 o.hospital_id,
                                                                                 o.periodo_inicial_id,
                                                                                 o.periodo_final_id,
                                                                                 o.unico_id,
                                                                                 o.bloque_unido,
                                                                                 o.orden_1,
                                                                                 o.orden_2,
                                                                                 o.orden_3
                                                                    ORDER BY op.periodo_id) orden
                                                    FROM pho_oferta o,
                                                         pho_periodos_fechas p,
                                                         pho_periodos_fechas p2,
                                                         pho_ofertas_periodos op
                                                    WHERE     o.bloque_unido = 0
                                                          AND o.persona_id IS NULL
                                                          AND o.periodo_inicial_id = p.periodo_id
                                                          AND o.curso_academico_id = p.curso_Aca_id
                                                          AND o.fase_id = p.fase_id
                                                          AND o.periodo_final_id = p2.periodo_id
                                                          AND o.curso_academico_id = p2.curso_Aca_id
                                                          AND o.fase_id = p2.fase_id
                                                          AND o.id = op.oferta_id
                                                    GROUP BY o.curso_academico_id,
                                                             o.asignatura_id,
                                                             o.bloque_id,
                                                             o.especialidad_id,
                                                             o.fase_id,
                                                             o.hospital_id,
                                                             o.periodo_inicial_id,
                                                             o.periodo_final_id,
                                                             o.unico_id,
                                                             o.bloque_unido,
                                                             o.orden_1,
                                                             o.orden_2,
                                                             o.orden_3,
                                                             p.fecha_inicio,
                                                             p2.fecha_fin,
                                                             o.persona_id,
                                                             o.fecha,
                                                             o.grupo_id,
                                                             op.periodo_id) x
                                              GROUP BY x.curso_academico_id,
                                                       x.asignatura_id,
                                                       x.bloque_id,
                                                       x.especialidad_id,
                                                       x.fase_id,
                                                       x.hospital_id,
                                                       x.periodo_inicial_id,
                                                       x.periodo_final_id,
                                                       x.unico_id,
                                                       x.bloque_unido,
                                                       x.orden_1,
                                                       x.orden_2,
                                                       x.orden_3,
                                                       x.fecha_inicio,
                                                       x.fecha_fin,
                                                       x.persona_id,
                                                       x.fecha,
                                                       x.grupo_id,
                                                       x.plazas) xx,
                                             (SELECT id, id - 1 periodo_ant, nombre FROM pho_periodos) p
                                        WHERE (   p1 = p.id
                                               OR (    p2 = p.id
                                                   AND p1 != p.periodo_ant)
                                               OR (    p3 = p.id
                                                   AND p2 != p.periodo_ant)
                                               OR (    p4 = p.id
                                                   AND p3 != p.periodo_ant)
                                               OR (    p5 = p.id
                                                   AND p4 != p.periodo_ant)
                                               OR (    p6 = p.id
                                                   AND p5 != p.periodo_ant))
                                        UNION ALL
                                        SELECT xx.curso_academico_id,
                                               xx.asignatura_id,
                                               xx.bloque_id,
                                               xx.especialidad_id,
                                               xx.fase_id,
                                               xx.hospital_id,
                                               p.id periodo_inicial_id,
                                               pack_pho_practicas.pho_buscar_ultimo_cons(p.id,
                                                                                         p1,
                                                                                         p2,
                                                                                         p3,
                                                                                         p4,
                                                                                         p5,
                                                                                         p6) periodo_final_id,
                                               xx.unico_id,
                                               xx.bloque_unido,
                                               xx.orden_1,
                                               xx.orden_2,
                                               xx.orden_3,
                                               xx.fecha_inicio,
                                               xx.fecha_fin,
                                               xx.persona_id,
                                               xx.fecha,
                                               xx.grupo_id,
                                               xx.plazas
                                        FROM (SELECT x.curso_academico_id,
                                                     x.asignatura_id,
                                                     x.bloque_id,
                                                     x.especialidad_id,
                                                     x.fase_id,
                                                     x.hospital_id,
                                                     x.periodo_inicial_id,
                                                     x.periodo_final_id,
                                                     x.unico_id,
                                                     x.bloque_unido,
                                                     x.orden_1,
                                                     x.orden_2,
                                                     x.orden_3,
                                                     x.fecha_inicio,
                                                     x.fecha_fin,
                                                     x.persona_id,
                                                     x.fecha,
                                                     x.plazas,
                                                     x.grupo_id,
                                                     MIN(DECODE(orden, 1, periodo_id, NULL)) p1,
                                                     MIN(DECODE(orden, 2, periodo_id, NULL)) p2,
                                                     MIN(DECODE(orden, 3, periodo_id, NULL)) p3,
                                                     MIN(DECODE(orden, 4, periodo_id, NULL)) p4,
                                                     MIN(DECODE(orden, 5, periodo_id, NULL)) p5,
                                                     MIN(DECODE(orden, 6, periodo_id, NULL)) p6
                                              FROM (SELECT o.curso_academico_id,
                                                           o.asignatura_id,
                                                           o.bloque_id,
                                                           o.especialidad_id,
                                                           o.fase_id,
                                                           o.hospital_id,
                                                           o.periodo_inicial_id,
                                                           o.periodo_final_id,
                                                           o.unico_id,
                                                           o.bloque_unido,
                                                           o.orden_1,
                                                           o.orden_2,
                                                           o.orden_3,
                                                           p.fecha_inicio,
                                                           p2.fecha_fin,
                                                           persona_id,
                                                           fecha,
                                                           o.grupo_id grupo_id,
                                                           COUNT(*) plazas,
                                                           op.periodo_id,
                                                           ROW_NUMBER()
                                                               OVER(PARTITION BY o.curso_academico_id,
                                                                                 o.asignatura_id,
                                                                                 o.bloque_id,
                                                                                 o.especialidad_id,
                                                                                 o.fase_id,
                                                                                 o.hospital_id,
                                                                                 o.periodo_inicial_id,
                                                                                 o.periodo_final_id,
                                                                                 o.unico_id,
                                                                                 o.bloque_unido,
                                                                                 o.orden_1,
                                                                                 o.orden_2,
                                                                                 o.orden_3,
                                                                                 persona_id
                                                                    ORDER BY op.periodo_id) orden
                                                    FROM pho_oferta o,
                                                         pho_periodos_fechas p,
                                                         pho_periodos_fechas p2,
                                                         pho_ofertas_periodos op
                                                    WHERE     o.bloque_unido = 0
                                                          AND o.persona_id IS NOT NULL
                                                          AND o.periodo_inicial_id = p.periodo_id
                                                          AND o.curso_academico_id = p.curso_Aca_id
                                                          AND o.fase_id = p.fase_id
                                                          AND o.periodo_final_id = p2.periodo_id
                                                          AND o.curso_academico_id = p2.curso_Aca_id
                                                          AND o.fase_id = p2.fase_id
                                                          AND o.id = op.oferta_id
                                                    GROUP BY o.curso_academico_id,
                                                             o.asignatura_id,
                                                             o.bloque_id,
                                                             o.especialidad_id,
                                                             o.fase_id,
                                                             o.hospital_id,
                                                             o.periodo_inicial_id,
                                                             o.periodo_final_id,
                                                             o.unico_id,
                                                             o.bloque_unido,
                                                             o.orden_1,
                                                             o.orden_2,
                                                             o.orden_3,
                                                             p.fecha_inicio,
                                                             p2.fecha_fin,
                                                             o.persona_id,
                                                             o.fecha,
                                                             o.grupo_id,
                                                             op.periodo_id) x
                                              GROUP BY x.curso_academico_id,
                                                       x.asignatura_id,
                                                       x.bloque_id,
                                                       x.especialidad_id,
                                                       x.fase_id,
                                                       x.hospital_id,
                                                       x.periodo_inicial_id,
                                                       x.periodo_final_id,
                                                       x.unico_id,
                                                       x.bloque_unido,
                                                       x.orden_1,
                                                       x.orden_2,
                                                       x.orden_3,
                                                       x.fecha_inicio,
                                                       x.fecha_fin,
                                                       x.persona_id,
                                                       x.fecha,
                                                       x.grupo_id,
                                                       x.plazas) xx,
                                             (SELECT id, id - 1 periodo_ant, nombre FROM pho_periodos) p
                                        WHERE (   p1 = p.id
                                               OR (    p2 = p.id
                                                   AND p1 != p.periodo_ant)
                                               OR (    p3 = p.id
                                                   AND p2 != p.periodo_ant)
                                               OR (    p4 = p.id
                                                   AND p3 != p.periodo_ant)
                                               OR (    p5 = p.id
                                                   AND p4 != p.periodo_ant)
                                               OR (    p6 = p.id
                                                   AND p5 != p.periodo_ant))) x,
                                       pho_asignaturas a,
                                       pho_bloques b,
                                       pho_especialidades e,
                                       pho_fases f,
                                       pho_hospitales h,
                                       pho_fases_asignaturas fa
                                  WHERE     x.asignatura_id = a.id
                                        AND x.bloque_id = b.id
                                        AND x.especialidad_id = e.id
                                        AND x.fase_id = f.id
                                        AND x.fase_id = fa.fase_id
                                        AND x.asignatura_id = fa.asignatura_id
                                        AND x.hospital_id = h.id) xx,
                                 pho_ext_asignaturas_cursadas ac
                            WHERE     (   xx.persona_id = ac.persona_id
                                       OR xx.persona_id IS NULL)
                                  AND xx.codigo_asignatura = ac.asignatura_id)) xxx
                WHERE orden = 1) xxxx
          WHERE (   fase_id = 1
                 OR (    fase_id = 2
                     AND NOT EXISTS
                             (SELECT 1
                              FROM pho_oferta o
                              WHERE     o.persona_id = xxxx.persona_id_mostrar
                                    AND o.especialidad_id = xxxx.especialidad_id
                                    AND fase_id = 1))
                 OR (fase_id = 3
      /*and not exists
                                        (select 1
                                         from pho_oferta o
                                         where     o.persona_id = xxxx.persona_id_mostrar
                                               and o.especialidad_id = xxxx.especialidad_id
                                               and fase_id in (1, 2))*/
                                )
                 OR fase_id > 3)) x,
         pho_limites l,
         pho_cursos c
    WHERE     x.ESPECIALIDAD_ID = l.ESPECIALIDAD_ID
          AND x.HOSPITAL_ID = l.HOSPITAL_ID
          AND c.ID = l.curso_academico_id
          AND c.activo = 1;

CREATE OR REPLACE PACKAGE BODY UJI_PRACTICASHOSPITALARIAS.pack_generar
IS
   CURSOR matriculados (
      p_fase   IN NUMBER)
   IS
        SELECT persona_id, nota, orden
          FROM (  SELECT persona_id,
                         COUNT (DISTINCT ac.asignatura_id)          num_asignaturas,
                         nota,
                         COUNT (DISTINCT ac.asignatura_id) * 100 + nota orden
                    FROM pho_ext_asignaturas_cursadas ac
                         JOIN pho_asignaturas a
                            ON a.codigo_asignatura = ac.asignatura_id
                         JOIN pho_fases_asignaturas fa ON fa.asignatura_id = a.id
                   WHERE     ac.curso_academico_id = CURSO_ACADEMICO
                         AND fase_id = p_fase
                GROUP BY persona_id, nota)
      ORDER BY orden DESC;


   PROCEDURE actualizar_grupos (p_fase IN NUMBER)
   IS
      v_primero   NUMBER;

      CURSOR lista (
         p_fase   IN NUMBER)
      IS
         SELECT o.id oferta_id, g.id grupo_id
           FROM pho_grupos g, pho_grupos_Detalle d, pho_oferta o
          WHERE     g.id = d.grupo_id
                AND g.fase_id = p_fase
                AND g.fase_id = o.fase_id
                AND d.especialidad_id = o.especialidad_id
                AND o.bloque_unido = 1
                AND d.periodo_id BETWEEN o.periodo_inicial_id
                                     AND o.periodo_final_id;
   BEGIN
      FOR x IN lista (p_fase)
      LOOP
         SELECT grupo_id
           INTO v_primero
           FROM pho_oferta
          WHERE id = x.oferta_id;

         IF v_primero IS NULL
         THEN
            UPDATE pho_oferta
               SET grupo_id = x.grupo_id
             WHERE id = x.oferta_id;
         ELSE
            UPDATE pho_oferta
               SET grupo2_id = x.grupo_id
             WHERE id = x.oferta_id;
         END IF;

         COMMIT;
      END LOOP;
   END;

   PROCEDURE generar_turnos (fase_id             NUMBER,
                             fecha               DATE,
                             hora_inicial        VARCHAR2,
                             incremento_turno    NUMBER)
   IS
      fecha_inicio_matricula   DATE;
      incremento               NUMBER;
      fase_actual_id           NUMBER;
   BEGIN
      SELECT id
        INTO fase_actual_id
        FROM pho_fases f
       WHERE f.id = fase_id AND f.curso_academico_id = CURSO_ACADEMICO;

      DELETE FROM pho_turnos t
            WHERE t.fase_id = fase_actual_id;

      fecha_inicio_matricula := NVL (fecha, SYSDATE);
      incremento := 0;

      FOR matricula IN matriculados (fase_id)
      LOOP
         INSERT INTO pho_turnos (ID,
                                 PERSONA_ID,
                                 FECHA,
                                 FASE_ID)
                 VALUES (
                           (SELECT NVL (MAX (id) + 1, 1)
                              FROM pho_turnos),
                           matricula.persona_id,
                             TO_DATE (
                                   TO_CHAR (fecha_inicio_matricula,
                                            'DD/MM/YYYY')
                                || ' '
                                || hora_inicial,
                                'DD/MM/YYYY HH24:MI')
                           + (incremento / (24 * 60)),
                           fase_actual_id);

         incremento := incremento + incremento_turno;
      END LOOP;

      UPDATE pho_fases
         SET fecha_Desde = TRUNC (fecha_inicio_matricula),
             fecha_hasta = TRUNC (fecha_inicio_matricula) + 1
       WHERE id = fase_actual_id AND curso_academico_id = CURSO_ACADEMICO;

      UPDATE pho_fases
         SET fecha_Desde = TRUNC (fecha_inicio_matricula) + 1,
             fecha_hasta = TRUNC (fecha_inicio_matricula) + 300
       WHERE id = 3 AND curso_academico_id = CURSO_ACADEMICO;

      COMMIT;
   END;

   PROCEDURE refrescar_flags (p_fase IN NUMBER)
   IS
   BEGIN
      FOR matricula IN matriculados (p_fase)
      LOOP
         IF p_Fase = 1
         THEN
            pack_pho_practicas.refrescar_flag (matricula.persona_id, 1);
            pack_pho_practicas.refrescar_flag (matricula.persona_id, 2);
         ELSIF p_fase >= 4
         THEN
            pack_pho_practicas.refrescar_flag (matricula.persona_id, p_fase);
         END IF;
      END LOOP;
   END;

   PROCEDURE eliminar_oferta_no_disponible (p_fase IN NUMBER)
   IS
   BEGIN
      DELETE FROM pho_oferta
            WHERE     unico_id IN (SELECT unico_id
                                     FROM pho_oferta_eliminada
                                    WHERE fase_id = p_fase)
                  AND persona_id IS NULL;
   END;

   FUNCTION generar_oferta_fase (faseId IN NUMBER)
      RETURN NUMBER
   IS
      v_id    NUMBER;
      v_id2   NUMBER;

      CURSOR lista
      IS
         SELECT b.*,
                c.id                                                 curso_academico_id,
                DECODE (bloque_unido, 'S', 1, 0)                     unido,
                p1                                                   p_primero,
                NVL (p6, NVL (p5, NVL (p4, NVL (p3, NVL (p2, p1))))) p_ultimo
           FROM pho_vw_oferta_base b, pho_cursos c
          WHERE c.activo = 1 AND fase_id = faseId;
   BEGIN
      -- OJO PROCEDIMIENTO DESHABILITADO
      -- return -1;

      -----------------------------------

      IF faseId = 1
      THEN
         generar_oferta_fase_1 (faseId, 'S');
      ELSIF faseId = 2
      THEN
         generar_oferta_fase_2;
      ELSIF faseId = 3
      THEN
         generar_oferta_fase_3 (40, 'N');
      ELSIF faseId IN (4, 7)
      THEN
         --generar_oferta_fase_1(faseId, 'N');
         generar_oferta_fase_1 (faseId, 'S');
      ELSIF faseId = 5 THEN
        delete from pho_ofertas_periodos where oferta_id in (select id from pho_oferta where fase_id = 4 and persona_id is null);
        delete from pho_oferta where fase_id = 4 and persona_id is null;
        generar_oferta_fase_1 (faseId, 'N');
      ELSIF faseId = 8 THEN
        delete from pho_ofertas_periodos where oferta_id in (select id from pho_oferta where fase_id = 7 and persona_id is null);
        delete from pho_oferta where fase_id = 7 and persona_id is null;
        generar_oferta_fase_1 (faseId, 'N');
      ELSE -- Mantenemos la oferta de quinto cuando se realiza la matrícula de cuarto
         generar_oferta_fase_1 (faseId, 'N');
      END IF;

      actualizar_grupos (faseId);
      eliminar_oferta_no_disponible (faseId);

      RETURN 1;
   END;

   PROCEDURE generar_oferta_fase_1 (faseId         IN NUMBER,
                                    borrar_datos   IN VARCHAR2)
   IS
      v_id    NUMBER;
      v_id2   NUMBER;

      CURSOR lista
      IS
         SELECT b.*,
                c.id                                                 curso_academico_id,
                DECODE (bloque_unido, 'S', 1, 0)                     unido,
                p1                                                   p_primero,
                NVL (p6, NVL (p5, NVL (p4, NVL (p3, NVL (p2, p1))))) p_ultimo
           FROM pho_vw_oferta_base b,
                pho_cursos         c,
                pho_Fases          f,
                pho_especialidades e
          WHERE     c.activo = 1
                AND fase_id = faseId
                AND fase_id = f.id
                AND especialidad_id = e.id
                AND DECODE (f.semestre, 'A', e.semestre, f.semestre) =
                       e.semestre;
   BEGIN
      IF borrar_datos = 'S'
      THEN
         DELETE pho_ofertas_periodos;

         DELETE pho_oferta;
      END IF;

      refrescar_flags (faseId);

      FOR x IN lista
      LOOP
         FOR p IN 1 .. x.plazas
         LOOP
            v_id := hibernate_sequence.NEXTVAL;

            IF x.unido = 1
            THEN
               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 x.FASE_ID,
                                 x.HOSPITAL_ID,
                                 x.p_primero,
                                 x.p_ultimo,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 x.unido);
            ELSE
               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 x.FASE_ID,
                                 x.HOSPITAL_ID,
                                 x.p_primero,
                                 x.p_ultimo,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 x.unido);

               IF x.p1 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p1);
               END IF;

               IF x.p2 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p2);
               END IF;

               IF x.p3 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p3);
               END IF;

               IF x.p4 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p4);
               END IF;

               IF x.p5 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p5);
               END IF;

               IF x.p6 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p6);
               END IF;
            END IF;
         END LOOP;

         NULL;
      END LOOP;

      COMMIT;
   END;

   PROCEDURE generar_oferta_fase_2
   IS
      v_id       NUMBER;
      v_id2      NUMBER;
      v_error    NUMBER;
      v_libres   NUMBER;

      CURSOR lista
      IS
         SELECT xxx.*,
                plazas - ocupacion               libres,
                c.id                             curso_academico_id,
                DECODE (bloque_unido, 'S', 1, 0) unido
           FROM pho_Vw_oferta_base_2 xxx, pho_cursos c
          WHERE c.activo = 1 AND fase_id = 2        --and    asignatura_id = 1
                                            --and    bloque_id = 2
                                            --and    especialidad_id = 3
   ;
   BEGIN
      borrar_no_asignados (1);
      refrescar_flags (1);
      v_error := 1;

      FOR x IN lista
      LOOP
         v_error := 11;
         v_libres := x.libres;

         FOR p IN 1 .. v_libres
         LOOP
            --dbms_output.put_line ('P: ' || p);
            v_error := 2;
            v_id := hibernate_sequence.NEXTVAL;
            v_error := 3;

            IF x.unido = 1
            THEN
               v_error := 4;

               BEGIN
                  INSERT INTO pho_oferta (ID,
                                          CURSO_ACADEMICO_ID,
                                          ASIGNATURA_ID,
                                          BLOQUE_ID,
                                          ESPECIALIDAD_ID,
                                          FASE_ID,
                                          HOSPITAL_ID,
                                          PERIODO_INICIAL_ID,
                                          PERIODO_FINAL_ID,
                                          PERSONA_ID,
                                          FECHA,
                                          UNICO_ID,
                                          ORDEN_1,
                                          ORDEN_2,
                                          ORDEN_3,
                                          bloque_unido)
                          VALUES (
                                    v_id,
                                    x.curso_Academico_id,
                                    x.asignatura_id,
                                    x.bloque_id,
                                    x.ESPECIALIDAD_ID,
                                    x.FASE_ID,
                                    x.HOSPITAL_ID,
                                    x.primero,
                                    x.ultimo,
                                    NULL,
                                    NULL,
                                       x.asignatura_id
                                    || '_'
                                    || x.bloque_id
                                    || '_'
                                    || x.especialidad_id
                                    || '_'
                                    || x.hospital_id
                                    || '_'
                                    || x.grupo_id,
                                    NVL (x.ORDEN_1, 9999),
                                    NVL (x.ORDEN_2, 9999),
                                    NVL (x.ORDEN_3, 9999),
                                    x.unido);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     DBMS_OUTPUT.put_line (
                           v_error
                        || ' - '
                        || SQLERRM
                        || dbms_utility.format_error_backtrace
                        || x.asignatura_id
                        || ' '
                        || x.bloque_id
                        || ' '
                        || x.especialidad_id
                        || ' '
                        || x.hospital_id
                        || ' - '
                        || x.fase_id);
               END;
            ELSE
               v_error := 5;

               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 x.FASE_ID,
                                 x.HOSPITAL_ID,
                                 x.primero,
                                 x.ultimo,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 x.unido);

               v_error := 6;

               IF x.p1 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p1);
               END IF;

               IF x.p2 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p2);
               END IF;

               IF x.p3 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p3);
               END IF;

               IF x.p4 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p4);
               END IF;

               IF x.p5 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p5);
               END IF;

               IF x.p6 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p6);
               END IF;
            END IF;
         END LOOP;

         NULL;
         COMMIT;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (v_error || ' - ' || SQLERRM|| dbms_utility.format_error_backtrace);
   END;

   PROCEDURE generar_oferta_fase_3_1 (p_plazas IN NUMBER)
   IS
      v_id    NUMBER;
      v_id2   NUMBER;

      CURSOR lista
      IS
         SELECT b.*,
                c.id                             curso_academico_id,
                DECODE (bloque_unido, 'S', 1, 0) unido,
                p1                               p_primero,
                DECODE (NVL (p6, NVL (p5, NVL (p4, NVL (p3, NVL (p2, p1))))),
                        28, 32,
                        NVL (p6, NVL (p5, NVL (p4, NVL (p3, NVL (p2, p1))))))
                   p_ultimo
           FROM pho_vw_oferta_base b, pho_cursos c
          WHERE     c.activo = 1
                AND fase_id = 1
                AND asignatura_id IN (2,
                                      3,
                                      4,
                                      5);
   BEGIN
      FOR x IN lista
      LOOP
         FOR p IN 1 .. p_plazas
         LOOP
            v_id := hibernate_sequence.NEXTVAL;

            IF x.unido = 1
            THEN
               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 3,
                                 x.HOSPITAL_ID,
                                 x.p_primero,
                                 x.p_ultimo,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 x.unido);
            ELSE
               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 3,
                                 x.HOSPITAL_ID,
                                 x.p_primero,
                                 x.p_ultimo,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 x.unido);

               IF x.p1 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p1);
               END IF;

               IF x.p2 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p2);
               END IF;

               IF x.p3 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p3);
               END IF;

               IF x.p4 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p4);
               END IF;

               IF x.p5 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p5);
               END IF;

               IF x.p6 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p6);
               END IF;
            END IF;

            COMMIT;
         END LOOP;

         NULL;
         COMMIT;
      END LOOP;

      COMMIT;
   END;

   PROCEDURE generar_oferta_fase_3_1_1 (p_plazas IN NUMBER)
   IS
      v_id    NUMBER;
      v_id2   NUMBER;

      CURSOR lista
      IS
         SELECT b.*,
                c.id                             curso_academico_id,
                DECODE (bloque_unido, 'S', 1, 0) unido,
                p1                               p_primero,
                DECODE (NVL (p6, NVL (p5, NVL (p4, NVL (p3, NVL (p2, p1))))),
                        28, 32,
                        NVL (p6, NVL (p5, NVL (p4, NVL (p3, NVL (p2, p1))))))
                   p_ultimo
           FROM pho_vw_oferta_base b, pho_cursos c
          WHERE     c.activo = 1
                AND fase_id = 1
                AND asignatura_id IN (2,
                                      3,
                                      4,
                                      5);
   BEGIN
      FOR x IN lista
      LOOP
         FOR p IN 1 .. p_plazas
         LOOP
            IF x.p1 IS NOT NULL
            THEN
               v_id := hibernate_sequence.NEXTVAL;

               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 3,
                                 x.HOSPITAL_ID,
                                 x.p1,
                                 x.p1,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 1);
            END IF;

            IF x.p2 IS NOT NULL
            THEN
               v_id := hibernate_sequence.NEXTVAL;

               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 3,
                                 x.HOSPITAL_ID,
                                 x.p2,
                                 x.p2,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 1);
            END IF;

            IF x.p3 IS NOT NULL
            THEN
               v_id := hibernate_sequence.NEXTVAL;

               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 3,
                                 x.HOSPITAL_ID,
                                 x.p3,
                                 x.p3,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 1);
            END IF;

            IF x.p4 IS NOT NULL
            THEN
               v_id := hibernate_sequence.NEXTVAL;

               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 3,
                                 x.HOSPITAL_ID,
                                 x.p4,
                                 x.p4,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 1);
            END IF;

            IF x.p5 IS NOT NULL
            THEN
               v_id := hibernate_sequence.NEXTVAL;

               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 3,
                                 x.HOSPITAL_ID,
                                 x.p5,
                                 x.p5,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 1);
            END IF;

            IF x.p6 IS NOT NULL
            THEN
               v_id := hibernate_sequence.NEXTVAL;

               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 3,
                                 x.HOSPITAL_ID,
                                 x.p6,
                                 x.p6,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 1);
            END IF;

            COMMIT;
         END LOOP;

         NULL;
         COMMIT;
      END LOOP;

      COMMIT;
   END;

   PROCEDURE generar_oferta_fase_3_2 (p_plazas IN NUMBER)
   IS
      v_id       NUMBER;
      v_id2      NUMBER;
      v_error    NUMBER;
      v_libres   NUMBER;

      CURSOR lista
      IS
         SELECT xxx.ASIGNATURA_ID,
                ASIGNATURA,
                CODIGO_ASIGNATURA,
                BLOQUE_ID,
                BLOQUE,
                ESPECIALIDAD_ID,
                ESPECIALIDAD,
                FASE_ID,
                FASE,
                GRUPO_ID,
                TAMAÑO,
                PRIMERO,
                DECODE (ultimo, 28, 32, ultimo)  ULTIMO,
                P1,
                P2,
                P3,
                P4,
                P5,
                P6,
                CONSECUTIVOS,
                PLAZAS,
                OCUPACION,
                HOSPITAL_ID,
                HOSPITAL,
                CODIGO,
                BLOQUE_UNIDO,
                ORDEN_1,
                ORDEN_2,
                ORDEN_3,
                UNICO_ID,
                plazas - ocupacion               libres,
                c.id                             curso_academico_id,
                DECODE (bloque_unido, 'S', 1, 0) unido
           FROM pho_Vw_oferta_base_2 xxx, pho_cursos c
          WHERE c.activo = 1 AND fase_id = 2 AND asignatura_id = 1 --and    bloque_id = 2
                                                                  --and    especialidad_id = 3
   ;
   BEGIN
      FOR x IN lista
      LOOP
         v_error := 11;
         v_libres := x.libres;

         FOR p IN 1 .. p_plazas
         LOOP
            --dbms_output.put_line ('P: ' || p);
            v_error := 2;
            v_id := hibernate_sequence.NEXTVAL;
            v_error := 3;

            IF x.unido = 1
            THEN
               v_error := 4;

               BEGIN
                  INSERT INTO pho_oferta (ID,
                                          CURSO_ACADEMICO_ID,
                                          ASIGNATURA_ID,
                                          BLOQUE_ID,
                                          ESPECIALIDAD_ID,
                                          FASE_ID,
                                          HOSPITAL_ID,
                                          PERIODO_INICIAL_ID,
                                          PERIODO_FINAL_ID,
                                          PERSONA_ID,
                                          FECHA,
                                          UNICO_ID,
                                          ORDEN_1,
                                          ORDEN_2,
                                          ORDEN_3,
                                          bloque_unido)
                          VALUES (
                                    v_id,
                                    x.curso_Academico_id,
                                    x.asignatura_id,
                                    x.bloque_id,
                                    x.ESPECIALIDAD_ID,
                                    3,
                                    x.HOSPITAL_ID,
                                    x.primero,
                                    x.ultimo,
                                    NULL,
                                    NULL,
                                       x.asignatura_id
                                    || '_'
                                    || x.bloque_id
                                    || '_'
                                    || x.especialidad_id
                                    || '_'
                                    || x.hospital_id
                                    || '_'
                                    || x.grupo_id,
                                    NVL (x.ORDEN_1, 9999),
                                    NVL (x.ORDEN_2, 9999),
                                    NVL (x.ORDEN_3, 9999),
                                    x.unido);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     DBMS_OUTPUT.put_line (
                           v_error
                        || ' - '
                        || SQLERRM
                        || dbms_utility.format_error_backtrace
                        || x.asignatura_id
                        || ' '
                        || x.bloque_id
                        || ' '
                        || x.especialidad_id
                        || ' '
                        || x.hospital_id
                        || ' - '
                        || x.fase_id);
               END;
            ELSE
               v_error := 5;

               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 3,
                                 x.HOSPITAL_ID,
                                 x.primero,
                                 x.ultimo,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 x.unido);

               v_error := 6;

               IF x.p1 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p1);
               END IF;

               IF x.p2 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p2);
               END IF;

               IF x.p3 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p3);
               END IF;

               IF x.p4 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p4);
               END IF;

               IF x.p5 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p5);
               END IF;

               IF x.p6 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p6);
               END IF;
            END IF;
         END LOOP;

         NULL;
         COMMIT;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (v_error || ' - ' || SQLERRM|| dbms_utility.format_error_backtrace);
   END;

   PROCEDURE generar_oferta_fase_3_2_1 (p_plazas IN NUMBER)
   IS
      v_id       NUMBER;
      v_id2      NUMBER;
      v_error    NUMBER;
      v_libres   NUMBER;

      CURSOR lista
      IS
         SELECT xxx.ASIGNATURA_ID,
                ASIGNATURA,
                CODIGO_ASIGNATURA,
                BLOQUE_ID,
                BLOQUE,
                ESPECIALIDAD_ID,
                ESPECIALIDAD,
                FASE_ID,
                FASE,
                GRUPO_ID,
                TAMAÑO,
                PRIMERO,
                DECODE (ultimo, 28, 32, ultimo)  ULTIMO,
                P1,
                P2,
                P3,
                P4,
                P5,
                P6,
                CONSECUTIVOS,
                PLAZAS,
                OCUPACION,
                HOSPITAL_ID,
                HOSPITAL,
                CODIGO,
                BLOQUE_UNIDO,
                ORDEN_1,
                ORDEN_2,
                ORDEN_3,
                UNICO_ID,
                plazas - ocupacion               libres,
                c.id                             curso_academico_id,
                DECODE (bloque_unido, 'S', 1, 0) unido
           FROM pho_Vw_oferta_base_2 xxx, pho_cursos c
          WHERE c.activo = 1 AND fase_id = 2 AND asignatura_id = 1 --and    bloque_id = 2
                                                                  --and    especialidad_id = 3
   ;
   BEGIN
      FOR x IN lista
      LOOP
         v_error := 11;
         v_libres := x.libres;

         FOR p IN 1 .. p_plazas
         LOOP
            --dbms_output.put_line ('P: ' || p);
            v_error := 2;
            v_error := 3;

            IF x.p1 IS NOT NULL
            THEN
               v_id := hibernate_sequence.NEXTVAL;

               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 3,
                                 x.HOSPITAL_ID,
                                 x.p1,
                                 x.p1,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 1);

               v_error := 6;

               IF x.p2 IS NOT NULL
               THEN
                  v_id := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_oferta (ID,
                                          CURSO_ACADEMICO_ID,
                                          ASIGNATURA_ID,
                                          BLOQUE_ID,
                                          ESPECIALIDAD_ID,
                                          FASE_ID,
                                          HOSPITAL_ID,
                                          PERIODO_INICIAL_ID,
                                          PERIODO_FINAL_ID,
                                          PERSONA_ID,
                                          FECHA,
                                          UNICO_ID,
                                          ORDEN_1,
                                          ORDEN_2,
                                          ORDEN_3,
                                          bloque_unido)
                          VALUES (
                                    v_id,
                                    x.curso_Academico_id,
                                    x.asignatura_id,
                                    x.bloque_id,
                                    x.ESPECIALIDAD_ID,
                                    3,
                                    x.HOSPITAL_ID,
                                    x.p2,
                                    x.p2,
                                    NULL,
                                    NULL,
                                       x.asignatura_id
                                    || '_'
                                    || x.bloque_id
                                    || '_'
                                    || x.especialidad_id
                                    || '_'
                                    || x.hospital_id
                                    || '_'
                                    || x.grupo_id,
                                    NVL (x.ORDEN_1, 9999),
                                    NVL (x.ORDEN_2, 9999),
                                    NVL (x.ORDEN_3, 9999),
                                    1);
               END IF;

               IF x.p3 IS NOT NULL
               THEN
                  v_id := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_oferta (ID,
                                          CURSO_ACADEMICO_ID,
                                          ASIGNATURA_ID,
                                          BLOQUE_ID,
                                          ESPECIALIDAD_ID,
                                          FASE_ID,
                                          HOSPITAL_ID,
                                          PERIODO_INICIAL_ID,
                                          PERIODO_FINAL_ID,
                                          PERSONA_ID,
                                          FECHA,
                                          UNICO_ID,
                                          ORDEN_1,
                                          ORDEN_2,
                                          ORDEN_3,
                                          bloque_unido)
                          VALUES (
                                    v_id,
                                    x.curso_Academico_id,
                                    x.asignatura_id,
                                    x.bloque_id,
                                    x.ESPECIALIDAD_ID,
                                    3,
                                    x.HOSPITAL_ID,
                                    x.p3,
                                    x.p3,
                                    NULL,
                                    NULL,
                                       x.asignatura_id
                                    || '_'
                                    || x.bloque_id
                                    || '_'
                                    || x.especialidad_id
                                    || '_'
                                    || x.hospital_id
                                    || '_'
                                    || x.grupo_id,
                                    NVL (x.ORDEN_1, 9999),
                                    NVL (x.ORDEN_2, 9999),
                                    NVL (x.ORDEN_3, 9999),
                                    1);
               END IF;

               IF x.p4 IS NOT NULL
               THEN
                  v_id := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_oferta (ID,
                                          CURSO_ACADEMICO_ID,
                                          ASIGNATURA_ID,
                                          BLOQUE_ID,
                                          ESPECIALIDAD_ID,
                                          FASE_ID,
                                          HOSPITAL_ID,
                                          PERIODO_INICIAL_ID,
                                          PERIODO_FINAL_ID,
                                          PERSONA_ID,
                                          FECHA,
                                          UNICO_ID,
                                          ORDEN_1,
                                          ORDEN_2,
                                          ORDEN_3,
                                          bloque_unido)
                          VALUES (
                                    v_id,
                                    x.curso_Academico_id,
                                    x.asignatura_id,
                                    x.bloque_id,
                                    x.ESPECIALIDAD_ID,
                                    3,
                                    x.HOSPITAL_ID,
                                    x.p4,
                                    x.p4,
                                    NULL,
                                    NULL,
                                       x.asignatura_id
                                    || '_'
                                    || x.bloque_id
                                    || '_'
                                    || x.especialidad_id
                                    || '_'
                                    || x.hospital_id
                                    || '_'
                                    || x.grupo_id,
                                    NVL (x.ORDEN_1, 9999),
                                    NVL (x.ORDEN_2, 9999),
                                    NVL (x.ORDEN_3, 9999),
                                    1);
               END IF;

               IF x.p5 IS NOT NULL
               THEN
                  v_id := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_oferta (ID,
                                          CURSO_ACADEMICO_ID,
                                          ASIGNATURA_ID,
                                          BLOQUE_ID,
                                          ESPECIALIDAD_ID,
                                          FASE_ID,
                                          HOSPITAL_ID,
                                          PERIODO_INICIAL_ID,
                                          PERIODO_FINAL_ID,
                                          PERSONA_ID,
                                          FECHA,
                                          UNICO_ID,
                                          ORDEN_1,
                                          ORDEN_2,
                                          ORDEN_3,
                                          bloque_unido)
                          VALUES (
                                    v_id,
                                    x.curso_Academico_id,
                                    x.asignatura_id,
                                    x.bloque_id,
                                    x.ESPECIALIDAD_ID,
                                    3,
                                    x.HOSPITAL_ID,
                                    x.p5,
                                    x.p5,
                                    NULL,
                                    NULL,
                                       x.asignatura_id
                                    || '_'
                                    || x.bloque_id
                                    || '_'
                                    || x.especialidad_id
                                    || '_'
                                    || x.hospital_id
                                    || '_'
                                    || x.grupo_id,
                                    NVL (x.ORDEN_1, 9999),
                                    NVL (x.ORDEN_2, 9999),
                                    NVL (x.ORDEN_3, 9999),
                                    1);
               END IF;

               IF x.p6 IS NOT NULL
               THEN
                  v_id := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_oferta (ID,
                                          CURSO_ACADEMICO_ID,
                                          ASIGNATURA_ID,
                                          BLOQUE_ID,
                                          ESPECIALIDAD_ID,
                                          FASE_ID,
                                          HOSPITAL_ID,
                                          PERIODO_INICIAL_ID,
                                          PERIODO_FINAL_ID,
                                          PERSONA_ID,
                                          FECHA,
                                          UNICO_ID,
                                          ORDEN_1,
                                          ORDEN_2,
                                          ORDEN_3,
                                          bloque_unido)
                          VALUES (
                                    v_id,
                                    x.curso_Academico_id,
                                    x.asignatura_id,
                                    x.bloque_id,
                                    x.ESPECIALIDAD_ID,
                                    3,
                                    x.HOSPITAL_ID,
                                    x.p6,
                                    x.p6,
                                    NULL,
                                    NULL,
                                       x.asignatura_id
                                    || '_'
                                    || x.bloque_id
                                    || '_'
                                    || x.especialidad_id
                                    || '_'
                                    || x.hospital_id
                                    || '_'
                                    || x.grupo_id,
                                    NVL (x.ORDEN_1, 9999),
                                    NVL (x.ORDEN_2, 9999),
                                    NVL (x.ORDEN_3, 9999),
                                    1);
               END IF;
            END IF;

            COMMIT;
         END LOOP;

         NULL;
         COMMIT;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (v_error || ' - ' || SQLERRM|| dbms_utility.format_error_backtrace);
   END;

   PROCEDURE generar_oferta_fase_3 (p_plazas IN NUMBER, p_modo IN VARCHAR2)
   IS
      v_error   NUMBER;
   BEGIN
      borrar_no_asignados (3);
      borrar_no_asignados (2);
      refrescar_flags (1);
      v_error := 1;

      IF NVL (p_modo, 'X') <> '1P'
      THEN
         generar_oferta_fase_3_1 (p_plazas);
         generar_oferta_fase_3_2 (p_plazas);
      ELSE
         generar_oferta_fase_3_1_1 (p_plazas);
         generar_oferta_fase_3_2_1 (p_plazas);
      END IF;
   END;


   PROCEDURE generar_oferta_fase_3_old
   IS
      v_id       NUMBER;
      v_id2      NUMBER;
      v_error    NUMBER;
      v_libres   NUMBER;

      CURSOR lista
      IS
           SELECT DISTINCT xxx.id,
                           unico_id,
                           asignatura_id,
                           asignatura,
                           codigo_asignatura,
                           bloque_id,
                           bloque,
                           especialidad_id,
                           especialidad,
                           3          fase_id,
                           'Fase 3 - Sisé Pràctiques',
                           'EX'       fase_tipo,
                           primero,
                           ultimo,
                           consecutivos,
                           plazas,
                           hospital_id,
                           hospital,
                           codigo,
                           fecha,
                           fecha_inicio,
                           fecha_fin,
                           es_seleccionable,
                           fase_Actual_id,
                           orden_1,
                           orden_2,
                           orden_3,
                           grupo_id,
                           p1,
                           p2,
                           p3,
                           p4,
                           p5,
                           p6,
                           40         libres,
                           c.id       curso_academico_id,
                           consecutivos unido
             FROM "PHO_VW_OFERTA_base_3" xxx, pho_cursos c
            WHERE                                               --c.activo = 1
                      --AND fase_id = 2        --and
                      asignatura_id IN (1,
                                        2,
                                        3,
                                        4,
                                        5)
                  AND asignatura_id = 1
         ORDER BY asignatura_id, bloque_id              --and    bloque_id = 2
                                          --and    especialidad_id = 3
   ;
   BEGIN
      borrar_no_asignados (3);
      borrar_no_asignados (2);
      refrescar_flags (1);
      v_error := 1;

      FOR x IN lista
      LOOP
         v_error := 11;
         v_libres := x.libres;

         FOR p IN 1 .. 4
         LOOP
            --dbms_output.put_line ('P: ' || p);
            v_error := 2;
            v_id := hibernate_sequence.NEXTVAL;
            v_error := 3;

            IF x.unido = 1
            THEN
               v_error := 4;

               BEGIN
                  INSERT INTO pho_oferta (ID,
                                          CURSO_ACADEMICO_ID,
                                          ASIGNATURA_ID,
                                          BLOQUE_ID,
                                          ESPECIALIDAD_ID,
                                          FASE_ID,
                                          HOSPITAL_ID,
                                          PERIODO_INICIAL_ID,
                                          PERIODO_FINAL_ID,
                                          PERSONA_ID,
                                          FECHA,
                                          UNICO_ID,
                                          ORDEN_1,
                                          ORDEN_2,
                                          ORDEN_3,
                                          bloque_unido)
                          VALUES (
                                    v_id,
                                    x.curso_Academico_id,
                                    x.asignatura_id,
                                    x.bloque_id,
                                    x.ESPECIALIDAD_ID,
                                    3,
                                    x.HOSPITAL_ID,
                                    x.primero,
                                    x.ultimo,
                                    NULL,
                                    NULL,
                                       x.asignatura_id
                                    || '_'
                                    || x.bloque_id
                                    || '_'
                                    || x.especialidad_id
                                    || '_'
                                    || x.hospital_id
                                    || '_'
                                    || x.grupo_id,
                                    NVL (x.ORDEN_1, 9999),
                                    NVL (x.ORDEN_2, 9999),
                                    NVL (x.ORDEN_3, 9999),
                                    x.unido);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     DBMS_OUTPUT.put_line (
                           v_error
                        || ' - '
                        || SQLERRM
                        || dbms_utility.format_error_backtrace
                        || x.asignatura_id
                        || ' '
                        || x.bloque_id
                        || ' '
                        || x.especialidad_id
                        || ' '
                        || x.hospital_id
                        || ' - '
                        || x.fase_id);
               END;
            ELSE
               v_error := 5;

               INSERT INTO pho_oferta (ID,
                                       CURSO_ACADEMICO_ID,
                                       ASIGNATURA_ID,
                                       BLOQUE_ID,
                                       ESPECIALIDAD_ID,
                                       FASE_ID,
                                       HOSPITAL_ID,
                                       PERIODO_INICIAL_ID,
                                       PERIODO_FINAL_ID,
                                       PERSONA_ID,
                                       FECHA,
                                       UNICO_ID,
                                       ORDEN_1,
                                       ORDEN_2,
                                       ORDEN_3,
                                       bloque_unido)
                       VALUES (
                                 v_id,
                                 x.curso_Academico_id,
                                 x.asignatura_id,
                                 x.bloque_id,
                                 x.ESPECIALIDAD_ID,
                                 3,
                                 x.HOSPITAL_ID,
                                 x.primero,
                                 x.ultimo,
                                 NULL,
                                 NULL,
                                    x.asignatura_id
                                 || '_'
                                 || x.bloque_id
                                 || '_'
                                 || x.especialidad_id
                                 || '_'
                                 || x.hospital_id
                                 || '_'
                                 || x.grupo_id,
                                 NVL (x.ORDEN_1, 9999),
                                 NVL (x.ORDEN_2, 9999),
                                 NVL (x.ORDEN_3, 9999),
                                 x.unido);

               v_error := 6;

               IF x.p1 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p1);
               END IF;

               IF x.p2 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p2);
               END IF;

               IF x.p3 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p3);
               END IF;

               IF x.p4 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p4);
               END IF;

               IF x.p5 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p5);
               END IF;

               IF x.p6 IS NOT NULL
               THEN
                  v_id2 := hibernate_sequence.NEXTVAL;

                  INSERT INTO pho_ofertas_periodos
                       VALUES (v_id2, v_id, x.p6);
               END IF;
            END IF;
         END LOOP;

         NULL;
         COMMIT;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (v_error || ' - ' || SQLERRM|| dbms_utility.format_error_backtrace);
   END;

   PROCEDURE borrar_no_asignados (faseId IN NUMBER)
   IS
   BEGIN
      DELETE pho_ofertas_periodos
       WHERE oferta_id IN (SELECT id
                             FROM pho_oferta
                            WHERE fase_id = faseId AND persona_id IS NULL);

      DELETE pho_oferta
       WHERE fase_id = faseId AND persona_id IS NULL;

      COMMIT;
   END;

   PROCEDURE generar_muestra_Fase_1
   IS
   BEGIN
      UPDATE pho_oferta o
         SET persona_id = 591555
       WHERE     unico_id = '1_1_1_1_1'
             AND persona_id IS NULL
             AND id =
                    (SELECT MIN (id)
                       FROM pho_oferta o2
                      WHERE persona_id IS NULL AND o.unico_id = o2.unico_id);

      UPDATE pho_oferta o
         SET persona_id = 591555
       WHERE     unico_id = '1_2_21_1_3'
             AND persona_id IS NULL
             AND id =
                    (SELECT MIN (id)
                       FROM pho_oferta o2
                      WHERE persona_id IS NULL AND o.unico_id = o2.unico_id);

      UPDATE pho_oferta o
         SET persona_id = 591555
       WHERE     unico_id = '4_5_35_5_5'
             AND persona_id IS NULL
             AND id =
                    (SELECT MIN (id)
                       FROM pho_oferta o2
                      WHERE persona_id IS NULL AND o.unico_id = o2.unico_id);

      UPDATE pho_oferta o
         SET persona_id = 591555
       WHERE     unico_id = '2_3_31_2_2'
             AND persona_id IS NULL
             AND id =
                    (SELECT MIN (id)
                       FROM pho_oferta o2
                      WHERE persona_id IS NULL AND o.unico_id = o2.unico_id);

      UPDATE pho_oferta o
         SET persona_id = 591555
       WHERE     unico_id = '3_4_32_1_3'
             AND persona_id IS NULL
             AND id =
                    (SELECT MIN (id)
                       FROM pho_oferta o2
                      WHERE persona_id IS NULL AND o.unico_id = o2.unico_id);

      UPDATE pho_oferta o
         SET persona_id = 591555
       WHERE     unico_id = '5_6_33_1_4'
             AND persona_id IS NULL
             AND id =
                    (SELECT MIN (id)
                       FROM pho_oferta o2
                      WHERE persona_id IS NULL AND o.unico_id = o2.unico_id);

      UPDATE pho_oferta o
         SET persona_id = 591555
       WHERE     unico_id = '5_7_34_8_17'
             AND persona_id IS NULL
             AND id =
                    (SELECT MIN (id)
                       FROM pho_oferta o2
                      WHERE persona_id IS NULL AND o.unico_id = o2.unico_id);

      COMMIT;
   END;

   PROCEDURE notificar_turnos(faseId in number)
   IS
      CURSOR lista
      IS
         SELECT DISTINCT
                persona_id,
                busca_cuenta (persona_id)         email,
                busca_nombre_persona (persona_id) nombre
           FROM pho_turnos
           WHERE FASE_ID = faseId;



      v_asunto   VARCHAR2 (2000);
      v_texto    VARCHAR2 (4000);
      v_para     VARCHAR2 (200);
      v_sep      VARCHAR2 (10) := '<br />';
   BEGIN
      v_asunto :=
         'Torn de sel·lecció de les pràctiques hospitalàries / Turno de selección de las prácticas hospitalarias';

      FOR x IN lista
      LOOP
         v_para := x.email;
         v_texto :=
               'Estimat/Estimada '
            || x.nombre
            || v_Sep
            || v_sep
            || 'En el següent enllaç pots consultar els teus torns per a la sel·lecció de les pràctiques hospitalàries.'
            || v_Sep
            || v_Sep
            || '<a href="http://ujiapps.uji.es/pho">http://ujiapps.uji.es/pho</a>'
            || v_sep
            || v_Sep
            || 'Atentament'
            || v_Sep
            || v_sep
            || 'Universitat Jaume I'
            || v_sep
            || v_sep
            || '---------------'
            || v_sep
            || v_sep
            || 'Estimado/Estimada '
            || x.nombre
            || v_Sep
            || v_sep
            || 'En el siguiente enlace podrás consultar tus turnos para la matrícula de las prácticas hospitalarias.'
            || v_Sep
            || v_Sep
            || '<a href="http://ujiapps.uji.es/pho">http://ujiapps.uji.es/pho</a>'
            || v_sep
            || v_Sep
            || 'Atentamente'
            || v_Sep
            || v_sep
            || 'Universitat Jaume I';

         gri_www.euji_envios_html.mail (p_from          => 'no_reply@uji.es',
                                        p_to            => v_para,
                                        p_subject       => v_Asunto,
                                        p_texto         => v_Texto,
                                        p_permite_dup   => 'S',
                                        p_formato       => 'HTM');
      END LOOP;
   END;

   procedure generar_matricula(faseId in number) is
   begin
     refrescar_flags(faseId);
   end;

   function get_curso_aca return number is
   begin
     return curso_academico;
   end;

END;
/

create or replace force view UJI_PRACTICASHOSPITALARIAS.PHO_VW_ESTADO_MATRICULA
(
   PERSONA_ID,
   FASE_ID,
   SELECCIONADOS,
   TOTAL
)
   bequeath definer as
   select t.persona_id,
          t.fase_id,
          (select count(*)
             from pho_oferta o
            where o.fase_id = t.fase_id and o.persona_id = t.persona_id)
             seleccionados,
          (select sum(decode(t.fase_id, 2, numero_optativas, numero_obligatorias))
             from pho_ext_asignaturas_cursadas ac
                  join pho_asignaturas a on ac.asignatura_id = a.codigo_asignatura
                  join pho_bloques b on b.asignatura_id = a.id
                  join pho_fases_asignaturas fa on fa.ASIGNATURA_ID = a.id
            where PERSONA_ID = t.persona_id and fa.fase_id = t.fase_id and b.id in (select bloque_id
                                                                                      from pho_oferta
                                                                                     where fase_id = fa.fase_id))
             total
     from pho_turnos t;