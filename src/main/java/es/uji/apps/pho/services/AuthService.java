package es.uji.apps.pho.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.pho.exceptions.NoAutorizadoException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.dao.ApaDAO;
import org.springframework.stereotype.Service;

@Service
public class AuthService extends CoreBaseService
{
    @InjectParam
    public ApaDAO apaDAO;

    public boolean isAdmin(Long personaId)
    {
        return apaDAO.hasPerfil("PHO", "ADMIN", personaId);
    }

    public void checkIsAdmin(Long personaId) throws NoAutorizadoException
    {
        if (!isAdmin(personaId))
        {
            throw new NoAutorizadoException(
                    "Només els administradors poden accedir a aquest recurs");
        }
    }
}