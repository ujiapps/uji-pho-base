package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.BloqueDAO;
import es.uji.apps.pho.models.Bloque;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BloqueService extends BaseService<Bloque>
{
    @Autowired
    public BloqueService(BloqueDAO dao)
    {
        super(dao, Bloque.class);
    }
}