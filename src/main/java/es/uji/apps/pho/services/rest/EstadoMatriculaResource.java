package es.uji.apps.pho.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.pho.exceptions.NoAutorizadoException;
import es.uji.apps.pho.models.Turno;
import es.uji.apps.pho.services.AuthService;
import es.uji.apps.pho.services.EstadoMatriculaService;
import es.uji.apps.pho.services.TurnoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("estadomatricula")
public class EstadoMatriculaResource extends CoreBaseService {

    @InjectParam
    private EstadoMatriculaService estadoMatriculaService;

    @InjectParam
    private TurnoService turnoService;

    @InjectParam
    private AuthService authService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getEstadoMatricula(@QueryParam("personaId") Long personaId, @QueryParam("faseId") Long faseId) throws NoAutorizadoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        if (authService.isAdmin(connectedUserId)) {
            return UIEntity.toUI(estadoMatriculaService.getEstadoMatricula(personaId, faseId));
        }

        Turno turno = turnoService.getTurnoActivo(connectedUserId);
        if (turno == null) {
            throw new NoAutorizadoException("El torn no està actiu");
        }
        return UIEntity.toUI(estadoMatriculaService.getEstadoMatricula(connectedUserId, turno
                .getFase().getId()));
    }
}