package es.uji.apps.pho.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.pho.dao.OfertaPlazasDAO;
import es.uji.apps.pho.models.OfertaPlazas;

@Service
public class OfertaPlazasService
{
    @Autowired
    private OfertaPlazasDAO ofertaPlazasDAO;

    public List<OfertaPlazas> getOfertaPlazasByFaseId(Long faseId, Long connectedUserId)
    {
        return ofertaPlazasDAO.getOfertaPlazasByFaseId(faseId);
    }
}
