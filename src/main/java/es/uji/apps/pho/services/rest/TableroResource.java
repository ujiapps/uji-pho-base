package es.uji.apps.pho.services.rest;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.pho.exceptions.AlumnoNoEncontradoException;
import es.uji.apps.pho.exceptions.NoAutorizadoException;
import es.uji.apps.pho.models.FilaTablero;
import es.uji.apps.pho.services.AuthService;
import es.uji.apps.pho.services.FaseService;
import es.uji.apps.pho.services.GrupoService;
import es.uji.apps.pho.services.TableroService;
import es.uji.apps.pho.services.TurnoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.util.List;
import java.util.stream.Collectors;

@Path("tablero")
public class TableroResource extends CoreBaseService
{
    @InjectParam
    private TableroService tableroService;

    @InjectParam
    private AuthService authService;

    @InjectParam
    private TurnoService turnoService;

    @InjectParam
    private GrupoService grupoService;

    @InjectParam
    private FaseService faseService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTablero(@QueryParam("personaId") Long personaId, @QueryParam("faseId") Long faseId)
            throws NoAutorizadoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Boolean isAdmin = authService.isAdmin(connectedUserId);
        Long alumnoId = isAdmin && (personaId != null) ? personaId : connectedUserId;
        Long faseActivaId = isAdmin && (faseId != null) ? faseId : null;

        if (!authService.isAdmin(connectedUserId))
        {
            turnoService.checkTurnoActivo(connectedUserId);
        }

        List<FilaTablero> informacionFilaTablero = tableroService
                .getInformacionTableroByPersonaId(alumnoId);
        return tableroToUI(informacionFilaTablero, alumnoId, faseActivaId);
    }

    @GET
    @Path("admin")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTableroAdmin(@QueryParam("personaId") Long personaId, @QueryParam("faseId") Long faseId)
            throws NoAutorizadoException, AlumnoNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        if (personaId == null)
        {
            throw new AlumnoNoEncontradoException();
        }

        authService.checkIsAdmin(connectedUserId);
        List<FilaTablero> informacionFilaTablero = tableroService
                .getInformacionTableroByPersonaId(personaId);
        return tableroToUI(informacionFilaTablero, personaId, faseId);
    }

    protected List<UIEntity> tableroToUI(List<FilaTablero> infoTablero, Long alumnoId, Long faseId)
    {
        return infoTablero.stream().map(t -> tableroToUI(t, alumnoId, faseId))
                .collect(Collectors.toList());
    }

    protected UIEntity tableroToUI(FilaTablero filaTablero, Long alumnoId, Long faseId)
    {
        UIEntity uiEntity = UIEntity.toUI(filaTablero);
        uiEntity.put("ofertaNombre", filaTablero.getNombre());

        if (filaTablero.getGrupo() == null)
        {
            uiEntity.put("ofertaTooltip", filaTablero.getTooltip());
        }
        else
        {
            uiEntity.put("ofertaTooltip", filaTablero.getTooltipConGrupo());
        }
        uiEntity.put("estado", filaTablero.getEstadoTablero(alumnoId, faseId));
        return uiEntity;
    }

}