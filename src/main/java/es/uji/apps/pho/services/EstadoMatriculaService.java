package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.EstadoMatriculaDAO;
import es.uji.apps.pho.models.EstadoMatricula;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EstadoMatriculaService
{
    @Autowired
    private EstadoMatriculaDAO estadoMatriculaDAO;

    public EstadoMatricula getEstadoMatricula(Long personaId, Long faseId)
    {
        return estadoMatriculaDAO.getEstadoMatricula(personaId, faseId);
    }
}
