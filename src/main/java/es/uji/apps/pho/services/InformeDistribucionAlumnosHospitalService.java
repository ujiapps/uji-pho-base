package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.PeriodoFechaDAO;
import es.uji.apps.pho.models.FilaTablero;
import es.uji.apps.pho.models.PeriodoFecha;
import es.uji.apps.pho.models.Persona;
import es.uji.apps.pho.models.UI.UIFilaInformeDistribucionAlumnnosHospital;
import es.uji.apps.pho.models.UI.UIPaginaInformeDistribucionAlumno;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class InformeDistribucionAlumnosHospitalService
{
    @Autowired
    private PeriodoFechaDAO periodoFechaDAO;

    @Autowired
    private TableroService tableroService;

    @Autowired
    private PersonaService personaService;

    private static List<Persona> personas = new ArrayList<>();

    @Role({ "ADMIN" })
    public List<UIPaginaInformeDistribucionAlumno> getPaginasInforme(Long cursoId, Long faseId,
            Long connectedUserId)
    {
        List<PeriodoFecha> periodoFechas = periodoFechaDAO.getAllByFaseId(faseId);
        List<FilaTablero> filasMatricula = tableroService.getFilasSeleccionadas(faseId);

        Map<String, List<FilaTablero>> filasPorEspecialidad = filasMatricula.stream()
                .collect(Collectors.groupingBy(f -> f.getEspecialidadConHospital()));

        List<UIPaginaInformeDistribucionAlumno> paginasInforme = new ArrayList<>();
        for (Map.Entry<String, List<FilaTablero>> entry : filasPorEspecialidad.entrySet())
        {
            UIPaginaInformeDistribucionAlumno pagina = new UIPaginaInformeDistribucionAlumno();
            List<UIFilaInformeDistribucionAlumnnosHospital> uiFilasInforme = new ArrayList<>();
            for (PeriodoFecha periodoFecha : periodoFechas)
            {
                uiFilasInforme.add(getUIFilaInforme(periodoFecha, entry.getValue()));
            }
            pagina.setEspecialidad(entry.getKey());
            pagina.setFilas(uiFilasInforme);
            paginasInforme.add(pagina);
        }

        return paginasInforme.stream()
                .sorted(Comparator.comparing(UIPaginaInformeDistribucionAlumno::getEspecialidad))
                .collect(Collectors.toList());
    }

    private UIFilaInformeDistribucionAlumnnosHospital getUIFilaInforme(PeriodoFecha periodoFecha,
            List<FilaTablero> filasMatricula)
    {
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Long periodoId = periodoFecha.getPeriodo().getId();

        UIFilaInformeDistribucionAlumnnosHospital fila = new UIFilaInformeDistribucionAlumnnosHospital();
        fila.setNumero(periodoFecha.getPeriodo().getId());
        fila.setFechaInicio(humanDateFormat.format(periodoFecha.getFechaInicio()));
        fila.setFechaFin(humanDateFormat.format(periodoFecha.getFechaFin()));
        List<FilaTablero> filasTablero = getFilaTableroByPeriodo(periodoId, filasMatricula);
        fila.setAlumnos(
                filasTablero.stream().map(f -> getPersonaByIdCached(f.getPersonaId()).getNombre())
                        .collect(Collectors.joining(", ")));
        return fila;
    }

    private List<FilaTablero> getFilaTableroByPeriodo(Long periodoId,
            List<FilaTablero> filasMatricula)
    {
        return filasMatricula.stream()
                .filter(f -> periodoId >= f.getPrimero() && periodoId <= f.getUltimo())
                .collect(Collectors.toList());
    }

    private Persona getPersonaByIdCached(Long personaId)
    {
        if (personas.isEmpty())
        {
            personas = personaService.getPersonas();
        }
        return personas.stream().filter(p -> p.getId().equals(personaId)).findFirst().orElse(null);
    }

}
