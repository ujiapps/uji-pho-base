package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.OfertaDAO;
import es.uji.apps.pho.exceptions.NoAutorizadoException;
import es.uji.apps.pho.exceptions.PlazasNoDisponiblesException;
import es.uji.apps.pho.models.FilaTablero;
import es.uji.apps.pho.models.Oferta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfertaService
{
    @Autowired
    private OfertaDAO ofertaDAO;

    @Autowired
    private FaseService faseService;

    @Autowired
    private TableroService tableroService;

    public void seleccionar(String unicoId, Long faseId, Long personaId)
            throws PlazasNoDisponiblesException
    {
        checkOfertaSeleccionablePorUsuario(unicoId, faseId, personaId);
        List<Oferta> ofertas = ofertaDAO.getOfertasDisponibles(unicoId, faseId);
        for (Oferta oferta : ofertas)
        {
            Long columnasAfectadas = ofertaDAO.asignaOfertaAPersona(oferta, personaId);
            if (columnasAfectadas == 1)
            {
                return;
            }
        }
        throw new PlazasNoDisponiblesException();
    }

    public void deseleccionar(String unicoId, Long faseId, Long personaId)
            throws PlazasNoDisponiblesException, NoAutorizadoException
    {
        checkOfertaSeleccionablePorUsuario(unicoId, faseId, personaId);
        Long columnasAfectadas = ofertaDAO.desasignaOfertaAPersona(unicoId, faseId, personaId);
        if (columnasAfectadas != 1)
        {
            throw new NoAutorizadoException();
        }
    }

    protected void checkOfertaSeleccionablePorUsuario(String unicoId, Long faseId, Long personaId)
            throws PlazasNoDisponiblesException
    {
        List<FilaTablero> filasTablero = tableroService.getFilasTableroByUnicoIdFaseAndPersonaId(unicoId,
                faseId, personaId);
        Long numeroNoSeleccionables = filasTablero.stream()
                .filter(ft -> ft.isSeleccionable() == false).count();
        if (numeroNoSeleccionables > 0)
        {
            throw new PlazasNoDisponiblesException();
        }
    }

    public Long eliminarOferta(List<String> unicosIds)
    {
        return ofertaDAO.eliminarOferta(unicosIds);
    }
}
