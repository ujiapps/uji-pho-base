package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.EspecialidadDAO;
import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.Especialidad;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EspecialidadService extends BaseService<Especialidad>
{
    @Autowired
    public EspecialidadService(EspecialidadDAO dao)
    {
        super(dao, Especialidad.class);
    }

    public void delete(Long id, Long connectedUserId) throws RecursoConHijosException
    {
        try
        {
            dao.delete(modelClass, id);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new RecursoConHijosException("Abans d'esborrar l'especialitat, has d'eliminar els períodes discontinus");
        }
    }
}