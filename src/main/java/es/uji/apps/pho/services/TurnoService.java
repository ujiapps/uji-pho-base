package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.TurnoDAO;
import es.uji.apps.pho.exceptions.NoAutorizadoException;
import es.uji.apps.pho.models.Turno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Service
public class TurnoService extends BaseService<Turno>
{
    @Autowired
    public TurnoService(TurnoDAO dao)
    {
        super(dao, Turno.class);
    }

    public Turno getTurnoActivo(Long connectedUserId)
    {
        Date now = Date.from(Instant.now());
        return ((TurnoDAO) dao).getTurnoActivo(connectedUserId, now);
    }

    public void checkTurnoActivo(Long connectedUserId) throws NoAutorizadoException
    {
        Turno turno = getTurnoActivo(connectedUserId);
        if (turno == null)
        {
            throw new NoAutorizadoException("El teu torn no està actiu");
        }
    }

    public List<Turno> getTurnosPersona(Long connectedUserId)
    {
        return ((TurnoDAO) dao).getTurnosPersona(connectedUserId);
    }

    public List<Turno> getTurnosConNombrePersonas(Long connectedUserId) {
        return ((TurnoDAO) dao).getTurnosConNombrePersonas(connectedUserId);
    }
}