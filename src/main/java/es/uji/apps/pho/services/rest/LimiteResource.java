package es.uji.apps.pho.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.Limite;
import es.uji.apps.pho.services.LimiteService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("limites")
public class LimiteResource extends CoreBaseService
{
    @InjectParam
    private LimiteService limiteService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getLimites()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Limite> listaLimites = limiteService.getAll(connectedUserId);
        return UIEntity.toUI(listaLimites);
    }

    @POST
    public UIEntity insertLimite(UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Limite limite = entity.toModel(Limite.class);
        limiteService.insert(limite, connectedUserId);
        return UIEntity.toUI(limite);
    }

    @PUT
    @Path("{limiteId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateLimite(@PathParam("limiteId") Long limiteId, UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Limite limite = entity.toModel(Limite.class);
        limiteService.update(limite, connectedUserId);
        limite = limiteService.getLimiteById(limiteId, connectedUserId);
        return UIEntity.toUI(limite);
    }

    @DELETE
    @Path("{limiteId}")
    public Response deleteLimite(@PathParam("limiteId") Long limiteId, UIEntity entity)
            throws RecursoConHijosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        limiteService.delete(limiteId, connectedUserId);
        return Response.ok().build();
    }
}
