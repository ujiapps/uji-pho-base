package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.PeriodoDAO;
import es.uji.apps.pho.models.Periodo;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeriodoService extends BaseService<Periodo>
{
    @Autowired
    public PeriodoService(PeriodoDAO dao)
    {
        super(dao, Periodo.class);
    }
}