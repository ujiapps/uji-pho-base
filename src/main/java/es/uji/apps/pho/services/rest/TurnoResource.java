package es.uji.apps.pho.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.Fase;
import es.uji.apps.pho.models.Persona;
import es.uji.apps.pho.models.Turno;
import es.uji.apps.pho.services.TurnoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Path("turnos")
public class TurnoResource extends CoreBaseService
{
    @InjectParam
    private TurnoService turnoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTurnos()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Turno> listaTurnos = turnoService.getTurnosConNombrePersonas(connectedUserId);
        return UIEntity.toUI(listaTurnos);
    }

    @PUT
    @Path("{turnoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTurno(@PathParam("turnoId") Long turnoId, UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Turno turno = entityToTurno(entity);
        turnoService.update(turno, connectedUserId);
        return UIEntity.toUI(turno);
    }

    private Turno entityToTurno(UIEntity entity)
    {
        Turno turno = new Turno();
        turno.setId(Long.parseLong(entity.get("id")));
        Fase fase = new Fase(Long.parseLong(entity.get("faseId")));
        turno.setFase(fase);
        Persona persona = new Persona(Long.parseLong(entity.get("personaId")));
        turno.setPersona(persona);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(entity.get("fecha"), formatter);
        Date fecha = Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
        turno.setFecha(fecha);
        return turno;
    }

    @DELETE
    @Path("{turnoId}")
    public Response deleteTurno(@PathParam("turnoId") Long turnoId, UIEntity entity)
            throws RecursoConHijosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        turnoService.delete(turnoId, connectedUserId);
        return Response.ok().build();
    }

    @POST
    public UIEntity insertTurno(UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Turno turno = entity.toModel(Turno.class);
        turnoService.insert(turno, connectedUserId);
        return UIEntity.toUI(turno);
    }
}