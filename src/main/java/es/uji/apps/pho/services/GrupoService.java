package es.uji.apps.pho.services;

import java.util.Date;
import java.util.List;

import es.uji.apps.pho.dao.GrupoDAO;
import es.uji.apps.pho.models.Grupo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.pho.dao.FaseDAO;
import es.uji.apps.pho.models.Fase;

@Service
public class GrupoService extends BaseService<Grupo>
{
    @Autowired
    public GrupoService(GrupoDAO dao)
    {
        super(dao, Grupo.class);
    }

    public List<Grupo> getAllByFaseId(Long faseId, Long connectedUserId)
    {
        return ((GrupoDAO)dao).getAllByFaseId(faseId);
    }
}