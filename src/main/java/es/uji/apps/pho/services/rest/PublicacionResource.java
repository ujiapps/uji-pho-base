package es.uji.apps.pho.services.rest;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.pho.exceptions.FaseNoEncontradaException;
import es.uji.apps.pho.exceptions.NoFaseActivaException;
import es.uji.apps.pho.models.Curso;
import es.uji.apps.pho.models.Fase;
import es.uji.apps.pho.models.Persona;
import es.uji.apps.pho.models.Turno;
import es.uji.apps.pho.models.UI.UIPaginaInformeMatricula;
import es.uji.apps.pho.models.UI.UITurno;
import es.uji.apps.pho.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.GrupoMenu;
import es.uji.commons.web.template.model.Menu;
import es.uji.commons.web.template.model.Pagina;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Path("publicacion")
public class PublicacionResource extends CoreBaseService
{
    @InjectParam
    private TurnoService turnoService;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private FaseService faseService;

    @InjectParam
    private CursoService cursoService;

    @InjectParam
    private InformeMatriculaService informeMatriculaService;

    @InjectParam
    private InformeDistribucionAlumnosHospitalService informeDistribucionAlumnosHospitalService;

    @InjectParam
    private AuthService authService;

    private Pagina getPaginaPublicacion(String idioma) throws ParseException
    {
        GrupoMenu grupo = new GrupoMenu("Facultat de medicina");
        Pagina pagina = new Pagina("", "", idioma, "PHO");
        pagina.setTitulo("Pràctiques hospitalàries");
        Menu menu = new Menu();
        menu.addGrupo(grupo);
        pagina.setMenu(menu);
        return pagina;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template intro(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        HTMLTemplate template = new HTMLTemplate("pho/intro", new Locale(idioma), "pho");

        Persona persona = personaService.getPersonaById(connectedUserId);
        template.put("persona", persona);
        template.put("curso", cursoService.getCursoActivo().getId());

        Date now = new Date();

        List<UITurno> turnos = turnoService.getTurnosPersona(connectedUserId).stream()
                .filter(t -> t.getFechaFin().after(now)).map(t -> turnoToUI(t))
                .collect(Collectors.toList());

        template.put("turnos", turnos);
        template.put("matriculaFinalizada", turnos.isEmpty());

        Pagina pagina = getPaginaPublicacion(idioma);
        template.put("idioma", idioma);
        template.put("pagina", pagina);
        template.put("server", "http://ujiapps.uji.es");
        template.put("urlBase", "/pho/rest/publicacion");

        return template;
    }

    private UITurno turnoToUI(Turno turno)
    {
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        SimpleDateFormat jsDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

        UITurno uiTurno = new UITurno();
        uiTurno.setFaseNombre(turno.getFase().getNombre());
        uiTurno.setFaseTipo(turno.getFase().getTipo());
        uiTurno.setFecha(humanDateFormat.format(turno.getFecha()));
        uiTurno.setFechaJS(jsDateFormat.format(turno.getFecha()));
        uiTurno.setActivo(turno.isActivo());
        return uiTurno;
    }

    @GET
    @Path("tablero")
    @Produces(MediaType.TEXT_HTML)
    public Template tablero(@QueryParam("personaId") Long personaId, @QueryParam("faseId") Long faseId)
            throws NoFaseActivaException, FaseNoEncontradaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Boolean isAdmin = authService.isAdmin(connectedUserId);
        Long alumnoId = isAdmin && (personaId != null) ? personaId : connectedUserId;
        Fase fase = isAdmin && (faseId != null) ? faseService.getFaseById(faseId) : faseService.getFaseActiva();
        Turno turno = isAdmin ? new Turno() : turnoService.getTurnoActivo(alumnoId);

        Template template = new HTMLTemplate("pho/tablero");
        template.put("persona", personaService.getPersonaById(alumnoId));
        template.put("curso", cursoService.getCursoActivo().getId());
        template.put("fase", fase);
        template.put("turno", turno);
        return template;
    }

    @GET
    @Path("tablero/admin")
    @Produces(MediaType.TEXT_HTML)
    public Template tableroAdmin(@QueryParam("faseId") Long faseId,
                                 @QueryParam("personaId") Long personaId)
            throws NoFaseActivaException, FaseNoEncontradaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Fase fase = (faseId == null) ? faseService.getFaseActiva()
                : faseService.get(faseId, connectedUserId);

        Template template = new HTMLTemplate("pho/tablero-admin");
        template.put("persona", personaService.getPersonaById(personaId));
        template.put("curso", cursoService.getCursoActivo().getId());
        template.put("fase", fase);
        return template;
    }

    @GET
    @Path("informe-matricula/{faseId}")
    @Produces("application/pdf")
    public Template generaInformeMatricula(@PathParam("faseId") Long faseId)
            throws NoFaseActivaException, FaseNoEncontradaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Curso curso = cursoService.getCursoActivo();
        Fase fase = (faseId == null) ? faseService.getFaseActiva()
                : faseService.get(faseId, connectedUserId);

        faseService.checkAlumnoEnFase(connectedUserId, fase.getId());

        Template template = new PDFTemplate("pho/informe-matricula", new Locale("CA"), "pho");
        template.put("curso", curso.getId());
        List<UIPaginaInformeMatricula> paginasInforme = new ArrayList<>();
        paginasInforme.add(informeMatriculaService
                .getInformeMatriculaAlumnoByFaseId(connectedUserId, fase.getId()));
        template.put("paginas", paginasInforme);
        template.put("fechaActual", humanDateFormat.format(new Date()));
        return template;
    }

    @GET
    @Path("informe-matricula")
    @Produces("application/pdf")
    public Template generaInformeMatricula()
            throws NoFaseActivaException, FaseNoEncontradaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Fase fase;

        try
        {
            fase = faseService.getFaseActiva();
        }
        catch (NoFaseActivaException e)
        {
            fase = faseService.getFaseByUserId(connectedUserId);
        }

        return generaInformeMatricula(fase.getId());
    }

    @GET
    @Path("informe-matricula-todos/{faseId}")
    @Produces("application/pdf")
    public Template generaInformeMatriculaTodos(@PathParam("faseId") Long faseId)
            throws NoFaseActivaException, FaseNoEncontradaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Curso curso = cursoService.getCursoActivo();
        Fase fase = (faseId == null) ? faseService.getFaseActiva()
                : faseService.get(faseId, connectedUserId);

        Template template = new PDFTemplate("pho/informe-matricula", new Locale("CA"), "pho");
        template.put("curso", curso.getId());
        template.put("paginas", informeMatriculaService.getInformeMatriculaTodos(connectedUserId,
                curso.getId(), fase.getId()));
        template.put("fechaActual", humanDateFormat.format(new Date()));
        return template;
    }

    @GET
    @Path("informe-distribucion-alumnos/{faseId}")
    @Produces("application/pdf")
    public Template generaInformeDistribucionAlumnos(@PathParam("faseId") Long faseId)
            throws NoFaseActivaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Curso curso = cursoService.getCursoActivo();
        Fase fase = (faseId == null) ? faseService.getFaseActiva()
                : faseService.get(faseId, connectedUserId);

        Template template = new PDFTemplate("pho/informe-distribucion-alumnos", new Locale("CA"),
                "pho");
        template.put("persona", personaService.getPersonaById(connectedUserId));
        template.put("curso", curso.getId());
        template.put("paginas", informeDistribucionAlumnosHospitalService
                .getPaginasInforme(curso.getId(), fase.getId(), connectedUserId));
        template.put("fechaActual", humanDateFormat.format(new Date()));
        return template;
    }
}