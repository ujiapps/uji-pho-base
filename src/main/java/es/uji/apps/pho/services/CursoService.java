package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.FaseDAO;
import es.uji.apps.pho.models.Fase;
import es.uji.commons.db.BaseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.pho.dao.CursoDAO;
import es.uji.apps.pho.models.Curso;

import java.util.List;

@Service
public class CursoService extends BaseService<Curso>
{
    @Autowired
    public CursoService(CursoDAO dao)
    {
        super(dao, Curso.class);
    }

    public List<Curso> getActivos(Long connectedUserId)
    {
        return ((CursoDAO) dao).getActivos();
    }

    public Curso getCursoActivo()
    {
        return ((CursoDAO) dao).getCursoActivo();
    }
}
