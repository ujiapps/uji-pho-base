package es.uji.apps.pho.services.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.Fase;
import es.uji.apps.pho.models.PeriodoFecha;
import es.uji.apps.pho.services.PeriodoFechaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("periodosfechas")
public class PeriodoFechaResource extends CoreBaseService
{
    @InjectParam
    private PeriodoFechaService periodoFechaService;

    @PathParam("faseId")
    Long faseId;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPeriodoFechaFechas()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<PeriodoFecha> listaPeriodoFechas = periodoFechaService.getAllByFaseId(faseId,
                connectedUserId);
        return UIEntity.toUI(listaPeriodoFechas);
    }

    @POST
    public UIEntity insertPeriodoFecha(UIEntity entity) throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        PeriodoFecha periodo = periodoFechaUIToActaRevision(entity);
        periodo.setFase(new Fase(faseId));
        periodoFechaService.insert(periodo, connectedUserId);
        return UIEntity.toUI(periodo);
    }

    @PUT
    @Path("{periodoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updatePeriodoFecha(@PathParam("periodoId") Long periodoId, UIEntity entity)
            throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        PeriodoFecha periodo = periodoFechaUIToActaRevision(entity);
        periodoFechaService.update(periodo, connectedUserId);
        return UIEntity.toUI(periodo);
    }

    @DELETE
    @Path("{periodoId}")
    public Response deletePeriodoFecha(@PathParam("periodoId") Long periodoId, UIEntity entity)
            throws RecursoConHijosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        periodoFechaService.delete(periodoId, connectedUserId);
        return Response.ok().build();
    }

    private PeriodoFecha periodoFechaUIToActaRevision(UIEntity entity) throws ParseException
    {
        PeriodoFecha periodoFecha = entity.toModel(PeriodoFecha.class);

        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        periodoFecha.setFechaInicio(format.parse(entity.get("fechaInicio")));
        periodoFecha.setFechaFin(format.parse(entity.get("fechaFin")));

        return periodoFecha;
    }
}
