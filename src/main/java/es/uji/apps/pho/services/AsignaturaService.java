package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.AsignaturaDAO;
import es.uji.apps.pho.models.Asignatura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AsignaturaService extends BaseService<Asignatura>
{
    @Autowired
    public AsignaturaService(AsignaturaDAO dao)
    {
        super(dao, Asignatura.class);
    }
}