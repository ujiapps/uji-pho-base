package es.uji.apps.pho.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.Hospital;
import es.uji.apps.pho.services.HospitalService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("hospitales")
public class HospitalResource extends CoreBaseService
{
    @InjectParam
    private HospitalService hospitalService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getHospitales()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Hospital> listaHospitales = hospitalService.getAll(connectedUserId);
        return UIEntity.toUI(listaHospitales);
    }

    @PUT
    @Path("{hospitalId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateHospital(@PathParam("hospitalId") Long hospitalId, UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Hospital hospital = entity.toModel(Hospital.class);
        hospitalService.update(hospital, connectedUserId);
        return UIEntity.toUI(hospital);
    }

    @DELETE
    @Path("{hospitalId}")
    public Response deleteHospital(@PathParam("hospitalId") Long hospitalId, UIEntity entity)
            throws RecursoConHijosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        hospitalService.delete(hospitalId, connectedUserId);
        return Response.ok().build();
    }

    @POST
    public UIEntity insertHospital(UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Hospital hospital = entity.toModel(Hospital.class);
        hospitalService.insert(hospital, connectedUserId);
        return UIEntity.toUI(hospital);
    }
}