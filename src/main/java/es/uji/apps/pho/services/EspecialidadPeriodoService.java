package es.uji.apps.pho.services;

import com.mysema.query.Tuple;

import es.uji.apps.pho.dao.EspecialidadPeriodoDAO;
import es.uji.apps.pho.exceptions.FaseNoEncontradaException;
import es.uji.apps.pho.models.EspecialidadPeriodo;
import es.uji.apps.pho.models.Periodo;
import es.uji.commons.rest.UIEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.pho.dao.EspecialidadDAO;
import es.uji.apps.pho.models.Especialidad;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EspecialidadPeriodoService
{
    @Autowired
    private EspecialidadPeriodoDAO especialidadPeriodoDAO;

    @Autowired
    private EspecialidadDAO especialidadDAO;

    public List<EspecialidadPeriodo> getPeriodosByEspecialidadId(Long especialidadId, Long connectedUserId)
    {
        return especialidadPeriodoDAO.getPeriodosByEspecialidadId(especialidadId);
    }

    public List<Tuple> getNumeroPeriodosPorEspecialidad(Long connectedUserId)
    {
        return especialidadPeriodoDAO.getNumeroPeriodosPorEspecialidad();
    }

    public void actualizaEstadoPeriodos(Long especialidadId, List<UIEntity> estadoPeriodos, Long connectedUserId)
            throws FaseNoEncontradaException
    {
        List<Long> periodosActivosIds = estadoPeriodos.stream().filter(ui -> ui.get("seleccionado").equals("true")).map(ui -> Long.parseLong(ui.get("id"))).collect(Collectors.toList());
        List<Long> periodosInactivosIds = estadoPeriodos.stream().filter(ui -> ui.get("seleccionado").equals("false")).map(ui -> Long.parseLong(ui.get("id"))).collect(Collectors.toList());

        especialidadPeriodoDAO.borraPeriodosInactivos(especialidadId, periodosInactivosIds);

        for (Long periodoActivoid : periodosActivosIds)
        {
            addPeriodoActivoSiNoExiste(especialidadId, periodoActivoid);
        }

        Especialidad especialidad = especialidadDAO.getEspecialidadById(especialidadId);
        especialidad.setPeriodoInicial(null);
        especialidad.setPeriodoFinal(null);
        especialidadDAO.update(especialidad);
    }

    private void addPeriodoActivoSiNoExiste(Long especialidadId, Long periodoId)
    {
        EspecialidadPeriodo especialidadPeriodo = especialidadPeriodoDAO.getEspecialidadPeriodoByEspecialidadIdAndPeriodoId(especialidadId, periodoId);

        if (especialidadPeriodo == null)
        {
            EspecialidadPeriodo newEspecialidadPeriodo = new EspecialidadPeriodo();
            Periodo periodo = new Periodo(periodoId);
            Especialidad especialidad = new Especialidad(especialidadId);
            newEspecialidadPeriodo.setPeriodo(periodo);
            newEspecialidadPeriodo.setEspecialidad(especialidad);
            especialidadPeriodoDAO.insert(newEspecialidadPeriodo);
        }
    }

}