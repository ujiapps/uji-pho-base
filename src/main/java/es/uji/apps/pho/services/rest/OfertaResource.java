package es.uji.apps.pho.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.pho.exceptions.NoAutorizadoException;
import es.uji.apps.pho.exceptions.NoFaseActivaException;
import es.uji.apps.pho.exceptions.PlazasNoDisponiblesException;
import es.uji.apps.pho.models.Fase;
import es.uji.apps.pho.models.FilaTablero;
import es.uji.apps.pho.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.hibernate.exception.ConstraintViolationException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;

@Path("oferta")
public class OfertaResource extends CoreBaseService {

    @InjectParam
    private OfertaService ofertaService;

    @InjectParam
    private TableroService tableroService;

    @InjectParam
    private TurnoService turnoService;

    @InjectParam
    private TableroResource tableroResource;

    @InjectParam
    private AuthService authService;

    @InjectParam
    private FaseService faseService;

    @PUT
    @Path("{unicoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> modificaOferta(@PathParam("unicoId") String unicoId, UIEntity entity)
            throws PlazasNoDisponiblesException, NoAutorizadoException, NoFaseActivaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        if (!authService.isAdmin(connectedUserId)) {
            turnoService.checkTurnoActivo(connectedUserId);
        }

        Long alumnoId = authService.isAdmin(connectedUserId) && (entity.getLong("alumnoId") != null) ? entity.getLong("alumnoId") : connectedUserId;
        Long faseId = authService.isAdmin(connectedUserId) && (entity.getLong("faseId") != null) ? entity.getLong("faseId") : faseService.getFaseActiva().getId();

        Boolean seleccionado = entity.getBoolean("seleccionado");
        ParamUtils.checkNotNull(unicoId, seleccionado);

        try {
            if (seleccionado) {
                ofertaService.deseleccionar(unicoId, faseId, alumnoId);
            } else {
                ofertaService.seleccionar(unicoId, faseId, alumnoId);
            }
        } catch (PlazasNoDisponiblesException e) {
            throw new PlazasNoDisponiblesException(e.getMessage());
        } catch (Exception e) {
            throw new NoAutorizadoException();
        }

        List<FilaTablero> informacionFilaTablero = tableroService
                .getInformacionTableroByPersonaId(alumnoId);

        return tableroResource.tableroToUI(informacionFilaTablero, alumnoId, faseId);
    }
}