package es.uji.apps.pho.services.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.Grupo;
import es.uji.apps.pho.models.GrupoDetalle;
import es.uji.apps.pho.services.GrupoDetalleService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("gruposdetalle")
public class GrupoDetalleResource extends CoreBaseService
{
    @InjectParam
    private GrupoDetalleService grupoDetalleService;

    @PathParam("grupoId")
    Long grupoId;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGrupoDetalles()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<GrupoDetalle> listaGrupoDetalles = grupoDetalleService.getAllByGrupoId(grupoId,
                connectedUserId);
        return UIEntity.toUI(listaGrupoDetalles);
    }

    @PUT
    @Path("{grupoDetalleId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateGrupoDetalle(@PathParam("grupoDetalleId") Long grupoDetalleId,
            UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        GrupoDetalle grupoDetalle = entity.toModel(GrupoDetalle.class);
        grupoDetalleService.update(grupoDetalle, connectedUserId);
        grupoDetalle = grupoDetalleService.get(grupoDetalleId, connectedUserId);
        return UIEntity.toUI(grupoDetalle);
    }

    @DELETE
    @Path("{grupoDetalleId}")
    public Response deleteGrupoDetalle(@PathParam("grupoDetalleId") Long grupoDetalleId,
            UIEntity entity) throws RecursoConHijosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        grupoDetalleService.delete(grupoDetalleId, connectedUserId);
        return Response.ok().build();
    }

    @POST
    public UIEntity insertGrupoDetalle(UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        GrupoDetalle grupoDetalle = entity.toModel(GrupoDetalle.class);
        grupoDetalle.setGrupo(new Grupo(grupoId));
        grupoDetalleService.insert(grupoDetalle, connectedUserId);
        return UIEntity.toUI(grupoDetalle);
    }
}