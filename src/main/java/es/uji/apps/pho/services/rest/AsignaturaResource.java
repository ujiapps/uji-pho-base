package es.uji.apps.pho.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.Asignatura;
import es.uji.apps.pho.services.AsignaturaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("asignaturas")
public class AsignaturaResource extends CoreBaseService
{
    @InjectParam
    private AsignaturaService asignaturaService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAsignaturas()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Asignatura> listaAsignaturas = asignaturaService.getAll(connectedUserId);
        return UIEntity.toUI(listaAsignaturas);
    }

    @PUT
    @Path("{asignaturaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateAsignatura(@PathParam("asignaturaId") Long asignaturaId, UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Asignatura asignatura = entity.toModel(Asignatura.class);
        asignaturaService.update(asignatura, connectedUserId);
        return UIEntity.toUI(asignatura);
    }

    @DELETE
    @Path("{asignaturaId}")
    public Response deleteAsignatura(@PathParam("asignaturaId") Long asignaturaId, UIEntity entity)
            throws RecursoConHijosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        asignaturaService.delete(asignaturaId, connectedUserId);
        return Response.ok().build();
    }

    @POST
    public UIEntity insertAsignatura(UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Asignatura asignatura = entity.toModel(Asignatura.class);
        asignaturaService.insert(asignatura, connectedUserId);
        return UIEntity.toUI(asignatura);
    }
}