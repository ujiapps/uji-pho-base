package es.uji.apps.pho.services.rest;

import com.mysema.query.Tuple;
import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.pho.exceptions.FaseNoEncontradaException;
import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.Especialidad;
import es.uji.apps.pho.models.EspecialidadPeriodo;
import es.uji.apps.pho.models.QEspecialidadPeriodo;
import es.uji.apps.pho.services.EspecialidadPeriodoService;
import es.uji.apps.pho.services.EspecialidadService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("especialidades")
public class EspecialidadResource extends CoreBaseService
{
    @InjectParam
    private EspecialidadService especialidadService;

    @InjectParam
    private EspecialidadPeriodoService especialidadPeriodoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEspecialidades()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Especialidad> listaEspecialidades = especialidadService.getAll(connectedUserId);
        List<Tuple> listaNumeroPeriodosPorEspecialidadId = especialidadPeriodoService
                .getNumeroPeriodosPorEspecialidad(connectedUserId);
        return especialidadesConNumeroPeriodosToUI(listaEspecialidades,
                listaNumeroPeriodosPorEspecialidadId);
    }

    private List<UIEntity> especialidadesConNumeroPeriodosToUI(
            List<Especialidad> listaEspecialidades,
            List<Tuple> listaNumeroPeriodosPorEspecialidadId)
    {
        List<UIEntity> especialidadesUI = new ArrayList<>();

        for (Especialidad especialidad : listaEspecialidades)
        {
            UIEntity reunionUI = UIEntity.toUI(especialidad);
            reunionUI.put("numeroPeriodos", getNumeroPeriodosByEspecialidadId(especialidad.getId(),
                    listaNumeroPeriodosPorEspecialidadId));
            especialidadesUI.add(reunionUI);
        }

        return especialidadesUI;

    }

    private Long getNumeroPeriodosByEspecialidadId(Long especialidadId,
            List<Tuple> listaNumeroPeriodosPorEspecialidadId)
    {
        Long num = 0L;

        for (Tuple tupla : listaNumeroPeriodosPorEspecialidadId)
        {
            Long id = tupla.get(QEspecialidadPeriodo.especialidadPeriodo.especialidad.id);

            if (id.equals(especialidadId))
            {
                return tupla.get(QEspecialidadPeriodo.especialidadPeriodo.especialidad.id.count());
            }
        }

        return num;
    }

    @GET
    @Path("{especialidadId}/periodos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEspecialidadPeriodos(@PathParam("especialidadId") Long especialidadId)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<EspecialidadPeriodo> listaPeriodos = especialidadPeriodoService
                .getPeriodosByEspecialidadId(especialidadId, connectedUserId);
        return UIEntity.toUI(listaPeriodos);
    }

    @PUT
    @Path("{especialidadId}/periodos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setPeriodosEspecialidadActivos(@PathParam("especialidadId") Long especialidadId,
            UIEntity entity) throws FaseNoEncontradaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<UIEntity> estadoPeriodos = entity.getRelations().get("periodos");

        especialidadPeriodoService.actualizaEstadoPeriodos(especialidadId, estadoPeriodos,
                connectedUserId);
        return Response.ok().build();
    }

    @POST
    public UIEntity insertEspecialidad(UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Especialidad especialidad = entity.toModel(Especialidad.class);
        especialidadService.insert(especialidad, connectedUserId);
        return UIEntity.toUI(especialidad);
    }

    @PUT
    @Path("{especialidadId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateEspecialidad(@PathParam("especialidadId") Long especialidadId,
            UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Especialidad especialidad = entity.toModel(Especialidad.class);
        especialidadService.update(especialidad, connectedUserId);
        return UIEntity.toUI(especialidad);
    }

    @DELETE
    @Path("{especialidadId}")
    public Response deleteEspecialidad(@PathParam("especialidadId") Long especialidadId,
            UIEntity entity) throws RecursoConHijosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        especialidadService.delete(especialidadId, connectedUserId);
        return Response.ok().build();
    }
}
