package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.PeriodoFechaDAO;
import es.uji.apps.pho.models.PeriodoFecha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeriodoFechaService extends BaseService<PeriodoFecha>
{
    @Autowired
    public PeriodoFechaService(PeriodoFechaDAO dao)
    {
        super(dao, PeriodoFecha.class);
    }

    public List<PeriodoFecha> getAllByFaseId(Long faseId, Long connectedUserId)
    {
        return ((PeriodoFechaDAO) dao).getAllByFaseId(faseId);
    }
}