package es.uji.apps.pho.services.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.pho.commands.RemoteGenerarOfertaCommand;
import es.uji.apps.pho.exceptions.GenerarOfertaException;
import es.uji.apps.pho.exceptions.NoAutorizadoException;
import es.uji.apps.pho.exceptions.NoFaseActivaException;
import es.uji.apps.pho.exceptions.PlazasNoDisponiblesException;
import es.uji.apps.pho.models.FilaTablero;
import es.uji.apps.pho.models.OfertaEliminada;
import es.uji.apps.pho.services.AuthService;
import es.uji.apps.pho.services.OfertaEliminadaService;
import es.uji.apps.pho.services.OfertaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("ofertaeliminada")
public class OfertaEliminadaResource extends CoreBaseService
{
    @InjectParam
    private OfertaEliminadaService ofertaEliminadaService;

    @InjectParam
    private AuthService authService;

    @POST
    @Path("{faseId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addOfertaEliminada(@PathParam("faseId") Long faseId, UIEntity ui)
            throws NoAutorizadoException, IOException, GenerarOfertaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        List<String> unicosIds = ui.getArray("unicosIds");
        Long preservarNumeroPlazas = ui.getLong("preservarNumeroPlazas");

        ofertaEliminadaService.addOfertaEliminada(unicosIds, preservarNumeroPlazas, faseId, connectedUserId);
        return Response.ok().build();
    }

    @DELETE
    @Path("reset/{faseId}")
    public Response borraOfertaEliminada(@PathParam("faseId") Long faseId)
            throws IOException, GenerarOfertaException, NoAutorizadoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        ofertaEliminadaService.borraOfertaEliminada(faseId, connectedUserId);

        RemoteGenerarOfertaCommand remoteGenerarOfertaCommand = new RemoteGenerarOfertaCommand();
        remoteGenerarOfertaCommand.init();
        remoteGenerarOfertaCommand.execute(faseId);
        return Response.ok().build();
    }
}