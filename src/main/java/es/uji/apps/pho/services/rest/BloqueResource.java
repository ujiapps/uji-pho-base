package es.uji.apps.pho.services.rest;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.Asignatura;
import es.uji.apps.pho.models.Bloque;
import es.uji.apps.pho.services.BloqueService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;

@Path("bloques")
public class BloqueResource extends CoreBaseService
{
    @InjectParam
    private BloqueService bloqueService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getBloques()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Bloque> listaBloques = bloqueService.getAll(connectedUserId);
        return UIEntity.toUI(listaBloques);
    }

    @PUT
    @Path("{bloqueId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateBloque(@PathParam("bloqueId") Long bloqueId, UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Bloque bloque = uiToBloque(entity);
        bloqueService.update(bloque, connectedUserId);
        bloque = bloqueService.get(bloqueId, connectedUserId);
        return UIEntity.toUI(bloque);
    }

    @DELETE
    @Path("{bloqueId}")
    public Response deleteBloque(@PathParam("bloqueId") Long bloqueId, UIEntity entity)
            throws RecursoConHijosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        bloqueService.delete(bloqueId, connectedUserId);
        return Response.ok().build();
    }

    @POST
    public UIEntity insertBloque(UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Bloque bloque = uiToBloque(entity);
        bloqueService.insert(bloque, connectedUserId);
        return UIEntity.toUI(bloque);
    }

    private Bloque uiToBloque(UIEntity entity)
    {
        Bloque bloque = entity.toModel(Bloque.class);
        bloque.setAsignatura(new Asignatura(entity.get("asignaturaId")));
        return bloque;
    }
}