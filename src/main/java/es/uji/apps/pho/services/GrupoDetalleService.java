package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.GrupoDetalleDAO;
import es.uji.apps.pho.models.GrupoDetalle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GrupoDetalleService extends BaseService<GrupoDetalle>
{
    @Autowired
    public GrupoDetalleService(GrupoDetalleDAO dao)
    {
        super(dao, GrupoDetalle.class);
    }

    public List<GrupoDetalle> getAllByGrupoId(Long grupoId, Long connectedUserId)
    {
        return ((GrupoDetalleDAO)dao).getAllByGrupoId(grupoId);
    }
}