package es.uji.apps.pho.services.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.pho.models.Curso;
import es.uji.apps.pho.models.Fase;
import es.uji.apps.pho.services.CursoService;
import es.uji.apps.pho.services.FaseAsignaturaService;
import es.uji.apps.pho.services.FaseService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("cursos")
public class CursoResource extends CoreBaseService
{
    @InjectParam
    private CursoService cursoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getFases()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Curso> listaCursos = cursoService.getActivos(connectedUserId);
        return UIEntity.toUI(listaCursos);
    }
}