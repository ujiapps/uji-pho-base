package es.uji.apps.pho.services.rest;

import java.io.IOException;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.pho.commands.RemoteGenerarOfertaCommand;
import es.uji.apps.pho.exceptions.GenerarOfertaException;
import es.uji.apps.pho.exceptions.NoAutorizadoException;
import es.uji.apps.pho.services.AuthService;
import es.uji.apps.pho.services.TableroService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;

@Path("command")
public class CommandResource extends CoreBaseService
{
    @InjectParam
    private TableroService tableroService;

    @InjectParam
    private AuthService authService;

    @PUT
    @Path("generar-oferta/{faseId}")
    public Response generarOferta(@PathParam("faseId") Long faseId)
            throws NoAutorizadoException, IOException, GenerarOfertaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        RemoteGenerarOfertaCommand remoteGenerarOfertaCommand = new RemoteGenerarOfertaCommand();
        remoteGenerarOfertaCommand.init();
        remoteGenerarOfertaCommand.execute(faseId);
        return Response.ok().build();
    }

}