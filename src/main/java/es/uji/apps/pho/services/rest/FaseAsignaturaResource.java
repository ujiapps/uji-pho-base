package es.uji.apps.pho.services.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.FaseAsignatura;
import es.uji.apps.pho.services.FaseAsignaturaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("faseasignaturas")
public class FaseAsignaturaResource extends CoreBaseService
{
    @InjectParam
    private FaseAsignaturaService faseAsignaturaService;

    @PathParam("faseId")
    Long faseId;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getFaseAsignaturas()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<FaseAsignatura> listaFaseAsignaturas = faseAsignaturaService.getAllByFaseId(faseId,
                connectedUserId);
        return UIEntity.toUI(listaFaseAsignaturas);
    }

    @PUT
    @Path("{faseAsignaturaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateFaseAsignatura(@PathParam("faseAsignaturaId") Long faseAsignaturaId,
            UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        FaseAsignatura fase = entity.toModel(FaseAsignatura.class);
        faseAsignaturaService.update(fase, connectedUserId);
        return UIEntity.toUI(fase);
    }

    @DELETE
    @Path("{faseAsignaturaId}")
    public Response deleteFaseAsignatura(@PathParam("faseAsignaturaId") Long faseAsignaturaId,
            UIEntity entity) throws RecursoConHijosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        faseAsignaturaService.delete(faseAsignaturaId, connectedUserId);
        return Response.ok().build();
    }

    @POST
    public UIEntity insertFaseAsignatura(UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        FaseAsignatura fase = entity.toModel(FaseAsignatura.class);
        faseAsignaturaService.insert(fase, connectedUserId);
        return UIEntity.toUI(fase);
    }
}