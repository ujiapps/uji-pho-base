package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.TableroDAO;
import es.uji.apps.pho.models.FilaTablero;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TableroService
{
    @Autowired
    private TableroDAO tableroDAO;

    public List<FilaTablero> getInformacionTableroByPersonaId(Long personaId)
    {
        return tableroDAO.getTableroByPersonaId(personaId);
    }

    public List<FilaTablero> getFilasSeleccionadasByPersonaId(Long personaId)
    {
        return tableroDAO.getFilasSeleccionadasByPersonaId(personaId);
    }

    public List<FilaTablero> getFilasSeleccionadas(Long faseId)
    {
        return tableroDAO.getFilasSeleccionadas(faseId);
    }

    public List<FilaTablero> getFilasTableroByUnicoIdFaseAndPersonaId(String unicoId, Long faseId, Long personaId)
    {
        return tableroDAO.getFilasTableroByOfertaAndPersonaId(unicoId, faseId, personaId);
    }
}
