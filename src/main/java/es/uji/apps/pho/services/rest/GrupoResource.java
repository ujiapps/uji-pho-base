package es.uji.apps.pho.services.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.Fase;
import es.uji.apps.pho.models.Grupo;
import es.uji.apps.pho.services.GrupoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("grupos")
public class GrupoResource extends CoreBaseService
{
    @InjectParam
    private GrupoService grupoService;

    @PathParam("faseId")
    Long faseId;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGruposByFaseId()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Grupo> listaGrupos = grupoService.getAllByFaseId(faseId, connectedUserId);
        return UIEntity.toUI(listaGrupos);
    }

    @PUT
    @Path("{grupoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateGrupo(@PathParam("grupoId") Long grupoId, UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Grupo grupo = entity.toModel(Grupo.class);
        grupoService.update(grupo, connectedUserId);
        return UIEntity.toUI(grupo);
    }

    @DELETE
    @Path("{grupoId}")
    public Response deleteGrupo(@PathParam("grupoId") Long grupoId, UIEntity entity)
            throws RecursoConHijosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        grupoService.delete(grupoId, connectedUserId);
        return Response.ok().build();
    }

    @POST
    public UIEntity insertGrupo(UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Grupo grupo = entity.toModel(Grupo.class);
        grupo.setFase(new Fase(faseId));
        grupoService.insert(grupo, connectedUserId);
        return UIEntity.toUI(grupo);
    }

    @Path("{grupoId}/gruposdetalle")
    public GrupoDetalleResource getGrupoDetalleResourceResource(
            @InjectParam GrupoDetalleResource grupoDetalleResource)
    {
        return grupoDetalleResource;
    }
}