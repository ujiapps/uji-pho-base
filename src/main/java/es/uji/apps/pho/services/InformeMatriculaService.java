package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.PeriodoFechaDAO;
import es.uji.apps.pho.models.FilaTablero;
import es.uji.apps.pho.models.Matricula;
import es.uji.apps.pho.models.PeriodoFecha;
import es.uji.apps.pho.models.UI.UIFilaInformeMatricula;
import es.uji.apps.pho.models.UI.UIPaginaInformeMatricula;
import es.uji.commons.rest.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class InformeMatriculaService
{
    @Autowired
    private PeriodoFechaDAO periodoFechaDAO;

    @Autowired
    private TableroService tableroService;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private MatriculaService matriculaService;

    private HashMap<String, Long> comentarios;

    public UIPaginaInformeMatricula getInformeMatriculaAlumnoByFaseId(Long personaId, Long faseId)
    {
        List<PeriodoFecha> periodoFechas = periodoFechaDAO.getAllByFaseId(faseId);
        List<FilaTablero> filasMatricula = tableroService
                .getFilasSeleccionadasByPersonaId(personaId);
        UIPaginaInformeMatricula uiPaginaInformeMatricula = new UIPaginaInformeMatricula();
        List<UIFilaInformeMatricula> filasInforme = getFilasInforme(filasMatricula, periodoFechas);
        uiPaginaInformeMatricula.setFilas(filasInforme);
        uiPaginaInformeMatricula.setPersona(personaService.getPersonaById(personaId));
        List<String> comentarios = filasInforme.stream().filter(f -> f.getIndiceComentario() != null).map(f -> MessageFormat.format("({0}) {1}", f.getIndiceComentario(), f.getComentario())).distinct().collect(Collectors.toList());
        uiPaginaInformeMatricula.setComentarios(comentarios);
        return uiPaginaInformeMatricula;
    }

    @Role({"ADMIN"})
    public List<UIPaginaInformeMatricula> getInformeMatriculaTodos(Long connectedUserId,
                                                                   Long cursoId, Long faseId)
    {
        List<UIPaginaInformeMatricula> paginasInforme = new ArrayList<>();
        List<PeriodoFecha> periodoFechas = periodoFechaDAO.getAllByFaseId(faseId);
        List<FilaTablero> filasMatriculaTodas = tableroService.getFilasSeleccionadas(faseId);
        for (Matricula matricula : matriculaService.getMatriculasCurso(cursoId))
        {
            List<FilaTablero> filasMatricula = filasMatriculaTodas.stream()
                    .filter(fm -> fm.getPersonaId().equals(matricula.getPersona().getId()))
                    .collect(Collectors.toList());
            UIPaginaInformeMatricula uiPaginaInformeMatricula = new UIPaginaInformeMatricula();

            List<UIFilaInformeMatricula> filasInforme = getFilasInforme(filasMatricula,
                    periodoFechas);
            uiPaginaInformeMatricula.setFilas(filasInforme);
            uiPaginaInformeMatricula.setPersona(matricula.getPersona());

            List<String> comentarios = filasInforme.stream().filter(f -> f.getIndiceComentario() != null).map(f -> MessageFormat.format("({0}) {1}", f.getIndiceComentario(), f.getComentario())).distinct().collect(Collectors.toList());
            uiPaginaInformeMatricula.setComentarios(comentarios);
            if (filasInforme.stream().filter(fi -> fi.getEspecialidad() != null).count() > 0)
            {
                paginasInforme.add(uiPaginaInformeMatricula);
            }
        }

        return paginasInforme.stream()
                .sorted(Comparator.comparing(UIPaginaInformeMatricula::getPersonaNombre))
                .collect(Collectors.toList());
    }

    protected List<UIFilaInformeMatricula> getFilasInforme(List<FilaTablero> filasMatricula,
                                                           List<PeriodoFecha> periodoFechas)
    {
        comentarios = new HashMap<>();
        List<UIFilaInformeMatricula> uiFilasInforme = new ArrayList<>();
        for (PeriodoFecha periodoFecha : periodoFechas)
        {
            UIFilaInformeMatricula fila = getUiFilaInforme(periodoFecha, filasMatricula);
            uiFilasInforme.add(fila);
        }
        return uiFilasInforme;
    }

    private UIFilaInformeMatricula getUiFilaInforme(PeriodoFecha periodoFecha,
                                                    List<FilaTablero> filasMatricula)
    {
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Long periodoId = periodoFecha.getPeriodo().getId();

        UIFilaInformeMatricula fila = new UIFilaInformeMatricula();
        fila.setNumero(periodoFecha.getPeriodo().getId());
        fila.setFechaInicio(humanDateFormat.format(periodoFecha.getFechaInicio()));
        fila.setFechaFin(humanDateFormat.format(periodoFecha.getFechaFin()));
        Optional<FilaTablero> filaTablero = getFilaTableroByPeriodo(periodoId, filasMatricula);
        if (filaTablero.isPresent())
        {
            fila.setAsignatura(filaTablero.get().getCodigoAsignatura());
            fila.setHospital(filaTablero.get().getHospitalNombre());
            fila.setEspecialidad(filaTablero.get().getEspecialidadNombre());
            fila.setFase(filaTablero.get().getFaseTipo().equals("OP") ? "Optativa" : "Obligatòria");
            fila.setComentario(filaTablero.get().getComentariosLimites());
            fila.setIndiceComentario(getIndiceComentario(filaTablero.get().getComentariosLimites()));
        }
        return fila;
    }

    private Long getIndiceComentario(String comentariosLimites)
    {
        if (comentariosLimites == null || comentariosLimites.isEmpty())
        {
            return null;
        }
        if (!comentarios.containsKey(comentariosLimites))
        {
            Long index = comentarios.size() + 1L;
            comentarios.put(comentariosLimites, index);
            return index;
        }
        return comentarios.get(comentariosLimites);
    }

    private Optional<FilaTablero> getFilaTableroByPeriodo(Long periodoId,
                                                          List<FilaTablero> filasMatricula)
    {
        return filasMatricula.stream()
                .filter(f -> periodoId >= f.getPrimero() && periodoId <= f.getUltimo()).findFirst();
    }

}
