package es.uji.apps.pho.services;

import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.commons.db.BaseDAO;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

public class BaseService<T>
{
    protected BaseDAO dao;
    protected Class<T> modelClass;

    public BaseService(BaseDAO dao, Class<T> modelClass)
    {
        this.dao = dao;
        this.modelClass = modelClass;
    }

    public List<T> getAll(Long connectedUserId)
    {
        return dao.get(modelClass);
    }

    public T get(Long id, Long connectedUserId)
    {
        List<T> items = dao.get(modelClass, id);
        return (items.isEmpty()) ? null : items.get(0);
    }

    public T insert(T model, Long connectedUserId)
    {
        return dao.insert(model);
    }

    public T update(T model, Long connectedUserId)
    {
        return dao.update(model);
    }

    public void delete(Long id, Long connectedUserId) throws RecursoConHijosException
    {
        try
        {
            dao.delete(modelClass, id);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new RecursoConHijosException();
        }
    }
}