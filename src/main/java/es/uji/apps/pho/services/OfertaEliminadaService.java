package es.uji.apps.pho.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.pho.commands.RemoteEliminarOfertaNoDisponibleCommand;
import es.uji.apps.pho.dao.OfertaDAO;
import es.uji.apps.pho.dao.OfertaEliminadaDAO;
import es.uji.apps.pho.exceptions.GenerarOfertaException;
import es.uji.apps.pho.models.OfertaEliminada;

@Service
public class OfertaEliminadaService
{
    @Autowired
    private OfertaEliminadaDAO ofertaEliminadaDAO;

    @Autowired
    private OfertaDAO ofertaDAO;

    public void borraOfertaEliminada(Long faseId, Long connectedUserId)
    {
        ofertaEliminadaDAO.borraOfertaEliminadaFase(faseId);
    }

    public void addOfertaEliminada(List<String> unicosIds, Long preservarNumeroPlazas, Long faseId, Long connectedUserId)
            throws IOException, GenerarOfertaException
    {
        for (OfertaEliminada ofertaEliminada : getOfertaEliminada(unicosIds, preservarNumeroPlazas, faseId))
        {

            OfertaEliminada oferta = ofertaEliminadaDAO.getOfertaEliminada(faseId, ofertaEliminada.getUnicoId());
            if (oferta == null)
            {
                ofertaEliminadaDAO.insert(ofertaEliminada);
            }
            else
            {
                oferta.setPreservarNumeroPlazas(ofertaEliminada.getPreservarNumeroPlazas());
                ofertaEliminadaDAO.update(oferta);
            }
        }

        RemoteEliminarOfertaNoDisponibleCommand remoteEliminarOfertaNoDisponibleCommand = new RemoteEliminarOfertaNoDisponibleCommand();
        remoteEliminarOfertaNoDisponibleCommand.init();
        remoteEliminarOfertaNoDisponibleCommand.execute(faseId);
    }

    private List<OfertaEliminada> getOfertaEliminada(List<String> unicosIds, Long preservarNumeroPlazas, Long faseId)
    {
        List<OfertaEliminada> ofertaEliminada = new ArrayList<>();

        for (String unicoId : unicosIds)
        {
            ofertaEliminada.add(new OfertaEliminada(unicoId, preservarNumeroPlazas, faseId));
        }

        return ofertaEliminada;
    }
}
