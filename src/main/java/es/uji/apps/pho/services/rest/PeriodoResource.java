package es.uji.apps.pho.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.Periodo;
import es.uji.apps.pho.services.PeriodoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("periodos")
public class PeriodoResource extends CoreBaseService
{
    @InjectParam
    private PeriodoService periodoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPeriodos()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Periodo> listaPeriodos = periodoService.getAll(connectedUserId);
        return UIEntity.toUI(listaPeriodos);
    }

    @POST
    public UIEntity insertPeriodo(UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Periodo periodo = entity.toModel(Periodo.class);
        periodoService.insert(periodo, connectedUserId);
        return UIEntity.toUI(periodo);
    }

    @PUT
    @Path("{periodoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updatePeriodo(@PathParam("periodoId") Long periodoId, UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Periodo periodo = entity.toModel(Periodo.class);
        periodoService.update(periodo, connectedUserId);
        return UIEntity.toUI(periodo);
    }

    @DELETE
    @Path("{periodoId}")
    public Response deletePeriodo(@PathParam("periodoId") Long periodoId, UIEntity entity)
            throws RecursoConHijosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        periodoService.delete(periodoId, connectedUserId);
        return Response.ok().build();
    }
}
