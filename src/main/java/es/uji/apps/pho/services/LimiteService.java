package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.LimiteDAO;
import es.uji.apps.pho.models.Limite;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LimiteService extends BaseService<Limite>
{
    @Autowired
    public LimiteService(LimiteDAO dao)
    {
        super(dao, Limite.class);
    }


    public Limite getLimiteById(Long limiteId, Long connectedUserId)
    {
        return ((LimiteDAO)dao).getLimiteById(limiteId);
    }


}