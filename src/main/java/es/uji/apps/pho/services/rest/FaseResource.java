package es.uji.apps.pho.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.pho.exceptions.RecursoConHijosException;
import es.uji.apps.pho.models.Fase;
import es.uji.apps.pho.services.FaseAsignaturaService;
import es.uji.apps.pho.services.FaseService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

@Path("fases")
public class FaseResource extends CoreBaseService
{
    @InjectParam
    private FaseService faseService;

    @InjectParam
    private FaseAsignaturaService faseAsignaturaService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getFases()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Fase> listaFases = faseService.getAll(connectedUserId);
        return modelToUI(listaFases);
    }

    @PUT
    @Path("{faseId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateFase(@PathParam("faseId") Long faseId, UIEntity entity,
            @QueryParam("syncAsignaturas") Boolean syncAsignaturas) throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Fase fase = uiToModel(entity);
        faseService.update(fase, connectedUserId);

        if (syncAsignaturas != null && syncAsignaturas)
        {
            String asignaturasIds = entity.get("asignaturasIds");
            faseAsignaturaService.sincronizaAsignaturas(faseId, asignaturasIds.split(", "));
        }
        return UIEntity.toUI(fase);
    }

    private Fase uiToModel(UIEntity entity) throws ParseException
    {
        Fase fase = entity.toModel(Fase.class);
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        if (entity.get("fechaDesde") != null && !entity.get("fechaDesde").isEmpty())
        {
            fase.setFechaDesde(format.parse(entity.get("fechaDesde")));
        }

        if (entity.get("fechaHasta") != null && !entity.get("fechaHasta").isEmpty())
        {
            fase.setFechaHasta(format.parse(entity.get("fechaHasta")));
        }

        return fase;
    }

    @DELETE
    @Path("{faseId}")
    public Response deleteFase(@PathParam("faseId") Long faseId, UIEntity entity)
            throws RecursoConHijosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        faseService.delete(faseId, connectedUserId);
        return Response.ok().build();
    }

    @POST
    public UIEntity insertFase(UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Fase fase = entity.toModel(Fase.class);
        faseService.insert(fase, connectedUserId);
        return UIEntity.toUI(fase);
    }

    @Path("{faseId}/faseasignaturas")
    public FaseAsignaturaResource getFaseAsignaturaResource(
            @InjectParam FaseAsignaturaResource faseAsignaturaResource)
    {
        return faseAsignaturaResource;
    }

    @Path("{faseId}/grupos")
    public GrupoResource getFaseAsignaturaResource(@InjectParam GrupoResource grupoResource)
    {
        return grupoResource;
    }

    @Path("{faseId}/periodosfechas")
    public PeriodoFechaResource getPeriodoFechaResource(
            @InjectParam PeriodoFechaResource periodoFechaResource)
    {
        return periodoFechaResource;
    }

    protected List<UIEntity> modelToUI(List<Fase> fases)
    {
        return fases.stream().map(f -> modelToUI(f)).collect(Collectors.toList());
    }

    protected UIEntity modelToUI(Fase fase)
    {
        UIEntity uiEntity = UIEntity.toUI(fase);
        uiEntity.put("asignaturas",
                fase.getFaseAsignaturas().stream()
                        .map(fa -> fa.getAsignatura().getCodigoAsignatura()).sorted()
                        .collect(Collectors.joining(", ")));
        return uiEntity;
    }
}