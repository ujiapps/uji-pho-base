package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.FaseDAO;
import es.uji.apps.pho.exceptions.FaseNoEncontradaException;
import es.uji.apps.pho.exceptions.NoFaseActivaException;
import es.uji.apps.pho.models.Fase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class FaseService extends BaseService<Fase> {
    @Autowired
    public FaseService(FaseDAO dao) {
        super(dao, Fase.class);
    }

    @Override
    public List<Fase> getAll(Long connectedUserId) {
        return ((FaseDAO) dao).getAll();
    }

    public Fase getFaseActiva() throws NoFaseActivaException {
        Date now = new Date();
        return ((FaseDAO) dao).getFaseActivaByFecha(now);
    }

    public List<Fase> getFaseByCursoIdAndPersonaId(Long cursoId, Long personaId) {
        return ((FaseDAO) dao).getFasesByCursoIdAndPersonaId(cursoId, personaId);
    }

    public Fase getFaseById(Long faseId) throws FaseNoEncontradaException {
        return ((FaseDAO) dao).getFaseById(faseId);
    }

    public Fase getFaseByUserId(Long connectedUserId) throws FaseNoEncontradaException {
        return ((FaseDAO) dao).getFaseByUserId(connectedUserId);
    }

    public void checkAlumnoEnFase(Long connectedUserId, Long faseId) throws FaseNoEncontradaException {
        ((FaseDAO) dao).getFaseByAlumnoAndFaseId(connectedUserId, faseId);
    }
}