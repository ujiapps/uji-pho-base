package es.uji.apps.pho.services.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.pho.models.Curso;
import es.uji.apps.pho.models.Matricula;
import es.uji.apps.pho.services.CursoService;
import es.uji.apps.pho.services.MatriculaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("matriculas")
public class MatriculaResource extends CoreBaseService
{
    @InjectParam
    private MatriculaService matriculaService;

    @InjectParam
    private CursoService cursoService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAlumnos()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Long cursoId = cursoService.getCursoActivo().getId();
        List<Matricula> listaMatriculas = matriculaService.getMatriculasCurso(cursoId);
        return UIEntity.toUI(listaMatriculas);
    }
}