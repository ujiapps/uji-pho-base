package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.MatriculaDAO;
import es.uji.apps.pho.models.Matricula;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatriculaService
{
    @Autowired
    private MatriculaDAO matriculaDAO;

    public List<Matricula> getMatriculasCurso(Long cursoId)
    {
        return matriculaDAO.getMatriculasCurso(cursoId);
    }

}
