package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.HospitalDAO;
import es.uji.apps.pho.models.Hospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HospitalService extends BaseService<Hospital>
{
    @Autowired
    public HospitalService(HospitalDAO dao)
    {
        super(dao, Hospital.class);
    }
}
