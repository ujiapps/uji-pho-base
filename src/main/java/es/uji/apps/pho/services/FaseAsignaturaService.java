package es.uji.apps.pho.services;

import java.util.List;

import es.uji.apps.pho.dao.FaseAsignaturaDAO;
import es.uji.apps.pho.models.Asignatura;
import es.uji.apps.pho.models.Fase;
import es.uji.apps.pho.models.FaseAsignatura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class FaseAsignaturaService extends BaseService<FaseAsignatura>
{
    @Autowired
    public FaseAsignaturaService(FaseAsignaturaDAO dao)
    {
        super(dao, FaseAsignatura.class);
    }

    public List<FaseAsignatura> getAllByFaseId(Long faseId, Long connectedUserId)
    {
        return ((FaseAsignaturaDAO) dao).getAllByFaseId(faseId);
    }

    @Transactional
    public void sincronizaAsignaturas(Long faseId, String[] asignaturasIds)
    {
        Fase fase = new Fase(faseId);
        ((FaseAsignaturaDAO) dao).borraAsignaturasFase(faseId);
        for (String asignaturaId : asignaturasIds)
        {
            if (!asignaturaId.isEmpty()) {
                FaseAsignatura faseAsignatura = new FaseAsignatura();
                faseAsignatura.setFase(fase);
                faseAsignatura.setAsignatura(new Asignatura(asignaturaId));
                dao.insert(faseAsignatura);
            }
        }
    }
}