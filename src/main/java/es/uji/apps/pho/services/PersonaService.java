package es.uji.apps.pho.services;

import es.uji.apps.pho.dao.PersonaDAO;
import es.uji.apps.pho.models.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaService
{
    @Autowired
    private PersonaDAO personaDAO;

    public Persona getPersonaById(Long personaId)
    {
        return personaDAO.getPersonaById(personaId);
    }

    public List<Persona> getPersonas()
    {
        return personaDAO.getPersonas();
    }
}
