package es.uji.apps.pho.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class NoFaseActivaException extends CoreBaseException
{

    public NoFaseActivaException()
    {
        super("No hi ha cap fase activa");
    }

    public NoFaseActivaException(String message)
    {
        super(message);
    }

}
