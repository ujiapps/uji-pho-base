package es.uji.apps.pho.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class RecursoConHijosException extends CoreBaseException
{
    public RecursoConHijosException()
    {
        super("El registre te altres ítems rel·lacionats");
    }

    public RecursoConHijosException(String message)
    {
        super(message);
    }
}
