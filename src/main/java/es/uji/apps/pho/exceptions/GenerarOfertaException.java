package es.uji.apps.pho.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class GenerarOfertaException extends CoreBaseException
{

    public GenerarOfertaException()
    {
        super("No tens permís per accedir");
    }

    public GenerarOfertaException(String message)
    {
        super(message);
    }

}
