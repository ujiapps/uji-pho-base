package es.uji.apps.pho.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class PlazasNoDisponiblesException extends CoreBaseException
{

    public PlazasNoDisponiblesException()
    {
        super("No n'hi han plaçes disponibles");
    }

    public PlazasNoDisponiblesException(String message)
    {
        super(message);
    }

}
