package es.uji.apps.pho.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class AlumnoNoEncontradoException extends CoreBaseException
{
    public AlumnoNoEncontradoException()
    {
        super("Alumne no trobat.");
    }

    public AlumnoNoEncontradoException(String message)
    {
        super(message);
    }
}
