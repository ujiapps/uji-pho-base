package es.uji.apps.pho.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class FaseNoEncontradaException extends CoreBaseException
{

    public FaseNoEncontradaException()
    {
        super("No hi ha cap fase activa");
    }

    public FaseNoEncontradaException(String message)
    {
        super(message);
    }

}
