package es.uji.apps.pho.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class NoAutorizadoException extends CoreBaseException
{
    public NoAutorizadoException()
    {
        super("No autoritzat");
    }

    public NoAutorizadoException(String message)
    {
        super(message);
    }
}
