package es.uji.apps.pho.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_CURSOS")
public class Curso implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Boolean activo;

    @OneToMany(mappedBy = "curso")
    private Set<Fase> fase;

    @OneToMany(mappedBy = "curso")
    private Set<Oferta> ofertas;

    public Curso()
    {
    }

    public Curso(Long cursoId)
    {
        id = cursoId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Boolean isActivo()
    {
        return activo;
    }

    public void setActivo(Boolean activo)
    {
        this.activo = activo;
    }

    public Set<Fase> getFase()
    {
        return fase;
    }

    public void setFase(Set<Fase> fase)
    {
        this.fase = fase;
    }

    public Set<Oferta> getOfertas()
    {
        return ofertas;
    }

    public void setOfertas(Set<Oferta> ofertas)
    {
        this.ofertas = ofertas;
    }
}