package es.uji.apps.pho.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

import es.uji.commons.rest.annotations.DataTag;
import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_GRUPOS")
public class Grupo implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @DataTag
    private String nombre;

    @ManyToOne
    @JoinColumn(name = "FASE_ID")
    private Fase fase;

    @OneToMany(mappedBy = "grupo")
    private Set<GrupoDetalle> grupoDetalles;

    public Grupo()
    {
    }

    public Grupo(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Fase getFase()
    {
        return fase;
    }

    public void setFase(Fase fase)
    {
        this.fase = fase;
    }

    public Set<GrupoDetalle> getGrupoDetalles()
    {
        return grupoDetalles;
    }

    public void setGrupoDetalles(Set<GrupoDetalle> grupoDetalles)
    {
        this.grupoDetalles = grupoDetalles;
    }
}