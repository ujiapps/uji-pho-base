package es.uji.apps.pho.models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_OFERTAS_PERIODOS")
public class OfertaPeriodo implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PERIODO_ID")
    private Periodo periodo;

    @ManyToOne
    @JoinColumn(name = "OFERTA_ID")
    private Oferta oferta;

    public OfertaPeriodo()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Periodo getPeriodo()
    {
        return periodo;
    }

    public void setPeriodo(Periodo periodo)
    {
        this.periodo = periodo;
    }

    public Oferta getOferta()
    {
        return oferta;
    }

    public void setOferta(Oferta oferta)
    {
        this.oferta = oferta;
    }
}