package es.uji.apps.pho.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_VW_OFERTA_PLAZAS")
public class OfertaPlazas implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "UNICO_ID")
    private String id;

    @Column(name = "FASE_ID")
    private Long faseId;

    @Column(name = "FASE")
    private String fase;

    @Column(name = "CURSO")
    private Long curso;

    @Column(name = "HOSPITAL_ID")
    private Long hospitalId;

    @Column(name = "HOSPITAL")
    private String hospital;

    @Column(name = "ESPECIALIDAD_ID")
    private Long especialidadId;

    @Column(name = "ESPECIALIDAD")
    private String especialidad;

    @Column(name = "PERIODO_INICIAL_ID")
    private Long periodoInicial;

    @Column(name = "PERIODO_FINAL_ID")
    private Long periodoFinal;

    @Column(name = "PLAZAS_LIBRES")
    private Long plazasLibres;

    public OfertaPlazas()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getFaseId()
    {
        return faseId;
    }

    public void setFaseId(Long faseId)
    {
        this.faseId = faseId;
    }

    public String getFase()
    {
        return fase;
    }

    public void setFase(String fase)
    {
        this.fase = fase;
    }

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public Long getHospitalId()
    {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId)
    {
        this.hospitalId = hospitalId;
    }

    public String getHospital()
    {
        return hospital;
    }

    public void setHospital(String hospital)
    {
        this.hospital = hospital;
    }

    public Long getEspecialidadId()
    {
        return especialidadId;
    }

    public void setEspecialidadId(Long especialidadId)
    {
        this.especialidadId = especialidadId;
    }

    public String getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(String especialidad)
    {
        this.especialidad = especialidad;
    }

    public Long getPeriodoInicial()
    {
        return periodoInicial;
    }

    public void setPeriodoInicial(Long periodoInicial)
    {
        this.periodoInicial = periodoInicial;
    }

    public Long getPeriodoFinal()
    {
        return periodoFinal;
    }

    public void setPeriodoFinal(Long periodoFinal)
    {
        this.periodoFinal = periodoFinal;
    }

    public Long getPlazasLibres()
    {
        return plazasLibres;
    }

    public void setPlazasLibres(Long plazasLibres)
    {
        this.plazasLibres = plazasLibres;
    }
}