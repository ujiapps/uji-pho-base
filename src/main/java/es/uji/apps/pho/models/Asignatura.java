package es.uji.apps.pho.models;

import es.uji.commons.rest.annotations.DataTag;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_ASIGNATURAS")
public class Asignatura implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    @Column
    @DataTag
    private String nombre;

    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "ESTUDIO_NOMBRE")
    private String estudioNombre;

    @Column
    @DataTag
    private Long curso;

    @Column
    @DataTag
    private String semestre;

    @Column
    private Long orden;

    @Column(name = "CODIGO_ASIGNATURA")
    @DataTag
    private String codigoAsignatura;

    @OneToMany(mappedBy = "asignatura")
    private Set<Oferta> ofertas;

    @OneToMany(mappedBy = "asignatura")
    private Set<Bloque> bloques;

    @OneToMany(mappedBy = "asignatura")
    private Set<FaseAsignatura> faseAsignaturas;

    public Asignatura()
    {
        estudioId = 229L;
        estudioNombre = "Grau en Medicina";
    }

    public Asignatura(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getEstudioNombre()
    {
        return estudioNombre;
    }

    public void setEstudioNombre(String estudioNombre)
    {
        this.estudioNombre = estudioNombre;
    }

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public Set<Oferta> getOfertas()
    {
        return ofertas;
    }

    public void setOfertas(Set<Oferta> ofertas)
    {
        this.ofertas = ofertas;
    }

    public Set<Bloque> getBloques()
    {
        return bloques;
    }

    public void setBloques(Set<Bloque> bloques)
    {
        this.bloques = bloques;
    }

    public String getCodigoAsignatura()
    {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura)
    {
        this.codigoAsignatura = codigoAsignatura;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }
}