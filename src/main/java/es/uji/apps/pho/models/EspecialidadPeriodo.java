package es.uji.apps.pho.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_ESPECIALIDADES_PERIODOS")
public class EspecialidadPeriodo implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ESPECIALIDAD_ID")
    private Especialidad especialidad;

    @ManyToOne
    @JoinColumn(name = "PERIODO_ID")
    private Periodo periodo;

    public EspecialidadPeriodo()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Especialidad getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(Especialidad especialidad)
    {
        this.especialidad = especialidad;
    }

    public Periodo getPeriodo()
    {
        return periodo;
    }

    public void setPeriodo(Periodo periodo)
    {
        this.periodo = periodo;
    }
}