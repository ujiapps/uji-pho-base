package es.uji.apps.pho.models;

import javax.persistence.*;

import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "PHO_VW_OFERTA")
public class FilaTablero implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static String DESHABILITADO = "deshabilitado";
    public static String SELECCIONADO = "seleccionado";
    public static String LIBRE = "libre";
    public static String SELECCIONADO_FASE_ANTERIOR = "seleccionado-fase-anterior";

    @Id
    @Column
    private String id;

    @Column(name = "UNICO_ID")
    private String unicoId;

    @Column(name = "ASIGNATURA_ID")
    private String asignaturaId;

    @Column(name = "ASIGNATURA")
    private String asignaturaNombre;

    @Column(name = "CODIGO_ASIGNATURA")
    private String codigoAsignatura;

    @Column(name = "BLOQUE_ID")
    private Long bloqueId;

    @Column(name = "COMENTARIOS_LIMITES")
    private String comentariosLimites;

    @Column(name = "COMENTARIOS_ESPECIALIDAD")
    private String comentariosEspecialidad;

    @Column(name = "BLOQUE")
    private String bloqueNombre;

    @Column(name = "ESPECIALIDAD_ID")
    private Long especialidadId;

    @Column(name = "ESPECIALIDAD")
    private String especialidadNombre;

    @Column(name = "FASE_ID")
    private Long faseId;

    @Column(name = "FASE")
    private String faseNombre;

    @Column(name = "FASE_TIPO")
    private String faseTipo;

    @ManyToOne
    @JoinColumn(name = "GRUPO_ID")
    private Grupo grupo;

    @Column(name = "TAMAÑO")
    private Long tamanyo;

    @Column(name = "PRIMERO")
    private Long primero;

    @Column(name = "ULTIMO")
    private Long ultimo;

    @Column(name = "P1")
    private Long p1;

    @Column(name = "P2")
    private Long p2;

    @Column(name = "P3")
    private Long p3;

    @Column(name = "P4")
    private Long p4;

    @Column(name = "P5")
    private Long p5;

    @Column(name = "P6")
    private Long p6;

    @Column(name = "CONSECUTIVOS")
    private Boolean consecutivos;

    @Column(name = "PLAZAS")
    private Long plazas;

    @Column(name = "HOSPITAL_ID")
    private Long hospitalId;

    @Column(name = "HOSPITAL")
    private String hospitalNombre;

    @Column(name = "CODIGO")
    private String hospitalCodigo;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "FECHA_INICIO")
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Id
    @Column(name = "PERSONA_ID_MOSTRAR")
    private Long personaIdMostrar;

    @Column(name = "ES_SELECCIONABLE")
    private Boolean seleccionable;

    @Column(name = "FASE_ACTUAL_ID")
    private Long faseActualId;

    @Column(name = "ORDEN_1")
    private Long ordenAsignatura;

    @Column(name = "ORDEN_2")
    private Long ordenEspecialidad;

    @Column(name = "ORDEN_3")
    private Long ordenBloque;

    public FilaTablero()
    {
    }

    public String getUnicoId()
    {
        return unicoId;
    }

    public void setUnicoId(String unicoId)
    {
        this.unicoId = unicoId;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getAsignaturaNombre()
    {
        return asignaturaNombre;
    }

    public void setAsignaturaNombre(String asignaturaNombre)
    {
        this.asignaturaNombre = asignaturaNombre;
    }

    public Long getBloqueId()
    {
        return bloqueId;
    }

    public void setBloqueId(Long bloqueId)
    {
        this.bloqueId = bloqueId;
    }

    public String getBloqueNombre()
    {
        return bloqueNombre;
    }

    public void setBloqueNombre(String bloqueNombre)
    {
        this.bloqueNombre = bloqueNombre;
    }

    public Long getEspecialidadId()
    {
        return especialidadId;
    }

    public void setEspecialidadId(Long especialidadId)
    {
        this.especialidadId = especialidadId;
    }

    public String getEspecialidadNombre()
    {
        return especialidadNombre;
    }

    public void setEspecialidadNombre(String especialidadNombre)
    {
        this.especialidadNombre = especialidadNombre;
    }

    public Grupo getGrupo()
    {
        return grupo;
    }

    public void setGrupo(Grupo grupo)
    {
        this.grupo = grupo;
    }

    public Long getTamanyo()
    {
        return tamanyo;
    }

    public void setTamanyo(Long tamanyo)
    {
        this.tamanyo = tamanyo;
    }

    public Long getPrimero()
    {
        return primero;
    }

    public void setPrimero(Long primero)
    {
        this.primero = primero;
    }

    public Long getUltimo()
    {
        return ultimo;
    }

    public void setUltimo(Long ultimo)
    {
        this.ultimo = ultimo;
    }

    public Long getP1()
    {
        return p1;
    }

    public void setP1(Long p1)
    {
        this.p1 = p1;
    }

    public Long getP2()
    {
        return p2;
    }

    public void setP2(Long p2)
    {
        this.p2 = p2;
    }

    public Long getP3()
    {
        return p3;
    }

    public void setP3(Long p3)
    {
        this.p3 = p3;
    }

    public Long getP4()
    {
        return p4;
    }

    public void setP4(Long p4)
    {
        this.p4 = p4;
    }

    public Long getP5()
    {
        return p5;
    }

    public void setP5(Long p5)
    {
        this.p5 = p5;
    }

    public Long getP6()
    {
        return p6;
    }

    public void setP6(Long p6)
    {
        this.p6 = p6;
    }

    public Long getPlazas()
    {
        return plazas;
    }

    public void setPlazas(Long plazas)
    {
        this.plazas = plazas;
    }

    public Long getHospitalId()
    {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId)
    {
        this.hospitalId = hospitalId;
    }

    public String getHospitalNombre()
    {
        return hospitalNombre;
    }

    public void setHospitalNombre(String hospitalNombre)
    {
        this.hospitalNombre = hospitalNombre;
    }

    public String getHospitalCodigo()
    {
        return hospitalCodigo;
    }

    public void setHospitalCodigo(String hospitalCodigo)
    {
        this.hospitalCodigo = hospitalCodigo;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getCodigoAsignatura()
    {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura)
    {
        this.codigoAsignatura = codigoAsignatura;
    }

    public Long getFaseId()
    {
        return faseId;
    }

    public void setFaseId(Long faseId)
    {
        this.faseId = faseId;
    }

    public String getFaseNombre()
    {
        return faseNombre;
    }

    public void setFaseNombre(String faseNombre)
    {
        this.faseNombre = faseNombre;
    }

    public Boolean isConsecutivos()
    {
        return consecutivos;
    }

    public void setConsecutivos(Boolean consecutivos)
    {
        this.consecutivos = consecutivos;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getFechaInicio()
    {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio)
    {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public Long getPersonaIdMostrar()
    {
        return personaIdMostrar;
    }

    public void setPersonaIdMostrar(Long personaIdMostrar)
    {
        this.personaIdMostrar = personaIdMostrar;
    }

    public Boolean isSeleccionable()
    {
        return seleccionable;
    }

    public void setSeleccionable(Boolean seleccionable)
    {
        this.seleccionable = seleccionable;
    }

    public Long getFaseActualId()
    {
        return faseActualId;
    }

    public void setFaseActualId(Long faseActualId)
    {
        this.faseActualId = faseActualId;
    }

    public Long getOrdenAsignatura()
    {
        return ordenAsignatura;
    }

    public void setOrdenAsignatura(Long ordenAsignatura)
    {
        this.ordenAsignatura = ordenAsignatura;
    }

    public Long getOrdenEspecialidad()
    {
        return ordenEspecialidad;
    }

    public void setOrdenEspecialidad(Long ordenEspecialidad)
    {
        this.ordenEspecialidad = ordenEspecialidad;
    }

    public Long getOrdenBloque()
    {
        return ordenBloque;
    }

    public void setOrdenBloque(Long ordenBloque)
    {
        this.ordenBloque = ordenBloque;
    }

    public String getComentariosEspecialidad()
    {
        return comentariosEspecialidad;
    }

    public void setComentariosEspecialidad(String comentariosEspecialidad)
    {
        this.comentariosEspecialidad = comentariosEspecialidad;
    }

    public String getComentariosLimites()
    {
        return comentariosLimites;
    }

    public void setComentariosLimites(String comentariosLimites)
    {
        this.comentariosLimites = comentariosLimites;
    }

    public String getFaseTipo()
    {
        return faseTipo;
    }

    public void setFaseTipo(String faseTipo)
    {
        this.faseTipo = faseTipo;
    }

    public String getNombre()
    {
        if (primero.equals(ultimo))
        {
            return MessageFormat.format("B{0}", primero);
        }

        return MessageFormat.format("B{0}-{1}", primero, ultimo);
    }

    public String getTooltip()
    {
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String pattern = "{0} del {1} al {2} [{3}]";
        return MessageFormat.format(pattern, getNombre(), humanDateFormat.format(fechaInicio),
                humanDateFormat.format(fechaFin), faseNombre);
    }

    public String getTooltipConGrupo()
    {
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String pattern = "{0} del {1} al {2} [Grup: {3}] [{4}]";
        return MessageFormat.format(pattern, getNombre(), humanDateFormat.format(fechaInicio),
                humanDateFormat.format(fechaFin), this.getGrupo().getNombre(), faseNombre);
    }

    public String getEstadoTablero(Long connectedUserId, Long faseId)
    {
        if (!isSeleccionable())
        {
            return FilaTablero.DESHABILITADO;
        }

        Long faseActual = (faseId != null) ? faseId : getFaseActualId();

        if (connectedUserId.equals(getPersonaId()))
        {
            return getFaseId().equals(faseActual) ? FilaTablero.SELECCIONADO : FilaTablero.SELECCIONADO_FASE_ANTERIOR;
        }

        return FilaTablero.LIBRE;
    }

    public String getEspecialidadConHospital()
    {
        return MessageFormat.format("{0} - {1}", hospitalNombre, especialidadNombre);
    }
}