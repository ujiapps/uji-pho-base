package es.uji.apps.pho.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Set;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_OFERTA")
public class Oferta implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private Curso curso;

    @ManyToOne
    @JoinColumn(name = "ASIGNATURA_ID")
    private Asignatura asignatura;

    @ManyToOne
    @JoinColumn(name = "BLOQUE_ID")
    private Bloque bloque;

    @ManyToOne
    @JoinColumn(name = "ESPECIALIDAD_ID")
    private Especialidad especialidad;

    @ManyToOne
    @JoinColumn(name = "FASE_ID")
    private Fase fase;

    @ManyToOne
    @JoinColumn(name = "HOSPITAL_ID")
    private Hospital hospital;

    @ManyToOne
    @JoinColumn(name = "PERIODO_INICIAL_ID")
    private Periodo periodoInicial;

    @ManyToOne
    @JoinColumn(name = "PERIODO_FINAL_ID")
    private Periodo periodoFinal;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "UNICO_ID")
    private String unicoId;

    @Column(name = "ORDEN_1")
    private Long ordenAsignatura;

    @Column(name = "ORDEN_2")
    private Long ordenEspecialidad;

    @Column(name = "ORDEN_3")
    private Long ordenBloque;

    @Column(name = "BLOQUE_UNIDO")
    private Boolean bloqueUnido;

    @OneToMany(mappedBy = "oferta")
    private Set<OfertaPeriodo> ofertasPeriodo;

    public Oferta()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Curso getCurso()
    {
        return curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public Asignatura getAsignatura()
    {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura)
    {
        this.asignatura = asignatura;
    }

    public Bloque getBloque()
    {
        return bloque;
    }

    public void setBloque(Bloque bloque)
    {
        this.bloque = bloque;
    }

    public Especialidad getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(Especialidad especialidad)
    {
        this.especialidad = especialidad;
    }

    public Fase getFase()
    {
        return fase;
    }

    public void setFase(Fase fase)
    {
        this.fase = fase;
    }

    public Hospital getHospital()
    {
        return hospital;
    }

    public void setHospital(Hospital hospital)
    {
        this.hospital = hospital;
    }

    public Periodo getPeriodoInicial()
    {
        return periodoInicial;
    }

    public void setPeriodoInicial(Periodo periodoInicial)
    {
        this.periodoInicial = periodoInicial;
    }

    public Periodo getPeriodoFinal()
    {
        return periodoFinal;
    }

    public void setPeriodoFinal(Periodo periodoFinal)
    {
        this.periodoFinal = periodoFinal;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getUnicoId()
    {
        return unicoId;
    }

    public void setUnicoId(String unicoId)
    {
        this.unicoId = unicoId;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Long getOrdenAsignatura()
    {
        return ordenAsignatura;
    }

    public void setOrdenAsignatura(Long ordenAsignatura)
    {
        this.ordenAsignatura = ordenAsignatura;
    }

    public Long getOrdenEspecialidad()
    {
        return ordenEspecialidad;
    }

    public void setOrdenEspecialidad(Long ordenEspecialidad)
    {
        this.ordenEspecialidad = ordenEspecialidad;
    }

    public Long getOrdenBloque()
    {
        return ordenBloque;
    }

    public void setOrdenBloque(Long ordenBloque)
    {
        this.ordenBloque = ordenBloque;
    }

    public Boolean getBloqueUnido()
    {
        return bloqueUnido;
    }

    public void setBloqueUnido(Boolean bloqueUnido)
    {
        this.bloqueUnido = bloqueUnido;
    }

    public String agrupacionPeriodo()
    {
        return MessageFormat.format("{0}|{1}", periodoInicial, periodoFinal);
    }

    public Set<OfertaPeriodo> getOfertasPeriodo()
    {
        return ofertasPeriodo;
    }

    public void setOfertasPeriodo(Set<OfertaPeriodo> ofertasPeriodo)
    {
        this.ofertasPeriodo = ofertasPeriodo;
    }
}