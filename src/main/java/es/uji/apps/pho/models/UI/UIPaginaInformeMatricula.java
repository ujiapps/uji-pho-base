package es.uji.apps.pho.models.UI;

import es.uji.apps.pho.models.Persona;

import java.util.List;

public class UIPaginaInformeMatricula
{
    private Persona persona;
    private List<UIFilaInformeMatricula> filas;
    private List<String> comentarios;


    public Persona getPersona()
    {
        return persona;
    }

    public String getPersonaNombre()
    {
        return persona.getNombre();
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public List<UIFilaInformeMatricula> getFilas()
    {
        return filas;
    }

    public void setFilas(List<UIFilaInformeMatricula> filas)
    {
        this.filas = filas;
    }

    public List<String> getComentarios()
    {
        return comentarios;
    }

    public void setComentarios(List<String> comentarios)
    {
        this.comentarios = comentarios;
    }
}
