package es.uji.apps.pho.models;

import es.uji.commons.rest.annotations.DataTag;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Set;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_PERIODOS")
public class Periodo implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @DataTag
    private String nombre;

    @Column(name = "SEMESTRE_1")
    private Long semestre1;

    @Column(name = "SEMESTRE_2")
    private Long semestre2;

    @OneToMany(mappedBy = "periodo")
    private Set<PeriodoFecha> periodoFechas;

    @OneToMany(mappedBy = "periodo")
    private Set<EspecialidadPeriodo> especialidadPeriodos;

    @OneToMany(mappedBy = "periodoInicial")
    private Set<Oferta> ofertasPeriodoInicial;

    @OneToMany(mappedBy = "periodoFinal")
    private Set<Oferta> ofertasPeriodoFinal;

    @OneToMany(mappedBy = "periodo")
    private Set<GrupoDetalle> grupoDetalles;

    @OneToMany(mappedBy = "periodo")
    private Set<OfertaPeriodo> ofertasPeriodo;

    public Periodo()
    {
    }

    public Periodo(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getSemestre1()
    {
        return semestre1;
    }

    public void setSemestre1(Long semestre1)
    {
        this.semestre1 = semestre1;
    }

    public Long getSemestre2()
    {
        return semestre2;
    }

    public void setSemestre2(Long semestre2)
    {
        this.semestre2 = semestre2;
    }

    public Set<PeriodoFecha> getPeriodoFechas()
    {
        return periodoFechas;
    }

    public void setPeriodoFechas(Set<PeriodoFecha> periodoFechas)
    {
        this.periodoFechas = periodoFechas;
    }

    public Set<EspecialidadPeriodo> getEspecialidadPeriodos()
    {
        return especialidadPeriodos;
    }

    public void setEspecialidadPeriodos(Set<EspecialidadPeriodo> especialidadPeriodos)
    {
        this.especialidadPeriodos = especialidadPeriodos;
    }

    public Set<Oferta> getOfertasPeriodoInicial()
    {
        return ofertasPeriodoInicial;
    }

    public void setOfertasPeriodoInicial(Set<Oferta> ofertasPeriodoInicial)
    {
        this.ofertasPeriodoInicial = ofertasPeriodoInicial;
    }

    public Set<Oferta> getOfertasPeriodoFinal()
    {
        return ofertasPeriodoFinal;
    }

    public void setOfertasPeriodoFinal(Set<Oferta> ofertasPeriodoFinal)
    {
        this.ofertasPeriodoFinal = ofertasPeriodoFinal;
    }

    public Set<GrupoDetalle> getGrupoDetalles()
    {
        return grupoDetalles;
    }

    public void setGrupoDetalles(Set<GrupoDetalle> grupoDetalles)
    {
        this.grupoDetalles = grupoDetalles;
    }

    public Set<OfertaPeriodo> getOfertasPeriodo()
    {
        return ofertasPeriodo;
    }

    public void setOfertasPeriodo(Set<OfertaPeriodo> ofertasPeriodo)
    {
        this.ofertasPeriodo = ofertasPeriodo;
    }
}