package es.uji.apps.pho.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;

import java.io.Serializable;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_LIMITES")
public class Limite implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Long plazas;

    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private Curso curso;

    @ManyToOne
    @JoinColumn(name = "HOSPITAL_ID")
    private Hospital hospital;

    @ManyToOne
    @JoinColumn(name = "ESPECIALIDAD_ID")
    private Especialidad especialidad;

    @Column(name = "COMENTARIOS")
    private String comentarios;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Curso getCurso()
    {
        return curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public Hospital getHospital()
    {
        return hospital;
    }

    public void setHospital(Hospital hospital)
    {
        this.hospital = hospital;
    }

    public Especialidad getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(Especialidad especialidad)
    {
        this.especialidad = especialidad;
    }

    public Long getPlazas()
    {
        return plazas;
    }

    public void setPlazas(Long plazas)
    {
        this.plazas = plazas;
    }

    public String getComentarios()
    {
        return comentarios;
    }

    public void setComentarios(String comentarios)
    {
        this.comentarios = comentarios;
    }
}
