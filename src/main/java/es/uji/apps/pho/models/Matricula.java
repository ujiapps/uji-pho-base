package es.uji.apps.pho.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_MATRICULAS")
public class Matricula implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String flags;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private Persona persona;

    @Column(name = "CURSO_ACADEMICO_ID")
    private Long cursoId;

    public Matricula()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getFlags()
    {
        return flags;
    }

    public void setFlags(String flags)
    {
        this.flags = flags;
    }

    public Persona getPersona()
    {
        return persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }
}
