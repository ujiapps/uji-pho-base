package es.uji.apps.pho.models.UI;

import java.text.MessageFormat;

public class UIFilaInformeMatricula
{
    private Long numero;
    private String fechaInicio;
    private String fechaFin;
    private String asignatura;
    private String especialidad;
    private String hospital;
    private String fase;
    private String comentario;
    private Long indiceComentario;

    public Long getNumero()
    {
        return numero;
    }

    public void setNumero(Long numero)
    {
        this.numero = numero;
    }

    public String getFechaInicio()
    {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio)
    {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public String getAsignatura()
    {
        return asignatura;
    }

    public void setAsignatura(String asignatura)
    {
        this.asignatura = asignatura;
    }

    public String getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(String especialidad)
    {
        this.especialidad = especialidad;
    }

    public String getHospital()
    {
        return hospital;
    }

    public void setHospital(String hospital)
    {
        this.hospital = hospital;
    }

    public String getFase()
    {
        return fase;
    }

    public void setFase(String fase)
    {
        this.fase = fase;
    }

    public String getComentario()
    {
        return comentario;
    }

    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }

    public Long getIndiceComentario()
    {
        return indiceComentario;
    }

    public void setIndiceComentario(Long indiceComentario)
    {
        this.indiceComentario = indiceComentario;
    }
}
