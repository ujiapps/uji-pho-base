package es.uji.apps.pho.models;

import es.uji.commons.rest.annotations.DataTag;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_BLOQUES")
public class Bloque implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @DataTag
    private String nombre;

    @Column
    private Long orden;

    @Column(name = "NUMERO_OBLIGATORIAS")
    private Long numeroObligatorias;

    @Column(name = "NUMERO_OPTATIVAS")
    private Long numeroOptativas;

    @DataTag
    @Column(name = "CURSO")
    private Long curso;

    @DataTag
    @Transient
    private String cursoNombre;

    @ManyToOne
    @JoinColumn(name = "ASIGNATURA_ID")
    private Asignatura asignatura;

    @OneToMany(mappedBy = "bloque")
    private Set<Especialidad> especialidades;

    @OneToMany(mappedBy = "bloque")
    private Set<Oferta> ofertas;

    public Bloque()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getNumeroObligatorias()
    {
        return numeroObligatorias;
    }

    public void setNumeroObligatorias(Long numeroObligatorias)
    {
        this.numeroObligatorias = numeroObligatorias;
    }

    public Long getNumeroOptativas()
    {
        return numeroOptativas;
    }

    public void setNumeroOptativas(Long numeroOptativas)
    {
        this.numeroOptativas = numeroOptativas;
    }

    public Asignatura getAsignatura()
    {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura)
    {
        this.asignatura = asignatura;
    }

    public Set<Especialidad> getEspecialidades()
    {
        return especialidades;
    }

    public void setEspecialidades(Set<Especialidad> especialidades)
    {
        this.especialidades = especialidades;
    }

    public Set<Oferta> getOfertas()
    {
        return ofertas;
    }

    public void setOfertas(Set<Oferta> ofertas)
    {
        this.ofertas = ofertas;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public String getCursoNombre()
    {
        return curso.toString() + " - " + nombre;
    }
}