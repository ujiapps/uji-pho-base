package es.uji.apps.pho.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_TURNOS")
public class Turno implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Date fecha;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private Persona persona;

    @ManyToOne
    @JoinColumn(name = "FASE_ID")
    private Fase fase;

    public Turno()
    {
    }

    public Turno(Long personaId, Long faseId)
    {
        this.persona = new Persona(personaId);
        this.fase = new Fase(faseId);
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Fase getFase()
    {
        return fase;
    }

    public void setFase(Fase fase)
    {
        this.fase = fase;
    }

    public Date getFechaFin()
    {
        LocalDate tomorrow = fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                .plusDays(1);
        return Date.from(tomorrow.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public Persona getPersona()
    {
        return persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public Boolean isActivo()
    {
        Date now = new Date();
        LocalDateTime nowTruncated = now.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                .atStartOfDay();
        LocalDateTime fechaTurnoTruncated = fecha.toInstant().atZone(ZoneId.systemDefault())
                .toLocalDate().atStartOfDay();
        return nowTruncated.equals(fechaTurnoTruncated);
    }
}