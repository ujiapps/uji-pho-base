package es.uji.apps.pho.models.UI;

public class UITurno
{
    private String faseNombre;
    private String faseTipo;
    private String fecha;
    private String fechaJS;
    private Boolean activo;

    public String getFaseNombre()
    {
        return faseNombre;
    }

    public void setFaseNombre(String faseNombre)
    {
        this.faseNombre = faseNombre;
    }

    public String getFecha()
    {
        return fecha;
    }

    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }

    public String getFechaJS()
    {
        return fechaJS;
    }

    public void setFechaJS(String fechaJS)
    {
        this.fechaJS = fechaJS;
    }

    public void setFaseTipo(String faseTipo)
    {
        this.faseTipo = faseTipo;
    }

    public String getFaseTipo()
    {
        return faseTipo;
    }

    public Boolean isActivo()
    {
        return activo;
    }

    public void setActivo(Boolean activo)
    {
        this.activo = activo;
    }
}
