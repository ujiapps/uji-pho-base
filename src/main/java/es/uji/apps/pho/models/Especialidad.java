package es.uji.apps.pho.models;

import es.uji.commons.rest.annotations.DataTag;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Set;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_ESPECIALIDADES")
public class Especialidad implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @DataTag
    private String nombre;

    @Column
    private String tipo;

    @Column(name = "PERIODOS_OBLIGATORIOS")
    private String periodosObligatorios;

    @Column(name = "PERIODOS_OPTATIVOS")
    private String periodosOptativos;

    @Column
    private String semestre;

    @ManyToOne
    @JoinColumn(name = "BLOQUE_ID")
    private Bloque bloque;

    @DataTag
    @Column(name = "CURSO")
    private Long curso;

    @Column(name = "COMENTARIOS")
    private String comentarios;

    @DataTag
    @Transient
    private String cursoNombre;

    @ManyToOne
    @JoinColumn(name = "PERIODO_INICIAL_ID")
    private Periodo periodoInicial;

    @ManyToOne
    @JoinColumn(name = "PERIODO_FINAL_ID")
    private Periodo periodoFinal;

    @OneToMany(mappedBy = "especialidad")
    private Set<EspecialidadPeriodo> especialidadPeriodos;

    @OneToMany(mappedBy = "especialidad")
    private Set<Oferta> ofertas;

    @OneToMany(mappedBy = "especialidad")
    private Set<GrupoDetalle> grupoDetalles;

    public Especialidad()
    {
    }

    public Especialidad(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getPeriodosObligatorios()
    {
        return periodosObligatorios;
    }

    public void setPeriodosObligatorios(String periodosObligatorios)
    {
        this.periodosObligatorios = periodosObligatorios;
    }

    public String getPeriodosOptativos()
    {
        return periodosOptativos;
    }

    public void setPeriodosOptativos(String periodosOptativos)
    {
        this.periodosOptativos = periodosOptativos;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public Bloque getBloque()
    {
        return bloque;
    }

    public void setBloque(Bloque bloque)
    {
        this.bloque = bloque;
    }

    public Periodo getPeriodoInicial()
    {
        return periodoInicial;
    }

    public void setPeriodoInicial(Periodo periodoInicial)
    {
        this.periodoInicial = periodoInicial;
    }

    public Periodo getPeriodoFinal()
    {
        return periodoFinal;
    }

    public void setPeriodoFinal(Periodo periodoFinal)
    {
        this.periodoFinal = periodoFinal;
    }

    public Set<EspecialidadPeriodo> getEspecialidadPeriodos()
    {
        return especialidadPeriodos;
    }

    public void setEspecialidadPeriodos(Set<EspecialidadPeriodo> especialidadPeriodos)
    {
        this.especialidadPeriodos = especialidadPeriodos;
    }

    public Set<Oferta> getOfertas()
    {
        return ofertas;
    }

    public void setOfertas(Set<Oferta> ofertas)
    {
        this.ofertas = ofertas;
    }

    public Set<GrupoDetalle> getGrupoDetalles()
    {
        return grupoDetalles;
    }

    public void setGrupoDetalles(Set<GrupoDetalle> grupoDetalles)
    {
        this.grupoDetalles = grupoDetalles;
    }

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public String getCursoNombre()
    {
        return curso.toString() + " - " + nombre;
    }

    public String getComentarios()
    {
        return comentarios;
    }

    public void setComentarios(String comentarios)
    {
        this.comentarios = comentarios;
    }
}