package es.uji.apps.pho.models;

import es.uji.commons.rest.annotations.DataTag;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_EXT_PERSONAS")
public class Persona implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @DataTag
    private String nombre;

    @Column
    private String identificacion;

    @Column
    private String cuenta;

    @Column(name = "TURNO_ID")
    private Long turnoId;

    @Column(name = "TURNO_FECHA")
    private Date turnoFecha;

    @OneToMany(mappedBy = "persona")
    private Set<Matricula> matriculas;

    @OneToMany(mappedBy="persona")
    private Set<Turno> turnos;

    public Persona()
    {
    }

    public Persona(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getIdentificacion()
    {
        return identificacion;
    }

    public void setIdentificacion(String identificacion)
    {
        this.identificacion = identificacion;
    }

    public String getCuenta()
    {
        return cuenta;
    }

    public void setCuenta(String cuenta)
    {
        this.cuenta = cuenta;
    }

    public Long getTurnoId()
    {
        return turnoId;
    }

    public void setTurnoId(Long turnoId)
    {
        this.turnoId = turnoId;
    }

    public Date getTurnoFecha()
    {
        return turnoFecha;
    }

    public void setTurnoFecha(Date turnoFecha)
    {
        this.turnoFecha = turnoFecha;
    }

    public Set<Turno> getTurnos() {
        return turnos;
    }

    public void setTurnos(Set<Turno> turnos) {
        this.turnos = turnos;
    }
}