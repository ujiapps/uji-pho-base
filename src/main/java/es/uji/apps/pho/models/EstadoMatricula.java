package es.uji.apps.pho.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "PHO_VW_ESTADO_MATRICULA")
public class EstadoMatricula implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Id
    @Column(name = "FASE_ID")
    private Long faseId;

    @Column
    private Long seleccionados;

    @Column
    private Long total;

    public EstadoMatricula()
    {
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Long getFaseId()
    {
        return faseId;
    }

    public void setFaseId(Long faseId)
    {
        this.faseId = faseId;
    }

    public Long getSeleccionados()
    {
        return seleccionados;
    }

    public void setSeleccionados(Long seleccionados)
    {
        this.seleccionados = seleccionados;
    }

    public Long getTotal()
    {
        return total;
    }

    public void setTotal(Long total)
    {
        this.total = total;
    }
}