package es.uji.apps.pho.models.UI;

import java.util.List;

public class UIPaginaInformeDistribucionAlumno
{
    private String especialidad;
    private List<UIFilaInformeDistribucionAlumnnosHospital> filas;

    public String getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(String especialidad)
    {
        this.especialidad = especialidad;
    }

    public List<UIFilaInformeDistribucionAlumnnosHospital> getFilas()
    {
        return filas;
    }

    public void setFilas(List<UIFilaInformeDistribucionAlumnnosHospital> filas)
    {
        this.filas = filas;
    }
}
