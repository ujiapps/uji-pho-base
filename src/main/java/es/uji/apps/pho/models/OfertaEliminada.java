package es.uji.apps.pho.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_OFERTA_ELIMINADA")
public class OfertaEliminada implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "UNICO_ID")
    private String unicoId;

    @Id
    @Column(name = "FASE_ID")
    private Long faseId;

    @Column(name = "PRESERVAR_NUMERO_PLAZAS")
    private Long preservarNumeroPlazas;

    public OfertaEliminada()
    {
    }

    public OfertaEliminada(String unicoId, Long preservarNumeroPlazas, Long faseId)
    {
        this.unicoId = unicoId;
        this.faseId = faseId;
        this.preservarNumeroPlazas = preservarNumeroPlazas;
    }

    public String getUnicoId()
    {
        return unicoId;
    }

    public void setUnicoId(String unicoId)
    {
        this.unicoId = unicoId;
    }

    public Long getFaseId()
    {
        return faseId;
    }

    public void setFaseId(Long faseId)
    {
        this.faseId = faseId;
    }

    public Long getPreservarNumeroPlazas()
    {
        return preservarNumeroPlazas;
    }

    public void setPreservarNumeroPlazas(Long preservarNumeroPlazas)
    {
        this.preservarNumeroPlazas = preservarNumeroPlazas;
    }
}