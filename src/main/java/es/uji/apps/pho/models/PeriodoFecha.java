package es.uji.apps.pho.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_PERIODOS_FECHAS")
public class PeriodoFecha implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "FECHA_INICIO")
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @ManyToOne
    @JoinColumn(name = "CURSO_ACA_ID")
    private Curso curso;

    @ManyToOne
    @JoinColumn(name = "PERIODO_ID")
    private Periodo periodo;

    @ManyToOne
    @JoinColumn(name = "FASE_ID")
    private Fase fase;

    public PeriodoFecha()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFechaInicio()
    {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio)
    {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public Curso getCurso()
    {
        return curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public Periodo getPeriodo()
    {
        return periodo;
    }

    public void setPeriodo(Periodo periodo)
    {
        this.periodo = periodo;
    }

    public Fase getFase()
    {
        return fase;
    }

    public void setFase(Fase fase)
    {
        this.fase = fase;
    }
}