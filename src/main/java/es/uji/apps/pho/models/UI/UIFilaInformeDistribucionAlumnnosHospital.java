package es.uji.apps.pho.models.UI;

public class UIFilaInformeDistribucionAlumnnosHospital
{
    private Long numero;
    private String fechaInicio;
    private String fechaFin;
    private String alumnos;

    public Long getNumero()
    {
        return numero;
    }

    public void setNumero(Long numero)
    {
        this.numero = numero;
    }

    public String getFechaInicio()
    {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio)
    {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public String getAlumnos()
    {
        return alumnos;
    }

    public void setAlumnos(String alumnos)
    {
        this.alumnos = alumnos;
    }
}
