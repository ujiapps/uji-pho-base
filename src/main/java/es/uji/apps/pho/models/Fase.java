package es.uji.apps.pho.models;

import es.uji.commons.rest.annotations.DataTag;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@BatchSize(size = 100)
@Table(name = "PHO_FASES")
public class Fase implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static String OBLIGATORIA = "OB";
    public static String OPTATIVA = "OP";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @DataTag
    private String nombre;

    @Column(name = "FECHA_DESDE")
    private Date fechaDesde;

    @Column(name = "FECHA_HASTA")
    private Date fechaHasta;

    @Column
    private String tipo;

    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private Curso curso;

    @OneToMany(mappedBy = "fase")
    private Set<Turno> turnos;

    @OneToMany(mappedBy = "fase")
    private Set<Oferta> ofertas;

    @OneToMany(mappedBy = "fase")
    private Set<Grupo> grupos;

    @OneToMany(mappedBy = "fase")
    private Set<PeriodoFecha> periodoFechas;

    @OneToMany(mappedBy = "fase")
    private Set<FaseAsignatura> faseAsignaturas;

    public Fase()
    {
    }

    public Fase(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Curso getCurso()
    {
        return curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Date getFechaDesde()
    {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde)
    {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta()
    {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta)
    {
        this.fechaHasta = fechaHasta;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Set<Turno> getTurnos()
    {
        return turnos;
    }

    public void setTurnos(Set<Turno> turnos)
    {
        this.turnos = turnos;
    }

    public Set<Oferta> getOfertas()
    {
        return ofertas;
    }

    public void setOfertas(Set<Oferta> ofertas)
    {
        this.ofertas = ofertas;
    }

    public Set<Grupo> getGrupos()
    {
        return grupos;
    }

    public void setGrupos(Set<Grupo> grupos)
    {
        this.grupos = grupos;
    }

    public Set<PeriodoFecha> getPeriodoFechas()
    {
        return periodoFechas;
    }

    public void setPeriodoFechas(Set<PeriodoFecha> periodoFechas)
    {
        this.periodoFechas = periodoFechas;
    }

    public Set<FaseAsignatura> getFaseAsignaturas()
    {
        return faseAsignaturas;
    }

    public void setFaseAsignaturas(Set<FaseAsignatura> faseAsignaturas)
    {
        this.faseAsignaturas = faseAsignaturas;
    }
}