package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.pho.models.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public class TurnoDAO extends BaseDAODatabaseImpl
{
    QTurno qTurno = QTurno.turno;
    QFase qFase = QFase.fase;
    QCurso qCurso = QCurso.curso;

    public Turno getTurnoActivo(Long connectedUserId, Date fecha)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<Turno> turnos = query.from(qTurno).join(qTurno.fase, qFase).join(qFase.curso, qCurso)
                .where(qTurno.persona.id.eq(connectedUserId).and(qCurso.activo.eq(true)))
                .list(qTurno);

        Optional turnoActivoOptional = turnos.stream()
                .filter(t -> t.getFecha().before(fecha) && t.getFechaFin().after(fecha))
                .findFirst();

        if (!turnoActivoOptional.isPresent())
        {
            return null;
        }

        return (Turno) turnoActivoOptional.get();
    }

    public List<Turno> getTurnosPersona(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qTurno).join(qTurno.fase, qFase).fetch().join(qFase.curso, qCurso).fetch()
                .where(qTurno.persona.id.eq(connectedUserId).and(qCurso.activo.eq(true)))
                .orderBy(qTurno.fecha.asc()).list(qTurno);
    }

    public List<Turno> getTurnosConNombrePersonas(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qTurno).orderBy(qTurno.fecha.asc()).list(qTurno);
    }
}
