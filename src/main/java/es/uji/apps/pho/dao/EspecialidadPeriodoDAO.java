package es.uji.apps.pho.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.pho.models.EspecialidadPeriodo;
import es.uji.apps.pho.models.QEspecialidadPeriodo;
import org.springframework.stereotype.Repository;

import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class EspecialidadPeriodoDAO extends BaseDAODatabaseImpl
{
    private QEspecialidadPeriodo qEspecialidadPeriodo = QEspecialidadPeriodo.especialidadPeriodo;

    public List<EspecialidadPeriodo> getPeriodosByEspecialidadId(Long especialidadId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qEspecialidadPeriodo)
                .where(qEspecialidadPeriodo.especialidad.id.eq(especialidadId))
                .list(qEspecialidadPeriodo);

    }

    public List<Tuple> getNumeroPeriodosPorEspecialidad()
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qEspecialidadPeriodo).groupBy(qEspecialidadPeriodo.especialidad.id).list(
                qEspecialidadPeriodo.especialidad.id, qEspecialidadPeriodo.especialidad.id.count());
    }

    @Transactional
    public void borraPeriodosInactivos(Long especialidadId, List<Long> periodosInactivosIds)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qEspecialidadPeriodo);

        deleteClause.where(qEspecialidadPeriodo.periodo.id.in(periodosInactivosIds)
                .and(qEspecialidadPeriodo.especialidad.id.eq(especialidadId))).execute();

    }

    public EspecialidadPeriodo getEspecialidadPeriodoByEspecialidadIdAndPeriodoId(
            Long especialidadId, Long periodoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<EspecialidadPeriodo> resultado = query.from(qEspecialidadPeriodo)
                .where(qEspecialidadPeriodo.periodo.id.eq(periodoId)
                        .and(qEspecialidadPeriodo.especialidad.id.eq(especialidadId)))
                .list(qEspecialidadPeriodo);

        if (resultado.size() == 0)
        {
            return null;
        }

        return resultado.get(0);
    }
}
