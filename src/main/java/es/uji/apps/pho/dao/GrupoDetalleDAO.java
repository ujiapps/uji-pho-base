package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.pho.models.*;
import org.springframework.stereotype.Repository;

import es.uji.commons.db.BaseDAODatabaseImpl;

import java.util.List;

@Repository
public class GrupoDetalleDAO extends BaseDAODatabaseImpl
{

    public List<GrupoDetalle> getAllByGrupoId(Long grupoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QGrupoDetalle qGrupoDetalle = QGrupoDetalle.grupoDetalle;
        QGrupo qGrupo = QGrupo.grupo;
        QPeriodo qPeriodo = QPeriodo.periodo;
        QEspecialidad qEspecialidad = QEspecialidad.especialidad;

        return query.from(qGrupoDetalle).join(qGrupoDetalle.grupo, qGrupo).fetch()
                .join(qGrupoDetalle.periodo, qPeriodo).fetch()
                .join(qGrupoDetalle.especialidad, qEspecialidad).fetch()
                .where(qGrupoDetalle.grupo.id.eq(grupoId)).list(qGrupoDetalle);
    }
}
