package es.uji.apps.pho.dao;

import java.util.List;

import com.mysema.query.jpa.impl.JPADeleteClause;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.pho.models.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class FaseAsignaturaDAO extends BaseDAODatabaseImpl
{
    public List<FaseAsignatura> getAllByFaseId(Long faseId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QFaseAsignatura qFaseAsignatura = QFaseAsignatura.faseAsignatura;
        QAsignatura qAsignatura = QAsignatura.asignatura;

        return query.from(qFaseAsignatura).join(qFaseAsignatura.asignatura, qAsignatura).fetch()
                .where(qFaseAsignatura.fase.id.eq(faseId)).list(qFaseAsignatura);
    }

    public void borraAsignaturasFase(Long faseId)
    {
        QFaseAsignatura qFaseAsignatura = QFaseAsignatura.faseAsignatura;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qFaseAsignatura);

        deleteClause.where(qFaseAsignatura.fase.id.eq(faseId)).execute();
    }
}
