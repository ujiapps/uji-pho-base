package es.uji.apps.pho.dao;

import java.util.List;


import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.pho.models.OfertaPlazas;
import es.uji.apps.pho.models.QOfertaPlazas;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class OfertaPlazasDAO extends BaseDAODatabaseImpl
{
    public List<OfertaPlazas> getOfertaPlazasByFaseId(Long faseId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QOfertaPlazas qOfertaPlazas = QOfertaPlazas.ofertaPlazas;

        return query.from(qOfertaPlazas)
                .where(qOfertaPlazas.faseId.eq(faseId)).list(qOfertaPlazas);
    }
}
