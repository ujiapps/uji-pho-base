package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.pho.models.FilaTablero;
import es.uji.apps.pho.models.QAsignatura;
import es.uji.apps.pho.models.QFase;
import es.uji.apps.pho.models.QFilaTablero;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TableroDAO extends BaseDAODatabaseImpl {
    public List<FilaTablero> getTableroByPersonaId(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QFilaTablero qFilaTablero = QFilaTablero.filaTablero;

        return query
                .from(qFilaTablero)
                .where(qFilaTablero.personaIdMostrar.eq(personaId))
                .orderBy(qFilaTablero.ordenAsignatura.asc(), qFilaTablero.ordenEspecialidad.asc(),
                        qFilaTablero.ordenBloque.asc()).list(qFilaTablero);
    }

    public List<FilaTablero> getFilasSeleccionadasByPersonaId(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QFilaTablero qFilaTablero = QFilaTablero.filaTablero;

        return query.from(qFilaTablero).where(qFilaTablero.personaId.eq(personaId))
                .orderBy(qFilaTablero.bloqueId.asc()).list(qFilaTablero);
    }

    public List<FilaTablero> getFilasSeleccionadas(Long faseId) {
        JPAQuery query = new JPAQuery(entityManager);
        QFilaTablero qFilaTablero = QFilaTablero.filaTablero;
        //QAsignatura qAsignatura = QAsignatura.asignatura;

        return query
                .from(qFilaTablero)
                .where(qFilaTablero.personaId.isNotNull().and(qFilaTablero.faseId.eq(faseId)))
                //.and(qFilaTablero.asignaturaId.eq(qAsignatura.id)).and(qAsignatura.curso.eq(4L)))
                .orderBy(qFilaTablero.bloqueId.asc()).list(qFilaTablero);
    }

    public List<FilaTablero> getFilasTableroByOfertaAndPersonaId(String unicoId, Long faseId, Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QFilaTablero qFilaTablero = QFilaTablero.filaTablero;

        return query
                .from(qFilaTablero)
                .where(qFilaTablero.personaIdMostrar.eq(personaId).and(
                        qFilaTablero.unicoId.eq(unicoId)).and(qFilaTablero.faseId.eq(faseId)))
                .orderBy(qFilaTablero.ordenAsignatura.asc(), qFilaTablero.ordenEspecialidad.asc(),
                        qFilaTablero.ordenBloque.asc()).list(qFilaTablero);
    }
}
