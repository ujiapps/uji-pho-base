package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.pho.exceptions.FaseNoEncontradaException;
import es.uji.apps.pho.models.Especialidad;
import es.uji.apps.pho.models.QEspecialidad;
import es.uji.commons.db.BaseDAODatabaseImpl;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EspecialidadDAO extends BaseDAODatabaseImpl
{
    public Especialidad getEspecialidadById(Long especialidadId) throws FaseNoEncontradaException
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEspecialidad qEspecialidad = QEspecialidad.especialidad;

        List<Especialidad> especialidades = query.from(qEspecialidad).where(qEspecialidad.id.eq(especialidadId))
                .list(qEspecialidad);

        if (especialidades.isEmpty())
        {
            throw new FaseNoEncontradaException();
        }

        return especialidades.get(0);
    }
}
