package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.pho.models.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CursoDAO extends BaseDAODatabaseImpl
{
    public Curso getCursoActivo()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;

        List<Curso> cursos = query.from(qCurso).where(qCurso.activo.isTrue())
                .orderBy(qCurso.id.desc()).list(qCurso);

        if (cursos.isEmpty())
        {
            return null;
        }

        return cursos.get(0);
    }

    public List<Curso> getActivos()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;
        return query.from(qCurso).where(qCurso.activo.eq(true)).list(qCurso);
    }
}
