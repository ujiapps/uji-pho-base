package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.pho.models.Oferta;
import es.uji.apps.pho.models.QOferta;
import es.uji.apps.pho.models.QOfertaPeriodo;
import es.uji.commons.db.BaseDAODatabaseImpl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class OfertaDAO extends BaseDAODatabaseImpl
{
    public List<Oferta> getOfertasDisponibles(String unicoId, Long faseId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QOferta qOferta = QOferta.oferta;

        return query.from(qOferta)
                .where(qOferta.unicoId.eq(unicoId).and(qOferta.personaId.isNull()).and(qOferta.fase.id.eq(faseId))).list(qOferta);
    }

    public List<Oferta> getOfertasTodasByUnicoId(String unicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QOferta qOferta = QOferta.oferta;

        return query.from(qOferta)
                .where(qOferta.unicoId.eq(unicoId)).list(qOferta);
    }

    @Transactional
    public Long asignaOfertaAPersona(Oferta oferta, Long connectedUserId)
    {
        QOferta qOferta = QOferta.oferta;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qOferta);

        return updateClause.where(qOferta.id.eq(oferta.getId()).and(qOferta.personaId.isNull()))
                .set(qOferta.personaId, connectedUserId).execute();
    }

    @Transactional
    public Long desasignaOfertaAPersona(String unicoId, Long faseId, Long connectedUserId)
    {
        QOferta qOferta = QOferta.oferta;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qOferta);

        return updateClause
                .where(qOferta.unicoId.eq(unicoId).and(qOferta.fase.id.eq(faseId)).and(qOferta.personaId.eq(connectedUserId)))
                .setNull(qOferta.personaId).execute();
    }

    @Transactional
    public Long eliminarOferta(List<String> unicosIds)
    {
        QOferta qOferta = QOferta.oferta;
        QOfertaPeriodo qOfertaPeriodo = QOfertaPeriodo.ofertaPeriodo;

        JPAQuery query = new JPAQuery(entityManager);
        List<Long> ofertasPeriodosIds = query.from(qOfertaPeriodo).join(qOfertaPeriodo.oferta, qOferta)
                .where(qOferta.unicoId.in(unicosIds)).list(qOfertaPeriodo).stream().map(op -> op.getId()).collect(Collectors.toList());

        JPADeleteClause deleteClauseOfertaPeriodo = new JPADeleteClause(entityManager, qOfertaPeriodo);
        deleteClauseOfertaPeriodo
                .where(qOfertaPeriodo.id.in(ofertasPeriodosIds)).execute();


        JPADeleteClause deleteClauseOferta = new JPADeleteClause(entityManager, qOferta);
        return deleteClauseOferta
                .where(qOferta.unicoId.in(unicosIds)).execute();

    }
}
