package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.pho.models.Matricula;
import es.uji.apps.pho.models.QMatricula;
import es.uji.apps.pho.models.QPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MatriculaDAO extends BaseDAODatabaseImpl
{

    public List<Matricula> getMatriculasCurso(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMatricula qMatricula = QMatricula.matricula;
        QPersona qPersona = QPersona.persona;

        return query.from(qMatricula).join(qMatricula.persona, qPersona).fetch()
                .list(qMatricula);
    }
}
