package es.uji.apps.pho.dao;


import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.pho.models.OfertaEliminada;
import es.uji.apps.pho.models.QOfertaEliminada;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class OfertaEliminadaDAO extends BaseDAODatabaseImpl
{
    @Transactional
    public Long borraOfertaEliminadaFase(Long faseId)
    {
        QOfertaEliminada qOfertaEliminada = QOfertaEliminada.ofertaEliminada;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qOfertaEliminada);
        return deleteClause.where(qOfertaEliminada.faseId.eq(faseId)).execute();
    }


    public OfertaEliminada getOfertaEliminada(Long faseId, String unicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QOfertaEliminada qOfertaEliminada = QOfertaEliminada.ofertaEliminada;

        List<OfertaEliminada> result = query.from(qOfertaEliminada)
                .where(qOfertaEliminada.faseId.eq(faseId).and(qOfertaEliminada.unicoId.eq(unicoId))).list(qOfertaEliminada);

        if (result.isEmpty())
        {
            return null;
        }

        return result.get(0);
    }
}
