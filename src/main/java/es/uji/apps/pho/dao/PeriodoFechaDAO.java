package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.pho.models.PeriodoFecha;
import es.uji.apps.pho.models.QPeriodoFecha;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PeriodoFechaDAO extends BaseDAODatabaseImpl
{
    public List<PeriodoFecha> getPeriodosByCursoAndFase(Long curso)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QPeriodoFecha qPeriodoFecha = QPeriodoFecha.periodoFecha;

        return query.from(qPeriodoFecha)
                .where(qPeriodoFecha.curso.id.eq(curso))
                .orderBy(qPeriodoFecha.periodo.id.asc()).list(qPeriodoFecha);
    }

    public List<PeriodoFecha> getAllByFaseId(Long faseId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QPeriodoFecha qPeriodoFecha = QPeriodoFecha.periodoFecha;

        return query.from(qPeriodoFecha).where(qPeriodoFecha.fase.id.eq(faseId))
                .orderBy(qPeriodoFecha.periodo.id.asc()).list(qPeriodoFecha);
    }
}
