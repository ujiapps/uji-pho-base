package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.pho.models.EstadoMatricula;
import es.uji.apps.pho.models.QEstadoMatricula;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EstadoMatriculaDAO extends BaseDAODatabaseImpl
{
    public EstadoMatricula getEstadoMatricula(Long personaId, Long faseId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEstadoMatricula qEstadoMatricula = QEstadoMatricula.estadoMatricula;

        List<EstadoMatricula> estadosMatricula = query
                .from(qEstadoMatricula)
                .where(qEstadoMatricula.personaId.eq(personaId).and(
                        qEstadoMatricula.faseId.eq(faseId))).list(qEstadoMatricula);

        if (estadosMatricula.isEmpty())
        {
            return null;
        }

        return estadosMatricula.get(0);
    }
}
