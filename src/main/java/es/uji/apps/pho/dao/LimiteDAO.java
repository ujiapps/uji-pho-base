package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.pho.models.Limite;
import es.uji.apps.pho.models.QEspecialidad;
import es.uji.apps.pho.models.QHospital;
import es.uji.apps.pho.models.QLimite;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LimiteDAO extends BaseDAODatabaseImpl
{
    public Limite getLimiteById(Long limiteId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QLimite qLimite = QLimite.limite;
        QEspecialidad qEspecialidad = QEspecialidad.especialidad;
        QHospital qHospital = QHospital.hospital;

        List<Limite> limites = query.from(qLimite).join(qLimite.especialidad, qEspecialidad).fetch()
                .join(qLimite.hospital, qHospital).fetch().where(qLimite.id.eq(limiteId))
                .list(qLimite);

        if (limites.isEmpty())
        {
            return null;
        }

        return limites.get(0);
    }
}
