package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.pho.exceptions.FaseNoEncontradaException;
import es.uji.apps.pho.exceptions.NoFaseActivaException;
import es.uji.apps.pho.models.*;
import es.uji.commons.db.BaseDAODatabaseImpl;

import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class FaseDAO extends BaseDAODatabaseImpl
{
    public Fase getFaseActivaByFecha(Date fecha) throws NoFaseActivaException
    {
        JPAQuery query = new JPAQuery(entityManager);
        QFase qFase = QFase.fase;
        List<Fase> fases = query.from(qFase)
                .where(qFase.fechaDesde.before(fecha).and(qFase.fechaHasta.after(fecha)))
                .orderBy(qFase.id.desc()).list(qFase);

        if (fases.isEmpty())
        {
            throw new NoFaseActivaException("No hi ha cap fase activa");
        }

        return fases.get(0);
    }

    public List<Fase> getAll()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QFase qFase = QFase.fase;
        QFaseAsignatura qFaseAsignatura = QFaseAsignatura.faseAsignatura;
        QAsignatura qAsignatura = QAsignatura.asignatura;
        return query.from(qFase).leftJoin(qFase.faseAsignaturas, qFaseAsignatura).fetch()
                .leftJoin(qFaseAsignatura.asignatura, qAsignatura).fetch().list(qFase);
    }

    public List<Fase> getFasesByCursoIdAndPersonaId(Long cursoId, Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QFase qFase = QFase.fase;
        QCurso qCurso = QCurso.curso;
        QMatricula qMatricula = QMatricula.matricula;

        return query.from(qFase).join(qFase.curso, qCurso).fetch().where(qCurso.id.eq(cursoId))
                .list(qFase);

    }

    public Fase getFaseById(Long faseId) throws FaseNoEncontradaException
    {
        JPAQuery query = new JPAQuery(entityManager);
        QFase qFase = QFase.fase;
        QCurso qCurso = QCurso.curso;
        QMatricula qMatricula = QMatricula.matricula;

        List<Fase> fases = query.from(qFase).where(qFase.id.eq(faseId))
                .list(qFase);

        if (fases.isEmpty())
        {
            throw new FaseNoEncontradaException();
        }

        return fases.get(0);
    }

    public Fase getFaseByUserId(Long connectedUserId) throws FaseNoEncontradaException
    {
        JPAQuery query = new JPAQuery(entityManager);
        QFase qFase = QFase.fase;
        QOferta qOferta = QOferta.oferta;

        List<Fase> fases = query.from(qFase).join(qFase.ofertas, qOferta).where(qOferta.personaId.eq(connectedUserId)).distinct().list(qFase);

        if (fases.isEmpty())
        {
            throw new FaseNoEncontradaException();
        }

        return fases.get(0);
    }

    public Fase getFaseByAlumnoAndFaseId(Long connectedUserId, Long faseId) throws FaseNoEncontradaException
    {
        JPAQuery query = new JPAQuery(entityManager);
        QFase qFase = QFase.fase;
        QOferta qOferta = QOferta.oferta;

        List<Fase> fases = query.from(qFase).join(qFase.ofertas, qOferta).where(qOferta.personaId.eq(connectedUserId).and(qOferta.fase.id.eq(faseId))).distinct().list(qFase);

        if (fases.isEmpty())
        {
            throw new FaseNoEncontradaException();
        }

        return fases.get(0);
    }
}
