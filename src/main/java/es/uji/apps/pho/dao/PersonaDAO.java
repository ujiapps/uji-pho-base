package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.pho.models.Persona;
import es.uji.apps.pho.models.QPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonaDAO extends BaseDAODatabaseImpl
{
    public Persona getPersonaById(Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QPersona qPersona = QPersona.persona;

        List<Persona> personas = query.from(qPersona).where(qPersona.id.eq(personaId))
                .list(qPersona);

        if (personas.isEmpty())
        {
            return null;
        }

        return personas.get(0);
    }

    public List<Persona> getPersonas()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QPersona qPersona = QPersona.persona;
        return query.from(qPersona).list(qPersona);
    }

}
