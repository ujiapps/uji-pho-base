package es.uji.apps.pho.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.pho.models.Grupo;
import es.uji.apps.pho.models.QGrupo;
import org.springframework.stereotype.Repository;
import es.uji.commons.db.BaseDAODatabaseImpl;

import java.util.List;

@Repository
public class GrupoDAO extends BaseDAODatabaseImpl
{

    public List<Grupo> getAllByFaseId(Long faseId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QGrupo qGrupo = QGrupo.grupo;
        return query.from(qGrupo).where(qGrupo.fase.id.eq(faseId)).list(qGrupo);
    }
}
