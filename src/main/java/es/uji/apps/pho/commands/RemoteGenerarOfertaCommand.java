package es.uji.apps.pho.commands;

import es.uji.apps.pho.exceptions.GenerarOfertaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Component
public class RemoteGenerarOfertaCommand
{
    private GenerarOfertaCommand generarOfertaCommand;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        RemoteGenerarOfertaCommand.dataSource = dataSource;
    }

    public void init()
    {
        this.generarOfertaCommand = new GenerarOfertaCommand(dataSource);
    }

    public Long execute(Long actaId) throws IOException, GenerarOfertaException
    {
        return generarOfertaCommand.execute(actaId);
    }

    private class GenerarOfertaCommand extends StoredProcedure
    {
        private static final String SQL = "pack_generar.generar_oferta_fase";
        private static final String FASE_ID = "faseId";

        public GenerarOfertaCommand(DataSource dataSource)
        {
            super(dataSource, SQL);
            declareParameter(new SqlOutParameter("codigoError", Types.BIGINT));
            declareParameter(new SqlParameter(FASE_ID, Types.BIGINT));
            setFunction(true);
            compile();
        }

        public Long execute(Long faseId) throws IOException, GenerarOfertaException
        {
            Map<String, Object> inParams = new HashMap();
            Map<String, Object> results = new HashMap();
            inParams.put(FASE_ID, faseId);
            try
            {
                results = execute(inParams);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SQLException sqlException = (SQLException) e.getCause();
                System.out.println(sqlException);
                throw new GenerarOfertaException();
            }

            return !results.isEmpty() ? (Long) results.get("codigoError") : null;
        }
    }
}