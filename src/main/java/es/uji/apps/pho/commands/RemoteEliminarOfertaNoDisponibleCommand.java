package es.uji.apps.pho.commands;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import es.uji.apps.pho.exceptions.GenerarOfertaException;

@Component
public class RemoteEliminarOfertaNoDisponibleCommand
{
    private EliminarOfertaNoDisponibleCommand eliminarOfertaNoDisponibleCommand;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        RemoteEliminarOfertaNoDisponibleCommand.dataSource = dataSource;
    }

    public void init()
    {
        this.eliminarOfertaNoDisponibleCommand = new EliminarOfertaNoDisponibleCommand(dataSource);
    }

    public Long execute(Long actaId) throws IOException, GenerarOfertaException
    {
        return eliminarOfertaNoDisponibleCommand.execute(actaId);
    }

    private class EliminarOfertaNoDisponibleCommand extends StoredProcedure
    {
        private static final String SQL = "pack_generar.eliminar_oferta_no_disponible";
        private static final String FASE_ID = "faseId";

        public EliminarOfertaNoDisponibleCommand(DataSource dataSource)
        {
            super(dataSource, SQL);
            declareParameter(new SqlOutParameter("codigoError", Types.BIGINT));
            declareParameter(new SqlParameter(FASE_ID, Types.BIGINT));
            setFunction(true);
            compile();
        }

        public Long execute(Long faseId) throws IOException, GenerarOfertaException
        {
            Map<String, Object> inParams = new HashMap();
            Map<String, Object> results = new HashMap();
            inParams.put(FASE_ID, faseId);
            try
            {
                results = execute(inParams);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SQLException sqlException = (SQLException) e.getCause();
                System.out.println(sqlException);
                throw new GenerarOfertaException();
            }

            return !results.isEmpty() ? (Long) results.get("codigoError") : null;
        }
    }
}